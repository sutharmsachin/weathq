/**
 * Created by cbluser51 on 22/2/18.
 */
'use strict';

var Config = require('../Config');
var async = require('async');
var client = require('twilio')(Config.smsConfig.twilioCredentials.accountSid, Config.smsConfig.twilioCredentials.authToken);
const mobilySms = require('mobily-sms')('wethaq','haq104');
let request = require('request');

function sendSMS(smsOptions, cb) {
    client.messages.create(smsOptions, function (err, message) {
        console.log('SMS RES', err, message);
        if (err) {
            console.log(err)
            cb(err, null);
        }
        else {
            console.log(message.sid);
            cb(null, null);
        }
    });
    // cb(null, null); // Callback is outside as sms sending confirmation can get delayed by a lot of time
}

let sendSMSToUser = function (criteria, externalCB) {
    // console.log('sendSMSToUser')
console.log("qwewerwqerewrweqrqwe",criteria)
    // let smsOptions={
    //     body: 'Hello there!',
    //     from: 'whatsapp:'+Config.smsConfig.twilioCredentials.smsFromNumber,
    //     to: 'whatsapp: +919501374350'
    // };

      client.messages
            .create({
                body: criteria.body,
                from: 'whatsapp:'+Config.smsConfig.twilioCredentials.smsFromNumber,
                to: 'whatsapp:'+criteria.to
                  },
                  function (err, message) {
                    if (err) {
                        console.log(err);
                        externalCB(err)
                    }
                    else {
                        console.log('message',message);
                        externalCB(Config.APP_CONSTANTS.STATUS_MSG.SUCCESS.DEFAULT)
                    }
                })     
};

let sendSmsMobily=function (criteria,callback) {
    mobilySms.sendSms(criteria.body,criteria.to,'Wethaq-app',{},function(error,result){
        console.log("1233",error,result);
        callback(error,result);
        /*
         *ErrorCode:0 => Not connect to server
         *ErrorCode:1 => SMS sent successfully
         *ErrorCode:2 => Your balance is 0
         *ErrorCode:3 => Your balance is not enough.
         *ErrorCode:4 => Invalid mobile number (or invalid username).
         *ErrorCode:5 => Invalid password.
         *ErrorCode:6 => SMS-API not responding, please try again.
         *ErrorCode:10 => SMS counts don’t match mobiles numbers count.
         *ErrorCode:13 => Mobile number is not active as a Sender Name.
         *ErrorCode:14 => Sender name is not active from Mobily.ws and mobile telecommunications companies
         *ErrorCode:15 => Mobile number(s) is not specified or incorrect.
         *ErrorCode:16 => Sender name is not specified.
         *ErrorCode:17 => Message text is not specified or not encoded properly with Mobily.ws Unicode.
         */
    });
}
let sendSmsUnisoft=function (criteria,callback) {
    request({
        method: 'POST',
        url: 'http://api.unifonic.com/rest/Messages/Send',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
        body: "AppSid=9SBP8Ztr8IXMqKM67xgpeldoFZc9&Recipient="+criteria.to+"&Body="+criteria.body
    }, function (error, response, body) {
        console.log("========err",error)
        console.log("========body",body)
        callback(JSON.parse(body))
    });
};

let sendSmsBulkUnisoft=function (criteria,callback) {
        let recipientLength=criteria.to.length;
        let recipients=criteria.to;
        let count=Math.ceil(recipientLength/1000);
        if(recipientLength==1){
            request({
                method: 'POST',
                url: 'http://api.unifonic.com/rest/Messages/Send',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                body: "AppSid=9SBP8Ztr8IXMqKM67xgpeldoFZc9&Recipient="+recipients[0]+"&Body="+criteria.body
            }, function (error, response, body) {
                console.log("========err",error)
                console.log("========body",body)
                callback(JSON.parse(body))
            });
        } else if(count == 1){
            let receivers='';
            for(let j=0;j<criteria.to.length;j++){
                receivers+=criteria.to[j]+','
            }
            request({
                method: 'POST',
                url: 'http://api.unifonic.com/rest/Messages/SendBulk',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                body: "AppSid=9SBP8Ztr8IXMqKM67xgpeldoFZc9&Recipient="+receivers+"&Body="+criteria.body
            }, function (error, response, body) {
                console.log("========err",error)
                console.log("========body",body)
                callback(JSON.parse(body))
            });
        }else{
            for(let i=0; i< count ; i++){
                (function (i) {
                    let recipient=recipients.slice(i*1000,1000* (i+1));
                    let receivers='';
                    for(let j=0;j<recipient.length;j++){
                        receivers+=recipient[j]+','
                    }
                    request({
                        method: 'POST',
                        url: 'http://api.unifonic.com/rest/Messages/SendBulk',
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded'
                        },
                        body: "AppSid=9SBP8Ztr8IXMqKM67xgpeldoFZc9&Recipient="+receivers+"&Body="+criteria.body
                    }, function (error, response, body) {
                        if(i == count-1){
                            callback(JSON.parse(body))
                        }
                    });
                }(i))
            }
        }

};
module.exports = {
    sendSMSToUser: sendSMSToUser,
    sendSmsBulkUnisoft: sendSmsBulkUnisoft,
    sendSmsUnisoft: sendSmsUnisoft,
    sendSmsMobily:sendSmsMobily
};