/**
 * Created by Shumi on 4/7/17.
 */
var Config = require('../Config');
var UniversalFunctions = require('../Utils/UniversalFunction');
var async = require('async');
var Path = require('path');
var knox = require('knox');
var fsExtra = require('fs-extra');

/*
 1) Save Local Files
 2) Create Thumbnails
 3) Upload Files to S3
 4) Delete Local files
 */

//Set Base URL for Images
//var baseFolder = Config.awsS3Config.s3BucketCredentials.folder.profilePicture + '/';
//var baseURL = Config.awsS3Config.s3BucketCredentials.s3URL + '/' + baseFolder;

function uploadFileToS3WithThumbnail(fileData, userId, callbackParent) {
    //Verify File Data
    var profilePicURL = {
        original: null,
        thumbnail: null
    };  
    var originalPath = null;
    var thumbnailPath = null;
    var dataToUpload = [];
    var dimensions;
   // console.log("++++++++++++++++++++id",userId)
    async.series([
         (cb) =>{
            //Validate fileData && userId
            if (!userId || !fileData || !fileData.filename) {
                console.log('in upload file to s3',userId,fileData);
                cb(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR)
            } else {
                // TODO Validate file extensions
                cb();
            }
        },
         (cb) =>{
            var sizeOf = require('image-size');
            sizeOf(fileData.path, function (err, dimen) {
               //  console.log("++++++++++++++++++++dimen",dimen)
                dimensions=dimen;
                cb();
            });
        },
         (cb) =>{
            //Set File Names
            profilePicURL.original = UniversalFunctions.getFileNameWithUserId(false, fileData.filename, userId);
            profilePicURL.thumbnail = UniversalFunctions.getFileNameWithUserId(true, fileData.filename, userId);
            cb();
        },
         (cb) =>{
            //Save File
            var path = Path.resolve(".") + "/uploads/" + profilePicURL.original;
            saveFile(fileData.path, path, function (err, data) {
                if(fileData.bytes>700000) {
                    if (dimensions.type == 'png') {
                        var gm = require('gm').subClass({imageMagick: true});
                        gm(path)
                            //-background white -alpha remove
                            .quality("95")
                            .bitdepth("5")
                            .write(path, function (err, data) {
                                cb(err, data)
                            })

                    } else if (dimensions.type == 'jpg') {
                        var gm = require('gm').subClass({imageMagick: true});
                        gm(path)
                        //define jpeg:extent=300kb
                            .define("jpeg:extent=700kb")
                            .write(path, function (err, data) {
                                cb(err, data)
                            })

                    }
                }else{
                    cb(err, data)
                }
               /* var gm = require('gm').subClass({imageMagick: true});
                gm(path)
                  //  .define("filter:support=2")
                   // .compress("Group4")

                   // .filter("support=2")
                    //.unsharp("0.25x0.08+8.3+0.045")
                  // .dither("None")
                  //  .posterize("136")

               // .resize("1024x768")
               // .resize(dimensions.height+"x"+dimensions.width)
                .quality("95")
                .bitdepth("5")

               // .quality("05")
                //  .define("png:compression-level=0")
                //  .define("png:compression-filter=5")
                //  .define("png:compression-strategy=2")
                    // .define("png:compression-level=9")
                    // .define("png:compression-strategy=1")
                    // .define("png:exclude-chunk=all")
                    // .interlace("none")
                     //.colorspace("sRGB *.png")
                     // .average()
                    .write(path, function (err, data) {
                        cb(err, data)
                    })
               /!*  var gm = require('gm').subClass({imageMagick: true});
                 gm(path)
                 //  .define("filter:support=2")
                 .bitdepth("48")
                 .define("png:compression-filter=2")
                 .define("png:compression-level=9")
                 .define("png:compression-strategy=1")
                 // .filter("support=2")
                 // .unsharp("0.25x0.08+8.3+0.045")
                 // .dither("None")
                 // .posterize("136")
                 // .quality("70")
                 // .define("png:compression-level=9")
                 // .define("png:compression-strategy=1")
                 // .define("png:exclude-chunk=all")
                 // .interlace("none")
                 // .colorspace("sRGB *.png")
                 //  .average()
                 .write(path, function (err, data) {
                 cb(err, data)
                 })*!/*/
            })
        },
         (cb) =>{
            //Create Thumbnail
            let imageRatio;
            let imageSize = fileData.bytes;
            console.log("1111",imageSize)
             if(imageSize>50000){
                 imageRatio= 50000/imageSize;
                 originalPath = Path.resolve(".") + "/uploads/" + profilePicURL.original;
                 thumbnailPath = Path.resolve(".") + "/uploads/" + profilePicURL.thumbnail;
             }
             else{
                 imageRatio=1;
                 originalPath = Path.resolve(".") + "/uploads/" + profilePicURL.original;
                 thumbnailPath = Path.resolve(".") + "/uploads/" + profilePicURL.original;
             }
                // originalPath = Path.resolve(".") + "/uploads/" + profilePicURL.original;
                // thumbnailPath = Path.resolve(".") + "/uploads/" + profilePicURL.thumbnail;

            
         //   console.log("=========fileData==============",imageSize);
            createThumbnailImage(fileData.path, thumbnailPath,dimensions,imageRatio,(err, data) =>{
                dataToUpload.push({
                    originalPath: originalPath,
                    nameToSave: profilePicURL.original
                });
                dataToUpload.push({
                    originalPath: thumbnailPath,
                    nameToSave: profilePicURL.thumbnail
                });
                cb(err, data)
            })
        },
         (cb) =>{
            //Upload both images on S3
           // console.log("=========a===============",dataToUpload);
            parallelUploadTOS3(dataToUpload, cb);
        }
    ],  (err, result)=> {
        callbackParent(err, profilePicURL)
    });
}



function uploadFileToS3WithoutThumbnail(fileData, userId, callbackParent) {
    //Verify File Data
    var profilePicURL = {
        original: null,
        thumbnail: null
    };
    var originalPath = null;
    var thumbnailPath = null;
    var dataToUpload = [];
    var dimensions;

    async.series([
         (cb)=> {
            //Validate fileData && userId
            if (!userId || !fileData || !fileData.filename) {
                console.log('in upload file to s3',userId,fileData);
                cb(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR)
            } else {
                // TODO Validate file extensions
                cb();
            }
        },
         (cb)=> {
            var sizeOf = require('image-size');
            sizeOf(fileData.path, function (err, dimen) {
                // console.log("++++++++++++++++++++dimen",dimen)
                dimensions=dimen;
                cb();
            });
        },
         (cb) =>{
            //Set File Names
            profilePicURL.original = UniversalFunctions.getFileNameWithUserId(false, fileData.filename, userId);
            profilePicURL.thumbnail = UniversalFunctions.getFileNameWithUserId(true, fileData.filename, userId);
            cb();
        },
         (cb) =>{
            //Save File
            var path = Path.resolve(".") + "/uploads/" + profilePicURL.original;
            saveFile(fileData.path, path, function (err, data) {
                cb(err, data)
            })
        },
         (cb) =>{
            //Create Thumbnail
            originalPath = Path.resolve(".") + "/uploads/" + profilePicURL.original;
            thumbnailPath = Path.resolve(".") + "/uploads/" + profilePicURL.thumbnail;

            dataToUpload.push({
                originalPath: originalPath,
                nameToSave: profilePicURL.original
            });

            cb(null,null);
        },
         (cb)=> {
           // console.log("=========a===============",dataToUpload)
            //Upload both images on S3
            parallelUploadTOS3(dataToUpload, cb);
        }
    ],  (err, result) =>{
        callbackParent(err, profilePicURL)
    });
}


function uploadFileToS3(fileData, userId, callbackParent) {
    //Verify File Data
    var profilePicURL = {
        original: null,
        thumbnail: null
    };
    var originalPath = null;
    var thumbnailPath = null;
    var dataToUpload = [];
    var dimensions;

    async.series([
         (cb) =>{
            
            //Validate fileData && userId
            if (!userId ||!fileData) {
                console.log('in upload file to s3',userId,fileData);
                cb(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR)
            } else {
                // TODO Validate file extensions
                cb();
            }
        },
         (cb) =>{
            //Create Thumbnail
            // originalPath = Path.resolve(".") + fileData;
originalPath = fileData
            dataToUpload.push({
                originalPath: originalPath,
                nameToSave: userId
            });

            cb(null,null);
        },
         (cb) =>{
            console.log("=========a===============",dataToUpload)
            //Upload both images on S3
            parallelPdfUploadTOS3(dataToUpload, cb);
        }
    ],  (err, result) =>{
        callbackParent(err, {})
    });
}


function parallelUploadTOS3(filesArray, callback) {
    //Create S3 Client
    var client = knox.createClient({
        key: Config.awsS3Config.s3BucketCredentials.accessKeyId
        , secret: Config.awsS3Config.s3BucketCredentials.secretAccessKey
        , bucket: Config.awsS3Config.s3BucketCredentials.bucket
    });
    var s3ClientOptions = {
        'x-amz-acl': 'public-read',
    // 'Content-Type': 'application/pdf'
    };
    var taskToUploadInParallel = [];
    filesArray.forEach((fileData) =>{
        taskToUploadInParallel.push(( (fileData) =>{
            return  (internalCB)=> {
                if (!fileData.originalPath || !fileData.nameToSave) {
                    internalCB(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR)
                } else {
                    
                    client.putFile(fileData.originalPath, fileData.nameToSave, s3ClientOptions, (err, result) =>{
                        if (err) console.log("========error in put file=========",err)
                        
                        if(fileData.originalPath)
                        deleteFile(fileData.originalPath);
                        internalCB(err, result);
                    })
                }
            }
        })(fileData))
    });

    async.parallel(taskToUploadInParallel, callback)
}


function parallelPdfUploadTOS3(filesArray, callback) {
    //Create S3 Client
    var client = knox.createClient({
        key: Config.awsS3Config.s3BucketCredentials.accessKeyId
        , secret: Config.awsS3Config.s3BucketCredentials.secretAccessKey
        , bucket: Config.awsS3Config.s3BucketCredentials.bucket
    });
    var s3ClientOptions = {
        'x-amz-acl': 'public-read',
    'Content-Type': 'application/pdf'
    };
    var taskToUploadInParallel = [];
    filesArray.forEach((fileData) =>{
        taskToUploadInParallel.push(( (fileData) =>{
            return  (internalCB)=> {
                if (!fileData.originalPath || !fileData.nameToSave) {
                    internalCB(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR)
                } else {
                    
                    client.putFile(fileData.originalPath, fileData.nameToSave, s3ClientOptions, (err, result) =>{
                        if (err) console.log("========error in put file=========",err)
                        
                        if(fileData.originalPath)
                        deleteFile(fileData.originalPath);
                        internalCB(err, result);
                    })
                }
            }
        })(fileData))
    });

    async.parallel(taskToUploadInParallel, callback)
}





/*
 Save File on the disk
 */
function saveFile(fileData, path, callback) {
    fsExtra.copy(fileData, path, callback);
}

function deleteFile(path) {
    fsExtra.remove(path, function (err) {
        console.log('error deleting file>>',err)
    });
}

/*
 Create thumbnail image using graphics magick
 */

function createThumbnailImage(originalPath, thumbnailPath,dimensions,imageRatio, callback) {
    let ratio=1;
    console.log(imageRatio)
        var gm = require('gm').subClass({imageMagick: true});
        gm(originalPath)
            .resize(dimensions.width  * imageRatio, dimensions.height * imageRatio )
            //  .average()
            .write(thumbnailPath, function (err, data) {
                callback(err)
            })
}

/*function createThumb(originalPath, thumbnailPath, callback) {
    var gm = require('gm').subClass({imageMagick: true});

    var readStream = fsExtra.createReadStream(originalPath);
    gm(readStream)
        .size({bufferStream: true}, function (err, size) {
            console.log("sixw***********",size)
            if (size){
                this.thumb(size.width ,size.height,thumbnailPath,3,
                    /!* .autoOrient()
                     .write(thumbnailPath1,*!/ function (err, data) {
                        callback()
                    })
            }
            /!*  if (size.width > 200) {
             this.resize(200, (size.height / size.width) * 200)
             } else {
             this.resize(100, (size.height / size.width) * 100)
             }*!/

        });
}*/



// const uploadFileOnS3BucketAfterRead = async (filePath, fileName,reportData,mimeType, callback ) => {
//         let fileBuffer = await readdirAsync(filePath);
//         console.log("fileBuffer", fileBuffer);
//         let s3bucket = new AWS.S3();
//         let mimeType = "eventDetails/pdf";
//         let uploadPath = "tickets/" + fileName;

//         if(reportData){
//             uploadPath = reportData + "/" + fileName
//         }
//         let params = {
//             Bucket: bucketName,
//             Key: uploadPath,
//             Body: fileBuffer,
//             ACL: 'public-read',
//             ContentType: mimeType
//         };
//          s3bucket.putObject(params,function (resp) {
//             console.log(arguments);
//             console.log('Successfully uploaded package.');
//           });   
            
// }

// function readdirAsync(path, data) {
//         fs.readFile(path, (err, data) => {
//            if(err){

//            cb(err)
//            }
//            else{
//                cb(null,data)
//            }
      
//     });
// }


module.exports = {
    uploadFileToS3WithThumbnail: uploadFileToS3WithThumbnail,
    uploadFileToS3WithoutThumbnail:uploadFileToS3WithoutThumbnail,
    uploadFileToS3: uploadFileToS3,
    // uploadFileOnS3BucketAfterRead:uploadFileOnS3BucketAfterRead
};
