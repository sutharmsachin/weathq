'use strict';

const Config = require('../Config');
const TokenManager = require('./TokenManager');
const Models = require('../Models');
const mongoose = require('mongoose');
const Service = require('../services');
const moment = require('moment');
const async = require('async');
const _ = require('lodash');
const FCM = require('fcm-node');
const serverKey = 'AAAAhobH7C4:APA91bFFzGuuvzDQ1zUUY8Ikk_1WCT_wjJMUrI6ejBzv5WP-rSkIhYqSPG6g9nbNCZYs8BIXb8FkpqzbLHyT7XfQ7rHtFhVhC0TPC4r30f1CtbM9xKwi1pLfvYpuLmEx_QhQ8DhHd9BW';
const fcm = new FCM(serverKey);
let io = null;
const NotificationManager = require('../Lib/pushNotification');
const UniversalFunctions = require('../Utils/UniversalFunction');
// let NotificationManager = require('../Lib/pushNotification');
exports.connectSocket = function (server) {
    if (!server.app) {
        server.app = {}
    }
    server.app.socketConnections = {};
    io = require('socket.io').listen(server.listener);

    io.on('connection', function (socket) {

        let socketId = null;
        console.log("...........................CONNECTION..................................");

        socket.on('userAuth', function (data) {
          //  console.log("jhhhhhhhhhhhhhhhhhhhh", data);
            verifyToken(data, function (err, response) {
                if (err || !response || !response.userData) {
                    /////////////////////////////////////////////////
                }
                else {
                    if (server.app.socketConnections.hasOwnProperty(response.userData._id)) {

                      //  console.log("2222222222222222222222", socket.id, server.app.socketConnections);
                        server.app.socketConnections[response.userData._id].socketId = socket.id;
                        //  server.app.socketConnections[socket.id]['userId'] = response.userData._id;
                        socketId = socket.id;

                        updateSocketId(socketId, response.userData._id,data.type ,function (err, result) {
                            io.to(socketId).emit('socketConnected', {
                                statusCode: 100,
                                message: 'Socket id Updated',
                                data: {socketId: socket.id}
                            });
                        })

                    }
                    else {
                      //  console.log("33333333333333333333333", socket.id);

                        server.app.socketConnections[response.userData._id] = {
                            socketId: socket.id
                        };
                        server.app.socketConnections[socket.id] = {
                            userId: response.userData._id
                        };
                        socketId = socket.id;

                        updateSocketId(socketId, response.userData._id,data.type, function (err, result) {
                            io.to(socketId).emit('socketConnected', {
                                statusCode: 100,
                                message: 'Added To socketConnections',
                                data: {socketId: socket.id}
                            });
                        })
                    }
                }
            })
        });

        socket.on('sendMessage', function (data) {
            console.log("sendMessage____________________", data);
            // console.log("!2222",typeof(data));
            // console.log("receiverId", data.receiverId);
            // console.log("senderId", data.senderId);
            if (data.senderId && data.receiverId) {
                getUser(data.senderId,data.type, function (err, res) {
                 //   console.log("111111111111111111",res);
                    if (res && res.length) {
                        var type="";
                        if(data.type=="user"){
                            type="admin"
                        }
                        if(data.type=="admin"){
                            type="user"
                        }
                        getUser(data.receiverId,type,  function (err1, res1) {
                          //  console.log("222222222222",res1)
                            if (res1 && res1.length) {
                                createRequest(data, function (err2, res2) {
                                //    console.log("33333333333333",res2);
                                    if (res2.chatId) {
                                        var emitData = {
                                            senderId: data.senderId,
                                            receiverId: data.receiverId,
                                            msgId: res2.msgId,
                                            message: {
                                                senderId: data.senderId,
                                                text: data.text || [],
                                                url: {original: data.original, thumbnail: data.thumbnail} || {original: "", thumbnail: ""},
                                                msgType: data.msgType,
                                                templateId: data.templateId||"",
                                                cost: data.cost || 0,
                                                status : "Pending",
                                                duration: data.duration || 0,
                                                htmlUrl: data.htmlUrl || "",
                                                timeStamp: Date.now()
                                            },
                                            requestCreatedAt: Date.now(),
                                            categoryId: data.categoryId || "",
                                            title: data.title || "",
                                            categoryName: data.categoryName || [],
                                        };
                                       // console.log("44444444444444444",emitData);
                                        io.to(res1[0].socketId).emit("sendMessage", emitData);
                                        //console.log("______________________123sendMessage_____________", emitData);
                                        console.log("__________________SOCKET sendMessage_____________", res1[0].socketId, server.app.socketConnections);
                                    }
                                    else {
                                        console.log("__________________noSOCKET_ID____________________")
                                    }
                                });
                            }
                            else {
                                console.log("________ReceiverId doesNotExist_____________")
                            }
                        })
                    }
                    else {
                        console.log("_______SenderId doesNotExist_____________")
                    }
                })
            }
            else {
                console.log("________blank data from frontEnd_____________message")
            }
        });

        socket.on('disconnect', function () {
          //  console.log('Socket disconnected---1->>>>>>>>>');
            console.log('Socket disconnected---->>>>>>>>>', server.app.socketConnections);
            if (server.app.socketConnections.hasOwnProperty(socket.id)) var userId = server.app.socketConnections[socket.id].userId;
            if (server.app.socketConnections.hasOwnProperty(userId)) delete server.app.socketConnections[userId];
            if (server.app.socketConnections.hasOwnProperty(socket.id)) delete server.app.socketConnections[socket.id];
        });


    });
};

exports.enterZone = function (dataToEmit) {
    console.log("enterZone__________________");
    io.emit('EnterZone', dataToEmit);
};

var updateSocketId = function (socketId, userId,type, cb) {
    let criteria = {
        _id: userId,
    };
    let dataToSave = {
        socketId: socketId
    };
    let option = {lean: true, new: true};
    if(type=="admin") {
        Service.adminServices.updateAdmin(criteria, dataToSave, option, function (err, result) {
            if (err) {
                cb()
            } else {

                if (result && result.length) {
                    cb(null, {userId: userId, socketId: socketId})
                } else {
                    cb(null)
                }
            }
        })
    }
    if(type=="user") {
        Service.userServices.updateUser(criteria, dataToSave, option, function (err, result) {
            if (err) {
                cb()
            } else {

                if (result && result.length) {
                    cb(null, {userId: userId, socketId: socketId})
                } else {
                    cb(null)
                }
            }
        })
    }
};

var verifyToken = function (data, cb) {
    let criteria = {
        accessToken: data.accessToken,
    };
    let projection = {};
    let option = {lean: true};
    if(data.type=="admin"){
        Service.adminServices.getAdmin(criteria, projection, option, function (err, result) {
            if (err) {
                cb()
            } else {

                if (result && result.length) {
                    cb(null, {userData: result[0]})
                } else {
                    cb(null)
                }
            }
        })
    }
    if(data.type=="user") {
        Service.userServices.getUsers(criteria, projection, option, function (err, result) {
            if (err) {
                cb()
            } else {

                if (result && result.length) {
                    cb(null, {userData: result[0]})
                } else {
                    cb(null)
                }
            }
        })
    }
};

exports.offerDone = function (socketId,dataToEmit) {
    console.log("completeOffer",dataToEmit);
    io.to(socketId).emit("completeOffer", dataToEmit);
};

var getUser = function (_id,type, cb) {
    // console.log("type______",type,_id);
    let criteria = {
        _id: _id,
    };
    let projection = {
        _id: 1,
        profilePicture: 1,
        name: 1,
        socketId: 1,
    };
    let option = {lean: true};
    if(type=="user"){
        Service.userServices.getUsers(criteria, projection, option, function (err, result) {
            if (err) {
                cb()
            } else {
                if (result && result.length) {
                     //console.log("________________SOCKET getuser");
                    cb(null, result)
                } else {
                    cb(null)
                }
            }
        })
    }
    if(type=="admin"){
        Service.adminServices.getAdmin(criteria, projection, option, function (err, result) {
            if (err) {
                cb()
            } else {
                if (result && result.length) {
                 //    console.log("________________SOCKET getadmin");
                    cb(null, result)
                } else {
                    cb(null)
                }
            }
        })
    }

};

var createRequest = function (data, cb) {
    let responseObject = {};
    let chatId = null;
    let dToken = null;
    let flag = 0;
    let body = {};
    let msgId = mongoose.Types.ObjectId();
    let oldchat;
    let userName;
    async.auto({

        checkChat: function (cb1) {
            if (responseObject.statusCode == 400) {
                cb1(null)
            }
            else {
                let criteria = {
                    $or: [{
                        adminId: data.senderId,
                        userId: data.receiverId
                    }, {
                        userId: data.senderId,
                        adminId: data.receiverId
                    }],
                };
                    criteria.categoryId=data.categoryId;
                    criteria.title=data.title;
                let populateArray=[
                    {
                        path:'userId',
                        select:'_id deviceToken'
                    }
                ];
                Service.requestServices.getRequestPopulate(criteria, {}, {},populateArray, function (err, result) {
                    if (err) {
                        cb1()
                    } else {
                      //  console.log("________CHECK CHAT__________",result)
                        if (result.length) {
                            chatId = result[0]._id;
                            if(data.receiverId==(result[0].userId._id).toString() && result[0].userId.deviceToken!=undefined) dToken = result[0].userId.deviceToken;
                            oldchat = result[0];
                            flag = 1
                        }else{
                            Service.userServices.getUsers({_id:{$in:[data.senderId,data.receiverId]}},{name:1},{lean:true},(err,res)=>{
                                userName=res[0].name;
                              //  flag=0
                            });
                             flag=0
                        }
                        cb1(null)
                    }
                })
            }
        },
        createChat: ['checkChat', function (err, cb1) {
           // console.log("flag",flag);
            if (responseObject.statusCode == 400) {
                cb1(null)
            }
            else {
                let dataToSet = {};
                if (flag == 0) {
                    dataToSet = {
                        message: [{
                            _id:msgId,
                            senderId: data.senderId,
                            isRead: false,
                            text: data.text || [],
                            cost: data.cost || 0,
                            templateId: data.templateId || null,
                            duration: data.duration || 0,
                            htmlUrl: data.htmlUrl || "",
                            url: {original: data.original, thumbnail: data.thumbnail} || {
                                original: "",
                                thumbnail: ""
                            },
                            msgType: data.msgType,
                            timeStamp: Date.now()
                        }],
                        requestCreatedAt: Date.now(),
                        categoryId: data.categoryId || null,
                        title: data.title || "",
                        address:data.address || "",
                        lat:data.lat ||0,
                        long:data.long ||0,
                        startAt:data.startAt ||0,
                        endAt:data.endAt ||data.startAt+25200000,
                        categoryName: data.categoryName || [],
                        timeStamp: Date.now()
                    };
                    if(data.type=="user"){
                        dataToSet.userId=data.senderId;
                        dataToSet.adminId=data.receiverId
                    }
                    if(data.msgType=="Offer"){
                        //dataToSet.designSent=true;
                        dataToSet.status="Pending";
                    }
                    if(data.msgType=="Template"){
                        dataToSet.status="Completed";
                    }
                    if(data.type=="admin"){
                        dataToSet.userId=data.receiverId;
                        dataToSet.adminId=data.senderId
                    }
                    Service.requestServices.createRequest(dataToSet, function (err, result) {
                        if (err) {
                            cb1()
                        } else {
                            let subject = "Custom Template Request";
                            let template='<html><body><p>User with name '+userName+' requested a custom template for category '+data.categoryName[1]+'</p></body></html>'
                            UniversalFunctions.sendMail('requestdesign.w@gmail.com', subject, template, function (err,result) {
                            });
                            //console.log("________Create Rqst",result,dataToSet)
                            chatId = result._id;
                            responseObject.chatId = chatId;
                            cb1(null)
                        }
                    })
                }
                else {
                    let criteria = {
                        _id: chatId
                    };
                    dataToSet = {
                        $push: {
                            message: {
                                _id:msgId,
                                senderId: data.senderId,
                                text: data.text || [],
                                url: {original: data.original, thumbnail: data.thumbnail} || {original: "", thumbnail: ""},
                                msgType: data.msgType,
                                templateId: data.templateId|| null,
                                cost: data.cost || 0,
                                duration: data.duration || 0,
                                htmlUrl: data.htmlUrl ||"",
                                timeStamp: Date.now()
                            }
                        },
                        timeStamp: Date.now()
                    };
                    if(data.type=="user"){
                        dataToSet.userId=data.senderId;
                        dataToSet.adminId=data.receiverId;
                        dataToSet.isRead=false;
                    }
                    if(data.type=="admin"){
                        dataToSet.userId=data.receiverId;
                        dataToSet.adminId=data.senderId
                    }
                    if(data.msgType=="Offer"){
                        dataToSet.status="Pending";
                    }
                    if(data.msgType=="Template"){
                        dataToSet.status="Completed";
                    }
                    Service.requestServices.updateRequest(criteria, dataToSet, {}, function (err, result) {
                        if (err) {
                            cb1()
                        } else {
                            //console.log("________Update Request________",result,dataToSet)
                            chatId = result._id;
                            responseObject.chatId = chatId;
                            responseObject.msgId = msgId;
                            cb1(null)
                        }
                    })
                }
            }
        }],
        sendPush:['createChat', function (err,cb1){
            if(dToken!=undefined){
                let data={
                    title:'Wethaq',
                    multi:false,
                    timestamp:new Date().getTime(),
                };
                data.body='Admin messaged you.';
                data.type=7;
                NotificationManager.sendPush(data,dToken, function (err, result) {
                    cb1(null)
                });
            }else{
                cb1(null)
            }
        }]
    }, function (err, result) {
        //console.log("777777777777777777777",responseObject)
        cb(err, responseObject)
    })
};