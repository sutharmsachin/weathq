/**
 * Created by Shumi Gupta on 30/3/17.
 */
"use strict";
const Jwt = require('jsonwebtoken');
const Config = require('../Config');
const Service = require('../services');
var async = require('async');

const setToken = function (tokenData, callback) {

    if (!tokenData.id || !tokenData.type) {
        callback(Config.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_ID);
    } else {
        let tokenToSend = Jwt.sign(tokenData,Config.APP_CONSTANTS.SERVER.JWT_SECRET_KEY);
        setTokenInDB(tokenData.id,tokenData.type, tokenToSend, function (err, data) {
            callback(err, {accessToken: tokenToSend})
        })
    }
};



const setTokenInDB = function (userId,userType, tokenToSave, cb) {
    let criteria = {
        _id: userId
    };
    let setQuery = {
        accessToken : tokenToSave
    };
            //console.log("userType",tokenToSave);
            if (userType === Config.APP_CONSTANTS.DATABASE.USER_TYPE.user){
                Service.userServices.updateUser(criteria,setQuery,{new:true}, function (err, dataAry) {
                    if (err){
                        cb(err)
                    }else {
                        if (dataAry && dataAry._id){
                            cb();
                        }else {
                            cb(Config.APP_CONSTANTS.STATUS_MSG.ERROR.APP_ERROR)
                        }
                    }
                });

            }
            else if (userType == Config.APP_CONSTANTS.DATABASE.USER_TYPE.admin){
                Service.adminServices.updateAdmin(criteria,setQuery,{new:true}, function (err, dataAry) {
                    if (err){
                        cb(err)
                    }else {
                        //  console.log("=====================",dataAry)
                        if (dataAry){
                            console.log("here=======here==========",err)
                            cb();
                        }else {
                          //  console.log("here=================",err)
                            cb(Config.APP_CONSTANTS.STATUS_MSG.ERROR.APP_ERROR)
                        }
                    }

                });
            }
            else if (userType == Config.APP_CONSTANTS.DATABASE.USER_TYPE.restaurant){
                Service.restaurantServices.updateRestaurant(criteria,setQuery,{new:true}, function (err, dataAry) {
                    if (err){
                        cb(err)
                    }else {
                        //  console.log("=====================",dataAry)
                        if (dataAry){
                            console.log("here=======here==========",err)
                            cb();
                        }else {
                           // console.log("here=================",err)
                            cb(Config.APP_CONSTANTS.STATUS_MSG.ERROR.APP_ERROR)
                        }
                    }

                });
            }
            else {
                cb(Config.APP_CONSTANTS.STATUS_MSG.ERROR.APP_ERROR)
            }
};



const verifyToken = function (token, callback) {
  
    Jwt.verify(token, Config.APP_CONSTANTS.SERVER.JWT_SECRET_KEY, function (err, decoded) {
     //   console.log('jwt err',err,decoded)
        if (err) {
            callback(err)
        } else {
            getTokenFromDB(decoded.id, decoded.type,token, callback);
        }
    });
};

const verifyTokenAdmin = function (token, callback) {
 
    Jwt.verify(token, Config.APP_CONSTANTS.SERVER.JWT_SECRET_KEY, function (err, decoded) {
       // console.log('jwt err',err,decoded);
        if (err) {
            callback(err)
        } else {
            if(decoded.type==Config.APP_CONSTANTS.DATABASE.USER_TYPE.admin){
                getTokenFromDB(decoded.id, decoded.type,token, callback);

            }
            else{
                callback()
            }
        }
    });
};

const verifyTokenCommon = function (token, callback) {

    Jwt.verify(token, Config.APP_CONSTANTS.SERVER.JWT_SECRET_KEY, function (err, decoded) {
       // console.log('jwt err',err,decoded);
        if (err) {
            callback(err)
        } else {
            if(decoded.type==Config.APP_CONSTANTS.DATABASE.USER_TYPE.user || Config.APP_CONSTANTS.DATABASE.USER_TYPE.admin){
                getTokenFromDB(decoded.id, decoded.type,token, callback);

            }
            else{
                callback()
            }
        }
    });
};



var getTokenFromDB = function (userId, userType,token, callback) {
    var userData = null;
    var criteria = {
        _id: userId,
        accessToken : token
    };

   // console.log("=========criteria=====",criteria)
    async.series([
        function (cb) {
            if (userType == Config.APP_CONSTANTS.DATABASE.USER_TYPE.user){
               // criteria.isBlocked=false;
                criteria.isDeleted=false;
                //console.log("=============criteria===================",criteria)
                Service.userServices.getUsers(criteria,{},{lean:true}, function (err, dataAry) {
                    // console.log(":.......................err..........................",err,dataAry);
                    if (err){
                        cb(err);
                    }else {
                        if (dataAry && dataAry.length > 0) {
                            if (dataAry[0].isBlocked == true) {
                             cb(Config.APP_CONSTANTS.STATUS_MSG.ERROR.USER_BLOCKED);
                            }else{
                            userData = dataAry[0];
                            // console.log("=========");
                            cb();
                             }
                        }
                        else {
                            cb(Config.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_TOKEN);
                        }
                    }

                });
            }
            else if (userType == Config.APP_CONSTANTS.DATABASE.USER_TYPE.admin){
                Service.adminServices.getAdmin(criteria,{},{lean:true}, function (err, dataAry) {
                    if (err){
                        cb(err)
                    }else {
                        if (dataAry && dataAry.length > 0){
                            userData = dataAry[0];
                            cb();
                        }else {
                            cb(Config.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_TOKEN)
                        }
                    }

                });

            }
            else if (userType == Config.APP_CONSTANTS.DATABASE.USER_TYPE.restaurant){
                criteria.isDeleted=false;
                Service.restaurantServices.getRestaurant(criteria,{},{lean:true}, function (err, dataAry) {
                    if (err){
                        callback(err)
                    }else {
                        if (dataAry && dataAry.length > 0){
                            userData = dataAry[0];
                            if(userData.verified==true){
                                cb();
                            }
                            else{
                                callback(Config.APP_CONSTANTS.STATUS_MSG.ERROR.NOT_VERIFIED)
                            }
                        }
                        else {
                            callback(Config.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_TOKEN)
                        }
                    }

                });
            }
            else {
                cb(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR)
            }
        }
    ], function (err, result) {
        if (err){
            callback(err)
        }else {
            if (userData && userData._id){
                userData.id = userData._id;
                userData.type = userType;
            }
            callback(null,{userData: userData})
        }

    });
};

module.exports = {
    //expireToken: expireToken,
    setToken: setToken,
    verifyToken: verifyToken,
    verifyTokenAdmin:verifyTokenAdmin,
    verifyTokenCommon:verifyTokenCommon
   // decodeToken: decodeToken
};