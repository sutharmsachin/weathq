/**
 * Created by lalit on 11/18/16.
 */
'use strict';

var Service = require('../services');
var UniversalFunctions = require('../Utils/UniversalFunction');

var async = require('async');
var NotificationManager = require('../Lib/pushNotification');
var Jwt = require('jsonwebtoken');
var _ = require('lodash');
var ERROR = ('UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR');
var Models = require('../Models');
var nodemailer = require('nodemailer');
var sesTransport = require('nodemailer-ses-transport');
const messageManager = require('../Lib/messageManager');
var mongoose = require('mongoose');
var schedule = require('node-schedule');
const Config = require('../Config');
const qr = require('qr-image');
const Path = require('path');
const fsExtra = require('fs-extra');
const knox = require('knox');
const TinyURL = require('tinyurl');
exports.schedular = function () {

    var autoPush = [];
    var inviteEvent = [];
    var reminderEvent = [];
    var j = schedule.scheduleJob('*/1 * * * *', function () {      //start and end events
        // console.log("start event schedule");
        async.autoInject({
            sendAdminPushToAll: (cb) =>{
                    let startTime=new Date().setUTCMinutes(new Date().getUTCMinutes()-1);
                    let endTime=new Date().setUTCMinutes(new Date().getUTCMinutes());
                    let criteria = {
                        sendAt: {
                            $gte:startTime,$lte:endTime
                        },
                        isSent:false,
                        isDeleted:false
                    };
                    Service.scheduleNotifServices.getScheduleNotifs(criteria,{},{lean:true},function (err,result) {
                        if (err) {
                            cb(err)
                        }else{
                            if (result && result.length > 0) {
                                autoPush = result;
                                let flag=0;
                                for(let i=0;i<autoPush.length;i++){
                                    (function (i) {
                                        createPushAnd('AUTOPUSH',autoPush[i], function (err, res) {
                                            flag++;
                                            if (flag == autoPush.length) {
                                                cb(null);
                                            }
                                        })
                                    }(i))
                                }
                                }
                            else {
                                cb()
                            }
                        }
                    })
            },
            getInviteEvents:(cb)=>{
                let startTime=new Date().setUTCMinutes(new Date().getUTCMinutes()-1);
                let endTime=new Date().setUTCMinutes(new Date().getUTCMinutes());
                let criteria = {
                    inviteAt: {
                        $gte:startTime,$lte:endTime
                    },
                    isSent:false,
                    isDeleted:false,
                };
                let populate=[
                    {
                        path:'createdBy',
                        select:'_id name'
                    }
                ]
                Service.eventServices.getEventPopulate(criteria,{},{lean:true},populate,function (err,result) {
                    if (err) {
                        cb(err)
                    }else{
                        console.log("result==========222============",result)
                        if (result && result.length > 0) {
                            inviteEvent = result;
                            let flag=0;
                            for(let i=0;i<inviteEvent.length;i++){
                                (function (i) {
                                    createPushAnd('INVITE',inviteEvent[i], function (err, res) {
                                        flag++;
                                        if (flag == inviteEvent.length) {
                                            cb(null);
                                        }
                                    })
                                }(i))
                            }
                        }
                        else {
                            cb()
                        }
                    }
                })
            },
            getRemindersEvents:(cb)=>{
                let startTime=new Date().setUTCMinutes(new Date().getUTCMinutes()-1);
                let endTime=new Date().setUTCMinutes(new Date().getUTCMinutes());
                let aggregate=[
                    {$match:
                        {$and:[	{isDeleted:false},
                              {"inviteAt": {$gte: new Date(new Date().setUTCMinutes(new Date().getUTCMinutes()-1)).getTime(),
                              $lte:new Date(new Date().setUTCMinutes(new Date().getUTCMinutes())).getTime()
                              }},
                          
                          ]
                  }
                  },
                  
                    // {$project:{
                    //     _id:1,
                    //     invites: {$map: {input: "$invites", as: "item", in: "$$item.phoneNo"}},
                    //     reminder: {
                    //         $filter: {
                    //             input: "$reminder",
                    //             as: "item",
                    //             cond: {
                    //                 $and: [
                    //                     {$eq: ["$$item.isSent", false]},
                    //                     {$gte: ["$$item.inviteAt", new Date(startTime).getTime()]},
                    //                     {$lte: ["$$item.inviteAt", new Date(endTime).getTime()]}
                    //                 ]
                    //             }
                    //         }
                    //     }
                    // }
                    // },
                    //{$unwind:"$reminder"},
                ];
                Service.eventServices.eventAggregate(aggregate,(err,result)=>{
                    if(err)cb(err)
                    else{
                        console.log("====result==========323==========",result)
                        if (result && result.length > 0) {
                            reminderEvent = result;
                            let flag=0;
                            for(let i=0;i<reminderEvent.length;i++){
                                (function (i) {
                                    createPushAnd('REMINDER',reminderEvent[i], function (err, res) {
                                        flag++;
                                        if (flag == reminderEvent.length) {
                                            cb(null);
                                        }
                                    })
                                }(i))
                            }
                        }
                        else {
                            cb()
                        }
                        // cb(null)
                    }
                })
            }
        })
    })
};


var createPushAnd = function (type,result, callback) {
    let notified=false;
    let invited=false;
    let reminder=false;
    let dTokens=[];
    let deviceTokens=[];
    let emails=[];
    let invitees=[];
    let ids=[];
    let qrs=[];
    let existing=[];
    let unRegistered=[];
    console.log("createPushAnd", result)
    async.autoInject({
        getDeviceTokens: (cb)=> {
            if (type == 'AUTOPUSH') {
                let query = {};
                query.isDeleted=false;
                let projection={
                    deviceToken:1
                };
                Service.userServices.getUsers(query,projection,{},(err,result)=>{
                    if(err){
                        cb(err)
                    }
                    else{
                        if(result && result.length){
                            let flag=0;
                            for (let i = 0; i < result.length; i++) {
                                (function (i) {
                                    deviceTokens.push(result[i].deviceToken);
                                    flag++;
                                    if (flag == result.length) {
                                        cb(null)
                                    }
                                }(i))
                            }
                        }else {
                            cb(null);
                        }
                    }
                })
            }
            else if (type == 'INVITE') {
                let flag = 0;
                for (let i = 0; i < result.invites.length; i++) {
                    (function (i) {
                        // console.log("invites", result.invites[i])
                        result.invites[i].status='Pending';
                        let id=(result._id).toString();
                        let number=((result.invites[i].phoneNo).replace("+","")).toString();
                      //  let link='http://34.211.166.21/wethaq_admin/#/invitation/'+id+'/'+number;
                      ////////////////////ravi change here         
                      let link='http://admin.wethaqapp.net/wethaq_admin/#/invitation/'+id+'/'+number;
                    //   let link = 'http://34.211.166.21/wethaq_dev/wethaq_admin/#/invitation/'+id+'/'+number;   
                    console.log("== sms link ==",link);  
                    /////////////////end 
                      
                      let subject = "Event Invite";
                        TinyURL.shorten(link, function(res) {
                            link = res
                            // let template = '<html><head></head><body><table style="border-collapse:collapse;table-layout:fixed;min-width:320px;width:100%;background-color:#fff;text-align:left;direction:ltr"><tbody><tr><td><div style=";text-align:left;direction:ltr"><div style="Margin:0 auto;max-width:560px;min-width:280px;width:280px;width:calc(28000% - 167440px);text-align:left;direction:ltr"> <br></div><div style="Margin:0 auto;max-width:600px;min-width:320px;width:320px;width:calc(28000% - 167400px);text-align:left;direction:ltr"><div style="font-size:26px;line-height:32px;Margin-top:6px;Margin-bottom:20px;color:#c3ced9;font-family:Roboto,Tahoma,sans-serif;Margin-left:20px;Margin-right:20px;text-align:left;direction:ltr" align="center"><div align="center" style=";text-align:left;direction:ltr"> <img style="display:block;height:auto;width:100%;border:0;max-width:230px" src="https://s3-us-west-2.amazonaws.com/wethaqbucket/profilePic_1523699389214imageBackground.png" alt="" width="230"></div></div></div></div><div style=";text-align:left;direction:ltr"><div style="Margin:0 auto;max-width:600px;min-width:320px;width:320px;width:calc(28000% - 167400px);word-wrap:break-word;word-break:break-word;text-align:left;direction:ltr"><div style="border-collapse:collapse;display:table;width:100%;background-color:#ffffff;text-align:left;direction:ltr"><div style="text-align:left;color:#8f8f8f;font-size:16px;line-height:24px;font-family:Open Sans,sans-serif;max-width:600px;min-width:320px;width:320px;width:calc(28000% - 167400px);text-align:left;direction:ltr"><div style="Margin-left:20px;Margin-right:20px;Margin-top:24px;text-align:left;direction:ltr"><div style=";text-align:left;direction:ltr"><h1 style="Margin-top:0;Margin-bottom:20px;font-style:normal;font-weight:normal;color:#404040;font-size:28px;line-height:36px;text-align:center;text-align:left;direction:ltr">'+result.title+'</h1></div></div><div style="Margin-left:20px;Margin-right:20px;text-align:left;direction:ltr"><div style="line-height:10px;font-size:1px;text-align:left;direction:ltr"></div></div><div style="Margin-left:20px;Margin-right:20px;text-align:left;direction:ltr"><div style="line-height:10px;font-size:1px;text-align:left;direction:ltr"></div></div><div style="Margin-left:20px;Margin-right:20px;text-align:left;direction:ltr"><div style=";text-align:left;direction:ltr"><h1 style="Margin-top:0;Margin-bottom:0;font-style:normal;font-weight:normal;color:#404040;font-size:28px;line-height:36px;text-align:center;text-align:left;direction:ltr">'+result.invites[i].name+'أهلا بك</h1><h1 style="Margin-top:20px;Margin-bottom:0;font-style:normal;font-weight:normal;color:#404040;font-size:28px;line-height:36px;text-align:center;text-align:left;direction:ltr"> <strong>مرحباً بكم في تطبيق وثاق </strong></h1><h3 style="Margin-top:20px;Margin-bottom:12px;font-style:normal;font-weight:normal;color:#706f70;font-size:17px;line-height:26px;font-family:Cabin,Avenir,sans-serif;text-align:center;text-align:left;direction:ltr">التصميم المخصص للدعوة</h3></div></div><div style="font-size:12px;font-style:normal;font-weight:normal;line-height:19px;text-align:left;direction:ltr" align="center"> <img style="border:0;display:block;height:auto;width:100%;max-width:900px" alt="" width="600" src='+result.imageUrl+'></div><div style="Margin-left:20px;Margin-right:20px;text-align:left;direction:ltr"><div style=";text-align:left;direction:ltr"><h2>'+link+' : إليك الرابط </h2></div></div></div></div></div></div></td></tr></tbody></table></body></html>'
                            // UniversalFunctions.sendMail(result.invites[i].email, subject, template, function (err,result) {
                            //     // console.log("res",err,result)
                            // });
                            result.invites[i].createdAt = new Date().getTime();
                            let query = {
                                phoneNo: result.invites[i].phoneNo,
                                _id: {$ne: result.createdBy._id}
                            };
                            Service.userServices.getUsers(query, {}, {lean: true}, function (err, result2) {
                                if (err) {
                                    cb(err)
                                } else {
                                    invited = true;
                                    //   phoneNos.push(invites[i].phoneNo);
                                    if (result2 && result2.length) {
                                        existing.push({"_id": result2[0]._id, "phoneNo": result.invites[i].phoneNo});
                                        ids.push(result2[0]._id);
                                        emails.push(result2[0].email);
                                        dTokens.push(result2[0].deviceToken);
                                    }
                                    let criteria = {
                                        to: [result.invites[i].phoneNo],
                                        body: ""
                                    };

                                    if (result.message && result.message != '') {
                                        if (result.lang == "en") {
                                            criteria.body += "Hello " + result.invites[i].name + ", " + result.message + "\n";
                                        } else if (result.lang == "ar") {
                                            criteria.body += " مرحبًا" + "\u202A" + result.invites[i].name + ", " + "\u202C" + result.message + "\n";
                                        } else {
                                            criteria.body += " مرحبًا" + "\u202A" + result.invites[i].name + ", " + "\u202C" + result.message + "\n";
                                        }
                                    } else {
                                        if (result.lang == "en") {
                                            criteria.body += "Hello " + result.invites[i].name + " It’s an honor to invite you to " + result.title + "\n";
                                        } else if (result.lang == "ar") {
                                            criteria.body += " أهلا" + "\u202A" + result.invites[i].name + "\u202C" + " نتشرف بدعوتك لحضور مناسبة " + "\u202A" + result.title + "\n";
                                        } else {
                                            criteria.body += " أهلا" + "\u202A" + result.invites[i].name + "\u202C" + " نتشرف بدعوتك لحضور مناسبة " + "\u202A" + result.title + "\n";
                                        }
                                    }
                                    let template = '<html><head></head><body><table style="border-collapse:collapse;table-layout:fixed;min-width:320px;width:100%;background-color:#fff;text-align:left;direction:ltr"><tbody><tr><td><div style=";text-align:left;direction:ltr"><div style="Margin:0 auto;max-width:560px;min-width:280px;width:280px;width:calc(28000% - 167440px);text-align:left;direction:ltr"> <br></div><div style="Margin:0 auto;max-width:600px;min-width:320px;width:320px;width:calc(28000% - 167400px);text-align:left;direction:ltr"><div style="font-size:26px;line-height:32px;Margin-top:6px;Margin-bottom:20px;color:#c3ced9;font-family:Roboto,Tahoma,sans-serif;Margin-left:20px;Margin-right:20px;text-align:left;direction:ltr" align="center"><div align="center" style=";text-align:left;direction:ltr"> <img style="display:block;height:auto;width:100%;border:0;max-width:230px" src="https://s3-us-west-2.amazonaws.com/wethaqbucket/profilePic_1523699389214imageBackground.png" alt="" width="230"></div></div></div></div><div style=";text-align:left;direction:ltr"><div style="Margin:0 auto;max-width:600px;min-width:320px;width:320px;width:calc(28000% - 167400px);word-wrap:break-word;word-break:break-word;text-align:left;direction:ltr"><div style="border-collapse:collapse;display:table;width:100%;background-color:#ffffff;text-align:left;direction:ltr"><div style="text-align:left;color:#8f8f8f;font-size:16px;line-height:24px;font-family:Open Sans,sans-serif;max-width:600px;min-width:320px;width:320px;width:calc(28000% - 167400px);text-align:left;direction:ltr"><div style="Margin-left:20px;Margin-right:20px;Margin-top:24px;text-align:left;direction:ltr"><div style=";text-align:left;direction:ltr"><h1 style="Margin-top:0;Margin-bottom:20px;font-style:normal;font-weight:normal;color:#404040;font-size:28px;line-height:36px;text-align:center;text-align:left;direction:ltr">' + result.title + '</h1></div></div><div style="Margin-left:20px;Margin-right:20px;text-align:left;direction:ltr"><div style="line-height:10px;font-size:1px;text-align:left;direction:ltr"></div></div><div style="Margin-left:20px;Margin-right:20px;text-align:left;direction:ltr"><div style="line-height:10px;font-size:1px;text-align:left;direction:ltr"></div></div><div style="Margin-left:20px;Margin-right:20px;text-align:left;direction:ltr"><div style=";text-align:left;direction:ltr"><h1 style="Margin-top:0;Margin-bottom:0;font-style:normal;font-weight:normal;color:#404040;font-size:20px;line-height:36px;text-align:center;text-align:left;direction:ltr">' + result.invites[i].name + 'أهلا بك</h1><h1 style="Margin-top:20px;Margin-bottom:0;font-style:normal;font-weight:normal;color:#404040;font-size:28px;line-height:36px;text-align:center;text-align:left;direction:ltr"> <strong>مرحباً بكم في تطبيق وثاق </strong></h1><h3 style="Margin-top:20px;Margin-bottom:12px;font-style:normal;font-weight:normal;color:#706f70;font-size:17px;line-height:26px;font-family:Cabin,Avenir,sans-serif;text-align:center;text-align:left;direction:ltr">التصميم المخصص للدعوة</h3><h3 style="Margin-top:20px;Margin-bottom:12px;font-style:normal;font-weight:normal;color:#706f70;font-size:17px;line-height:26px;font-family:Cabin,Avenir,sans-serif;text-align:center;text-align:left;direction:ltr">' + criteria.body + '</h3></div></div><div style="font-size:12px;font-style:normal;font-weight:normal;line-height:19px;text-align:left;direction:ltr" align="center"> <img style="border:0;display:block;height:auto;width:100%;max-width:900px" alt="" width="600" src=' + result.imageUrl + '></div><div style="Margin-left:20px;Margin-right:20px;text-align:left;direction:ltr"><div style=";text-align:left;direction:ltr"><h2>' + link + ' : إليك الرابط</h2></div></div></div></div></div></div></td></tr></tbody></table></body></html>';

                                    UniversalFunctions.sendMail(result.invites[i].email, subject, template, function (err, result) {
                                        console.log("res", err, result)
                                    });
                                    if (result.lang == "en") {
                                        criteria.body += "For more info, please check the link: " + "\n";
                                    } else if (result.lang == "ar") {
                                        criteria.body += "وللمزيد من المعلومات الرجاء فتح الرابط التالي: " + "\n";
                                    } else criteria.body += "وللمزيد من المعلومات الرجاء فتح الرابط التالي" + "\n";
                                    criteria.body += link
                                    // console.log("criteria reg",criteria.body);
                                    messageManager.sendSmsUnisoft(criteria, function (result1) {
                                        console.log("registered sms", result1);
                                        // if (err == null) {
                                        if (result1.success=="true") {
                                            result.invites[i].statusCode = 200;
                                            result.invites[i].createdAt = new Date().getTime();
                                            // result.invites[i].message = [result1.Data.MessageEn, result1.Data.MessageAr];
                                            result.invites[i].message = [ "SMS sent successfully", "تم الإرسال"];
                                            invitees.push(result.invites[i]);
                                        } else {
                                            // console.log("1234555",err);
                                            result.invites[i].statusCode = 400;
                                            result.invites[i].createdAt = new Date().getTime();
                                            // result.invites[i].message = [err.MessageEn, err.MessageAr];
                                            result.invites[i].message = ["Mobile(s) number(s) is not specified or incorrect", "الأرقام المرسل لها غير صحيحه أو فارغه"];

                                            invitees.push(result.invites[i]);
                                        }
                                        flag++;
                                        if (flag == result.invites.length) {
                                            cb(null);
                                        }
                                    });

                                }
                            })
                        })
                    }(i))
                }
            }
            else if (type == 'REMINDER') {
                let criteria = {
                    to: result.invites
                };
                criteria.body =result.reminder.message;
                messageManager.sendSmsBulkUnisoft(criteria, function ( result1) {
                    console.log("bulk event sms", result1);
                    reminder=true;
                    cb(null);
                });
            }
            else{cb(null)}
        },
        createPush:(getDeviceTokens,cb) =>{
            if(type == 'AUTOPUSH') {
                let pushData={
                    title:result.title,
                    body:result.body,
                    multi:true,
                    type:7
                };
                 NotificationManager.sendPush(pushData,deviceTokens, function (err, result) {
                     notified=true
                 cb(null)
                 });
                }
            else if(type == 'INVITE'){
                if(ids && ids.length){
                    // console.log("notification",ids);
                    let query={
                        createdFor:ids,
                        createdBy:[result.createdBy._id],
                        type:0,
                        text:[result.title],
                        eventId:result._id,
                        timestamp:result.startAt,
                        categoryId:result.categoryId,
                        templateId:result.templateId,
                        notificationType:'Invite',
                        images:[{original:result.imageUrl,thumbnail:result.imageUrl}],
                    };
                    Service.notificationServices.createNotification(query,function (err,result) {
                        if(err){
                            cb(err)
                        }else{
                            cb(null);
                        }
                    });
                    // console.log("1111",unRegistered,phoneNos,ids);
                }else{
                    cb(null)
                }
            }
            else{cb(null)}
        },
        sendPush:(createPush,cb)=>{
            if(type == 'INVITE') {
                if (dTokens && dTokens.length) {
                    let data = {
                        type: 0,
                        title: 'Wethaq',
                        multi: true,
                        body: 'You are invited to attend “' + result.title + '”',
                        eventId: result._id,
                        timestamp: result.startAt,
                        images: {original: result.imageUrl, thumbnail: result.imageUrl},
                    };
                    NotificationManager.sendPush(data, dTokens, function (err, result) {
                        cb(null)
                    });
                    // console.log("1111",unRegistered,phoneNos,ids);
                } else
                {
                    cb(null)
                }
            }else{
                cb(null)
            }

        },
        createQr:(createPush,cb)=>{
            if(type == 'INVITE') {
                    if(result.isQrCode==true && result.invites && result.invites.length){
                        // console.log("payloadData qrCode",result.isQrCode);
                        let flag=0;
                        for(let i=0;i<result.invites.length;i++){
                            (function (i) {
                                let  pic = "qr-userId="+ result.invites[i].phoneNo+"eventId=" + result._id;
                                let qr_svg = qr.image(pic, {type: 'png'});
                                let date=new Date().getTime()
                                let stream = qr_svg.pipe(require('fs').createWriteStream('uploads/' +date+'eventId=' + result._id+'.png'));
                                stream.on('finish', function (err, result3) {
                                    let  qrImage = Path.resolve(".") + "/uploads/" +date+'eventId=' + result._id+'.png';
                                    let client = knox.createClient({
                                        key: Config.awsS3Config.s3BucketCredentials.accessKeyId
                                        , secret: Config.awsS3Config.s3BucketCredentials.secretAccessKey
                                        , bucket: Config.awsS3Config.s3BucketCredentials.bucket
                                    });
                                    let s3ClientOptions = {'x-amz-acl': 'public-read'};
                                    client.putFile(qrImage,date + result._id+'.png', s3ClientOptions, function (err, res) {
                                        console.log(err);
                                        deleteFile(qrImage);
                                        _.map(invitees, function(obj) {
                                            if (obj.phoneNo ==result.invites[i].phoneNo)
                                                obj.qr=Config.awsS3Config.s3BucketCredentials.s3URL +date + result._id+  '.png';
                                        });
                                        qrs.push({"qr":Config.awsS3Config.s3BucketCredentials.s3URL +date + result._id+  '.png',"userId":result.invites[i].phoneNo});
                                        flag++;
                                        if (flag == result.invites.length) {
                                            // console.log("qr",qrs);
                                            cb(null);
                                        }

                                    });
                                });
                            }(i))
                        }
                    }else{
                        cb(null)
                    }
            }else
                {
                cb(null)
            }

        },
        updateEvent:(createQr,createPush,sendPush,cb)=> {
            if(type == 'AUTOPUSH' && notified==true) {
                Service.scheduleNotifServices.updateScheduleNotifs({_id: result._id}, {isSent: true}, {new: true}, (err, result)=> {
                    if (err) {
                        cb(err)
                    } else {
                        cb(null)
                    }
                })
            }
            else if(type == 'INVITE' && invited==true) {
                let dataToset={
                    isSent:true
                }
                if(invitees && invitees.length){
                    dataToset.invites=invitees
                }
                if(qrs && qrs.length) {
                    dataToset.qrCodes=qrs;
                }
                Service.eventServices.updateEvent({_id: result._id},dataToset, {new: true}, function (err, result) {
                    if (err) {
                        cb(err)
                    } else {
                        cb(null)
                    }
                })
            }
            else if(type == 'REMINDER' && reminder==true) {
                let criteria = {
                    _id : mongoose.Types.ObjectId(result._id),
                    "reminder._id": mongoose.Types.ObjectId(result.reminder._id)
                };

                let dataToUpdate = {
                    "reminder.$.sent": true
                };
                Service.eventServices.updateEvent(criteria,dataToUpdate, {new: true}, function (err, result) {
                    if (err) {
                        cb(err)
                    } else {
                        cb(null)
                    }
                })
            }
            else{
                cb(null)
            }
        }

    }, function (err, result) {
        if (err)
            callback(err)
        else {
            callback(null)
        }

    })


}
function deleteFile(path) {
    fsExtra.remove(path, function (err) {
        console.log('error deleting file>>', err)
    });
}