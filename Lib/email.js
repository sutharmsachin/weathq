/**
 * Created by shumi on 16/1/17.
 */

var nodemailer = require('nodemailer');
var config = require('../Config');


exports.sendEmail = function(email, subject, content, cb) {
    var transporter = nodemailer.createTransport({
        service: 'Gmail',
        auth: {
            user: "",
            pass: ""
        },
        secure: true
    });
    transporter.sendMail({
        from: "biteappteam@gmail.com", // sender address
        to: email, // list of receivers
        subject: subject, // Subject line
        html: content
    }, function (err, data, res) {

        console.log("=====================email=====================",err,data,res);
        cb(null)
    });
}