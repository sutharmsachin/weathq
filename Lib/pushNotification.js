/**
 * Created by Shumi Gupta on 23/8/17.
 */
'use strict';


const FCM = require('fcm-node');
const serverKey = 'AAAAw-0_YKs:APA91bHCydGxRELflskXi5WSo2vPz1npl9aAbUR5A2gel2XjJHQ1Rz9DlpIAn0eqRTQEzqoDJeTpzrxdzNvUaMLcPfnGk3iEO9l0SS-5CbXMmGWgmKHywJAFecEBZmUNRcMU2ZAM_oCD';
const fcm = new FCM(serverKey);


const sendPush = function (data, deviceToken, callback) {

    let message={};
   // console.log("=================================",deviceToken);
    if(data.multi==true){
        let tokenToPushTo=900;
        let tokenLength=deviceToken.length;
        let count=Math.ceil(tokenLength/tokenToPushTo);
            for(let i=0; i< count ; i++){
                (function (i) {
                    let deviceTokens=deviceToken.splice(i*tokenToPushTo,tokenToPushTo);
                    let message = {
                        registration_ids: deviceTokens,
                        // collapse_key: 'demo',
                        notification: {
                            title: data.title,
                            body: data.body,
                            type: data.type,
                            sound: 'default',
                            // badge: data.unReadCount || 1
                        },
                        data: data,
                        priority: 'high'
                    };
                    fcm.send(message, function (err, result) {
                        console.log("Push", err,result);
                        if(i == count-1){
                            callback(null);
                        }
                    });
                }(i))
            }
        // message = {
        //      registration_ids: deviceToken,
        //     // collapse_key: 'demo',
        //     notification: {
        //         title: data.title,
        //         body: data.body,
        //         type: data.type,
        //         sound: 'default',
        //         // badge: data.unReadCount || 1
        //     },
        //     data: data,
        //     priority: 'high'
        // };
    }
    else{
        message = {
            to: deviceToken,
            // collapse_key: 'demo',
            notification: {
                title: data.title,
                body: data.body,
                type: data.type,
                sound: 'default',
                // badge: data.unReadCount || 1
            },
            data: data,
            priority: 'high'
        };
        console.log("message**************************", message);
        fcm.send(message, function (err, result) {
            if (err) {
                console.log("Something has gone wrong!", err);
                callback(null);
            } else {
                console.log("Successfully sent with response: ", result);
                callback(null, result);
            }
        });
    }

};
module.exports = {
    sendPush: sendPush
};