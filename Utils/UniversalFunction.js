/**
 * Created by Shumi Gupta on 29/3/17.
 */
const Boom = require('boom');
const CONFIG = require('../Config');
const Joi = require('joi');
const MD5 = require('md5');
const randomString = require("randomstring");

const sendSuccess = function (successMsg, data) {
    successMsg = successMsg || CONFIG.APP_CONSTANTS.STATUS_MSG.SUCCESS.DEFAULT.customMessage;
    if (typeof successMsg == 'object' && successMsg.hasOwnProperty('statusCode') && successMsg.hasOwnProperty('customMessage')) {
        return {statusCode:successMsg.statusCode, message: successMsg.customMessage, data: data || null};
    }else {
        return {statusCode:200, message: successMsg, data: data || null};
    }
};


const CryptData = function (stringToCrypt) {
    return MD5(MD5(stringToCrypt));
};

const sendError = function (data) {
    
  //  console.log(".....................log.................",data);
    if (typeof data == 'object' && data.hasOwnProperty('statusCode') && data.hasOwnProperty('customMessage')) {
        //console.log('attaching resposnetype',data.type)
        var errorToSend = Boom.create(data.statusCode, data.customMessage);
        errorToSend.output.payload.responseType = data.type;
        return errorToSend;
    } else {
        var errorToSend = '';
        if (typeof data == 'object') {
            if (data.name == 'MongoError') {
                errorToSend += CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.DB_ERROR.customMessage;
                if (data.code = 11000) {
                    var duplicateValue = data.errmsg && data.errmsg.substr(data.errmsg.lastIndexOf('{ : "') + 5);
                    duplicateValue = duplicateValue.replace('}','');
                    errorToSend += CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.DUPLICATE.customMessage + " : " + duplicateValue;
                    //console.log("==================errorToSend==================",data.message)
                    if (data.message.indexOf('email_1')>-1){
                        errorToSend = CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.DUPLICATE_EMAIL.customMessage;
                    }
                }
            } else if (data.name == 'ApplicationError') {
                errorToSend += CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.APP_ERROR.customMessage + ' : ';
            } else if (data.name == 'ValidationError') {
                errorToSend += CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.APP_ERROR.customMessage + data.message;
            } else if (data.name == 'CastError') {
                errorToSend += CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.DB_ERROR.customMessage + CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_ID.customMessage + data.value;
            }
        } else {
            errorToSend = data
        }
        var customErrorMessage = errorToSend;
        if (typeof customErrorMessage == 'string'){
            if (errorToSend.indexOf("[") > -1) {
                customErrorMessage = errorToSend.substr(errorToSend.indexOf("["));
            }
            customErrorMessage = customErrorMessage && customErrorMessage.replace(/"/g, '');
            customErrorMessage = customErrorMessage && customErrorMessage.replace('[', '');
            customErrorMessage = customErrorMessage && customErrorMessage.replace(']', '');
        }
        return Boom.create(400,customErrorMessage)
    }
};



const failActionFunction = function (request, reply, source, error) {

    console.log("..............err...........fail action.................",request.payload);
    var customErrorMessage = '';
    if (error.output.payload.message.indexOf("[") > -1) {
        customErrorMessage = error.output.payload.message.substr(error.output.payload.message.indexOf("["));
    } else {
        customErrorMessage = error.output.payload.message;
    }
    customErrorMessage = customErrorMessage.replace(/"/g, '');
    customErrorMessage = customErrorMessage.replace('[', '');
    customErrorMessage = customErrorMessage.replace(']', '');
    error.output.payload.message = customErrorMessage;
    delete error.output.payload.validation
    return reply(error);
};


const authorizationHeaderObj = Joi.object({
    authorization: Joi.string().required()
}).unknown();

let authorizationHeaderObjOptional = Joi.object({
    authorization: Joi.string().optional()
}).unknown();

const generateRandomString = function () {
    return randomString.generate(7);
};
const generateAlphaString = function () {
    return randomString.generate({
        length: 6,
        charset: 'alphabetic'
    });
};

const getFileNameWithUserId = function (thumbFlag, fullFileName, userId) {
    var prefix = CONFIG.APP_CONSTANTS.DATABASE.PROFILE_PIC_PREFIX.ORIGINAL;
    var ext = fullFileName && fullFileName.length > 0 && fullFileName.substr(fullFileName.lastIndexOf('.') || 0, fullFileName.length);
   // console.log("===============ext=================",ext)
    
    if (thumbFlag) {
        prefix = CONFIG.APP_CONSTANTS.DATABASE.PROFILE_PIC_PREFIX.THUMB;
    }
    return prefix + userId + ext;
};

var sendMail = function(email,subject,content,callback){
    var nodemailer = require('nodemailer');
    var sesTransport = require('nodemailer-ses-transport');
    var transporter = nodemailer.createTransport(sesTransport({
        accessKeyId:'AKIAJIMTWD3YMKOOBD3Q',
        secretAccessKey:'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',
        region:'us-west-2'
    }));

    transporter.sendMail({
        from: "Wethaq hello@wethaqapp.net",
        to: email,
        subject: subject,
        html: content
    }, function (err, data, res) {
        callback(null)
    });
};

module.exports = {
    failActionFunction:failActionFunction,
    sendSuccess:sendSuccess,
    sendError:sendError,
    authorizationHeaderObj:authorizationHeaderObj,
    CryptData:CryptData,
    generateRandomString:generateRandomString,
    getFileNameWithUserId:getFileNameWithUserId,
    generateAlphaString:generateAlphaString,
    sendMail:sendMail,
    authorizationHeaderObjOptional: authorizationHeaderObjOptional
}