/**
 * Created by Shumi Gupta on 30/3/17.
 */

const mongoose = require('mongoose');
const Config = require('../Config');
mongoose.Promise = global.Promise;
let Service = require('../services');
let async = require('async');
const TokenManager = require('../Lib/TokenManager');
const SocketManager = require('../Lib/SocketManager');
let path=Config.dbConfig.mongo.URI;
if(process.env.NODE_ENV == 'dev')path=Config.dbConfig.mongo.TEST_URI;
// mongoose.connect(Config.dbConfig.mongo.URI, function (err) {
mongoose.connect(path, function (err) {
    if (err) {
        console.log("DB Error: ", err);
        process.exit(1);
    } else {
        console.log('MongoDB Connected');
    }
});

exports.bootstrapAdmin = function (callback) {
    var adminData1 = {
        email: 'shumi.gupta@code-brew.com',
        password: '1e7eebb19ca71233686f26a43bbc18a9',
        name: 'shumi',
        superAdmin:true
    };
    var adminData2 = {
        email: 'shumigupta04@gmail.com',
        password: '1e7eebb19ca71233686f26a43bbc18a9',
        name: 'shumi brew',
        superAdmin:true
    };
    var adminData3 = {
        email: 'admin@wethaq.com',
        password: '1e7eebb19ca71233686f26a43bbc18a9',
        name: 'wethaq',
        superAdmin:true
    };
    var adminData4 = {
        email: 'wethaq@gmail.com',
        password: '1e7eebb19ca71233686f26a43bbc18a9',
        name: 'wethaq admin',
        superAdmin:true
    };
    
    async.parallel([
        function (cb) {
            insertData(adminData1.email, adminData1, cb)
        },
        function (cb) {
            insertData(adminData2.email, adminData2, cb)
        },
        function (cb) {
            insertData(adminData3.email, adminData3, cb)
        },
        function (cb) {
            insertData(adminData4.email, adminData4, cb)
        },
    ], function (err, done) {
        callback(err, 'Bootstrapping finished');
    })
};


exports.bootstrapInsertUser = function (callback) {
    var userData = {
        email:'guest@wethaq.com',
        name:'guest'
       // accessToken:"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjU5YzI1OGRhMTg0NzUwNTM1MjE1MmNhMyIsInR5cGUiOiJ1c2VyIiwiaWF0IjoxNTA1OTA4OTU0fQ.0Xu6gosSvHAcBxnn9Di9tsPFd2CIV40AUz6xL-q-VPq"
    };
    insertUser(userData)
};

function insertUser(adminData) {
    var needToCreate = true;
    var data1;

   // console.log("===================d==================",adminData);
    async.series([
        function (cb) {
        var criteria = {
            email:'guest@wethaq.com',
        };
        Service.userServices.getUsers(criteria, {}, {}, function (err, data) {
            if (data && data.length > 0) {
                needToCreate = false;
            }
            //console.log("===================datadata==================",data);

            cb(null)
        })
    },
        function (cb) {
        if (needToCreate) {
            Service.userServices.createUser(adminData, function (err, result) {
                data1=result;
               // console.log("=============data==============",data1);
                cb(null)
            })
        } else {
           // cb();
        }
    },
        function(cb){
                let tokenData = {
                    id: data1._id,
                    type :Config.APP_CONSTANTS.DATABASE.USER_TYPE.user
                };
                if (tokenData && tokenData.id){
                    TokenManager.setToken(tokenData, function (err, output) {
                        if (err) {
                            cb(err);
                        } else {
                          //  tokenToSend = output && output.accessToken || null;
                            cb(null);
                        }
                    });
                }

        }
    ], function (err, data) {
       // callback(err, 'Bootstrapping finished')
    })
}

exports.bootstrapAppVersion = function (callback) {
    var appVersion1 = {
        latestIOSVersion: 4,
        latestAndroidVersion: 1.5,
        criticalAndroidVersion:1.5 ,
        criticalIOSVersion: 4
    };
    
    async.parallel([
        function (cb) {
            insertVersionData(appVersion1, cb)
        }
    ], function (err, done) {
        callback(err, 'Bootstrapping finished For App Version');
    })
};

function insertVersionData(versionData, callback) {
    var needToCreate = true;
    async.series([
        function (cb) {
            var criteria = {
            };
            Service.appVersionServices.getAppVersion(criteria, {}, {}, function (err, data) {
                if (data && data.length > 0) {
                    needToCreate = false;
                }
                cb()
            })
        }, function (cb) {
            if (needToCreate) {
                Service.appVersionServices.createAppVersion(versionData, function (err, data) {
                    cb(err, data)
                })
            } else {
                cb();
            }
        }], function (err, data) {
        callback(err, 'Bootstrapping finished For Admin Data')
    })
}

function insertData(email, adminData, callback) {
    var needToCreate = true;
    async.series([function (cb) {
        var criteria = {
            email: email
        };
        Service.adminServices.getAdmin(criteria, {}, {}, function (err, data) {
            if (data && data.length > 0) {
                needToCreate = false;
            }
            cb()
        })
    }, function (cb) {
        if (needToCreate) {
            Service.adminServices.createAdmin(adminData, function (err, data) {
                cb(err, data)
            })
        } else {
            cb();
        }
    }], function (err, data) {
       // console.log('Bootstrapping finished for ' + email);
        callback(err, 'Bootstrapping finished')
    })
}

//Start Socket Server
exports.connectSocket = SocketManager.connectSocket;