/**
 * Created by cbl99 on 13/9/17.
 */


exports.privacyPolicy =  function(callback){


    let content =

        `
Privacy Policy
Version 1.0
Last Revised: October 4, 2017

Bite App Inc. (“Company”) is committed to protecting your privacy. We have prepared
this Privacy Policy to describe to you our practices regarding the Personal Data (as
defined below ) we collect from users of our website, located at
www.swipebites.com/privacy, and our mobile application (“Application”) (collectively,
the “Site”) and online services (“Services”).
1. Questions; Contacting Company; Reporting Violations. If you have any
questions or concerns or complaints about our Privacy Policy or our data collection or
processing practices, or if you want to report any security violations to us, please contact
us at the following address or phone number:
WeWork c/o Bite App Inc.
Attn: David Ko
1601 Market Street
Philadelphia, PA 19103
(484) 686-1919

2. User Consent. By submitting Personal Data through our Site or Services, you
agree to the terms of this Privacy Policy and you expressly consent to the collection, use
and disclosure of your Personal Data in accordance with this Privacy Policy.
3. A Note About Children. We do not intentionally gather Personal Data from
visitors who are under the age of 13. If a child under 13 submits Personal Data to
Company and we learn that the Personal Data is the information of a child under 13, we
will attempt to delete the information as soon as possible. If you believe that we might
have any Personal Data from a child under 13, please contact us at
team@swipebites.com.
4. A Note to Users Outside of the United States. If you are a non U.S. user of
the Site, by visiting the Site and providing us with data, you acknowledge and
agree that your Personal Data may be processed for the purposes identified in the
Privacy Policy. In addition, your Personal Data may be processed in the country
in which it was collected and in other countries, including the United States,
where laws regarding processing of Personal Data may be less stringent than the
laws in your country. By providing your data, you consent to such transfer.
5. Types of Data We Collect. “Personal Data” means data that allows someone
to identify or contact you, including, for example, your name, address, telephone
number, e-mail address, as well as any other non-public information about you that is
associated with or linked to any of the foregoing data. “Anonymous Data” means data
that is not associated with or linked to your Personal Data; Anonymous Data does not,
by itself, permit the identification of individual persons. We collect Personal Data and
Anonymous Data, as described below.

(a) Information You Provide to Us.
● We may collect Personal Data from you, such as your first and last
name, email, user name, and password when you create an account
to log in to our network (“Account”).
● If you use our Services on your mobile device, including through our
Application, we may collect your phone number and the unique
device id number.
● If you tell us where you are (e.g., by allowing your mobile device to
send us your location), we may store and use that information to
provide you with location-based information and advertising. If you
want to deactivate this feature, you can either reinstall the Application
or deactivate GPS on your mobile device.
● Our Site lets you store preferences like how your content is
displayed, your location, safe search settings, and bookmarked
posts. We may associate these choices with your ID, browser or the
mobile device, and you can edit these preferences at any time.
● When connecting to our Services via a service provider that uniquely
identifies your mobile device, we may receive this identification and
use it to offer extended services and/or functionality.
● Certain Services, such as two-factor authentication, may require our
collection of your phone number. We may associate that phone
number to your mobile device identification information.
● Our Services may allow you to place an order for pickup from one of
our participating restaurants. If you place an order for pickup through
our Services, we will collect all information necessary to complete the
transaction, including your name, credit or debit card information,
billing information and shipping information. We do not store this
information directly on our servers, but this information may be
shared with third parties who will help process and fulfill your
purchases.
● We retain information on your behalf, such as files and messages
that you store using your Account.
● If you provide us feedback or contact us via e-mail, we will collect
your name and e-mail address, as well as any other content included
in the e-mail, in order to send you a reply.
● When you post content (text, images, photographs, messages,
comments or any other kind of content that is not your e-mail
address) on our Site, the information contained in your posting will be
stored in our servers and other users will be able to see it, along with
your profile photo and any other information that you choose to make
public on your public profile page (“Profile”). The information that
you provide in your Profile will be visible to others, including
anonymous visitors to the Site.
● When you post messages on the message boards of our Site, the
information contained in your posting will be stored on our servers
and other users will be able to see it.
● When you participate in one of our surveys, we may collect additional
profile information.
● We also collect other types of Personal Data that you provide to us
voluntarily, such as your operating system and version, product

registration number, and other requested information if you contact
us via e-mail regarding support for the Services.
● If you participate in a sweepstakes, contest or giveaway on our Site,
we may ask you for your e-mail address and/or home number (to
notify you if you win or not). We may also ask for first and last
names, and sometimes post office addresses to verify your identity.
In some situations we may need additional information as a part of
the entry process, such as a prize selection choice. These
sweepstakes and contests are voluntary. We recommend that you
read the rules for each sweepstakes and contest that you enter.
● We may also collect Personal Data at other points in our Site that
state that Personal Data is being collected.

(b) Information You Provide to Facebook and other Social Networking
Services. The Services allow users to access Facebook to interact with friends and to
share on Facebook through Wall and friends’ News Feeds. If you are already logged
into the Services and Facebook or another Social Networking Service (“SNS”), when you
click on “Connect with Facebook,” or a similar connection on another SNS, you will be
prompted to merge your profiles. If you are already logged into the Services but not
logged into Facebook or another SNS that we support, when you click on “Connect with
Facebook,” or a similar connection on another SNS, you will be prompted to enter your
SNS credentials or to “Sign Up” for the SNS. By proceeding, you are allowing the
Services to access your information and you are agreeing to the Facebook or other
SNS’s Terms of Use in your use of the Services. Conversely, if you are not currently
registered as a user of the Services, and you click on “Sign in” using Facebook or
another SNS that we support, you will first be asked to enter your Facebook or SNS
credentials and then be given the option to register for the Services. In this case, we
may receive information from Facebook or another SNS to make it easier for you to
create an Account on the Site and show our relevant content from your Facebook or
SNS friends. Once you register on the Site and connect with Facebook or another SNS,
you will be able to automatically post recent activity back to Facebook or the other SNS.
Any information that we collect from your Facebook or other SNS account may depend
on the privacy settings you have with that SNS, so please consult the SNS’s privacy and
data practices. You have the option to disable Facebook Connect at any time by logging
into your Account through the Site and going to settings, “About Me,” “Linked Accounts,”
and then unselecting “Facebook.” Further, you can edit privacy settings for the reviews
that appear on Facebook or disconnect your Services activity stream by visiting the
Facebook Applications Settings page.
(c) Information Collected via Technology.
● Information Collected by Our Servers. To make our Site and
Services more useful to you, our servers (which may be hosted by a
third party service provider) collect information from you, including
your browser type, operating system, Internet Protocol (“IP”) address
(a number that is automatically assigned to your computer when you
use the Internet, which may vary from session to session), domain
name, and/or a date/time stamp for your visit.
● Log Files. As is true of most websites, we gather certain information
automatically and store it in log files. This information includes IP
addresses, browser type, Internet service provider (“ISP”),

referring/exit pages, operating system, date/time stamp, and
clickstream data. We use this information to analyze trends,
administer the Site, track users’ movements around the Site, gather
demographic information about our user base as a whole, and better
tailor our Services to our users’ needs. For example, some of the
information may be collected so that when you visit the Site or the
Services again, it will recognize you and the information could then
be used to serve advertisements and other information appropriate to
your interests. Except as noted in this Privacy Policy, we do not link
this automatically-collected data to Personal Data.
● Cookies. Like many online services, we use cookies to collect
information. “Cookies” are small pieces of information that a
website sends to your computer’s hard drive while you are viewing
the website. We may use both session Cookies (which expire once
you close your web browser) and persistent Cookies (which stay on
your computer until you delete them) to provide you with a more
personal and interactive experience on our Site. This type of
information is collected to make the Site more useful to you and to
tailor the experience with us to meet your special interests and
needs.
● Pixel Tags. In addition, we may use “Pixel Tags” (also referred to as
clear Gifs, Web beacons, or Web bugs). Pixel Tags are tiny graphic
images with a unique identifier, similar in function to Cookies, that are
used to track online movements of Web users. In contrast to
Cookies, which are stored on a user’s computer hard drive, Pixel
Tags are embedded invisibly in Web pages. Pixel Tags also allow us
to send e-mail messages in a format users can read, and they tell us
whether e-mails have been opened to ensure that we are sending
only messages that are of interest to our users. We may use this
information to reduce or eliminate messages sent to a user. We do
not tie the information gathered by Pixel Tags to our users’ Personal
Data.
● Collection of Data by Advertisers. We may also use third parties to
serve ads on the Site. Certain third parties may automatically collect
information about your visits to this and other websites, your IP
address, your ISP, the browser you use to visit our Site (but not your
name, address, e-mail address or telephone number). They do this
by using Cookies, Pixel Tags, or other technologies. Information
collected may be used, among other things, to deliver advertising
targeted to your interests and to better understand the usage and
visitation of our Site and the other sites tracked by these third parties.
This policy does not apply to, and we are not responsible for,
Cookies or Pixel Tags in third party ads, and we encourage you to
check the privacy policies of advertisers and/or ad services to learn
about their use of Cookies and other technologies. If you would like
more information about this practice and to know your
choices about not having this information used by these
companies, click here: http://www.aboutads.info/choices or
http://www.networkadvertising.org.

● How We Respond to Do Not Track Signals. We do not currently
respond to “do not track” signals or other mechanisms that
might enable users to opt out of tracking on our site.
● Flash LSOs. When we post videos, third parties may use local
shared objects, known as “Flash Cookies,” to store your preferences
for volume control or to personalize certain video features. Flash
Cookies are different from browser Cookies because of
the amount and type of data and how the data is stored.
Cookie management tools provided by your browser will not remove
Flash Cookies. To learn how to manage privacy and storage
settings for Flash Cookies, click here:
http://www.macromedia.
com/support/documentation/en/flashplayer/help/settings_manager07.
html.
● Mobile Services. We may also collect non-personal information from
your mobile device if you have downloaded our Application. This
information is generally used to help us deliver the most relevant
information to you. Examples of information that may be collected
and used include your geographic location, how you use the
Application, and information about the type of device you use. In
addition, in the event our Application crashes on your mobile device,
we will receive information about your mobile device model software
version and device carrier, which allows us to identify and fix bugs
and otherwise improve the performance of our Application. This
information is sent to us as aggregated information and is not
traceable to any individual and cannot be used to identify an
individual.
● Analytics Services and Interest-Based Advertising. In addition to the
tracking technologies we place, other companies may set their own
cookies or similar tools when you visit our Site. This includes third
party analytics services, including but not limited to Google Analytics
(“Analytics Services”), that we engage to help analyze how users
use the Site, as well as third parties that deliver content or offers. We
may receive reports based on these parties’ use of these tools on an
individual or aggregate basis. We use the information we get from
Analytics Services only to improve our Site and Services. The
information generated by the Cookies or other technologies about
your use of our Site and Services (the “Analytics Information”) is
transmitted to the Analytics Services. The Analytics Services use
Analytics Information to compile reports on user activity. The
Analytics Services may also transfer information to third parties
where required to do so by law, or where such third parties process
Analytics Information on their behalf. Each Analytics Services’ ability
to use and share Analytics Information is restricted by such Analytics
Services’ Terms of Use and Privacy Policy. By using our Site and
Services, you consent to the processing of data about you by
Analytics Services in the manner and for the purposes set out above.
For a full list of Analytics Services, please contact us at
team@swipebites.com. We may also partner with ad companies to
support our marketing efforts, including by serving you ads better
tailored to your likely interests. If you don’t want to take advantage of

these services, you may be able to opt-out by visiting
http://www.aboutads.info/ or http://networkadvertising.org/choices/ ,
or if you are located in the EU, http://www.youronlinechoices.eu/.

(d) Information Collected from You About Others. If you decide to invite a
third party to create an Account, we will collect your and the third party’s names and e-
mail addresses in order to send an e-mail and follow up with the third party. We rely
upon you to obtain whatever consents from the third party that may be required by law to
allow us to access and upload the third party’s names and e-mail addresses as required
above. You or the third party may contact us at team@swipebites.com to request the
removal of this information from our database.
(e) Information Collected from Third Party Companies. We may receive
Personal and/or Anonymous Data about you from companies that provide our Services
by way of companies that offer their products and/or services on our Site. These third
party companies may supply us with Personal Data. We may add this information to the
information we have already collected from you via our Site in order to improve the
Services we provide.
(f) Location Information.  If you have enabled location services on your
phone, we collect your location information to make a map available to the recipients of
your messages showing your location.  If you do not want this information collected by
us, you can disable location services on your phone.
6. Use of Your Personal Data
(a) General Use. In general, Personal Data you submit to us is used either
to respond to requests that you make, or to aid us in serving you better. We use your
Personal Data in the following ways:

● facilitate the creation of and secure your Account on our network;
● identify you as a user in our system;
● provide improved administration of our Site and Services;
● provide the Services you request;
● improve the quality of experience when you interact with our Site and
Services;
● send you a welcome e-mail to verify ownership of the e-mail address
provided when your Account was created;
● send you administrative e-mail notifications, such as security or
support and maintenance advisories;
● respond to your inquiries related to employment opportunities or other
requests;
● make telephone calls to you, from time to time, as a part of secondary
fraud protection or to solicit your feedback; and
● send newsletters, surveys, offers, and other promotional materials
related to our Services and for other marketing purposes of Company.
(b) Creation of Anonymous Data. We may create Anonymous Data
records from Personal Data by excluding information (such as your name) that makes
the data personally identifiable to you. We use this Anonymous Data to analyze request

and usage patterns so that we may enhance the content of our Services and improve
Site navigation. We reserve the right to use Anonymous Data and aggregated and other
de-identified information for any purpose and disclose Anonymous Data to third parties
in our sole discretion.
7. Disclosure of Your Personal Data. We disclose your Personal Data as
described below and as described elsewhere in this Privacy Policy.
(a) Third Parties Designated by You. When you use the Services, the
Personal Data you provide will be shared with the third parties that you designate to
receive such information, including other websites, your friends, relatives and business
associates. Depending on the type of access you grant to such third parties, they may
also be permitted to edit the information you have provided to us and to designate others
to access and edit such information. You may change your settings at any time as to
who has access to your information by going to your account settings and changing your
publishing options.
(b) Users. We will share your Personal Data with other users solely for the
purpose of providing the Services.
(c) Third Party Service Providers. We may share your Personal Data with
third party service providers to: provide you with the Services that we offer you through
our Site; to conduct quality assurance testing; to facilitate creation of accounts; to
provide technical support; and/or to provide other services to the Company. These third
party service providers are required not to use your Personal Data other than to provide
the services requested by Company.
(d) Affiliates. We may share some or all of your Personal Data with our
parent company, subsidiaries, joint ventures, or other companies under a common
control (“Affiliates”), in which case we will require our Affiliates to honor this Privacy
Policy.
(e) Corporate Restructuring. We may share some or all of your Personal
Data in connection with or during negotiation of any merger, financing, acquisition or
dissolution transaction or proceeding involving sale, transfer, divestiture, or disclosure of
all or a portion of our business or assets. In the event of an insolvency, bankruptcy, or
receivership, Personal Data may also be transferred as a business asset. If another
company acquires our company, business, or assets, that company will possess the
Personal Data collected by us and will assume the rights and obligations regarding your
Personal Data as described in this Privacy Policy.
(f) Social Networking Sites. Some of our Applications and Services may
enable you to post content to SNSs (e.g., Facebook or Instagram). If you choose to do
this, we will provide information to such SNSs in accordance with your elections. You
acknowledge and agree that you are solely responsible for your use of those websites
and that it is your responsibility to review the terms of use and privacy policy of the third
party provider of such SNSs. We will not be responsible or liable for: (i) the availability
or accuracy of such SNSs; (ii) the content, products or services on or availability of such
SNSs; or (iii) your use of any such SNSs.

(g) Disclosure to Third Party Companies. We may enter into agreements
with companies that desire access to Personal Information that we collect from you. AS
A RESULT, WE MAY PROVIDE YOUR PERSONAL INFORMATION TO SUCH THIRD
PARTY COMPANIES. BECAUSE WE DO NOT CONTROL THE PRIVACY
PRACTICES OF THESE THIRD PARTY COMPANIES, YOU SHOULD READ AND
UNDERSTAND THEIR PRIVACY POLICIES.
NOTICE TO CALIFORNIA RESIDENTS – YOUR CALIFORNIA PRIVACY
RIGHTS (AS PROVIDED BY CALIFORNIA CIVIL CODE SECTION 1798.83
If we share Personal Information to third parties for their own direct
marketing purposes, then you have the right to receive information about
the categories of entities to which we have disclosed such information and
the names and addresses of those entities. To submit your written request,
please send it to us at team@swipebites.com and put the following words
in the subject line of the email: &quot;California Privacy Request.&quot; Within 30
days after receipt of your request, we will send you a list of the categories
of Personal Information that we disclosed to third parties for their direct
marketing purposes during the immediately preceding calendar year, along
with the names and addresses such third parties, if any. Please note that
the California &quot;Shine the Light&quot; law does not cover all categories of
information that may be collected and disclosed or shared, and our policy
relates only to information covered by the law. Please note that we are
required to respond to one request per California user each year and we
are not required to respond to requests made by means other than through
this e-mail address.
(h) Public Profile. Certain portions of the information you provide to us may
also be displayed in your Profile. As an essential element of the Services, most of the
Personal Data you explicitly provide to us when you register or update your Profile is
displayed on your Profile. You are solely responsible for applying the appropriate level of
access to information you provide to be displayed in the public areas of the Site. If you
do not choose, the system may default to its most permissive setting. Your photos,
posts, friends, and other content you post to the Site are also meant for public
consumption. We may display this content on the Site and further distribute it to a wider
audience through third party sites and services. Once displayed on publicly viewable
web pages, that information can be collected and used by others. We cannot control
who reads your postings or what other users may do with the information that you
voluntarily post, so it is very important that you do not put Personal Data in your posts.
Once you have posted information publicly, while you will still be able to edit and delete it
on the Site, you will not be able to edit or delete such information cached, collected, and
stored elsewhere by others (e.g., search engines).
(i) Other Disclosures. Regardless of any choices you make regarding your
Personal Data (as described below), Company may disclose Personal Data if it believes
in good faith that such disclosure is necessary (a) in connection with any legal
investigation; (b) to comply with relevant laws or to respond to subpoenas or warrants
served on Company; (c) to protect or defend the rights or property of Company or users
of the Site or Services; and/or (d) to investigate or assist in preventing any violation or
potential violation of the law, this Privacy Policy, or our Terms of Use.

8. Third Party Websites. Our Site may contain links to third party websites. When
you click on a link to any other website or location, you will leave our Site and go to
another site, and another entity may collect Personal Data or Anonymous Data from you.
We have no control over, do not review, and cannot be responsible for, these outside
websites or their content. Please be aware that the terms of this Privacy Policy do not
apply to these outside websites or content, or to any collection of your Personal Data
after you click on links to such outside websites. We encourage you to read the privacy
policies of every website you visit. The links to third party websites or locations are for
your convenience and do not signify our endorsement of such third parties or their
products, content or websites.
9. Your Choices Regarding Information. You have several choices regarding the
use of information on our Service:
(a) Email Communications. We may periodically send you free newsletters
and e-mails that directly promote the use of our Site or Services. When you receive
newsletters or promotional communications from us, you may indicate a preference to
stop receiving further communications from us and you will have the opportunity to “opt-
out” by following the unsubscribe instructions provided in the e-mail you receive or by
contacting us directly (please see contact information below). Despite your indicated e-
mail preferences, we may send you service related communications, including notices of
any updates to our Terms of Use or Privacy Policy.
(b) Cookies. If you decide at any time that you no longer wish to accept
Cookies from our Service for any of the purposes described above, then you can instruct
your browser, by changing its settings, to stop accepting Cookies or to prompt you
before accepting a Cookie from the websites you visit. Consult your browser’s technical
information. If you do not accept Cookies, however, you may not be able to use all
portions of the Service or all functionality of the Service. If you have any questions about
how to disable or modify Cookies, please let us know at the contact information provided
below.
(c) De-Linking SNS. If you decide at any time that you no longer wish to
have your SNS account (e.g., Facebook) linked to your Account, then you may de-link
the SNS account in the “preferences” section in your account settings. You may also
manage the sharing of certain Personal Data with us when you connect with us through
an SNS, such as through Facebook Connect. Please refer to the privacy settings of the
SNS to determine how you may adjust our permissions and manage the interactivity
between the Services and your social media account or mobile device.
(d) Changing or Deleting Your Personal Data. All users may review,
update, correct or delete the Personal Data in their user accounts (including any
imported contacts) by contacting us or editing or deactivating their profile via the
Services in “Settings”. If you deactivate your profile in “Settings”, we will deactivate your
account after thirty (30) days of consecutive non-use. Additionally, If you completely
delete all of your Personal Data, then your user account may become deactivated. We
will use commercially reasonable efforts to honor your requests. However, we may
retain an archived copy of your records as required by law or for legitimate business
purposes.

(e) Applications. You can stop all collection of information by the
Application by uninstalling the Application. You may use the standard uninstall
processes as may be available as part of your mobile device or via the mobile
application marketplace or network. To serve ads in our Application where Cookie
technology is not available, the Company hashes users’ device ID. Users may choose
to reset or opt out of anonymous IDs at any time. If users choose to opt out, ads
delivered to the Application by ad serving technology will not be served on an
anonymous ID. You may at any time opt-out from further allowing us to have access to
your location data by changing the settings on your mobile device.
10. Security and Credit Card Information. The security of your personal
information is important to us. When you enter sensitive information (such as credit or
debit card number) on our order forms, we encrypt the transmission of that information
using secure socket layer technology (SSL). We may use third party payment
processors, such as Stripe, when you pay with your credit or debit card using our
Services. For more information on how payments are handled, or to understand the data
privacy and security afforded to such information, please visit Stripe’s privacy terms at
https://stripe.com/us/privacy. We follow generally accepted industry standards to protect
the personal information submitted to us, both during transmission and once we receive
it. No method of transmission over the Internet, or method of electronic storage, is 100%
secure, however. Therefore, we cannot guarantee its absolute security. If you have any
questions about security on our Web site, you can contact us at team@swipebites.com.
11. Changes to This Privacy Policy. This Privacy Policy is subject to occasional
revision, and if we make any material changes in the way we use your Personal Data,
we will notify you by sending you an e-mail to the last e-mail address you provided to us
and/or by prominently posting notice of the changes on our Site. Any changes to this
Privacy Policy will be effective upon the earlier of thirty (30) calendar days following our
dispatch of an e-mail notice to you or thirty (30) calendar days following our posting of
notice of the changes on our Site. These changes will be effective immediately for new
users of our Service. Please note that at all times you are responsible for updating your
Personal Data to provide us with your most current e-mail address. In the event that the
last e-mail address that you have provided us is not valid, or for any reason is not
capable of delivering to you the notice described above, our dispatch of the e-mail
containing such notice will nonetheless constitute effective notice of the changes
described in the notice. If you do not wish to permit changes in our use of your Personal
Data, you must notify us prior to the effective date of the changes that you wish to
deactivate your Account with us. Continued use of our Site or Service, following notice
of such changes shall indicate your acknowledgement of such changes and agreement
to be bound by the terms and conditions of such changes.

`

callback(null,content)

};