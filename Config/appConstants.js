/**
 * Created by Shumi Gupta on 29/3/17.
 */

const SERVER = {
    APP_NAME: 'Wethaq App',
    PORTS: {
        HAPI: 8001,
        HAPI_CLIENT: 8000,
        HAPI_TEST: 8002
    },
    JWT_SECRET_KEY: 'MaEHqzXzdWrCS6TS',
    MAX_DISTANCE_RADIUS_TO_SEARCH : 1000000000,
    THUMB_WIDTH : 50,
    paymentCount : 100,
    THUMB_HEIGHT : 50
};


const DATABASE = {
    PROMO_CODE:{
        PERCENTAGE:"Percentage",
        AMOUNT:"Amount"
    },
    PACKAGES:{
        PREMIUM: "PREMIUM",
        FREE: "FREE",
        WHATSAPP: "WHATSAPP"
    },
    PAYMENT_OPTIONS:{
        EVENT:"Event",
        CUSTOM:"Custom",
        REQUEST:"Request"
    },
    PROFILE_PIC_PREFIX : {
        ORIGINAL : 'profilePic_',
        THUMB : 'profileThumb_'
    },
    MSG_TYPES: {
        TEXT: 'Text',
        ACCEPT: 'Accept',
        REJECT: 'Reject',
        OFFER: 'Offer',
        INFO: 'Info',
        TEMPLATE: 'Template',
        IMAGE: 'Image'
    },
    DEVICE_TYPES: {
        IOS: 'Ios',
        ANDROID: 'Android'
    },
    LANGUAGE: {
        ENGLISH: 'en',
        ARAB: 'ar'
    },
    USER_TYPE:{
        user:"user",
        admin:"admin",
        restaurant:"restaurant"
    },
    GUEST_STATUS:{
        usingGuest:"usingGuest",
        onlyGuest:"onlyGuest",
        notUsingGuest:"notUsingGuest"
    },
    SEARCH_TYPE:{
        RESTAURANT:"RESTAURANT",
        DISH:"DISH",
        USER:"USER"
    },
    LOGIN_TYPE:{
        GOOGLE:"Google",
        FACEBOOK:"Facebook",
        PHONE_NO:"PhoneNo"
    },
    GENDER_TYPE:{
        MALE:"Male",
        FEMALE:"Female"
    },
    INIVTE_STATUS:{
        PENDING:"Pending",
        CANCELLED:"Cancelled",
        REJECTED:"Rejected",
        ACCEPTED:"Accepted",
        AWAITED:"Awaited",
        CHECKED:"Checked",
    },
    REQUEST_STATUS:{
        ACTIVE:"Active",
        COMPLETED:"Completed",
        REJECTED:"Rejected",
        ACCEPTED:"Accepted",
        PENDING:"Pending",
    },
    NOTIFICATION_TYPE:{
        INVITE:"Invite",
        NOTIFICATION:"Notification"
    },
    EVENT_TYPE:{
        "EVENT":"EVENT",
        "REQUEST_EVENT":"REQUEST_EVENT"
    }
};



const STATUS_MSG = {
    ERROR: {
        INVALID_KEYS:{
            statusCode:400,
            customMessage : 'Please enter valid Excel/Csv file',
            type : 'INVALID_KEYS'
        },
        COUNT_CANNOT_BE_ZERO:{
            statusCode:400,
            customMessage : 'Your qr code has been expired',
            type : 'COUNT_CANNOT_BE_ZERO'
        },
        CATEGORY_DELETED:{
            statusCode:400,
            customMessage : 'Category of the selected event has been deleted',
            type : 'CATEGORY_DELETED'
        },
        CATEGORY_DELETED_AR:{
            statusCode:400,
            customMessage : 'تم حذف فئة الحدث المحدد',
            type : 'CATEGORY_DELETED'
        },
        
        EMPTY_FILE:{
            statusCode:400,
            customMessage : 'You entered empty file',
            type : 'EMPTY_FILE'
        },
        USER_NOT_FOUND:{
            statusCode:404,
            customMessage : 'User is not registered with us',
            type : 'USER_NOT_FOUND'
        },
        SOCIAL_FOUND:{
            statusCode:400,
            customMessage : 'This number is associated with social account so you can not forgot password',
            type : 'SOCIAL_FOUND'
        },
        USER_NOT_FOUND_AR:{
            statusCode:404,
            customMessage : 'المستخدم غير مسجل لدينا',
            type : 'USER_NOT_FOUND_AR'
        },
        COUPON_NOT_FOUND:{
            statusCode:404,
            customMessage : 'Coupon not found',
            type : 'COUPON_NOT_FOUND'
        },
        COUPON_NOT_ACTIVE:{
            statusCode:400,
            customMessage : 'Coupon not active',
            type : 'COUPON_NOT_ACTIVE'
        },
        COUPON_EXPIRED:{
            statusCode:400,
            customMessage : 'Coupon already expired',
            type : 'COUPON_EXPIRED'
        },
        COUPON_USER_LIMIT:{
            statusCode:400,
            customMessage : 'Coupon used up by others',
            type : 'COUPON_USER_LIMIT'
        },
        COUPON_USED:{
            statusCode:400,
            customMessage : 'Coupon already used',
            type : 'COUPON_USED'
        },
        DB_ERROR: {
            statusCode:400,
            customMessage : 'DB Error : ',
            type : 'DB_ERROR'
        },
        INSUFFICIENT_BALANCE: {
            statusCode:406,
            customMessage : 'Insufficient balance in the wallet',
            type : 'INSUFFICIENT_BALANCE'
        },
        DUPLICATE_EMAIL: {
            statusCode:400,
            customMessage : 'Email already exists',
            type : 'DUPLICATE_EMAIL'
        },
        DUPLICATE_EMAIL_AR: {
            statusCode:400,
            customMessage : 'البريد الالكتروني موجود',
            type : 'DUPLICATE_EMAIL_AR'
        },
        DUPLICATE_SOCIAL: {
            statusCode:400,
            customMessage : 'Social account already exists',
            type : 'DUPLICATE_SOCIAL'
        },
        DUPLICATE_SOCIAL_AR: {
            statusCode:400,
            customMessage : 'الحساب الاجتماعي موجود بالفعل',
            type : 'DUPLICATE_SOCIAL_AR'
        },
        DUPLICATE: {
            statusCode:400,
            customMessage : 'already exists',
            type : 'DUPLICATE'
        },
        DUPLICATE_PHONE_NO: {
            statusCode:400,
            customMessage : 'Phone number already exists',
            type : 'DUPLICATE_PHONE_NO'
        },
        DUPLICATE_PHONE_NO_AR: {
            statusCode:400,
            customMessage : 'رقم الهاتف موجود',
            type : 'DUPLICATE_PHONE_NO_AR'
        },
        DUPLICATE_CARD: {
            statusCode:400,
            customMessage : 'This card has already been taken',
            type : 'DUPLICATE_CARD'
        },
        DUPLICATE_CATEGORY: {
            statusCode:400,
            customMessage : 'Category already exists',
            customMessageAr : 'الفئة موجودة',
            type : 'DUPLICATE_CATEGORY'
        },
        DUPLICATE_CATEGORY_AR: {
            statusCode:400,
            customMessage : 'الفئة موجودة',
            type : 'DUPLICATE_CATEGORY_AR'
        },
        DUPLICATE_PACKAGE: {
            statusCode:400,
            customMessage : 'Package already exists',
            customMessageAr : 'الحزمة موجودة',
            type : 'DUPLICATE_PACKAGE'
        },
        DUPLICATE_PACKAGE_AR: {
            statusCode:400,
            customMessage : 'الحزمة موجودة',
            type : 'DUPLICATE_PACKAGE_AR'
        },
        EVENT_NOT_FOUND: {
            statusCode:400,
            customMessage : 'Event Does not exists',
            type : 'EVENT_NOT_FOUND'
        },
        EVENT_NOT_FOUND_AR: {
            statusCode:400,
            customMessage : 'الحدث غير موجود',
            type : 'EVENT_NOT_FOUND_AR'
        },
        DUPLICATE_EVENT_AR: {
            statusCode:400,
            customMessage : 'المناسبة موجودة',
            type : 'DUPLICATE_EVENT_AR'
        },
        ACTION_ALREADY_DONE: {
            statusCode:400,
            customMessage : 'Invite action already done',
            type : 'ACTION_ALREADY_DONE'
        },
        INVITE_TIME_PASSED: {
            statusCode:400,
            customMessage : 'Please update invite time,it must be in future.',
            type : 'INVITE_TIME_PASSED'
        },
        INVITE_TIME_PASSED_AR: {
            statusCode:400,
            customMessage : 'يرجى تحديث وقت الدعوة.',
            type : 'INVITE_TIME_PASSED_AR'
        },
        ACTION_DONE: {
            statusCode:400,
            customMessage : 'Action already done',
            type : 'ACTION_DONE'
        },
        ACTION_ALREADY_DONE_AR: {
            statusCode:400,
            customMessage : 'الدعوة موجودة سابقا',
            type : 'ACTION_ALREADY_DONE_AR'
        },
        CHECK_IN_ALREADY_DONE: {
            statusCode:400,
            customMessage : 'This User is already checked in this event',
            type : 'CHECK_IN_ALREADY_DONE'
        },
        WRONG_EVENT: {
            statusCode:400,
            customMessage : 'You are not assigned to this event',
            type : 'WRONG_EVENT'
        },
        CHECK_IN_ALREADY_DONE_AR: {
            statusCode:400,
            customMessage : 'هذا المستخدم محدد في هذا المناسبة',
            type : 'CHECK_IN_ALREADY_DONE_AR'
        },
        GUARD_ALREADY_ASSIGNED: {
            statusCode:400,
            customMessage : 'You have already assigned a guard for this event.',
            type : 'GUARD_ALREADY_ASSIGNED_AR'
        },
        GUARD_ALREADY_ASSIGNED_AR: {
            statusCode:400,
            customMessage : 'لقد عينت حارسًا لهذه المناسبة.',
            type : 'GUARD_ALREADY_ASSIGNED_AR'
        },
        USER_ALREADY_EXIST: {
            statusCode:400,
            customMessage : 'You can not assign this number as it already exists as user',
            type : 'USER_ALREADY_EXIST'
        },
        USER_ALREADY_EXIST_AR: {
            statusCode:400,
            customMessage : 'لا يمكنك تعيين هذا الرقم لأنه موجود من قبل كمستخدم',
            type : 'USER_ALREADY_EXIST_AR'
        },
        DUPLICATE_TEMPLATE: {
            statusCode:400,
            customMessage : 'Template already exists',
            type : 'DUPLICATE_TEMPLATE'
        },
        DUPLICATE_TEMPLATE_AR: {
            statusCode:400,
            customMessage : 'القالب موجود',
            type : 'DUPLICATE_TEMPLATE_AR'
        },
        DUPLICATE_PROMO: {
            statusCode:400,
            customMessage : 'Promo code already exists',
            type : 'DUPLICATE_PROMO'
        },
        DUPLICATE_ADMIN: {
            statusCode:400,
            customMessage : 'Sub admin already exists',
            type : 'DUPLICATE_ADMIN'
        },
        INVALID_TOKEN: {
            statusCode:401,
            customMessage : 'Your session have been expired. Please login again!',
            type : 'INVALID_TOKEN'
        },
        INVALID_NEW_PASSWORD: {
            statusCode:400,
            customMessage : 'Please provide the valid new password',
            type : 'INVALID_NEW_PASSWORD'
        },
        INVALID_NEW_PASSWORD_AR: {
            statusCode:400,
            customMessage : 'يرجى ادخا كلمة المرور الجديدة',
            type : 'INVALID_NEW_PASSWORD_AR'
        },
        APP_ERROR: {
            statusCode:400,
            customMessage : 'Application Error',
            type : 'APP_ERROR'
        },
        INVALID_ID: {
            statusCode:400,
            customMessage : 'Invalid value Provided : ',
            type : 'INVALID_ID'
        },
        INVALID_PASSWORD: {
            statusCode:400,
            customMessage : 'Incorrect password ',
            type : 'INVALID_PASSWORD'
        },
        INVALID_PASSWORD_AR: {
            statusCode:400,
            customMessage : 'كلمة السر خاطئة',
            type : 'INVALID_PASSWORD_AR'
        },
        USER_BLOCKED: {
            statusCode:402,
            customMessage : 'Your account have been blocked by the admin',
            type : 'USER_BLOCKED'
        },
        USER_BLOCKED_AR: {
            statusCode:402,
            customMessage : 'تم حظر حسابك من قبل المسؤول',
            type : 'USER_BLOCKED_AR'
        },
        INVALID_USER_PASS: {
            statusCode:400,
            customMessage : 'Invalid Credentials  Provided : ',
            type : 'INVALID_USER_PASS'
        },
        INVALID_USER_PASS_AR: {
            statusCode:400,
            customMessage : 'بيانات الاعتماد غير صالحة ',
            type : 'INVALID_USER_PASS_AR'
        },
        IMP_ERROR: {
            statusCode:500,
            customMessage : 'Implementation error',
            type : 'IMP_ERROR'
        },
},
    SUCCESS : {
        CREATED: {
            statusCode:200,
            customMessage : 'Created Successfully',
            type : 'CREATED'
        },
        DEFAULT: {
            statusCode:200,
            customMessage : 'Success',
            type : 'DEFAULT'
        },
        UPDATED: {
            statusCode:200,
            customMessage : 'Updated Successfully',
            type : 'UPDATED'
        },
        LOGOUT: {
            statusCode:200,
            customMessage : 'Logged Out Successfully',
            type : 'LOGOUT'
        },
        DELETED: {
            statusCode:200,
            customMessage : 'Deleted Successfully',
            type : 'DELETED'
        },
        REGISTER: {
            statusCode:200,
            customMessage : 'Register Successfully',
            type : 'REGISTER'
        }
    }
};

const swaggerDefaultResponseMessages = {
    '200': {'description': 'Success'},
    '400': {'description': 'Bad Request'},
    '401': {'description': 'Unauthorized'},
    '404': {'description': 'Data Not Found'},
    '500': {'description': 'Internal Server Error'}
};



var APP_CONSTANTS = {
    SERVER: SERVER,
    DATABASE: DATABASE,
    STATUS_MSG: STATUS_MSG,
    swaggerDefaultResponseMessages: swaggerDefaultResponseMessages
};

module.exports = APP_CONSTANTS;