/**
 * Created by Shumi Gupta on 30/3/17.
 */
module.exports  = {
    userController:require('./user'),
    adminController:require('./admin')
};