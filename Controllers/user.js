/**
 * Created by Shumi Gupta on 30/3/17.
 */
"use strict";
const Service = require('../services');
const UniversalFunctions = require('../Utils/UniversalFunction');
const async = require('async');
const Config = require('../Config');
const TokenManager = require('../Lib/TokenManager');
const pushNotification = require('../Lib/pushNotification');
const _ = require('lodash');
const mongoose = require('mongoose');
const UploadManager = require('../Lib/UploadManager');
// const moment = require('moment');
const path = require('path');
const moment = require('moment-timezone');
const emailFunction = require('../Lib/email');
const SocketManager = require('../Lib/SocketManager');
const messageManager = require('../Lib/messageManager');
const qr = require('qr-image');
const Path = require('path');
const fsExtra = require('fs-extra');
const knox = require('knox');
const NotificationManager = require('../Lib/pushNotification');
const mongoXlsx = require('mongo-xlsx');
const offerDone = SocketManager.offerDone;
const TinyURL = require('tinyurl');
const fs = require('fs');
const pdf = require('html-pdf');
const handlebars = require('handlebars');

// const GoogleUrl = require( 'google-url' );
// const googleUrl = new GoogleUrl({ key: 'AIzaSyC-7hgMKChd9_9DmbNUP3LMDMacfTx6OXg' });

/*Service.eventServices.eventAggregate([
    {$match:{isDeleted:false}},
    {$group:{_id:"$createdBy",count:{$sum:1}}}
],(err,res)=>{
    for(let i=0;i<res.length;i++) {
        Service.userServices.updateUser({_id:res[i]._id}, {eventsCreated:res[i].count},{new:true},(err,res)=>{
          console.log("nopesss")


        })
    }
})*/
//sign up api for users with phone no password
const userSignUp = (payloadData,callback)=> {

    let data;
    let tokenToSend;
    let profilePicture={};
    let socialLogin=false;
    let events=[];
    let phoneNo=payloadData.phoneNo
    let finalVals=[];
    finalVals.push(payloadData.phoneNo)
    if(phoneNo.indexOf('+')!=0){
        let withPlus="+"+phoneNo;
        finalVals.push(withPlus);
    }
    if(phoneNo.indexOf('+')==0) {
        let withoutPlus = phoneNo.split('');
        withoutPlus.splice(0, 1);
        withoutPlus= withoutPlus.join('');
        finalVals.push(withoutPlus);
    }
    console.log("payload",payloadData)
async.autoInject({

    //function to check if same phone No already exist
    checkUserPhoneNo:(cb)=>{
        let query = {
            phoneNo:payloadData.phoneNo
        };
        checkAlreadyRegisteredUser(query,(err,result)=>{
            if(err){
                cb(err)
            }
            else{
                if(result==true){
                    if(payloadData.language=='en'){
                       cb(Config.APP_CONSTANTS.STATUS_MSG.ERROR.DUPLICATE_PHONE_NO)
                    }else if(payloadData.language=='ar'){
                      cb(Config.APP_CONSTANTS.STATUS_MSG.ERROR.DUPLICATE_PHONE_NO_AR)
                    }else{
                        cb(Config.APP_CONSTANTS.STATUS_MSG.ERROR.DUPLICATE_PHONE_NO)
                    }

                }
                else{
                    cb(null);
                }
            }
        })
    },

    //function to check if same email already exist
    checkUserEmail:(checkUserPhoneNo,cb)=>{

        if(payloadData.email){
            let query = {
                email:payloadData.email
            };
            checkAlreadyRegisteredUser(query,(err,result)=>{
                if(err){
                    cb(err)
                }
                else{
                    if(result==true){
                        if(payloadData.language=='en'){
                            cb(Config.APP_CONSTANTS.STATUS_MSG.ERROR.DUPLICATE_EMAIL)
                        }else if(payloadData.language=='ar'){
                            cb(Config.APP_CONSTANTS.STATUS_MSG.ERROR.DUPLICATE_EMAIL_AR)
                        }else{
                            cb(Config.APP_CONSTANTS.STATUS_MSG.ERROR.DUPLICATE_EMAIL)
                        }
                    }
                    else{
                        cb(null);
                    }
                }
            })
        }
        else{
            cb(null)
        }

    },

    //after both phone no and email are checked we create user's profile only if both email and phone no does not exist
    signUpUser:(checkUserPhoneNo,checkUserEmail,cb)=>{
        let query = {
            phoneNo:payloadData.phoneNo,
            name:payloadData.name
        };

        if(profilePicture){
            query.profilePicture=profilePicture
        }

        if(payloadData.email){
            query.email=payloadData.email
        }

        if(payloadData.gender){
            query.gender=payloadData.gender;
        }

        if(payloadData.dob){
            query.dob=changeDate(payloadData.dob);
        }
        if(payloadData.deviceToken){
            query.deviceToken=payloadData.deviceToken;
        }
        if(payloadData.appVersion){
            query.appVersion=payloadData.appVersion;
        }

        if(payloadData.loginType===Config.APP_CONSTANTS.DATABASE.LOGIN_TYPE.PHONE_NO){
            query.loginType=Config.APP_CONSTANTS.DATABASE.LOGIN_TYPE.PHONE_NO
        }
        else{
            query.loginType=payloadData.loginType;
            query.socialId=payloadData.socialId;
            query.profilePicture = payloadData.socialImage
        }
        if(payloadData.password){
            query.password=UniversalFunctions.CryptData(payloadData.password)
        }
        if(payloadData.guard==true){
            query.isGuard=true
        }
        Service.userServices.createUser(query,(err,result) =>{
            if(err){
                    callback(err)
            }
            else{
                data=result;
                cb(null);
            }
        })
    },

       getEvents:(signUpUser,cb)=>{
        let query={
            unRegistered:{$in:finalVals},
            isDeleted:false,
            isBlocked:false,
            startAt:{$gt:new Date().getTime()}
        };
        let projection={
            createdBy:1,
            imageUrl:1,
            title:1,
            message:1,
            startAt:1,
            categoryId:1,
            templateId:1,
            invites:{$elemMatch:{phoneNo:{$in:finalVals}}}
        };
        Service.eventServices.getEvent(query,projection,{},(err,result)=>{
            if(err){
                cb(err)
            }else {
                if(result && result.length) {
                    events = result;
                    cb(null)
                }else{
                    cb(null)
                }
            }

        })

    },
    createNotification:(getEvents,cb)=>{
        if(events && events.length){

            let flag=0;
            for(let i=0;i<events.length;i++){
                (function (i) {
                    console.log("payloadData qrCode",events[i]);
                    if(events[i].invites[0].status=="Pending") {
                        let obj= {
                            createdFor: [data._id],
                            createdBy: [events[i].createdBy],
                            type: 0,
                            text: [events[i].title],
                            eventId: events[i]._id,
                            timestamp:events[i].startAt,
                            categoryId: events[i].categoryId,
                            templateId: events[i].templateId,
                            notificationType: 'Invite',
                            images: [{original: events[i].imageUrl, thumbnail: events[i].imageUrl}]
                        };
                        Service.notificationServices.createNotification(obj, (err, result) => {
                            flag++;
                            if (flag == events.length) {
                                cb(null);
                            }
                        });
                    }else if(events[i].invites[0].status=="Accepted"){
                        Service.eventServices.updateEvent({_id :events[i]._id}, {$addToSet: {attendees: data._id}}, {}, (err, result) => {
                            flag++;
                            if (flag == events.length) {
                                cb(null);
                            }
                        });
                    }else{
                        flag++
                        if (flag == events.length) {
                            cb(null);
                        }
                    }
                }(i))
            }
        }else{
            cb(null)
        }
    },
    messageByAdmin:(signUpUser,cb)=>{
        if(payloadData.byAdmin=='true') {
            let criteria = {
                to: [payloadData.phoneNo],
                body: "Your Password for Wethaq is " + payloadData.password
            };
            messageManager.sendSmsUnisoft(criteria, function (result) {
                cb(null)
            })
        }else{
            cb(null)
        }
    },
    //this function is used to create the access token of the user
    accessToken:(signUpUser,cb)=>{
        if(!payloadData.byAdmin) {
            let tokenData = {
                id: data._id,
                type: Config.APP_CONSTANTS.DATABASE.USER_TYPE.user
            };
            if (tokenData && tokenData.id) {
                TokenManager.setToken(tokenData, (err, output) => {
                    if (err) {
                        cb(err);
                    } else {
                        data.tokenToSend = output && output.accessToken || null;
                        cb(null);
                    }
                });
            }
        }else{
            cb(null)
        }
    },

    createAdminNotification:(signUpUser,cb)=>{
        if(!payloadData.byAdmin) {
            let obj={
                title:"New user registered",
                userId:data._id,
                body:payloadData.name+" just signed up for wethaq."
            };
                Service.adminNotifServices.createAdminNotifs(obj, (err, result) => {
                    if (err) {
                        cb(err);
                    } else {
                        cb(null);
                    }
                });

        }else{
            cb(null)
        }
    }
},(err,result)=>{

    if(err){
        callback(err)
    }
    else{
        let dataToSend = getData(data);
        callback(null,dataToSend);
    }
});
};

//This is a common function for checking user's data if not deleted by admin
const checkAlreadyRegisteredUser = (criertria,callback)=>{

    let query = criertria;
    query.isDeleted = false;
    let dataToGet={
    _id:1
    };
    Service.userServices.getUsers(query,dataToGet,{},(err,result)=>{
        if(err){
            callback(err)
        }
        else{
            if(result.length){
                callback(null,true)
            }
            else{
                callback(null,false)
            }
        }
    })

};

//This is login api for normal users
const login = (payloadData,callback)=>{
    console.log("login",payloadData)
    let data;

    async.autoInject({
        //In this function we check if user is blocked,entered invalid password or not found in our database
        loginUser:(cb)=>{
            let query = {
                isDeleted:false
            };
            if(payloadData.type===1){
                query.phoneNo = payloadData.loginId
            }
            else{
               query.email = payloadData.loginId
            }

            Service.userServices.getUsers(query,{},{lean:true},(err,result) =>{
                if(err){
                    cb(err);
                }
                else{
                    console.log("1111",result)
                    if(result.length){
                        if(result[0].isBlocked==true){
                            if(payloadData.language=='en'){
                                callback(Config.APP_CONSTANTS.STATUS_MSG.ERROR.USER_BLOCKED)
                            } else if(payloadData.language=='ar'){
                                callback(Config.APP_CONSTANTS.STATUS_MSG.ERROR.USER_BLOCKED_AR)
                            }else{
                                callback(Config.APP_CONSTANTS.STATUS_MSG.ERROR.USER_BLOCKED)
                            }
                        }
                        else{
                            if(UniversalFunctions.CryptData(payloadData.password) === result[0].password){
                                data=result[0];
                                cb(null);
                            }
                            else{
                                if(payloadData.language=='en'){
                                    callback(Config.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_PASSWORD)
                                }else if(payloadData.language=='ar'){
                                    callback(Config.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_PASSWORD_AR)
                                }else{
                                    callback(Config.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_PASSWORD)
                                }
                            }
                        }
                    }
                    else{
                        if(payloadData.language=='en'){
                            callback(Config.APP_CONSTANTS.STATUS_MSG.ERROR.USER_NOT_FOUND)
                        }else if(payloadData.language=='ar'){
                            callback(Config.APP_CONSTANTS.STATUS_MSG.ERROR.USER_NOT_FOUND_AR)
                        }else{
                            callback(Config.APP_CONSTANTS.STATUS_MSG.ERROR.USER_NOT_FOUND)
                        }
                    }
                }
            })
        },
        updateData:(loginUser,cb)=>{
            let query={
                _id:data._id
            };
            let dataToSet={};
            if(payloadData.deviceToken){
                dataToSet.deviceToken=payloadData.deviceToken;
            }
            if(payloadData.appVersion){
                dataToSet.appVersion=payloadData.appVersion;
            }
            Service.userServices.updateUser(query,dataToSet,{new:true},(err,result)=>{
                if(err)
                    cb(err);
                else{
                    cb(null)
                }
            })
        },
        //We create access token for login only after above function's checks are verified
        accessToken:(loginUser,cb)=>{
            let tokenData = {
                id: data._id,
                type : Config.APP_CONSTANTS.DATABASE.USER_TYPE.user
            };
            if (tokenData && tokenData.id){
                TokenManager.setToken(tokenData, (err, output) =>{
                    if (err) {
                        cb(err);
                    } else {
                        data.tokenToSend = output && output.accessToken || null;
                        cb(null);
                    }
                });
            }
        }
    },(err,result)=>{
        if(err){
            callback(err)
        }
        else{
            let dataToSend = getData(data);
            callback(null,dataToSend);
        }
    });
};


//this is a common function used for sending data at login,sign up,update profile
function getData(data){

    let push=true;
    if(data.pushNotification==false){
        push=false;
    }
   return {
        accessToken:data.tokenToSend || data.accessToken,
        profilePicture:data.profilePicture,
        email:data.email || '',
        isGuard:data.isGuard  || false,
        name:data.name  || '',
        loginType:data.loginType  || 'PhoneNo',
        _id:data._id || '',
        dob:data.dob  || '',
        gender:data.gender  || '',
        phoneNo:data.phoneNo  || '',
        wallet:data.wallet  || 0,
        pushNotification:push
    }
}


//This is login api for social (fb and google)
const socialLoginUser = (payloadData,callback)=>{

    let data;
    let tokenToSend;
    async.autoInject({
        //In this function,we check if user is blocked or not found
        loginUser:(cb)=>{
            let query = {
                socialId:payloadData.socialId,
                loginType:payloadData.loginType,
                isDeleted:false
            };
            let dataToGet = {};
            Service.userServices.getUsers(query,dataToGet,{lean:true},(err,result) =>{
                if(err){
                    cb(err);
                }
                else{
                    if(result.length){
                        if(result[0].isBlocked==true){
                            if(payloadData.language=='en'){
                                callback(Config.APP_CONSTANTS.STATUS_MSG.ERROR.USER_BLOCKED)
                            }else if(payloadData.language=='ar'){
                                callback(Config.APP_CONSTANTS.STATUS_MSG.ERROR.USER_BLOCKED_AR)
                            }else{
                                callback(Config.APP_CONSTANTS.STATUS_MSG.ERROR.USER_BLOCKED)
                            }
                        }
                        else{
                            data=result[0];
                            cb(null);
                        }
                    }
                    else{
                        if(payloadData.language=='en'){
                            callback(Config.APP_CONSTANTS.STATUS_MSG.ERROR.USER_NOT_FOUND)
                        }else if(payloadData.language=='ar'){
                            callback(Config.APP_CONSTANTS.STATUS_MSG.ERROR.USER_NOT_FOUND_AR)
                        }else{
                            callback(Config.APP_CONSTANTS.STATUS_MSG.ERROR.USER_NOT_FOUND)
                        }
                    }
                }
            })
        },
        updateData:(loginUser,cb)=>{
            let query={
                _id:data._id
            };
            let dataToSet={};
            if(payloadData.deviceToken){
                dataToSet.deviceToken=payloadData.deviceToken;
            }
            if(payloadData.appVersion){
                dataToSet.appVersion=payloadData.appVersion;
            }
            Service.userServices.updateUser(query,dataToSet,{new:true},(err,result)=>{
                if(err)
                    cb(err);
                else{
                    cb(null)
                }
            })
        },
        //This is function for creating access token for user
        accessToken:(loginUser,cb)=> {

            let tokenData = {
                id: data._id,
                type: Config.APP_CONSTANTS.DATABASE.USER_TYPE.user
            };
            if (tokenData && tokenData.id) {
                TokenManager.setToken(tokenData,  (err, output) =>{
                    if (err) {
                        cb(err);
                    } else {
                        data.tokenToSend = output && output.accessToken || null;
                        cb(null);
                    }
                });
            }
        }
    },(err,result)=>{
        if(err){
            callback(err)
        }
        else{
            let dataToSend = getData(data);
            callback(null,dataToSend);
        }
    });
};

//This is api for skip login (guest login)
const skipLogin = (payloadData,callback)=>{

    //We use same  guest account for all users and return the guest account's access token
    let query = {
        email:"guest@wethaq.com"
    };
    let dataToGet = {
        accessToken:1
    };
    Service.userServices.getUsers(query,dataToGet,{},(err,result)=>{
        if(err){

        }
        else{

        }
        callback(null,{accessToken:result[0].accessToken})
    })
};

//This is update profile of user api in which we handle change password,update profile picture and other details.
const updateProfile = (payloadData,userData,callback) => {

    let data;
    let tokenToSend;
    let profilePicture={};
    let newPassword;
    console.log("1111122",payloadData);
    async.autoInject({
        //This function is used to check if user enters new phone no and is same as old one then we send error that it's already in the data.
        checkUserPhoneNo:(cb)=>{
            if(payloadData.phoneNo){
                let query = {
                    phoneNo:payloadData.phoneNo,
                   // _id:{$ne:userData._id}
                };
                if(payloadData.userId){
                    query._id={$ne:payloadData.userId}
                }else{
                    query._id={$ne:userData._id}
                }
                checkAlreadyRegisteredUser(query,(err,result)=>{
                    if(err){
                        cb(err)
                    }
                    else{
                        if(result==true){
                            if(payloadData.language=='en'){
                                cb(Config.APP_CONSTANTS.STATUS_MSG.ERROR.DUPLICATE_PHONE_NO)
                            }else if(payloadData.language=='ar'){
                                cb(Config.APP_CONSTANTS.STATUS_MSG.ERROR.DUPLICATE_PHONE_NO_AR)
                            }else{
                                cb(Config.APP_CONSTANTS.STATUS_MSG.ERROR.DUPLICATE_PHONE_NO)
                            }
                        }
                        else{
                            cb(null);
                        }
                    }
                })
            }
            else{
                cb(null)
            }

        },
        //This function is used to check if user enters new email and is same as old one then we send error that it's already in the data.
        checkUserEmail:(cb)=>{

            if(payloadData.email){
                let query = {
                    email:payloadData.email,
                   // _id:{$ne:userData._id}
                };
                if(payloadData.userId){
                    query._id={$ne:payloadData.userId}
                }else{
                    query._id={$ne:userData._id}
                }
                checkAlreadyRegisteredUser(query,(err,result)=>{
                    if(err){
                        cb(err)
                    }
                    else{
                        if(result==true){
                            if(payloadData.language=='en'){
                                cb(Config.APP_CONSTANTS.STATUS_MSG.ERROR.DUPLICATE_EMAIL)
                            }else if(payloadData.language=='ar'){
                                cb(Config.APP_CONSTANTS.STATUS_MSG.ERROR.DUPLICATE_EMAIL_AR)
                            }else{
                                cb(Config.APP_CONSTANTS.STATUS_MSG.ERROR.DUPLICATE_EMAIL)
                            }
                        }
                        else{
                            cb(null);
                        }
                    }
                })
            }
            else{
                cb(null)
            }
        },
        //If user changes image we upload it to server.
        uploadImage:(checkUserEmail,checkUserPhoneNo,cb)=>{
            if(payloadData.photo){
                let phoneNo=((userData.phoneNo||payloadData.phoneNo).toString()).replace('+','');
                let date=((new Date().getTime()).toString());
                let id = date.concat(phoneNo);
                UploadManager.uploadFileToS3WithThumbnail(payloadData.photo, id, (err,result)=>{
                    if(err){
                        console.log("here err",err);
                        cb(err)
                    }
                    else{
                        // console.log("=======================",result);
                        profilePicture.original=Config.awsS3Config.s3BucketCredentials.s3URL +result.original;
                        profilePicture.thumbnail= Config.awsS3Config.s3BucketCredentials.s3URL +result.thumbnail;
                        cb(null);
                    }
                })
            }
            else{
                cb(null);
            }
        },
        //This function is for change password,we first check of old password matches password in our database,then we check if user is sending new password or not.
        checkPassword:(cb)=> {
            if(payloadData.oldPassword){
                if(userData.password===UniversalFunctions.CryptData(payloadData.oldPassword)){
                    if(payloadData.newPassword){
                        newPassword= payloadData.newPassword;
                        cb(null);
                    }
                    else{
                        if(payloadData.language=='en'){
                            callback(Config.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_NEW_PASSWORD)
                        }else if(payloadData.language=='ar'){
                            callback(Config.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_NEW_PASSWORD_AR)
                        }else{
                            callback(Config.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_NEW_PASSWORD)
                        }
                    }
                }
                else{
                    if(payloadData.language=='en'){
                        callback(Config.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_PASSWORD)
                    }else if(payloadData.language=='ar'){
                        callback(Config.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_PASSWORD_AR)
                    }else{
                        callback(Config.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_PASSWORD)
                    }
                }
            }
            else{
                cb(null)
            }
        },
        //This is last function that runs after other function to update the entered data in our database
        updateProfileData:(checkPassword,uploadImage,cb)=>{

            let query = {};
            let dataToSet={
               // _id:userData._id
            };
            if(payloadData.userId){
                dataToSet._id=payloadData.userId
            }else{
                dataToSet._id=userData._id
            }
            if(payloadData.email||payloadData.email==''){
                query.email=payloadData.email
            }
            if(payloadData.name){
                query.name=payloadData.name
            }
            if(payloadData.phoneNo){
                query.phoneNo=payloadData.phoneNo
            }
            if(payloadData.gender){
                query.gender=payloadData.gender
            }
            if(payloadData.dob){
                query.dob=changeDate(payloadData.dob)
            }
            if(payloadData.language){
                query.language=payloadData.language
            }
            if(payloadData.photo){
                query.profilePicture=profilePicture
            }
            if(newPassword){
                query.password= UniversalFunctions.CryptData(newPassword)
            }
            console.log("criteria",query);
            Service.userServices.updateUser(dataToSet,query,{new:true},(err,result) =>{
                if(err) {
                    callback(err)
                }
                else{
                    data=result;
                    cb(null);
                }
            })
        }
    },(err,result)=>{
        if(err){
            callback(err)
        }
        else{
            let dataToSend = getData(data);
            callback(null,dataToSend);
        }
    });
};


//Front end
function changeDate(date){
    let date2=date;
    date2=date2.split(' ');
    let arabMonths = ["يناير", "فبراير", "مارس", "إبريل", "مايو", "يونيو","يوليو", "أغسطس", "سبتمبر", "أكتوبر", "نوفمبر", "ديسمبر"];
    let engMonth = ["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"];
    if(engMonth.indexOf(date2[0])==-1){
        let index=arabMonths.indexOf(date2[0]);
        date2[0]=engMonth[index];
        date2=date2.join()
    }else{
        date2=date2.join()
    }
console.log("== date ==", date2)
    return date2
}

function deleteFile(path) {
    fsExtra.remove(path, function (err) {
        console.log('error deleting file>>', err)
    });
}

//This is api to create event
const createEvent = (payloadData,userData,callback)=>{
    // console.log('==========  payloadData =========',payloadData)
    // console.log(new Date().getTime())
  if(payloadData.packageType == Config.APP_CONSTANTS.DATABASE.PACKAGES.FREE && payloadData.startAt == undefined){
      payloadData.startAt == new Date().getTime();
      payloadData.endAt == new Date(new Date().getTime() + (24 * 60 * 60 * 1000));
      payloadData.inviteAt == new Date(new Date().getTime() + (24 * 60 * 60 * 1000));
  }

    let data;
    let invites=[];
    //  let eventId;
    if(payloadData.invites) {
        invites = payloadData.invites
    }
    let ids=[];
    let dTokens=[];
    let existing=[];
    let qrs=[];
    let unRegistered=[];
    let invitees=[];
    let emails=[];
    let dummy;
    let startTime=new Date().setUTCMinutes(new Date().getUTCMinutes()-1);
    let endTime=new Date().setUTCMinutes(new Date().getUTCMinutes()+1);
    let eventId = mongoose.Types.ObjectId();
    console.log("== payload ==",payloadData,"");
    // startTime,endTime);
    async.autoInject({
            getDummy:(cb) => {
                if(payloadData.dummyTemplateId){
                    Service.dummyTempServices.getDummyTemplate({_id:payloadData.dummyTemplateId},{message:0},{},(err,result)=>{
                        if(err){
                            cb(err)
                        }
                        else {
                            if (result && result.length) {
                                dummy=result[0];
                                cb(null)
                            }
                            else{
                                cb(null)
                            }
                        }
                    })
                }
                else{
                    cb(null)
                }
            },
            insertEvent:(getDummy,checkInvites,cb) => {
                console.log("12345",invitees);
                let query = {
                    _id: eventId,
                    createdBy: userData._id,
                    categoryId: payloadData.categoryId,
                    templateId: payloadData.templateId,
                    packageType: payloadData.packageType,
                    // title : payloadData.title,
                    createdAt: new Date().getTime(),
                   // message:payloadData.message,
                    lang: payloadData.lang,
                   // htmlUrl:payloadData.htmlUrl,
                    imageUrl: payloadData.imageUrl,
                  //  address:payloadData.address,
                    // inviteAt:payloadData.inviteAt,
                   // startAt:payloadData.startAt,
                    categoryPrice: payloadData.categoryPrice,
                    smsPrice: payloadData.smsPrice,
                    totalSms: payloadData.totalSms,
                    totalPrice: payloadData.totalPrice
                };

                query.type = Config.APP_CONSTANTS.DATABASE.EVENT_TYPE.EVENT;
               
                if(payloadData.title)  query.title = payloadData.title;
                if(payloadData.inviteAt) query.inviteAt = payloadData.inviteAt;

                if(payloadData.invites && invites.length){
                    query.pending=(payloadData.invites).length;
                    query.total=(payloadData.invites).length;
                }
                if(payloadData.dummyTemplateId){
                    if(dummy.endAt){
                        query.endAt=dummy.endAt;
                    }else{
                        query.endAt=dummy.startAt+25200000;
                    }
                    if(dummy.long!=undefined && dummy.lat!=undefined){
                        query.location=[dummy.long,dummy.lat]
                    }
                    if(dummy.address!=undefined) {
                        query.address = dummy.address;
                    }
                    query.startAt=dummy.startAt;
                    query.htmlUrl=dummy.htmlUrl;
                }
                if(payloadData.long!=undefined && payloadData.lat!=undefined){
                    query.location=[payloadData.long,payloadData.lat]
                }
                if(payloadData.address){
                    query.address=payloadData.address
                }
                if(payloadData.message){
                    query.message=payloadData.message
                }
                if(!payloadData.dummyTemplateId){
                    if(payloadData.endAt){
                        query.endAt=payloadData.endAt;
                    }else{
                        query.endAt=payloadData.startAt+25200000;
                    }
                    if(payloadData.long!=undefined && payloadData.lat!=undefined){
                        query.location=[payloadData.long,payloadData.lat]
                    }
                    query.address=payloadData.address;
                    query.startAt=payloadData.startAt;
                    query.htmlUrl=payloadData.htmlUrl;
                }

                if(invitees && invitees.length){
                    query.invites=invitees;
                    query.unRegistered=unRegistered;
                    query.isSent=false;
                }else{
                    query.invites=payloadData.invites
                }
                if(payloadData.startAt){
                    query.startAt=payloadData.startAt;
                    if(payloadData.endAt){
                        query.endAt=payloadData.endAt;
                    }else{
                        query.endAt=payloadData.startAt+25200000;
                    }
                }else{
                    query.startAt=query.createdAt;
                    query.endAt=query.createdAt;
                }
                if(qrs && qrs.length) {
                    query.qrCodes=qrs;
                }
                query.isQrCode = payloadData.qrCode == 1;
             //   console.log("create query",query);
                Service.eventServices.createEvent(query,(err,result)=>{
                    if(err){
                        cb(err);
                    }
                    else{
                        // eventId=result._id;
                        data=result;
                        cb(null);
                    }
                })
            },
            createQr:(cb)=>{
                if(payloadData.qrCode && payloadData.inviteAt){
                    console.log('createQr')
               // if(payloadData && payloadData.package != Config.APP_CONSTANTS.DATABASE.PACKAGES.FREE){
                if(payloadData.qrCode==1 && invites && invites.length 
                    // && payloadData.inviteAt>startTime && payloadData.inviteAt<endTime
                    ){
                    console.log("payloadData qrCode",payloadData.qrCode);
                    let flag=0;
                    for(let i=0;i<invites.length;i++){
                        (function (i) {
                            let  pic = "qr-userId="+ invites[i].phoneNo+"eventId=" + eventId+"inviteCount="+invites[i].inviteCount;
                            let qr_svg = qr.image(pic, {type: 'png'});
                            let date=new Date().getTime();
                            // console.log("111",qr_svg);
                            let stream = qr_svg.pipe(require('fs').createWriteStream('uploads/' +date+'eventId=' + eventId+'.png'));
                            stream.on('finish', function (err, result) {
                                let  qrImage = Path.resolve(".") + "/uploads/" +date+'eventId=' + eventId+'.png';
                                let client = knox.createClient({
                                    key: Config.awsS3Config.s3BucketCredentials.accessKeyId
                                    , secret: Config.awsS3Config.s3BucketCredentials.secretAccessKey
                                    , bucket: Config.awsS3Config.s3BucketCredentials.bucket
                                });
                                let s3ClientOptions = {'x-amz-acl': 'public-read'};
                                client.putFile(qrImage,date + eventId+'.png', s3ClientOptions, function (err, res) {
                                    console.log(err);
                                    deleteFile(qrImage);
                                    _.map(invites, function(obj) {
                                        if (obj.phoneNo ==invites[i].phoneNo)
                                            obj.qr=Config.awsS3Config.s3BucketCredentials.s3URL +date + eventId+  '.png';
                                    });
                                    qrs.push({"qr":Config.awsS3Config.s3BucketCredentials.s3URL +date + eventId+ '.png'});
                                    flag++;
                                    if (flag == invites.length) {
                                        console.log("qr",qrs);
                                        cb(null);
                                    }

                                });
                            });
                        }(i))
                    }
                }else{
                    cb(null)
                }
            }
            else{
            cb(null)
            }
        },
            checkInvites:(getDummy,createQr,cb)=>{
                console.log("checkUnvites");
                if(payloadData.inviteAt){
                    // console.log('checkInvites',payloadData)
                if(payloadData.invites && invites.length > 0
                // && payloadData.inviteAt > startTime 
                // && payloadData.inviteAt < endTime
                ){
                    console.log("invite qr",invites);
                    let flag = 0;
                    for (let i = 0; i < invites.length; i++) {
                        (function (i) {
                            let id=eventId.toString();
                            let number=(invites[i].phoneNo.replace("+","")).toString().replace(/\s/g, '');
                            // let link = 'http://34.211.166.21/wethaq_dev/wethaq_admin/#/invitation/'+id+'/'+number; 
                            let link='http://admin.wethaqapp.net/wethaq_admin/#/invitation/'+id+'/'+number;
                        //    let link='http://34.211.166.21/wethaq_admin/#/invitation/'+id+'/'+number;
                        
                          TinyURL.shorten(link, function(res) {
                               link=res
console.log("link",link)
                            let subject = "Event Invite";
                            invites[i].status='Pending';
                            let query = {
                                phoneNo:invites[i].phoneNo,
                                _id:{$ne:userData._id}
                            };
                            console.log("== query ==",query)
                            Service.userServices.getUsers(query,{},{lean:true},function (err,result) {
                                if(err){
                                    cb(err)
                                }
                                else{
                                    console.log('== entering ==',result)
                                    if(result.length) {
                                        console.log( "== resulttttttt==", result[0] )
                                        existing.push({"_id": result[0]._id, "phoneNo": invites[i].phoneNo});
                                        ids.push(result[0]._id);
                                        emails.push(result[0].email);
                                        dTokens.push(result[0].deviceToken);
                                    }
                                    else{
                                        unRegistered.push(invites[i].phoneNo);
                                    }
                                       
                                    console.log("== unRegistered ==",unRegistered);
                                    let criteria = {
                                            to:[invites[i].phoneNo],
                                          //  body:template
                                        };
             if(payloadData.packageType == Config.APP_CONSTANTS.DATABASE.PACKAGES.FREE){
                       payloadData.title = "";
                    }
                                       criteria.body ="";
                                        if( payloadData.message )  {
                                            if(payloadData.lang=="en"){
                                                criteria.body+="Hello "+invites[i].name+", "+payloadData.message+"\n";
                                            }else if(payloadData.lang=="ar"){
                                                criteria.body+=" مرحبًا "+"\u202A"+invites[i].name+", "+"\u202C"+payloadData.message+"\n";
                                            }else{
                                                criteria.body+=" مرحبًا "+"\u202A"+invites[i].name+", "+"\u202C"+payloadData.message+"\n";
                                            }
                                          }
                                        {
                                            if(payloadData.lang=="en"){
                                                criteria.body+=/*"Hello "+invites[i].name+*/" It’s an honor to invite you to "+payloadData.title+"\n";
                                            }else if(payloadData.lang=="ar"){
                                                criteria.body+=/*" أهلا "+ "\u202A"+invites[i].name+*/"\u202C"+" نتشرف بدعوتك لحضور مناسبة "+ "\u202A"+payloadData.title+"\n";
                                            }else{
                                                criteria.body+=/*" أهلا "+ "\u202A"+invites[i].name+*/"\u202C"+" نتشرف بدعوتك لحضور مناسبة "+ "\u202A"+payloadData.title+"\n";
                                            }
                                        }
                                        let template = '<html><head></head><body><table style="border-collapse:collapse;table-layout:fixed;min-width:320px;width:100%;background-color:#fff;text-align:left;direction:ltr"><tbody><tr><td><div style=";text-align:left;direction:ltr"><div style="Margin:0 auto;max-width:560px;min-width:280px;width:280px;width:calc(28000% - 167440px);text-align:left;direction:ltr"> <br></div><div style="Margin:0 auto;max-width:600px;min-width:320px;width:320px;width:calc(28000% - 167400px);text-align:left;direction:ltr"><div style="font-size:26px;line-height:32px;Margin-top:6px;Margin-bottom:20px;color:#c3ced9;font-family:Roboto,Tahoma,sans-serif;Margin-left:20px;Margin-right:20px;text-align:left;direction:ltr" align="center"><div align="center" style=";text-align:left;direction:ltr"> <img style="display:block;height:auto;width:100%;border:0;max-width:230px" src="https://s3-us-west-2.amazonaws.com/wethaqbucket/profilePic_1523699389214imageBackground.png" alt="" width="230"></div></div></div></div><div style=";text-align:left;direction:ltr"><div style="Margin:0 auto;max-width:600px;min-width:320px;width:320px;width:calc(28000% - 167400px);word-wrap:break-word;word-break:break-word;text-align:left;direction:ltr"><div style="border-collapse:collapse;display:table;width:100%;background-color:#ffffff;text-align:left;direction:ltr"><div style="text-align:left;color:#8f8f8f;font-size:16px;line-height:24px;font-family:Open Sans,sans-serif;max-width:600px;min-width:320px;width:320px;width:calc(28000% - 167400px);text-align:left;direction:ltr"><div style="Margin-left:20px;Margin-right:20px;Margin-top:24px;text-align:left;direction:ltr"><div style=";text-align:left;direction:ltr"><h1 style="Margin-top:0;Margin-bottom:20px;font-style:normal;font-weight:normal;color:#404040;font-size:28px;line-height:36px;text-align:center;text-align:left;direction:ltr">'+payloadData.title+'</h1></div></div><div style="Margin-left:20px;Margin-right:20px;text-align:left;direction:ltr"><div style="line-height:10px;font-size:1px;text-align:left;direction:ltr"></div></div><div style="Margin-left:20px;Margin-right:20px;text-align:left;direction:ltr"><div style="line-height:10px;font-size:1px;text-align:left;direction:ltr"></div></div><div style="Margin-left:20px;Margin-right:20px;text-align:left;direction:ltr"><div style=";text-align:left;direction:ltr"><h1 style="Margin-top:0;Margin-bottom:0;font-style:normal;font-weight:normal;color:#404040;font-size:20px;line-height:36px;text-align:center;text-align:left;direction:ltr">'+invites[i].name+' أهلا بك</h1><h1 style="Margin-top:20px;Margin-bottom:0;font-style:normal;font-weight:normal;color:#404040;font-size:28px;line-height:36px;text-align:center;text-align:left;direction:ltr"> <strong>مرحباً بكم في تطبيق وثاق </strong></h1><h3 style="Margin-top:20px;Margin-bottom:12px;font-style:normal;font-weight:normal;color:#706f70;font-size:17px;line-height:26px;font-family:Cabin,Avenir,sans-serif;text-align:center;text-align:left;direction:ltr">التصميم المخصص للدعوة</h3><h3 style="Margin-top:20px;Margin-bottom:12px;font-style:normal;font-weight:normal;color:#706f70;font-size:17px;line-height:26px;font-family:Cabin,Avenir,sans-serif;text-align:center;text-align:left;direction:ltr">'+criteria.body+'</h3></div></div><div style="font-size:12px;font-style:normal;font-weight:normal;line-height:19px;text-align:left;direction:ltr" align="center"> <img style="border:0;display:block;height:auto;width:100%;max-width:900px" alt="" width="600" src='+payloadData.imageUrl+'></div><div style="Margin-left:20px;Margin-right:20px;text-align:left;direction:ltr"><div style=";text-align:left;direction:ltr"><h2>'+link+' : إليك الرابط</h2></div></div></div></div></div></div></td></tr></tbody></table></body></html>'

                                        UniversalFunctions.sendMail(invites[i].email, subject, template, function (err,result) {
                                            console.log("res",err,result)
                                        });

                                        if(payloadData.lang=="en"){
                                            criteria.body+="For more info, please check the link: "+"\n";
                                        }
                                        else if(payloadData.lang=="ar"){
                                            criteria.body+="وللمزيد من المعلومات الرجاء فتح الرابط التالي: "+ "\n";
                                        }
                                        else criteria.body+="وللمزيد من المعلومات الرجاء فتح الرابط التالي"+"\n";
                                         criteria.body+=link;

                                        if(payloadData.packageType == Config.APP_CONSTANTS.DATABASE.PACKAGES.PREMIUM){
                                            console.log('link - -- - -- - >>>>',link)
                                            console.log('== criteria ==',criteria)
                                            cb(null);
                                            // messageManager.sendSmsUnisoft(criteria, function ( result) {
                                            //     console.log("sms", err, result);
                                            //     // if(err==null){
                                            //     if(result.success=="true"){
                                            //         invites[i].statusCode=200;
                                            //         invites[i].createdAt=new Date().getTime();
                                            //         // invites[i].message=[result.Data.MessageEn,result.Data.MessageAr];
                                            //         invites[i].message = [ "SMS sent successfully", "تم الإرسال"];
                                            //         invitees.push(invites[i]);
                                            //     }else{
                                            //         // console.log("1234555",err);
                                            //         invites[i].statusCode=400;
                                            //         invites[i].createdAt=new Date().getTime();
                                            //         // invites[i].message=[err.MessageEn,err.MessageAr];
                                            //         invites[i].message = ["Mobile(s) number(s) is not specified or incorrect", "الأرقام المرسل لها غير صحيحه أو فارغه"];
                                            //         invitees.push(invites[i]);
                                            //     } flag++;
                                            //     if (flag == invites.length) {
                                            //         cb(null);
                                            //     }
                                            // })
                                                // messageManager.sendSMSToUser(criteria, (result) => {
                                            //         console.log("== result ==",result)
                                            //                 if(result.customMessage == 'Success'){
                                            //                 invites[i].statusCode=200;
                                            //                 invites[i].createdAt=new Date().getTime();
                                            //                 // invites[i].message=[result.Data.MessageEn,result.Data.MessageAr];
                                            //                 invites[i].message = [ "SMS sent successfully", "تم الإرسال"];
                                            //                 invitees.push(invites[i]);
                                            //             }else{
                                            //                 // console.log("1234555",err);
                                            //                 invites[i].statusCode=400;
                                            //                 invites[i].createdAt=new Date().getTime();
                                            //                 // invites[i].message=[err.MessageEn,err.MessageAr];
                                            //                 invites[i].message = ["Mobile(s) number(s) is not specified or incorrect", "الأرقام المرسل لها غير صحيحه أو فارغه"];
                                            //                 invitees.push(invites[i]);
                                            //             }
                                            //     flag++;
                                            //     if (flag == invites.length) {
                                            //         cb(null);
                                            //     }
                                            // });
                                        }
                                else if(payloadData.packageType == Config.APP_CONSTANTS.DATABASE.PACKAGES.WHATSAPP){
                                    console.log('== criteria ==',criteria)
                                            messageManager.sendSMSToUser(criteria, (result) => {
                                        console.log("== result ==",result)
                                                if(result.customMessage == 'Success'){
                                                invites[i].statusCode=200;
                                                invites[i].createdAt=new Date().getTime();
                                                // invites[i].message=[result.Data.MessageEn,result.Data.MessageAr];
                                                invites[i].message = [ "SMS sent successfully", "تم الإرسال"];
                                                invitees.push(invites[i]);
                                            }else{
                                                // console.log("1234555",err);
                                                invites[i].statusCode=400;
                                                invites[i].createdAt=new Date().getTime();
                                                // invites[i].message=[err.MessageEn,err.MessageAr];
                                                invites[i].message = ["Mobile(s) number(s) is not specified or incorrect", "الأرقام المرسل لها غير صحيحه أو فارغه"];
                                                invitees.push(invites[i]);
                                            }
                                            flag++;
                                            if (flag == invites.length) {
                                                cb(null);
                                            }
                                          })
                                        }
                                        else{
                                            invitees.push(invites[i]);
                                            flag++;
                                            if (flag == invites.length) {
                                                cb(null);
                                            }
                                        }

                                    }
                                })
                            });
                        }(i))
                    }
                }else{
                    cb(null)
                }
            }
                else{
                    cb(null)
                    }
            },
            sendNotificaiton:(insertEvent,cb)=>{
                console.log( '== ids ==', ids )
                if(ids && ids.length){
                    console.log("notification",ids);
                    let query={
                        createdFor:ids,
                        createdBy:[userData._id],
                        type:0,
                        text:[payloadData.title],
                        eventId:eventId,
                        timestamp:payloadData.startAt,
                        categoryId:payloadData.categoryId,
                        templateId:payloadData.templateId,
                        notificationType:'Invite',
                        images:[{original:payloadData.imageUrl,thumbnail:payloadData.imageUrl}],
                    };
                    Service.notificationServices.createNotification(query,function (err,result) {
                        if(err){
                            cb(err)
                        }else{
                            cb(null);
                        }
                    });
                    // console.log("1111",unRegistered,phoneNos,ids);
                }else{
                    cb(null)
                }
            },
            sendPush:(insertEvent,cb)=>{
                if(dTokens && dTokens.length && payloadData.packageType != Config.APP_CONSTANTS.DATABASE.PACKAGES.FREE){
                    let data={
                        type:0,
                        title:'Wethaq',
                        multi:true,
                        body: 'You are invited to attend “'+payloadData.title+'”',
                        eventId:eventId,
                        timestamp:payloadData.startAt,
                        images:{original:payloadData.imageUrl,thumbnail:payloadData.imageUrl},
                    };
                    NotificationManager.sendPush(data,dTokens, function (err, result) {
                        cb(null)
                    });
                    // console.log("1111",unRegistered,phoneNos,ids);
                }else{
                    cb(null)
                }
            },
            createTransc:(insertEvent,cb)=>{
                    let criteria = {
                        userId:userData._id,
                        eventId:eventId,
                        type:"Event"
                    };
                    if(payloadData.finalPrice){
                        criteria.price=payloadData.finalPrice
                    }if(payloadData.trxId){
                        criteria.trxId=payloadData.trxId
                    }

                    Service.transactionServices.createTransaction(criteria, function (err, result) {
                        if (err) {
                            cb(err);
                        } else {
                            cb(null);
                        }
                    })
            },
            updateTemplate:(insertEvent,cb)=>{
                Service.templateServices.updateTemplate({_id:payloadData.templateId}, {$inc: {usage:1}},{new:true},(err,res)=>{
                    cb(null);
                })
            },
            updateUser:(insertEvent,cb)=>{
                Service.userServices.updateUser({_id:userData._id}, {$inc: {eventsCreated:1}},{new:true},(err,res)=>{
                    cb(null);
                })
            },
         /*   createAdminNotification:(insertEvent,cb)=>{
                let obj={
                    title:"New event created",
                    userId:userData._id,
                    eventId:eventId,
                    body:userData.name+" just created a new event:"+payloadData.title
                };
                Service.adminNotifServices.createAdminNotifs(obj, (err, result) => {
                    if (err) {
                        cb(err);
                    } else {
                        cb(null);
                    }
                });
            },*/
            updateCoupon:(insertEvent,cb)=> {
                if(data && payloadData.couponCode && payloadData.couponCode!='') {
                    let obj = {
                        couponCode: payloadData.couponCode
                    };
                    let dataToSet = {};
                    dataToSet.$addToSet = {usedBy: userData._id};
                    Service.promoCodeServices.updatePromoCode(obj, dataToSet, {new: true}, function (err, result) {
                        if (err) {
                            cb(err);
                        } else {

                            cb(null);
                        }
                    })
                }else{
                    cb(null)
                }

            },
            updateWallet:(insertEvent,cb)=> {
                if(data) {
                    let obj = {
                        _id: userData._id
                    };
                    let dataToSet = {};
                    dataToSet.wallet=(userData.wallet+payloadData.totalSms)-invites.length;
                     Service.userServices.updateUser(obj, dataToSet, {lean:true,new: true}, function (err, result) {
                        if (err) {
                            cb(err);
                        } else {
                            data.wallet=result.wallet;
                            cb(null);
                        }
                    })
                }else{
                    cb(null)
                }

            },
            sendMail:(insertEvent,cb)=>{
               if(payloadData.packageType != Config.APP_CONSTANTS.DATABASE.PACKAGES.FREE){
                   console.log('sendEmailllll')
                // let inviteAtDate;
                // if(payloadData.title == undefined) payloadData.title = "";
                // if(payloadData.inviteAt == undefined){ 
                //  payloadData.inviteAt = null;
                // }
                // else {
                //     inviteAtDate = moment(data.inviteAt).tz('Asia/Riyadh').format("DD/MM/YYYY");
                // }
                // if(payloadData.package == Config.APP_CONSTANTS.DATABASE.PACKAGES.FREE) payloadData.title = ""
               
                let subject = "Event Created";
                let template='<html><head></head><body><table style="border-collapse:collapse;table-layout:fixed;min-width:320px;width:100%;background-color:#fff;text-align:left;direction:ltr"><tbody><tr><td><div style=";text-align:left;direction:ltr"><div style="Margin:0 auto;max-width:560px;min-width:280px;width:280px;width:calc(28000% - 167440px);text-align:left;direction:ltr"> <br></div><div style="Margin:0 auto;max-width:600px;min-width:320px;width:320px;width:calc(28000% - 167400px);text-align:left;direction:ltr"><div style="font-size:26px;line-height:32px;Margin-top:6px;Margin-bottom:20px;color:#c3ced9;font-family:Roboto,Tahoma,sans-serif;Margin-left:20px;Margin-right:20px;text-align:left;direction:ltr" align="center"><div align="center" style=";text-align:left;direction:ltr"> <img style="display:block;height:auto;width:100%;border:0;max-width:230px" src="https://s3-us-west-2.amazonaws.com/wethaqbucket/profilePic_1523699389214imageBackground.png" alt="" width="230"></div></div></div></div><div style=";text-align:left;direction:ltr"><div style="Margin:0 auto;max-width:600px;min-width:320px;width:320px;width:calc(28000% - 167400px);word-wrap:break-word;word-break:break-word;text-align:left;direction:ltr"><div style="border-collapse:collapse;display:table;width:100%;background-color:#ffffff;text-align:left;direction:ltr"><div style="text-align:left;color:#8f8f8f;font-size:16px;line-height:24px;font-family:Open Sans,sans-serif;max-width:600px;min-width:320px;width:320px;width:calc(28000% - 167400px);text-align:left;direction:ltr"><div style="Margin-left:20px;Margin-right:20px;Margin-top:24px;text-align:left;direction:ltr"><div style=";text-align:left;direction:ltr"><h1 style="Margin-top:0;Margin-bottom:20px;font-style:normal;font-weight:normal;color:#404040;font-size:28px;line-height:36px;text-align:center;text-align:left;direction:ltr">'+payloadData.title+'</h1></div></div><div style="Margin-left:20px;Margin-right:20px;text-align:left;direction:ltr"><div style="line-height:10px;font-size:1px;text-align:left;direction:ltr"></div></div><div style="Margin-left:20px;Margin-right:20px;text-align:left;direction:ltr"><div style="line-height:10px;font-size:1px;text-align:left;direction:ltr"></div></div><div style="Margin-left:20px;Margin-right:20px;text-align:left;direction:ltr"><div style=";text-align:left;direction:ltr"><h1 style="Margin-top:0;Margin-bottom:0;font-style:normal;font-weight:normal;color:#404040;font-size:28px;line-height:36px;text-align:center;text-align:left;direction:ltr">'+userData.name+'أهلا بك</h1><h1 style="Margin-top:20px;Margin-bottom:0;font-style:normal;font-weight:normal;color:#404040;font-size:28px;line-height:36px;text-align:center;text-align:left;direction:ltr"> <strong>مرحباً بكم في تطبيق وثاق </strong></h1><h3 style="Margin-top:20px;Margin-bottom:12px;font-style:normal;font-weight:normal;color:#706f70;font-size:17px;line-height:26px;font-family:Cabin,Avenir,sans-serif;text-align:center;text-align:left;direction:ltr">التصميم المخصص للدعوة</h3></div></div><div style="font-size:12px;font-style:normal;font-weight:normal;line-height:19px;text-align:left;direction:ltr" align="center"> <img style="border:0;display:block;height:auto;width:100%;max-width:900px" alt="" width="600" src='+payloadData.imageUrl+'></div><div style="Margin-left:20px;Margin-right:20px;Margin-top:20px;text-align:left;direction:ltr"><div style=";text-align:left;direction:ltr"><p style="Margin-top:0;Margin-bottom:0;text-align:right;text-align:left;direction:ltr"> سترسل الرسائل تحمل النص التالي '+data.message+'</p><p style="Margin-top:20px;Margin-bottom:0;text-align:right;text-align:left;direction:ltr">'+moment(data.inviteAt).tz('Asia/Riyadh').format("DD/MM/YYYY")+' سترسل الرسائل في تاريخ</p><p style="Margin-top:20px;Margin-bottom:20px;text-align:right;text-align:left;direction:ltr">'+moment(data.inviteAt).tz('Asia/Riyadh').format("HH:mm:a")+' الساعة</p></div></div><div style="Margin-left:20px;Margin-right:20px;text-align:left;direction:ltr"><div style=";text-align:left;direction:ltr"><p style="Margin-top:0;Margin-bottom:20px;text-align:left;direction:ltr"></p></div></div><div style="Margin-left:20px;Margin-right:20px;text-align:left;direction:ltr"><div style=";text-align:left;direction:ltr"><p>يرسل هذا الإيميل لمراجعة جميع تفاصيل الدعوة وفي حال وجود خطأ أو تعديل أو الإلغاء يمكنكم القيام به في صفحة الدعوة عن طريق التطبيق</p><p style="Margin-top:20px;Margin-bottom:0;font-size:16px;line-height:24px;text-align:right;text-align:left;direction:ltr">يمكنكم إضافة مدعوون إضافيين عن طريق التطبيق*</p><p>يمكنكم متابعة تأكيد الحضور لجميع المدعوين في صفحة الدعوة عن طريق التطبيق</p><p>ملاحظة هامة : مقيم الدعوه هو المسؤول عن تعيين الحارس وإضافته عن طريق صفحة الدعوة في التطبيق وقرائة الكود للمدعويين عن طريق يوز</p><p style="Margin-top:20px;Margin-bottom:0;font-size:16px;line-height:24px;text-align:right;text-align:left;direction:ltr">في حال وجود اي استفسار التوا عن طريق خدمة العملاء في إحدى قنواتنا*</p><p mstyle="Margin-top:20px;Margin-bottom:20px;font-size:16px;line-height:24px;text-align:right;text-align:left;direction:ltr">جميع التعديلات تتم قبل إرسال الرسائل للمدعوين في حال تم إرسال الدعوات ستكون هناك تكاليف إضافية*</p></div></div><div style="Margin-left:20px;Margin-right:20px;text-align:left;direction:ltr"><div style="line-height:20px;font-size:1px;text-align:left;direction:ltr"></div></div><div style="Margin-left:20px;Margin-right:20px;text-align:left;direction:ltr"><div style=";text-align:left;direction:ltr"><h1 style="Margin-top:0;Margin-bottom:20px;font-style:normal;font-weight:normal;color:#404040;font-size:22px;line-height:31px;text-align:center;text-align:left;direction:ltr" lang="x-size-26"> <span style="color:#f70505">ملاحظة هامة : مقيم الدعوه هو المسؤول عن تعيين الحارس وإضافته عن طريق صفحة الدعوة في التطبيق وقرائة الكود الخاص لكل مدعو عن طريق يوز الحارس من خلال التطبيق</span></h1></div></div><div style="Margin-left:20px;Margin-right:20px;text-align:left;direction:ltr"><div style="line-height:10px;font-size:1px;text-align:left;direction:ltr"></div></div></div></div></div></div></td></tr></tbody></table></body></html>'
                UniversalFunctions.sendMail(userData.email, subject, template, function (err,result) {
                    cb(null)
                });
            }
            else{
                cb(null)
            }
        }
    },
        (err,result)=>{
            if(err){
                callback(err);
            }
            else{
                callback(null,data);
            }
        })
};
//This is api to list upcoming events for guard and user
const getUpcomingEvents = (payloadData,userData,callback)=>{
    let events=[];
    let categories=[];
    let count=0;
    let freeEvents = [];
    let freeEventCount = 0;
    async.autoInject({
        getCategory:(cb)=>{
            if(payloadData.search && payloadData.search!='') {
                let query = {};
                query.name = {$regex: payloadData.search, $options: 'i'};
                Service.categoryServices.getCategory(query, {}, {}, (err, result) => {
                    if(err){
                        cb(err)
                    }else {
                        if (result && result.length) {
                            result.map(function (obj) {
                                categories.push(mongoose.Types.ObjectId(obj._id));
                            });
                            cb(null)
                        } else {
                            cb(null)
                        }
                    }
                })
            }else{
                cb()
            }
        },
        getEvents:(getCategory,cb)=>{
            let projection={
                _id:1, createdBy:1, categoryId:1, templateId:1, packageType: 1, location:1, __v:1, isDeleted:1, isQrCode:1, guards:1, invites:1,
                createdAt:1, address:1, total:1, checked:1, rejected:1, accepted:1, pending:1, endAt:1, startAt:1, inviteAt:1,
                totalPrice:1, smsPrice:1, totalSms:1, categoryPrice:1, qrCodes:1, htmlUrl:1, imageUrl:1, message:1, title:1,
                unRegistered:1, checkIns:1, attendees:1, guardId:1, type:1
            };
            let query = {
                isDeleted:false,
                isBlocked:false,
                endAt:{$gt:new Date().getTime()}
            };

            if(userData.isGuard==true){
                // query.guardId=userData._id;
                query.guards={$elemMatch: {userId:userData._id,status:"Accepted"}}
                if(payloadData.search && payloadData.search!=''){
                    query.$and=[];
                    query.$and.push({$or: [{categoryId: {$in:categories}},{title: {$regex: payloadData.search, $options: 'i'}}]})
                }
            }

            if(userData.isGuard==false){
                query.$and=[];
                query.$and.push({$or: [{attendees: {$in: [userData._id]}},{createdBy:userData._id}]});
                projection.qrCodes={ $elemMatch: { userId: userData.phoneNo } };
                if(payloadData.search && payloadData.search!=''){
                    query.$and.push({$or: [{categoryId: {$in:categories}},{title: {$regex: payloadData.search, $options: 'i'}}]})
                }
            }

            let populateArray=[
                {
                    path:'categoryId',
                    select:'_id name'
                },
                {
                    path:'createdBy',
                    select:'_id name phoneNo profilePicture email'
                },
                {
                    path:'guards.userId',
                    select:'_id name phoneNo profilePicture email'
                },
                {
                    path:'guardId',
                    select:'_id name phoneNo profilePicture email'
                },
                {
                    path:'templateId',
                    select:'isCustom'
                }
            ];
            if (payloadData.skip  && payloadData.limit) {
                var skip = parseInt(payloadData.skip);
                var limit = parseInt(payloadData.limit)
            }
            let options={
                lean:true,
                skip:skip,
                limit:limit,
                sort: {startAt: -1}
            };
            console.log("query upcoming",JSON.stringify(query) );
            Service.eventServices.getEventPopulate(query,projection,options,populateArray,(err,result)=>{
                if(err){
                    cb(err);
                }
                else{
                    // console.log(result)
                    events=result;
                   // console.log("====",events)
let templateId,isCustom;
                   for(let i = 0; i< result.length; i++){
    
    events[i].isCustom = events[i].templateId.isCustom 
    events[i].templateId = events[i].templateId._id 
}

                    if(userData.isGuard==true){
                        for(let i=0;i<events.length;i++){
                            events[i].invites=_.sortBy( events[i].invites,'name')
                        }
                    }
                    cb(null);
                }
            })
        },

        getCount:(cb)=>{
            let query = {
                isDeleted:false, 
                isBlocked:false, 
                endAt: { $gt: new Date().getTime() }
            };
            if(userData.isGuard==true){
                query.guards={$elemMatch: {userId:userData._id,status:"Accepted"}}
                // query.guardId=userData._id
            }
            if(userData.isGuard==false){
                query.$or=[{attendees: {$in: [userData._id]}},{createdBy:userData._id}];
            }
            if(payloadData.search && payloadData.search!=''){
                query.title={$regex: payloadData.search, $options: 'i'}
            }
            Service.eventServices.countEvents(query,(err,result)=>{
                if(err){
                    cb(err);
                }
                else{
                    count=result;
                    cb(null);
                }
            })
        },
        // getFreeEvents:(getCategory,cb)=>{
        //     let projection={
        //         _id:1, createdBy:1, categoryId:1, package:1, templateId:1, packageType: 1, location:1, __v:1, isDeleted:1, isQrCode:1, guards:1, invites:1,
        //         createdAt:1, address:1, total:1, checked:1, rejected:1, accepted:1, pending:1, endAt:1, startAt:1, inviteAt:1,
        //         totalPrice:1, smsPrice:1, totalSms:1, categoryPrice:1, qrCodes:1, htmlUrl:1, imageUrl:1, message:1, title:1,
        //         unRegistered:1, checkIns:1, attendees:1, guardId:1, type:1
        //     };
        //     let query = {
        //         isDeleted:false,
        //         isBlocked:false,
        //         packageType: Config.APP_CONSTANTS.DATABASE.PACKAGES.FREE
        //     };

        //     if(userData.isGuard==true){
        //         // query.guardId=userData._id;
        //         query.guards={$elemMatch: {userId:userData._id,status:"Accepted"}}
        //         if(payloadData.search && payloadData.search!=''){
        //             query.$and=[];
        //             query.$and.push({$or: [{categoryId: {$in:categories}},{title: {$regex: payloadData.search, $options: 'i'}}]})
        //         }
        //     }

        //     if(userData.isGuard==false){
        //         query.$and=[];
        //         query.$and.push({$or: [{attendees: {$in: [userData._id]}},{createdBy:userData._id}]});
        //         projection.qrCodes={ $elemMatch: { userId: userData.phoneNo } };
        //         if(payloadData.search && payloadData.search!=''){
        //             query.$and.push({$or: [{categoryId: {$in:categories}},{title: {$regex: payloadData.search, $options: 'i'}}]})
        //         }
        //     }

        //     let populateArray=[
        //         {
        //             path:'categoryId',
        //             select:'_id name'
        //         },
        //         {
        //             path:'createdBy',
        //             select:'_id name phoneNo profilePicture email'
        //         },
        //         {
        //             path:'guards.userId',
        //             select:'_id name phoneNo profilePicture email'
        //         },
        //         {
        //             path:'guardId',
        //             select:'_id name phoneNo profilePicture email'
        //         },
        //         {
        //             path:'templateId',
        //             select:'isCustom'
        //         }
        //     ];
        //     if (payloadData.skip  && payloadData.limit) {
        //         var skip = parseInt(payloadData.skip);
        //         var limit = parseInt(payloadData.limit)
        //     }
        //     let options={
        //         lean:true,
        //         skip:skip,
        //         limit:limit,
        //         sort: {startAt: -1}
        //     };
        //     console.log("query upcoming",JSON.stringify(query) );
        //     Service.eventServices.getEventPopulate(query,projection,options,populateArray,(err,result)=>{
        //         if(err){
        //             cb(err);
        //         }
        //         else{
        //             // console.log(result)
        //             freeEvents=result;
        //            // console.log("====",events)
        //             // if(userData.isGuard==true){
        //             //     for(let i=0;i<freeEvents.length;i++){
        //             //         freeEvents[i].invites=_.sortBy( freeEvents[i].invites,'name')
        //             //     }
        //             // }
        //             cb(null);
        //         }
        //     })
        // },

        // getFreeEventsCount:(cb)=>{
        //     let query = {
        //         isDeleted:false, 
        //         isBlocked:false, 
        //         packageType: Config.APP_CONSTANTS.DATABASE.PACKAGES.FREE
        //     };
        //     if(userData.isGuard==true){
        //         query.guards={$elemMatch: {userId:userData._id,status:"Accepted"}}
        //         // query.guardId=userData._id
        //     }
        //     if(userData.isGuard==false){
        //         query.$or=[{attendees: {$in: [userData._id]}},{createdBy:userData._id}];
        //     }
        //     if(payloadData.search && payloadData.search!=''){
        //         query.title={$regex: payloadData.search, $options: 'i'}
        //     }
        //     Service.eventServices.countEvents(query,(err,result)=>{
        //         if(err){
        //             cb(err);
        //         }
        //         else{
        //             freeEventCount=result;
        //             cb(null);
        //         }
        //     })
        // },
    },(err,result)=>{
        if(err){
            callback(err)
        }
        else{
            callback(null,{
                events: events,
                count: count,
                freeEvents:freeEvents,
                freeEventCount:freeEventCount
            });
        }
    });
};

//This is api to list ongoing events for guard
const getOngoingEvents = (payloadData,userData,callback)=>{
    let query = {
        isDeleted:false,
        isBlocked:false,
       startAt:{$lt:new Date().getTime()},
        endAt:{$gt:new Date().getTime()}
    };
    if(userData.isGuard==true) {
        query.guards={$elemMatch: {userId:userData._id,status:"Accepted"}}
        // query.guardId = userData._id;
    }
    let projection={
        _id:1,title:1,checked:1,total:1
    };
    Service.eventServices.getEvent(query,projection,{},(err,result)=>{
        if(err){
            callback(err);
        }
        else{
            callback(null,result);

        }
    })
};

const getMyEvents = (payloadData,userData,callback) => {
    let events=[];
    let count=0;
    async.autoInject({
        getEvents:(cb)=>{
            let query = {
                isDeleted:false,
                isBlocked:false,
                createdBy: userData._id
            };
            let projection={
            };
            let populateArray=[
                {
                    path:'categoryId',
                    select:'_id name'
                },
                {
                    path:'createdBy',
                    select:'_id name phoneNo profilePicture email'
                },
                {
                    path:'guards.userId',
                    select:'_id name phoneNo profilePicture email'
                },
                {
                    path:'guardId',
                    select:'_id name phoneNo profilePicture email'
                }
            ];
            if (payloadData.skip  && payloadData.limit) {
                var skip = parseInt(payloadData.skip);
                var limit = parseInt(payloadData.limit)
            }
            let options={
                lean:true,
                skip:skip,
                limit:limit,
                sort: {createdAt: -1}
            };
            Service.eventServices.getEventPopulate(query,projection,options,populateArray,(err,result)=>{
                if(err){
                    cb(err);
                }
                else{
                    events=result;
                    cb(null);

                }
            })
        },

        getCount:(cb)=>{
            let query = {
                isDeleted:false,
                isBlocked:false,
                createdBy: userData._id
            };
            Service.eventServices.countEvents(query,(err,result)=>{
                if(err){
                    cb(err);
                }
                else{
                    count=result;
                    cb(null);

                }
            })
        },
    },(err,result)=>{
        if(err){
            callback(err)
        }
        else{
            callback(null,{events:events,count:count});
        }
    });
};

const actionInvite = (payloadData,userData,callback)=>{
    let data;
    let creater;
    let categoryId;
    let templateId;
    let eventName;
    let notificationCount=0;
    let notificationId;
    let dToken='';
    console.log("payload",payloadData);
    async.autoInject({
            checkInvite:(cb)=>{
                let query = {
                    _id:payloadData.eventId,
                    isDeleted:false,
                    invites: {$elemMatch: {phoneNo:userData.phoneNo,status:{$ne:"Pending"}}}
                };
                Service.eventServices.getEvent(query,{},{},(err,result)=>{
                    if(err){
                        cb(err);
                    }
                    else{
                        if(result.length){
                            if(payloadData.language=='en'){
                                callback(Config.APP_CONSTANTS.STATUS_MSG.ERROR.ACTION_ALREADY_DONE)
                            }else if(payloadData.language=='ar') {
                                callback(Config.APP_CONSTANTS.STATUS_MSG.ERROR.ACTION_ALREADY_DONE_AR)
                            }else{
                                callback(Config.APP_CONSTANTS.STATUS_MSG.ERROR.ACTION_ALREADY_DONE)
                            }
                        }
                        else{
                            cb(null);
                        }
                    }
                })
            },
            updateInvite:(checkInvite,cb)=>{
                let query = {
                    _id:payloadData.eventId,
                    isDeleted:false,
                    invites: {$elemMatch: {phoneNo:userData.phoneNo}}
                };
                let options = {lean: true,new:true};
                let dataToSet = {};
                if(payloadData.status=='Accepted'){
                    dataToSet.$inc={pending: -1,accepted:1};
                    dataToSet['invites.$.status']="Accepted";
                    dataToSet.$addToSet={attendees:userData._id}
                }
                if(payloadData.status=='Rejected'){
                    dataToSet.$inc={pending: -1,rejected:1};
                    dataToSet['invites.$.status']="Rejected";
                }
                dataToSet['invites.$.updatedAt']=new Date().getTime();
                Service.eventServices.updateEvent(query,dataToSet,options,(err,result)=>{
                    if(err){
                        cb(err);
                    }
                    else{
                        console.log("updated result",result);
                        data=result;
                        creater=result.createdBy;
                        eventName=result.title;
                        categoryId=result.categoryId;
                        templateId=result.templateId;
                        cb(null)

                    }
                })
            },
            getDtoken:(updateInvite,cb)=>{
                if(creater!=null) {
                    Service.userServices.getUsers({_id: creater}, {deviceToken: 1}, {lean: true}, (err, result) => {
                        if (err)
                            cb(err);
                        else {
                            if (result && result.length) {
                                dToken = result[0].deviceToken;
                                cb(null)
                            } else {
                                cb(null)
                            }
                        }
                    })
                }else{
                    cb(null)
                }
            },
            checkNotification:(cb)=>{
                let query={
                    eventId:payloadData.eventId,
                };
                if(payloadData.status=='Accepted'){
                    query.type=1;
                }
                if(payloadData.status=='Rejected'){
                    query.type=2;
                }
                Service.notificationServices.getNotificationTag(query,{},{},function (err,result) {
                  if(err){
                      cb(err)
                  }else{
                      console.log("result",result);
                      if(result && result.length) {
                          notificationCount=result[0].createdBy.length;
                          notificationId=result[0]._id;
                          cb(null)
                      }else{
                          cb(null)
                      }
                  }
                })
            },
            createNotification:(checkNotification,updateInvite,cb)=>{
                console.log("2222",notificationCount);
                let query = {};
                if(payloadData.status=='Accepted'){
                    if(notificationCount==0 ){
                        query.text=[userData.name+' accepted your invite for “'+eventName+'”'];
                    }else{
                        query.text=[userData.name+' and '+notificationCount+' other accepted your invite for “'+eventName+'”'];
                    }

                    query.type=1;
                }
                if(payloadData.status=='Rejected'){
                    if(notificationCount==0 ){
                        query.text=[userData.name+' rejected your invite for “'+eventName+'”'];
                    }else{
                        query.text=[userData.name+' and '+notificationCount+' other rejected your invite for “'+eventName+'”'];
                    }
                    query.type=2;
                }
                query.templateId=templateId;
                query.categoryId=categoryId;
                query.createdFor=[creater];
                //query.$addToSet.images=userData.profilePicture
                query.eventId=payloadData.eventId;
                query.notificationType="Notification";
                query.timestamp=new Date().getTime();
                if(notificationCount==0 ) {
                    query.images=[userData.profilePicture];
                    query.createdBy=[userData._id];
                    Service.notificationServices.createNotification(query, (err, result) => {
                        if (err) {
                            cb(err);
                        }
                        else {
                            data = result;
                            cb(null);

                        }
                    })
                }else{
                    query.$addToSet={};
                    query.$addToSet.images=userData.profilePicture;
                    query.$addToSet.createdBy=userData._id;

                    Service.notificationServices.updateNotification({_id:notificationId},query,{new:true}, (err, result) => {
                        if (err) {
                            cb(err);
                        }
                        else {
                            data = result;
                            cb(null);

                        }
                    })
                }
            },
            removeNotification:(updateInvite,cb)=>{
                if(payloadData.notificationId) {
                    let query = {
                        _id:payloadData.notificationId
                    };
                    Service.notificationServices.updateNotification(query,{$addToSet: {deletedBy: userData._id}},{}, (err, result) => {
                        if (err) {
                            cb(err);
                        }
                        else {
                            cb(null)

                        }
                    })
                }else{
                    cb(null)
                }
            },
            sendPush:(getDtoken,cb)=>{
                if(creater!=null && dToken && dToken!=''){
                    let data={
                        title:'Wethaq',
                        multi:false,
                        eventId:payloadData.eventId,
                        timestamp:new Date().getTime(),
                        images:userData.profilePicture,
                    };
                    if(payloadData.status=='Accepted'){
                        data.body=userData.name+' accepted your invite for “'+eventName+'”';
                        data.type=1;
                    }
                    if(payloadData.status=='Rejected'){
                        data.body=userData.name+' rejected your invite for “'+eventName+'”';
                        data.type=2;
                    }
                    NotificationManager.sendPush(data,dToken, function (err, result) {
                        cb(null)
                    });
                }else{
                    cb(null)
                }
            }
        },
        (err,result)=>{
            if(err){
                callback(err)
            }
            else{
                callback(null);
            }
        })

};


//This is api to list user's notifications and invites
const getNotifications = (payloadData,userData,callback)=>{
    let query = {};
    query.deletedBy = {$nin: [userData._id]};
    query.createdFor = {$in: [userData._id]};
    if(payloadData.status){
        query.notificationType=payloadData.status
    }
    let projection={};
    let populateArray=[
        {
            path:'categoryId',
            select:'_id name'
        }
    ];
    let options = {lean: true,sort:{createdAt:-1}};
    console.log(' == query == ', query)
    Service.notificationServices.getNotificationPopulated(query,projection,options,populateArray,(err,result)=>{
        if(err){
            callback(err);
        }
        else{
            callback(null,result);

        }
    })
};

const  assignGuard = (payloadData,userData,callback)=>{
    console.log("payload guard assign",payloadData)
    let data;
    let creater;
    let guardId;
    let eventId;
    let categoryId;
    let templateId;
    let eventName;
    let eventImage;
    let startAt;
    let dToken='';
    let guard=false;
    let generatedString = UniversalFunctions.generateAlphaString();
    console.log("guard otp",generatedString)
    async.autoInject({
            getProfile:(cb)=>{
                Service.userServices.getUsers({phoneNo:payloadData.guardNo},{},{},(err,result)=>{
                    if(err){
                        cb(err);
                    }else{
                        if(result && result.length){
                            if(result[0].isGuard==true) {
                                guard = true;
                                dToken = result[0].deviceToken;
                                guardId = result[0]._id;
                                cb(null)
                            }else{
                                if(payloadData.language=='en'){
                                    callback(Config.APP_CONSTANTS.STATUS_MSG.ERROR.USER_ALREADY_EXIST)
                                }else if(payloadData.language=='ar') {
                                    callback(Config.APP_CONSTANTS.STATUS_MSG.ERROR.USER_ALREADY_EXIST_AR)
                                }else{
                                    callback(Config.APP_CONSTANTS.STATUS_MSG.ERROR.USER_ALREADY_EXIST)
                                }
                            }
                        }else{
                            cb(null)
                        }
                    }
                })
            },
          /*  checkGuard:(cb)=>{
                let query = {
                    _id:payloadData.eventId,
                    guards: {$elemMatch: {status:"Accepted"}},
                    isDeleted:false,
                };
                Service.eventServices.getEvent(query,{},{},(err,result)=>{
                    if(err){
                        cb(err);
                    }
                    else{
                        console.log("12223",query,result);
                        if(result && result.length){
                            if(payloadData.language=='en'){
                                callback(Config.APP_CONSTANTS.STATUS_MSG.ERROR.GUARD_ALREADY_ASSIGNED)
                            }
                            else if(payloadData.language=='ar') {
                                callback(Config.APP_CONSTANTS.STATUS_MSG.ERROR.GUARD_ALREADY_ASSIGNED_AR)
                            }
                            else{
                                callback(Config.APP_CONSTANTS.STATUS_MSG.ERROR.GUARD_ALREADY_ASSIGNED)
                            }
                        }
                        else{
                            cb(null);
                        }
                    }
                })
            },*/
            createProfile:(getProfile,cb)=>{
                  if(guard==false){
                      let query={
                          phoneNo:payloadData.guardNo,
                          isGuard:true,
                          loginType:'PhoneNo',
                          appVersion: '1.0',
                          password:UniversalFunctions.CryptData(generatedString)
                      };
                      Service.userServices.createUser(query,(err,result)=> {
                          if(err){
                              cb(err);
                          }else{
                                  guardId=result._id;
                                  cb(null)
                          }
                      })
                  }else{
                      cb(null)
                  }
            },
            assignGuards:(getProfile,createProfile,/*checkGuard,*/cb)=>{
                let query = {
                    _id:payloadData.eventId,
                };
                let options = {lean: true,new:true};
                let dataToSet = {
                    $push: {
                        guards: {
                            phoneNo:payloadData.guardNo,
                            userId: guardId
                        }
                    }
                };
                Service.eventServices.updateEvent(query,dataToSet,options,(err,result)=>{
                    if(err){
                        cb(err);
                    }
                    else{
                        data=result;
                        eventId=result._id;
                        creater=result.createdBy;
                        eventName=result.title;
                        categoryId=result.categoryId;
                        templateId=result.templateId;
                        startAt=result.startAt;
                        eventImage=result.imageUrl;
                        cb(null)

                    }
                })
            },
            sendMessage:(assignGuards,cb)=>{
                let criteria = {
                    to: [payloadData.guardNo]
                };
                if(guard==false){
                    criteria.body="You are assigned to " + eventName+".Please login using this password:"+ generatedString
                }else{
                    criteria.body="You are assigned to " + eventName+"."
                }
                console.log("pass",criteria.body)
                messageManager.sendSmsUnisoft(criteria, function ( result) {
                    console.log("sms", result);
                    cb(null)
                })
            },
            sendNotification:(assignGuards,cb)=>{
                console.log("111",data);
                if(data){

                    let query={
                        createdFor:[guardId],
                        createdBy:[userData._id],
                        type:3,
                        text:[eventName],
                        eventId:eventId,
                        timestamp:startAt,
                        categoryId:categoryId,
                        templateId:templateId,
                        notificationType:'Invite',
                        images:[{original:eventImage,thumbnail:eventImage}],
                    };
                    Service.notificationServices.createNotification(query,(err,result)=> {
                        if(err){
                            cb(err)
                        }else{
                            cb(null);
                        }
                    });
                    // console.log("1111",unRegistered,phoneNos,ids)
                }else{
                    cb(null)
                }
            },
            sendPush:(assignGuards,cb)=>{
                if(guard==true && dToken && dToken!=''){
                    let data={
                        title:'Wethaq',
                        multi:false,
                        type:3,
                        eventId:eventId,
                        timestamp:startAt,
                        images:{original:eventImage,thumbnail:eventImage},
                        body:"You are assigned to " + eventName+"."
                    };

                    NotificationManager.sendPush(data,dToken, function (err, result) {
                        cb(null)
                    });
                    // console.log("1111",unRegistered,phoneNos,ids);
                }else{
                    cb(null)
                }
            }

        },
        (err,result)=>{
            if(err){
                callback(err)
            }
            else{
                callback(null);
            }
        })

};

const guardActionInvite = (payloadData,userData,callback)=>{
    let data;
    let creater;
    let categoryId;
    let templateId;
    let eventName;
    let dToken='';
    let notificationCount=0;
    let notificationId;
    console.log("guardActionInvite",payloadData)
    async.autoInject({
            checkInvite:(cb)=>{
                let query = {
                    _id:payloadData.eventId,
                    isDeleted:false,
                    guards: {$elemMatch: {phoneNo:userData.phoneNo,status:{$ne:"Pending"}}}
                };
                Service.eventServices.getEvent(query,{},{},(err,result)=>{
                    if(err){
                        cb(err);
                    }
                    else{
                        if(result && result.length){
                            if(payloadData.language=='en'){
                                callback(Config.APP_CONSTANTS.STATUS_MSG.ERROR.ACTION_ALREADY_DONE)
                            }
                            else if(payloadData.language=='ar') {
                                callback(Config.APP_CONSTANTS.STATUS_MSG.ERROR.ACTION_ALREADY_DONE_AR)
                            }
                            else{
                                callback(Config.APP_CONSTANTS.STATUS_MSG.ERROR.ACTION_ALREADY_DONE)
                            }
                        }
                        else{
                            cb(null);
                        }
                    }
                })
            },
            updateInvite:(checkInvite,cb)=>{
                let query = {
                    _id:payloadData.eventId,
                    isDeleted:false,
                    guards: {$elemMatch: {phoneNo:userData.phoneNo}}
                };
                let options = {new:true};
                let dataToSet = {};
                if(payloadData.status=='Accepted'){
                    dataToSet['guards.$.status']="Accepted";
                    dataToSet.guardId=userData._id
                }
                if(payloadData.status=='Rejected'){
                    dataToSet['guards.$.status']="Rejected";
                }
                dataToSet['guards.$.updatedAt']=new Date().getTime();
                Service.eventServices.updateEvent(query,dataToSet,options,(err,result)=>{
                    if(err){
                        cb(err);
                    }
                    else{
                        data=result;
                        creater=result.createdBy;
                        eventName=result.title;
                        categoryId=result.categoryId;
                        templateId=result.templateId;
                        cb(null)

                    }
                })
            },
            checkNotification:(cb)=>{
                if(payloadData.status=='Rejected') {
                    let query = {
                        eventId: payloadData.eventId,
                    };
                    query.type = 5;
                    Service.notificationServices.getNotificationTag(query, {}, {}, function (err, result) {
                        if (err) {
                            cb(err)
                        } else {
                            console.log("result", result);
                            if (result && result.length) {
                                notificationCount = result[0].createdBy.length;
                                notificationId = result[0]._id;
                                cb(null)
                            } else {
                                cb(null)
                            }
                        }
                    })
                }else{
                    cb(null)
                }
            },
            getDtoken:(updateInvite,cb)=>{
                if(creater!=null) {
                    Service.userServices.getUsers({_id: creater}, {deviceToken: 1}, {lean: true}, (err, result) => {
                        if (err)
                            cb(err);
                        else {
                            if (result && result.length) {
                                dToken = result[0].deviceToken;
                                cb(null)
                            } else {
                                cb(null)
                            }
                        }
                    })
                }else{
                    cb(null)
                }
            },
            createNotification:(checkNotification,updateInvite,cb)=>{
                console.log("2222",notificationCount);
                let query = {};
                if(payloadData.status=='Accepted'){
                    query.text=[userData.name+' accepted your guard request for “'+eventName+'”'];
                    query.type=4;
                }
                if(payloadData.status=='Rejected'){
                    if(notificationCount==0 ){
                        query.text=[userData.name+' rejected your guard request for “'+eventName+'”'];
                    }else{
                        query.text=[userData.name+' and '+notificationCount+' other rejected your guard request for “'+eventName+'”'];
                    }
                    query.type=5;
                }
                query.templateId=templateId;
                query.categoryId=categoryId;
                query.createdFor=[creater];
                query.eventId=payloadData.eventId;
                query.notificationType="Notification";
                query.timestamp=new Date().getTime();
                if(notificationCount==0 ) {
                    query.images=[userData.profilePicture];
                    query.createdBy=[userData._id];
                    Service.notificationServices.createNotification(query, (err, result) => {
                        if (err) {
                            cb(err);
                        }
                        else {
                            data = result;
                            cb(null);

                        }
                    })
                }else{
                    query.$addToSet={};
                    query.$addToSet.images=userData.profilePicture;
                    query.$addToSet.createdBy=userData._id;

                    Service.notificationServices.updateNotification({_id:notificationId},query,{new:true}, (err, result) => {
                        if (err) {
                            cb(err);
                        }
                        else {
                            data = result;
                            cb(null);

                        }
                    })
                }
            },
            sendPush:(getDtoken,cb)=>{
                if(creater!=null && dToken && dToken!=''){
                    let data={
                        title:'Wethaq',
                        multi:false,
                        eventId:payloadData.eventId,
                        timestamp:new Date().getTime(),
                        images:userData.profilePicture,
                    };
                    if(payloadData.status=='Accepted'){
                        data.body=userData.name+' accepted your guard request for “'+eventName+'”';
                        data.type=4;
                    }
                    if(payloadData.status=='Rejected'){
                        data.body=userData.name+' rejected your guard request for “'+eventName+'”';
                        data.type=5;
                    }
                    NotificationManager.sendPush(data,dToken, function (err, result) {
                        cb(null)
                    });
                    // console.log("1111",unRegistered,phoneNos,ids);
                }else{
                    cb(null)
                }
            },
            removeNotification:(checkNotification,updateInvite,cb)=>{
                    let query = {};
                    let dataToSet={};
                    if(payloadData.status=="Accepted") {
                        query.eventId=payloadData.eventId;
                        query.type=3;
                        dataToSet.$addToSet={deletedBy:userData._id};
                        // dataToSet.createdFor=[];
                    }
                    if(payloadData.status=="Rejected") {
                        query.eventId=payloadData.eventId;
                        query.type=3;
                        // dataToSet.createdFor=[];
                        dataToSet.$addToSet={deletedBy:userData._id};
                    }
                    console.log("1111",query,dataToSet)
                    Service.notificationServices.updateAllNotification(query,dataToSet,{multi:true}, (err, result) => {
                        if (err) {
                            cb(err);
                        }
                        else {
                            cb(null)

                        }
                    })
            },
        },
        (err,result)=>{
            if(err){
                callback(err)
            }
            else{
                callback(null);
            }
        })

};


const guardCheckIns = (payloadData,userData,callback)=>{
    let userPhone;
    console.log("checkin",payloadData); 

    
    async.autoInject({
            checkEvent:(cb)=>{
                if(payloadData.eventId) {
                    let query = {
                        _id:payloadData.eventId,
                        // guardId:userData._id
                    };

                    query.guards={$elemMatch: {userId:userData._id,status:"Accepted"}}
               console.log("===== query =====", query)
                    Service.eventServices.getEvent(query, {}, {lean: true}, (err, result) => {
                      console.log("== event Details ==",result)
                        if (err) {
                            cb(err)
                        } else {
                            if (result && result.length) {
                                console.log(11111,result)
                                cb(null)
                            } else {
                                callback(Config.APP_CONSTANTS.STATUS_MSG.ERROR.WRONG_EVENT)
                            }
                        }
                    })
                }else{
                    cb(null)
                }
            },
            checkInvite:(checkEvent,cb)=>{
                // console.log(' == == ',checkEvent);
                let query = {
                    _id:mongoose.Types.ObjectId(payloadData.eventId),
                    isDeleted:false,
                };
                if(payloadData.userId){
                    query.invites={$elemMatch: {phoneNo:payloadData.userId, 
                        checkedAt:{$ne:0}
                    }}
                }
                if(payloadData.phoneNo){
                    query.invites={$elemMatch: {phoneNo:payloadData.phoneNo,
                        checkedAt:{$ne:0}
                    }}
                }
                Service.eventServices.getEvent(query,{},{},(err,result)=>{
                    if(err){
                        cb(err);
                    }
                    // else{
                        console.log("==resulttt ==", result)
                        if(result.length){
                            if(payloadData.language=='en'){
                                callback(Config.APP_CONSTANTS.STATUS_MSG.ERROR.CHECK_IN_ALREADY_DONE)
                            }else if(payloadData.language=='ar') {
                                callback(Config.APP_CONSTANTS.STATUS_MSG.ERROR.CHECK_IN_ALREADY_DONE_AR)
                            }else{
                                callback(Config.APP_CONSTANTS.STATUS_MSG.ERROR.CHECK_IN_ALREADY_DONE)
                            }
                        }
                        else{
                            cb(null);
                        }
                    // }
                })
            },
            updateInvite:(checkInvite,cb)=>{
                console.log(2222)
                let query = {
                    _id:mongoose.Types.ObjectId(payloadData.eventId),
                    isDeleted:false,
                };

                if(payloadData.userId){
                    query.invites={$elemMatch: {phoneNo:payloadData.userId}}
                }
                if(payloadData.phoneNo){
                    query.invites={$elemMatch: {phoneNo:payloadData.phoneNo}}
                }
                let options = {lean: true,new:true};
                let dataToSet = {
                    $inc: {checked:1},
                    $addToSet: { checkIns: payloadData.userId },
                    'invites.$.checkedAt': new Date().getTime(),
            };
                if(payloadData && payloadData.count && payloadData.count != 0){
                    dataToSet['$inc']  = {"invites.$.inviteCount": - payloadData.count }
                }
               
                // console.log("criteria",query);
                Service.eventServices.updateEvent(query,dataToSet,options,(err,result)=>{
                    // console.log("==event==",err,result);
                    if(err){
                        cb(err);
                    }
                    else{
                        cb(null)
                    }
                })
            },
            sendSms:(updateInvite,cb)=>{
                let criteria = {};
                if(payloadData.userId){
                    criteria.to=[payloadData.userId]
                }
                if(payloadData.phoneNo){
                    criteria.to=[payloadData.phoneNo]
                }
                criteria.body="Welcome to the event";
                // console.log("criteria checkin",criteria);
                messageManager.sendSmsUnisoft(criteria, function (result1) {
                        cb(null);
                });
            },
        },
        (err,result)=>{
            if(err){
                callback(err)
            }
            else{
                callback(null);
            }
        })

};

//This is api to add more guests in event
const addInvites = (payloadData,userData,callback)=>{
    let data;
    let invites;
    let title;
    let message='';
    let startAt;
    let categoryId;
    let templateId;
    let imageUrl;
    let eventId;
    let inviteAt;
    if(payloadData.invites) {
        invites = payloadData.invites
    }
    let ids=[];
    let qrs=[];
    let qrCode=false;
    let baseRate=0;
    let addedWallet=payloadData.addedWallet||0;
    let unRegistered=[];
    let dTokens=[];
    let existing=[];
    let invitees=[];
    let oldNos=[];
    let location=[]
    let map="";
    let lang="en";
    let inviteeLength=invites.length;
    let messageSent=false;
    let startTime=new Date().setUTCMinutes(new Date().getUTCMinutes()-1);
    let endTime=new Date().setUTCMinutes(new Date().getUTCMinutes()+1);
    // console.log("payload",payloadData);
    async.autoInject({
            getEvent:(cb)=>{
                Service.eventServices.getEvent({_id:payloadData.eventId},{},{},(err,result)=>{
                    if(err) {
                        cb(err)
                    }else{
                        if(result && result.length>0) {
                        //   console.log('==result==',result)
                            qrCode=result[0].isQrCode;
                                eventId=result[0]._id;
                                title=result[0].title;
                                message=result[0].message;
                                lang=result[0].lang;
                                categoryId=result[0].categoryId;
                                templateId=result[0].templateId;
                                imageUrl=result[0].imageUrl;
                                startAt=result[0].startAt;
                                inviteAt=result[0].inviteAt;
                                location=result[0].location;
                                baseRate=result[0].smsPrice/result[0].totalSms;
                                data=result[0];
                                if (result[0].invites.length) {
                                    let flag=0;
                                    // console.log("invites",result[0].invites)
                                    for (let i = 0; i < result[0].invites.length; i++) {
                                        (function (i) {
                                            oldNos.push({
                                                phoneNo:result[0].invites[i].phoneNo,
                                                name:result[0].invites[i].name,
                                                email:result[0].invites[i].email,
                                                inviteCount:result[0].invites[i].inviteCount
                                            });
                                            flag++;
                                            if (flag == result[0].invites.length) {
                                                // console.log("first",invites);
                                                // console.log("second",oldNos);
                                                invites=(_.differenceBy(invites,oldNos, 'phoneNo'));
                                                inviteeLength=invites.length;
                                                // console.log("third",invites);
                                                cb(null);
                                            }
                                        }(i))
                                    }
                                }else{
                                    /*if(result[0].location && result[0].location!=[0,0]) {
                                        googleUrl.shorten("https://www.google.com/maps?q=" + result[0].location[1] + "," + result[0].location[0], function (err, shortUrl) {
                                            console.log("123map", shortUrl, err)
                                            map = shortUrl.toString();
                                            cb(null);
                                        });
                                    }else{
                                        cb(null)
                                    }*/
                                    cb(null)
                                }
                        }else{
                            cb(null)
                        }
                    }
                })
            },
            updateWallet:(getEvent,cb)=>{
                if((addedWallet+userData.wallet)<inviteeLength){
                    Service.userServices.updateUser({_id:userData._id},{wallet:addedWallet+userData.wallet},{new:true,lean:true},(err,result)=>{
                        if(err){
                            cb(err)
                        }else{
                            callback(Config.APP_CONSTANTS.STATUS_MSG.ERROR.INSUFFICIENT_BALANCE)
                        }
                    })
                }
                if((addedWallet+userData.wallet)>=inviteeLength){
                    messageSent=true;
                    Service.userServices.updateUser({_id:userData._id},{wallet:((addedWallet+userData.wallet)-inviteeLength)},{new:true,lean:true},(err,result)=>{
                        if(err){
                            cb(err)
                        }else{
                            cb(null)
                        }
                    })
                }
            },
            checkInvites:(updateWallet,cb)=>{
                console.log("check invites",invites.length,inviteAt,startTime,endTime)
                if(payloadData.invites && invites.length /*&& inviteAt>startTime && inviteAt<endTime*/){
                    console.log("==invites==",invites);
                    let flag = 0;
                    for (let i = 0; i < invites.length; i++) {
                        (function (i) {
                            console.log("invites[i].name",invites[i].name)
                            let id=eventId.toString();
                            let number=(invites[i].phoneNo.replace("+","")).toString().replace(/\s/g, '');
                        //    let link='http://34.211.166.21/wethaq_admin/#/invitation/'+id+'/'+number;
                        // let link = 'http://34.211.166.21/wethaq_dev/wethaq_admin/#/invitation/'+id+'/'+number;   
                        let link='http://admin.wethaqapp.net/wethaq_admin/#/invitation/'+id+'/'+number;
                            let subject = "Event Invite";
                            TinyURL.shorten(link, function(res) {
                                link = res
                                // let template ='<html><head></head><body><table style="border-collapse:collapse;table-layout:fixed;min-width:320px;width:100%;background-color:#fff;text-align:left;direction:ltr"><tbody><tr><td><div style=";text-align:left;direction:ltr"><div style="Margin:0 auto;max-width:560px;min-width:280px;width:280px;width:calc(28000% - 167440px);text-align:left;direction:ltr"> <br></div><div style="Margin:0 auto;max-width:600px;min-width:320px;width:320px;width:calc(28000% - 167400px);text-align:left;direction:ltr"><div style="font-size:26px;line-height:32px;Margin-top:6px;Margin-bottom:20px;color:#c3ced9;font-family:Roboto,Tahoma,sans-serif;Margin-left:20px;Margin-right:20px;text-align:left;direction:ltr" align="center"><div align="center" style=";text-align:left;direction:ltr"> <img style="display:block;height:auto;width:100%;border:0;max-width:230px" src="https://s3-us-west-2.amazonaws.com/wethaqbucket/profilePic_1523699389214imageBackground.png" alt="" width="230"></div></div></div></div><div style=";text-align:left;direction:ltr"><div style="Margin:0 auto;max-width:600px;min-width:320px;width:320px;width:calc(28000% - 167400px);word-wrap:break-word;word-break:break-word;text-align:left;direction:ltr"><div style="border-collapse:collapse;display:table;width:100%;background-color:#ffffff;text-align:left;direction:ltr"><div style="text-align:left;color:#8f8f8f;font-size:16px;line-height:24px;font-family:Open Sans,sans-serif;max-width:600px;min-width:320px;width:320px;width:calc(28000% - 167400px);text-align:left;direction:ltr"><div style="Margin-left:20px;Margin-right:20px;Margin-top:24px;text-align:left;direction:ltr"><div style=";text-align:left;direction:ltr"><h1 style="Margin-top:0;Margin-bottom:20px;font-style:normal;font-weight:normal;color:#404040;font-size:28px;line-height:36px;text-align:center;text-align:left;direction:ltr">'+title+'</h1></div></div><div style="Margin-left:20px;Margin-right:20px;text-align:left;direction:ltr"><div style="line-height:10px;font-size:1px;text-align:left;direction:ltr"></div></div><div style="Margin-left:20px;Margin-right:20px;text-align:left;direction:ltr"><div style="line-height:10px;font-size:1px;text-align:left;direction:ltr"></div></div><div style="Margin-left:20px;Margin-right:20px;text-align:left;direction:ltr"><div style=";text-align:left;direction:ltr"><h1 style="Margin-top:0;Margin-bottom:0;font-style:normal;font-weight:normal;color:#404040;font-size:28px;line-height:36px;text-align:center;text-align:left;direction:ltr">'+invites[i].name+'أهلا بك</h1><h1 style="Margin-top:20px;Margin-bottom:0;font-style:normal;font-weight:normal;color:#404040;font-size:28px;line-height:36px;text-align:center;text-align:left;direction:ltr"> <strong>مرحباً بكم في تطبيق وثاق </strong></h1><h3 style="Margin-top:20px;Margin-bottom:12px;font-style:normal;font-weight:normal;color:#706f70;font-size:17px;line-height:26px;font-family:Cabin,Avenir,sans-serif;text-align:center;text-align:left;direction:ltr">التصميم المخصص للدعوة</h3></div></div><div style="font-size:12px;font-style:normal;font-weight:normal;line-height:19px;text-align:left;direction:ltr" align="center"> <img style="border:0;display:block;height:auto;width:100%;max-width:900px" alt="" width="600" src='+imageUrl+'></div><div style="Margin-left:20px;Margin-right:20px;text-align:left;direction:ltr"><div style=";text-align:left;direction:ltr"><h2>'+link+' : إليك الرابط </h2></div></div></div></div></div></div></td></tr></tbody></table></body></html>';
                                //     UniversalFunctions.sendMail(invites[i].email, subject, template, function (err,result) {
                                //         console.log("res",err,result)
                                //     });
                                invites[i].status = 'Pending';
                                invites[i].createdAt = +new Date();
                                
                                let query = {
                                    phoneNo: invites[i].phoneNo,
                                    _id: {$ne: userData._id}
                                };
                                Service.userServices.getUsers(query, {}, {lean: true}, function (err, result) {
                                    if (err) {
                                        cb(err)
                                    } else {
                                        if (result.length) {
                                            existing.push({"_id": result[0]._id, "phoneNo": invites[i].phoneNo});
                                            ids.push(result[0]._id);
                                            dTokens.push(result[0].deviceToken);
                                        }
                                        let criteria = {
                                            to: [invites[i].phoneNo],
                                        };
                                        // console.log("temp",template);
                                        // criteria.body+="From: "+userData.name+"\n";
                                        criteria.body = "";
                                        if (message && message != '') {
                                            if (lang == "en") {
                                                criteria.body += "Hello " + invites[i].name + ", " + message + "\n";
                                            } else if (lang == "ar") {
                                                criteria.body += " مرحبًا" + "\u202A" + invites[i].name + ", " + "\u202C" + message + "\n";
                                            } else {
                                                criteria.body += " مرحبًا" + "\u202A" + invites[i].name + ", " + "\u202C" + message + "\n";
                                            }
                                            //   criteria.body+="Hello "+invites[i].name+", "+ message + "\n";
                                        }
                                        else {
                                            if (lang == "en") {
                                                criteria.body += "Hello " + invites[i].name + " It’s an honor to invite you to " + title + "\n";
                                            } else if (lang == "ar") {
                                                criteria.body += " أهلا" + "\u202A" + invites[i].name + "\u202C" + " نتشرف بدعوتك لحضور مناسبة " + "\u202A" + title + "\n";
                                            } else {
                                                criteria.body += " أهلا" + "\u202A" + invites[i].name + "\u202C" + " نتشرف بدعوتك لحضور مناسبة " + "\u202A" + title + "\n";
                                            }
                                        }
                                        let template = '<html><head></head><body><table style="border-collapse:collapse;table-layout:fixed;min-width:320px;width:100%;background-color:#fff;text-align:left;direction:ltr"><tbody><tr><td><div style=";text-align:left;direction:ltr"><div style="Margin:0 auto;max-width:560px;min-width:280px;width:280px;width:calc(28000% - 167440px);text-align:left;direction:ltr"> <br></div><div style="Margin:0 auto;max-width:600px;min-width:320px;width:320px;width:calc(28000% - 167400px);text-align:left;direction:ltr"><div style="font-size:26px;line-height:32px;Margin-top:6px;Margin-bottom:20px;color:#c3ced9;font-family:Roboto,Tahoma,sans-serif;Margin-left:20px;Margin-right:20px;text-align:left;direction:ltr" align="center"><div align="center" style=";text-align:left;direction:ltr"> <img style="display:block;height:auto;width:100%;border:0;max-width:230px" src="https://s3-us-west-2.amazonaws.com/wethaqbucket/profilePic_1523699389214imageBackground.png" alt="" width="230"></div></div></div></div><div style=";text-align:left;direction:ltr"><div style="Margin:0 auto;max-width:600px;min-width:320px;width:320px;width:calc(28000% - 167400px);word-wrap:break-word;word-break:break-word;text-align:left;direction:ltr"><div style="border-collapse:collapse;display:table;width:100%;background-color:#ffffff;text-align:left;direction:ltr"><div style="text-align:left;color:#8f8f8f;font-size:16px;line-height:24px;font-family:Open Sans,sans-serif;max-width:600px;min-width:320px;width:320px;width:calc(28000% - 167400px);text-align:left;direction:ltr"><div style="Margin-left:20px;Margin-right:20px;Margin-top:24px;text-align:left;direction:ltr"><div style=";text-align:left;direction:ltr"><h1 style="Margin-top:0;Margin-bottom:20px;font-style:normal;font-weight:normal;color:#404040;font-size:28px;line-height:36px;text-align:center;text-align:left;direction:ltr">' + title + '</h1></div></div><div style="Margin-left:20px;Margin-right:20px;text-align:left;direction:ltr"><div style="line-height:10px;font-size:1px;text-align:left;direction:ltr"></div></div><div style="Margin-left:20px;Margin-right:20px;text-align:left;direction:ltr"><div style="line-height:10px;font-size:1px;text-align:left;direction:ltr"></div></div><div style="Margin-left:20px;Margin-right:20px;text-align:left;direction:ltr"><div style=";text-align:left;direction:ltr"><h1 style="Margin-top:0;Margin-bottom:0;font-style:normal;font-weight:normal;color:#404040;font-size:20px;line-height:36px;text-align:center;text-align:left;direction:ltr">' + invites[i].name + 'أهلا بك</h1><h1 style="Margin-top:20px;Margin-bottom:0;font-style:normal;font-weight:normal;color:#404040;font-size:28px;line-height:36px;text-align:center;text-align:left;direction:ltr"> <strong>مرحباً بكم في تطبيق وثاق </strong></h1><h3 style="Margin-top:20px;Margin-bottom:12px;font-style:normal;font-weight:normal;color:#706f70;font-size:17px;line-height:26px;font-family:Cabin,Avenir,sans-serif;text-align:center;text-align:left;direction:ltr">التصميم المخصص للدعوة</h3><h3 style="Margin-top:20px;Margin-bottom:12px;font-style:normal;font-weight:normal;color:#706f70;font-size:17px;line-height:26px;font-family:Cabin,Avenir,sans-serif;text-align:center;text-align:left;direction:ltr">' + criteria.body + '</h3></div></div><div style="font-size:12px;font-style:normal;font-weight:normal;line-height:19px;text-align:left;direction:ltr" align="center"> <img style="border:0;display:block;height:auto;width:100%;max-width:900px" alt="" width="600" src=' + imageUrl + '></div><div style="Margin-left:20px;Margin-right:20px;text-align:left;direction:ltr"><div style=";text-align:left;direction:ltr"><h2>' + link + ' : إليك الرابط</h2></div></div></div></div></div></div></td></tr></tbody></table></body></html>'

                                        // UniversalFunctions.sendMail(invites[i].email, subject, template, function (err, result) {
                                        //     console.log("==res==", err, result)
                                        //     cb(null)
                                        // });
                                        if (lang == "en") {
                                            criteria.body += "For more info, please check the link: " + "\n";
                                        } else if (lang == "ar") {
                                            criteria.body += "وللمزيد من المعلومات الرجاء فتح الرابط التالي: " + "\n";
                                        } else criteria.body += "وللمزيد من المعلومات الرجاء فتح الرابط التالي" + "\n";
                                        criteria.body += link;
                                        // if(payloadData.packageType == Config.APP_CONSTANTS.DATABASE.PACKAGES.PREMIUM) {
                                        //     cb(null);
                                            messageManager.sendSmsUnisoft(criteria, function (result) {
                                                console.log("sms",criteria, err, result);
                                                console.log( "== invites ==",invites[i])
                                                // if (err == null) {
                                                if (result.success == "true") {
                                                   console.log("= invite =", invites)
                                                    invites[i].statusCode = 200;
                                                    invites[i].createdAt = new Date().getTime();
                                                    // invites[i].message = [result.Data.MessageEn, result.Data.MessageAr];
                                                    invites[i].message = ["SMS sent successfully", "تم الإرسال"];
                                                    invitees.push(invites[i]);
                                                } else {
                                                    // console.log("1234555",err);
                                                    invites[i].statusCode = 400;
                                                    invites[i].createdAt = new Date().getTime();
                                                    // invites[i].message = [err.MessageEn, err.MessageAr];
                                                    invites[i].message = ["Mobile(s) number(s) is not specified or incorrect", "الأرقام المرسل لها غير صحيحه أو فارغه"];
                                                    invitees.push(invites[i]);
                                                }
                                                flag++;
                                                console.log("== FLAG ==",flag,"== INVITE-Length ==",invites.length,
                                                "== Invitees ==", invitees)
                                                if (flag == invites.length) {
                                                   console.log("callback console")
                                                    cb(null);
                                                }
                                            });
                                        // }
                                    }
                                })
                            })

                        }(i))
                    }
                    // console.log("1111",unRegistered,ids)
                }else{
                    cb(null)
                }
            },
            createQr:(updateWallet,cb)=>{
                if(qrCode==true && invites && invites.length  /*&& inviteAt>startTime*/ && inviteAt<endTime){
                    // console.log("payloadData qrCode",qrCode);
                    let flag=0;
                    if(payloadData.inviteCount == null){
                        payloadData.inviteCount = 1;
                    }else{
                        payloadData.inviteCount = payloadData.inviteCount - 1
                    }
                    for(let i=0;i<invites.length;i++){
                        (function (i) {
                            let  pic = "qr-userId="+ invites[i].phoneNo+"eventId=" + eventId+"inviteCount="+payloadData.inviteCount;
                            let qr_svg = qr.image(pic, {type: 'png'});
                            let date=new Date().getTime();
                            let stream = qr_svg.pipe(require('fs').createWriteStream('uploads/' +date+'eventId=' + eventId+'.png'));
                            stream.on('finish', function (err, result) {
                                let  qrImage = Path.resolve(".") + "/uploads/" +date+'eventId=' + eventId+'.png';
                                let client = knox.createClient({
                                    key: Config.awsS3Config.s3BucketCredentials.accessKeyId
                                    , secret: Config.awsS3Config.s3BucketCredentials.secretAccessKey
                                    , bucket: Config.awsS3Config.s3BucketCredentials.bucket
                                });
                                let s3ClientOptions = {'x-amz-acl': 'public-read'};
                                client.putFile(qrImage,date+ eventId+'.png', s3ClientOptions, function (err, res) {
                                    console.log(err);
                                    deleteFile(qrImage);
                                    _.map(invites, function(obj) {
                                        if (obj.phoneNo ==invites[i].phoneNo)
                                            obj.qr=Config.awsS3Config.s3BucketCredentials.s3URL +date + eventId+  '.png';
                                    });
                                    qrs.push({"qr":Config.awsS3Config.s3BucketCredentials.s3URL +date + eventId+ '.png',"userId":invites[i].phoneNo});
                                    flag++;
                                    if (flag == invites.length) {
                                        // console.log("qr",qrs);
                                        cb(null);
                                    }

                                });
                            });
                        }(i))
                    }
                }else{
                    cb(null)
                }
            },
            insertEvent:(checkInvites,createQr,cb)=>{
                // console.log("===12345====",invitees);
                let query = {
                    _id:payloadData.eventId
                };
                let dataToSet ={};
                if(invitees && invitees.length){
                    if(qrs && qrs.length) {
                        dataToSet.$addToSet={qrCodes:{$each:qrs},invites: {$each: invitees},unRegistered: {$each: unRegistered}};
                    }else{
                        dataToSet.$addToSet={invites: {$each: invitees},unRegistered: {$each: unRegistered}};
                    }
                    dataToSet.$inc={pending: invitees.length,total:invitees.length};
                }else{
                    dataToSet.$addToSet={invites: {$each: invites}};
                    dataToSet.$inc={pending: (invites).length,total:(invites).length};
                }
                // console.log("update event",dataToSet);
                Service.eventServices.updateEvent(query,dataToSet,{lean:true,new:true},(err,result)=>{
                    if(err){
                        cb(err);
                    }
                    else{
                        cb(null);
                    }
                })
            },
            sendNotificaiton:(insertEvent,checkInvites,cb)=>{
                if(ids && ids.length){
                    // console.log("notification",ids);
                    let query={
                        createdFor:ids,
                        createdBy:[userData._id],
                        type:0,
                        text:[title],
                        eventId:payloadData.eventId,
                        timestamp:startAt,
                        categoryId:categoryId,
                        templateId:templateId,
                        notificationType:'Invite',
                        images:[{original:imageUrl,thumbnail:imageUrl}],
                    };
                    Service.notificationServices.createNotification(query,function (err,result) {
                        if(err){
                            cb(err)
                        }else{
                            cb(null);
                        }
                    });
                }else{
                    cb(null)
                }
            },
        },
        (err,result)=>{
            if(err){
                callback(err);
            }
            else{
                // console.log("1111",unRegistered,ids);
                callback(null,data);
            }
        })
};

const importCsvXl = function (payloadData, userData, callback) {
    console.log("1111111111111111", payloadData);
    let path = payloadData.file.path;
    let model = null;
    let data;
    let formatStatus=true;
    async.auto({
        sendMailUser: function (cb) {
            mongoXlsx.xlsx2MongoData(path, model, (err, mongoData)=> {
                if (err) {
                    cb(err)
                }
                else {
                    data = mongoData;
                    console.log("2212121", data);
                    if (data.length) {
                        let fields = ['name','email','phoneNo'];
                        for(let i=0; i<data.length;i++){
                           // console.log("number",data[i])
                            if(data[i].phoneNo)data[i].phoneNo=(data[i].phoneNo).toString()
                        }
                        for (let key in data[0]) {
                            if (fields.indexOf(key) < 0) {
                                formatStatus = false;
                                break;
                            }
                        }
                        if (!formatStatus) {
                            cb(Config.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_KEYS)
                        } else {
                            cb()
                        }
                    } else {
                        cb(Config.APP_CONSTANTS.STATUS_MSG.ERROR.EMPTY_FILE);
                    }
                }
            });
        },
    }, function (err, result) {
        if (err)
            callback(err);
        else {
            callback(null,data)
        }

    })
};

const forgotPassword = function (payloadData, callback) {
    let data;
    let generatedString;
    console.log("1111111111",payloadData);
    async.autoInject({
            getUser:(cb)=>{
                let criteria = {
                    phoneNo: payloadData.phoneNo,
                };
                let projection = {};
                Service.userServices.getUsers(criteria, projection, {}, function (err, result) {
                    if (err) {
                        cb(err)
                    } else {
                        if (result.length) {
                            if(result[0].loginType=="PhoneNo"){
                                cb(null)
                            }else if(result[0].loginType=="Facebook" || result[0].loginType=="Google"){
                                callback(Config.APP_CONSTANTS.STATUS_MSG.ERROR.SOCIAL_FOUND);
                            }
                        } else {
                            callback(Config.APP_CONSTANTS.STATUS_MSG.ERROR.USER_NOT_FOUND);
                        }
                    }
                })
            },
            sendOtp:(getUser,cb)=> {
                generatedString = UniversalFunctions.generateRandomString();
                console.log("forgot ",generatedString)
                let obj = {
                    phoneNo: payloadData.phoneNo
                };
                let dataToSet = {
                    password: UniversalFunctions.CryptData(generatedString),
                };
                Service.userServices.updateUser(obj, dataToSet, {new: true}, function (err, result) {
                    if (err) {
                        cb(err);
                    } else {
                        data = result;
                        let criteria = {
                            to: [payloadData.phoneNo],
                            body: "Your new Password for the Wethaq is " + generatedString
                        };
                        messageManager.sendSmsUnisoft(criteria, function ( result) {
                            console.log("sms", result);
                            cb(null);
                        });

                    }
                });

            },
        }, function (err, result) {
            if (err) {
                callback(err)
            } else {
                callback(null)
            }
        }
   )
};

const applyPromo = function (payloadData,userData, callback) {
    let currentTime=new Date().getTime();
    let data={};
  //  console.log("1111111111",payloadData);
    async.autoInject({
            getCoupon:(cb)=>{
                let criteria = {
                    couponCode: payloadData.couponCode
                };
                let projection = {};
                Service.promoCodeServices.getPromoCode(criteria, projection, {}, function (err, result) {
                    if (err) {
                        cb(err)
                    } else {
                        if (result.length) {
                            if(result[0].startAt>currentTime){
                                callback(Config.APP_CONSTANTS.STATUS_MSG.ERROR.COUPON_NOT_ACTIVE);
                            }else if(result[0].endAt<currentTime){
                                callback(Config.APP_CONSTANTS.STATUS_MSG.ERROR.COUPON_EXPIRED);
                            }else if(((result[0].usedBy).toString()).indexOf(userData._id)!=-1){
                                callback(Config.APP_CONSTANTS.STATUS_MSG.ERROR.COUPON_USED);
                            }else{
                                data=result[0];
                                cb(null)
                            }
                        } else {
                            callback(Config.APP_CONSTANTS.STATUS_MSG.ERROR.COUPON_NOT_FOUND);
                        }
                    }
                })
            }
        }, function (err, result) {
            if (err) {
                callback(err)
            } else {
                callback(null,data)
            }
        }
   )
};

const completeOffer = function (payloadData,userData, callback) {
    //console.log("payload",payloadData);
    async.autoInject({
            updateStatus:(cb)=>{
                if(payloadData.msgId && payloadData.msgId!='') {
                    let obj = {
                        _id: payloadData.requestId,
                        message: {$elemMatch: {_id: payloadData.msgId}}
                    };
                    let dataToSet = {};

                    if (payloadData.status) {
                        dataToSet['message.$.status'] = payloadData.status
                    }
                    console.log("criteria", dataToSet, obj);
                    Service.requestServices.updateRequest(obj, dataToSet, {
                        lean: true,
                        new: true
                    }, function (err, result) {
                        if (err) {
                            cb(err);
                        } else {
                            console.log("result", result);
                            cb(null);
                        }
                    })
                }else{
                    cb(null)
                }
             },
            updateRequest:(cb)=>{
                let obj = {
                    _id: payloadData.requestId
                };
                let text="";
                let type="";
                if(payloadData.status=="Accepted") {
                    text="Offer Accepted.";
                    type="Accept";
                }
                if(payloadData.status=="Rejected") {
                    text="Offer Rejected.";
                    type="Reject";
                }
                let dataToSet = {
                    status:payloadData.status,
                    $push: {
                        message: {
                            senderId: userData._id,
                            text:[text],
                            url: {original: "", thumbnail: ""},
                            msgType: type,
                            cost: 0,
                            duration: 0,
                            htmlUrl:"",
                            timeStamp: Date.now()
                        }
                    },
                };
                if(payloadData.status=="Accepted"){
                    dataToSet.designAccept=true
                }
                if(payloadData.status=="Rejected"){
                    dataToSet.$inc={rejectTimes:1};
                }
                Service.requestServices.updateRequest(obj, dataToSet, {lean:true,new: true}, function (err, result) {
                    if (err) {
                        cb(err);
                    } else {
                        cb(null);
                    }
                })
            },
            createTransction:(updateRequest,cb)=>{
                if(payloadData.status=="Accepted") {
                    let criteria = {
                        userId:userData._id,
                        requestId:payloadData.requestId,
                        type:"Request"
                    };
                    if(payloadData.price){
                        criteria.price=payloadData.price
                    }if(payloadData.trxId){
                        criteria.trxId=payloadData.trxId
                    }

                    Service.transactionServices.createTransaction(criteria, function (err, result) {
                        if (err) {
                            cb(err);
                        } else {
                            cb(null);
                        }
                    })
                }else{
                    cb(null)
                }
            }
        }, function (err, result) {
        console.log("request",err,result)
            if (err) {
                callback(err)
            } else {
                callback(null)
            }
        }
    )

};

const sendOtp=(payloadData,callback)=>{
    let otp= Math.floor(Math.random() * (9999 - 1000) + 1000);
    async.autoInject({
            checkPhone:(cb)=>{
                if(payloadData.phoneNo) {
                    let query = {
                        phoneNo:payloadData.phoneNo
                    };
                    if(payloadData.userId){
                        query._id={$ne:payloadData.userId}
                    }
                    checkAlreadyRegisteredUser(query,(err,result)=>{
                        if(err){
                            cb(err)
                        }
                        else{
                            if(result==true){
                                if(payloadData.language=='en'){
                                    cb(Config.APP_CONSTANTS.STATUS_MSG.ERROR.DUPLICATE_PHONE_NO)
                                }else if(payloadData.language=='ar'){
                                    cb(Config.APP_CONSTANTS.STATUS_MSG.ERROR.DUPLICATE_PHONE_NO_AR)
                                }else{
                                    cb(Config.APP_CONSTANTS.STATUS_MSG.ERROR.DUPLICATE_PHONE_NO)
                                }
                            }
                            else{
                                cb(null);
                            }
                        }
                    })
                }else{
                    cb(null)
                }
            },
          checkEmail:(cb)=>{
            if(payloadData.email){
                let query = {
                    email:payloadData.email
                };
                if(payloadData.userId){
                    query._id={$ne:payloadData.userId}
                }
                checkAlreadyRegisteredUser(query,(err,result)=>{
                    if(err){
                        cb(err)
                    }
                    else{
                        if(result==true){
                            if(payloadData.language=='en'){
                                cb(Config.APP_CONSTANTS.STATUS_MSG.ERROR.DUPLICATE_EMAIL)
                            }else if(payloadData.language=='ar'){
                                cb(Config.APP_CONSTANTS.STATUS_MSG.ERROR.DUPLICATE_EMAIL_AR)
                            }else{
                                cb(Config.APP_CONSTANTS.STATUS_MSG.ERROR.DUPLICATE_EMAIL)
                            }
                        }
                        else{
                            cb(null);
                        }
                    }
                })
            }
            else{
                cb(null)
            }
        },
          checkSocial:(cb)=>{
            if(payloadData.socialId){
                let query = {
                    socialId:payloadData.socialId,
                };
                if(payloadData.userId){
                    query._id={$ne:payloadData.userId}
                }
                checkAlreadyRegisteredUser(query,(err,result)=>{
                    if(err){
                        cb(err)
                    }
                    else{
                        if(result==true){
                            if(payloadData.language=='en'){
                                cb(Config.APP_CONSTANTS.STATUS_MSG.ERROR.DUPLICATE_SOCIAL)
                            }else if(payloadData.language=='ar'){
                                cb(Config.APP_CONSTANTS.STATUS_MSG.ERROR.DUPLICATE_SOCIAL_AR)
                            }else{
                                cb(Config.APP_CONSTANTS.STATUS_MSG.ERROR.DUPLICATE_SOCIAL)
                            }
                        }
                        else{
                            cb(null);
                        }
                    }
                })
            }
            else{
                cb(null)
            }
        },
            sendOTP:(checkPhone,checkEmail,checkSocial,cb)=>{
            let criteria={
                to: [payloadData.phoneNo],
                body: "Your One Time Password for the Wethaq is " + otp  //live
            };
            messageManager.sendSmsUnisoft(criteria, function (result) {
                // console.log("otp ",result,typeof result,result.success)
                cb(null)
            })
        }
        }, function (err, result) {
            if (err) {
                callback(err)
            } else {
                callback(null,otp);
            }
        }
    )


};
const userLogout = function (userData, callback) {
    async.autoInject({
            removeToken:(cb)=>{
                let criteria = {
                    _id: userData._id
                };
                let setQuery = {
                    $set: {
                        accessToken:null,
                        deviceToken:null,
                    }
                };
                Service.userServices.updateUser(criteria, setQuery, {new: true}, function (err, dataAry) {
                    if (err) {
                        cb(err)
                    } else {
                        cb(null)
                    }
                });
            },
        }, function (err, result) {
            if (err) {
                callback(err)
            } else {
                callback(null)
            }
        }
    )

};


const getUserWallet = (payloadData,userData,callback)=>{
    let query = {
        isDeleted:false,
        _id:userData._id
    };
    let projection={
        wallet:1
    };
    Service.userServices.getUsers(query,projection,{},(err,result)=>{
        if(err){
            callback(err);
        }
        else{

            callback(null,{wallet:result[0].wallet});

        }
    })
};




const cancelGuard = (payloadData,userData,callback)=>{
    let data;
    let creater;
    let categoryId;
    let templateId;
    let eventName;
    let notificationCount=0;
    let guardId='';
    let dToken='';
    console.log("payload",payloadData);
    async.autoInject({
            updateInvite:(cb)=>{
                let query = {
                    _id:payloadData.eventId,
                    isDeleted:false,
                  //  guards: {$elemMatch: {phoneNo:payloadData.phoneNo}}
                };
                let options = {new:true};
                let dataToSet = {};
                // dataToSet.guards=[];

                dataToSet.$pull={guards:{"phoneNo":payloadData.phoneNo}}
                // dataToSet.guardId=null;
                Service.eventServices.updateEvent(query,dataToSet,options,(err,result)=>{
                    if(err){
                        cb(err);
                    }
                    else{
                        data=result;
                        creater=result.createdBy;
                        eventName=result.title;
                        categoryId=result.categoryId;
                        templateId=result.templateId;
                        cb(null)

                    }
                })
            },
            getDtoken:(updateInvite,cb)=>{
                if(creater!=null && payloadData.phoneNo) {
                    Service.userServices.getUsers({phoneNo: payloadData.phoneNo}, {_id:1,deviceToken: 1}, {lean: true}, (err, result) => {
                        if (err)
                            cb(err);
                        else {
                            if (result && result.length) {
                                dToken = result[0].deviceToken;
                                guardId = result[0]._id;
                                cb(null)
                            } else {
                                cb(null)
                            }
                        }
                    })
                }else{
                    cb(null)
                }
            },
            createNotification:(getDtoken,updateInvite,cb)=>{
                console.log("2222",notificationCount);
                let query = {};
                query.text=[userData.name+' cancelled your guard invite for “'+eventName+'”'];
                query.type=7;
                query.templateId=templateId;
                query.categoryId=categoryId;
                query.createdFor=[guardId];
                query.eventId=payloadData.eventId;
                query.notificationType="Notification";
                query.timestamp=new Date().getTime();
                query.images=[userData.profilePicture];
                query.createdBy=[userData._id];
                    Service.notificationServices.createNotification(query, (err, result) => {
                        if (err) {
                            cb(err);
                        }
                        else {
                            data = result;
                            cb(null);

                        }
                    })
            },
            sendMessage:(updateInvite,cb)=>{
                let criteria = {
                    to:[payloadData.phoneNo],
                };
                criteria.body =userData.name+' cancelled your guard invite for “'+eventName+'”';
                messageManager.sendSmsUnisoft(criteria, function (result) {
                    console.log("sms", result);
                        cb(null);
                });

            },
            removeNotification:(getDtoken,cb)=>{
                if(guardId && guardId!='') {
                    let query = {
                        eventId:payloadData.eventId,
                        type:3
                    };
                    Service.notificationServices.updateNotification(query,{createdFor:[],$addToSet: {deletedBy: guardId}},{}, (err, result) => {
                        if (err) {
                            cb(err);
                        }
                        else {
                            cb(null)

                        }
                    })
                }else{
                    cb(null)
                }
            },
            sendPush:(getDtoken,cb)=>{
                if(creater!=null && dToken && dToken!=''){
                    let data={
                        title:'Wethaq',
                        multi:false,
                        eventId:payloadData.eventId,
                        timestamp:new Date().getTime(),
                        images:userData.profilePicture,
                    };
                        data.body=userData.name+' cancelled your guard invite for “'+eventName+'”';
                        data.type=7;
                    NotificationManager.sendPush(data,dToken, function (err, result) {
                        cb(null)
                    });
                }else{
                    cb(null)
                }
            }
        },
        (err,result)=>{
            if(err){
                callback(err)
            }
            else{
                callback(null);
            }
        })
};

const cancelEvent = (payloadData,userData,callback)=>{
    let baseRate=0;
    let inviteeLength=0;
    let templateId;
    let packageName;
    let invitees=[];
    let sent=false;
    let phoneNos=[];
    let addedWallet=payloadData.addedWallet || 0;
    async.autoInject({
            getEvent:(cb)=>{
                let query = {
                    _id:payloadData.eventId,
                };
                let options = {lean: true,new:true};
                let dataToSet = {};
                Service.eventServices.getEvent(query,dataToSet,options,(err,result)=>{
                    if(err){
                        cb(err);
                    }
                    else{
                        console.log("====== payload =======",payloadData)
                        console.log("updated result",result);
                        inviteeLength=result[0].invites.length;
                        templateId=result[0].templateId;
                        invitees=result[0].invites;
                        baseRate=result[0].smsPrice/result[0].totalSms;
                        packageName = ( result[0].packageType != undefined ? result[0].packageType:result[0].packageType = ""); 
                       
                        // console.log( '== = = == = == == = == =  == = = == = =',
                        // payloadData.isReminder, result[0].inviteAt, payloadData.inviteAt  
                        // )
                       
                        if(
                            (!payloadData.isReminder && result[0].inviteAt>=new Date().getTime()) ||
                            (payloadData.isReminder&& payloadData.inviteAt && payloadData.inviteAt>=new Date().getTime())
                        ){
                            cb(null)
                        }
                    
                        if(
                            (!payloadData.isReminder && result[0].inviteAt<=new Date().getTime() ) ||
                            (payloadData.isReminder && payloadData.inviteAt && payloadData.inviteAt<=new Date().getTime()) ||
                            (payloadData.isReminder && !payloadData.inviteAt)
                        ){
                            if(invitees && invitees.length){
                            sent=true;
                            let flag = 0;
                            for (let i = 0; i <invitees.length; i++) {
                                (function (i) {
                                    phoneNos.push(invitees[i].phoneNo);
                                    flag++;
                                    if (flag == invitees.length) {
                                        cb(null);
                                    }
                                }(i))
                            }
                            }else{
                                cb(null)
                            }
                        }
                    }
                })
            },
            updateTemplate:(updateWallet,cb)=>{
                if(!payloadData.isReminder){
                    Service.templateServices.updateTemplate({_id:templateId}, {$inc: {usage:-1}},{new:true},(err,res)=>{
                        cb(null);
                    })
                }else cb(null)
            },
            updateUser:(cb)=>{
                  if(!payloadData.isReminder){
                      Service.userServices.updateUser({_id:userData._id}, {$inc: {eventsCreated:-1}},{new:true},(err,res)=>{
                          cb(null);
                      })
                  }else cb(null)
            },
            updateWallet:(getEvent,cb)=>{
                if(sent==true && (addedWallet+userData.wallet)<(inviteeLength)){
                    Service.userServices.updateUser({_id:userData._id},{wallet:addedWallet+userData.wallet},{new:true,lean:true},(err,result)=>{
                        if(err){
                            cb(err)
                        }else{
                            callback(Config.APP_CONSTANTS.STATUS_MSG.ERROR.INSUFFICIENT_BALANCE)
                        }
                    })
                }
                if(sent==true && (addedWallet+userData.wallet)>=(inviteeLength)){
                    Service.userServices.updateUser({_id:userData._id},{wallet:((addedWallet+userData.wallet)-(inviteeLength))},{new:true,lean:true},(err,result)=>{
                        if(err){
                            cb(err)
                        }else{
                            cb(null)
                        }
                    })
                }
                if(sent==false){

                    cb(null)
                }
            },
            updateEvent: (updateWallet,cb)=>{
                if(!payloadData.isReminder) {
                    Service.eventServices.updateEvent({_id: payloadData.eventId,cancelReason:payloadData.reason}, {isDeleted: true}, {}, (err, result) => {
                        if (err) {
                            cb(err)
                        } else {
                            cb(null)
                        }
                    })
                }else{
                    cb(null)
                }
            },
            sendMessage:(updateWallet,cb)=>{
            //    if(packageName == Config.APP_CONSTANTS.DATABASE.PACKAGES.PREMIUM){
                if(sent == true && phoneNos.length){
                    console.log("phoneNos", phoneNos);
                    let criteria = {
                        to: phoneNos
                    };
                    criteria.body = payloadData.reason;

                    console.log("criteria reg", criteria.body);
                    messageManager.sendSmsBulkUnisoft(criteria, function ( result1) {
                        console.log("bulk event sms", result1);
                        cb(null);
                    });
                    // messageManager.sendSMSToUser(criteria, function ( result2) {
                    //     console.log("bulk whatsapp event sms", result2);
                    //     cb(null);

                    // }
                    // );
                }else{
                    cb(null)
                }
            // }
            // else if(packageName == Config.APP_CONSTANTS.DATABASE.PACKAGES.WHATSAPP){
            //     if(sent == true && phoneNos.length){
            //         console.log("whatsAppPhoneNos", phoneNos);
                    
            //         let criteria = {
            //             to: phoneNos
            //         };
                    
            //         criteria.body = payloadData.reason;

            //         console.log("criteria reg", criteria.body);
            //         messageManager.sendSMSToUser(criteria, function ( result1) {
            //             console.log("bulk event sms", result1);
            //             cb(null);

            //         });
            //     }else{
            //         cb(null)
            //     }
            // }
           
            // else{
            //     cb(null)
            // }
          },
            eventReminder:(sendMessage,cb)=>{
                if(payloadData.isReminder){
                    Service.eventServices.updateEvent({_id: payloadData.eventId}, {
                        $push: {
                            reminder: {
                                message: payloadData.reason,
                                createdAt: new Date().getTime(),
                                inviteAt: payloadData.inviteAt,
                                sent: sent
                            }
                        },
                        $inc: {reminders: 1}
                    }, {new: true}, (err, res) => {
                        cb(null);
                    })
                }else cb(null)
            },

        },
        (err,result)=>{
            if(err){
                callback(err)
            }
            else{
                callback(null);
            }
        })

};

const contactUs = (payloadData,userData,callback)=>{
    async.autoInject({
            createFeedback:(cb)=>{
                let query = {
                    createdBy:userData._id,
                };
                if(payloadData.name && payloadData.name!=''){
                    query.name=payloadData.name
                }
                if(payloadData.message && payloadData.message!=''){
                    query.message=payloadData.message
                }
                if(payloadData.email && payloadData.email!=''){
                    query.email=payloadData.email
                }
                if(payloadData.phoneNo && payloadData.phoneNo!=''){
                    query.phoneNo=payloadData.phoneNo
                }
                Service.contactServices.createContact(query,(err,result)=>{
                    if(err){
                        cb(err)
                    }
                    else{
                        cb(null)
                    }

                })
            },
        sendMail:(cb)=>{
            let subject = "User Feedback";
            let template = "";
            template += "Name: "+payloadData.name+"<br>";
            template += "Email Address: "+payloadData.email+"<br>";
            template += "Phone Number: "+payloadData.phoneNo+"<br>";
            template += "Message: "+payloadData.message+"<br>";
            UniversalFunctions.sendMail("hello@wethaqapp.net", subject, template, function (err,result) {
           //     console.log("res",err,result)
                cb()
            });
        }
        }, function (err, result) {
            if (err) {
                callback(err)
            } else {
                callback(null)
            }
        }
    )

};

const createReqEvent = (payloadData,userData,callback)=>{
    let data;
    let invites=[];
    if(payloadData.invites) {
        invites = payloadData.invites
    }

    let ids=[];
    let dTokens=[];
    let existing=[];
    let qrs=[];
    let unRegistered=[];
    let invitees=[];
    let emails=[];
    let template;
    let request;
    let startTime=new Date().setUTCMinutes(new Date().getUTCMinutes()-10);
    let endTime=new Date().setUTCMinutes(new Date().getUTCMinutes()+10);
    let eventId = mongoose.Types.ObjectId();
    // console.log("payload",payloadData);
    async.autoInject({
            getTemplate:(cb)=>{
                Service.templateServices.getTemplate({_id:payloadData.templateId},{},{new:true},(err,result)=>{
                    if(err){
                        cb(err)
                    }else {
                        if (result && result.length) {
                            template=result[0];
                            // console.log('template',template)
                          
                        cb(null)
                       }else{
                            cb(null)
                        }
                    }
                })
            },
            getRequest:(cb)=>{
                if(payloadData.requestId && payloadData.requestId!=''){
                    Service.requestServices.getRequest({_id:payloadData.requestId},{message:0},{},(err,result)=>{
                        if(err){
                            cb(err)
                        }else{
                            if (result && result.length) {
                                request=result[0];
                                cb(null)
                            }else{
                                cb(null)
                            }
                        }
                    })
                }else{
                    cb(null)
                }
            },
            insertEvent:(getTemplate,checkInvites,getRequest,cb)=>{
                // console.log("12345",invitees);
                let query = {
                    _id:eventId,
                    createdAt:new Date().getTime(),
                    createdBy:userData._id,
                    // categoryId:template.categoryId,
                    templateId:payloadData.templateId,
                    // title:payloadData.title,
                    packageType:payloadData.packageType,
                    message:payloadData.message,
                    // htmlUrl:template.htmlUrl,
                    //imageUrl:template.imageUrl,
                    address:request.address,
                    // inviteAt:payloadData.inviteAt,
                    startAt:request.startAt,
                    smsPrice:payloadData.smsPrice,
                    totalSms:payloadData.totalSms,
                };
                
                 template.imageUrl != undefined ?
                 query.imageUrl = template.imageUrl : query.imageUrl = "";

                query.type = Config.APP_CONSTANTS.DATABASE.EVENT_TYPE.REQUEST_EVENT;

                if(payloadData.title) query.title = payloadData.title;
                if(payloadData.inviteAt) query.inviteAt = payloadData.inviteAt;
                if(payloadData.invites && invites.length){
                    query.pending=(payloadData.invites).length;
                    query.total=(payloadData.invites).length;
                }
                if(payloadData.address){
                    query.address=payloadData.address;
                }if(payloadData.lang){
                    query.lang=payloadData.lang;
                }
                if(request.long && request.lat){
                    query.location=[request.long,request.lat]
                }
                if(payloadData.lat!=undefined && payloadData.long!=undefined){
                    query.location=[payloadData.long,payloadData.lat];
                }
                if(payloadData.totalPrice){
                    query.totalPrice=(payloadData.totalPrice);
                }
                if(invitees && invitees.length){
                    query.invites=_.sortBy(invitees,'name');
                    query.unRegistered=unRegistered;
                    query.isSent=true;
                }else{
                    query.invites=_.sortBy(payloadData.invites,'name');
                }
                if(request.endAt){
                    query.endAt=request.endAt;
                }else{
                    query.endAt=request.startAt+25200000;
                }
                if(qrs && qrs.length) {
                    query.qrCodes=qrs;
                }
                query.isQrCode = payloadData.qrCode == 1;

                if(payloadData.startAt){
                    query.startAt=payloadData.startAt;
                    if(payloadData.endAt){
                        query.endAt=payloadData.endAt;
                    }else{
                        query.endAt=payloadData.startAt+25200000;
                    }
                }
                console.log("create query",query);
                Service.eventServices.createEvent(query,(err,result)=>{
                    if(err){
                        cb(err);
                    }
                    else{
                        data=result;
                        cb(null);
                    }
                })
            },
            createQr:(cb)=>{
                if(payloadData.qrCode && payloadData.inviteAt){
                if(payloadData.qrCode==1 && invites && invites.length 
                    // && payloadData.inviteAt>startTime && payloadData.inviteAt<endTime
                    ){
                    // console.log("payloadData qrCode",payloadData.qrCode);
                    let flag=0;
                    for(let i=0;i<invites.length;i++){
                        (function (i) {
                            let  pic = "qr-userId="+ invites[i].phoneNo+"eventId="+eventId+"inviteCount="+invites[i].inviteCount;
                            let qr_svg = qr.image(pic, {type: 'png'});
                            let date=new Date().getTime();
                            // console.log("111",qr_svg);
                            let stream = qr_svg.pipe(require('fs').createWriteStream('uploads/' +date+'eventId=' + eventId+'.png'));
                            stream.on('finish', function (err, result) {
                                let  qrImage = Path.resolve(".") + "/uploads/" +date+'eventId=' + eventId+'.png';
                                let client = knox.createClient({
                                    key: Config.awsS3Config.s3BucketCredentials.accessKeyId
                                    , secret: Config.awsS3Config.s3BucketCredentials.secretAccessKey
                                    , bucket: Config.awsS3Config.s3BucketCredentials.bucket
                                });
                                let s3ClientOptions = {'x-amz-acl': 'public-read'};
                                client.putFile(qrImage,date + eventId+'.png', s3ClientOptions, function (err, res) {
                                    console.log(err);
                                    deleteFile(qrImage);
                                    _.map(invites, function(obj) {
                                        if (obj.phoneNo == invites[i].phoneNo)
                                            obj.qr = Config.awsS3Config.s3BucketCredentials.s3URL +date + eventId+  '.png';
                                    });
                                    qrs.push({"qr":Config.awsS3Config.s3BucketCredentials.s3URL +date + eventId+ '.png'});
                                    flag++;
                                    if (flag == invites.length) {
                                        // console.log("qr",qrs);
                                        cb(null);
                                    }

                                });
                            });
                        }(i))
                    }
                }else{
                    cb(null)
                }
            }
            else{
                cb(null)
            }
            },
            checkInvites:(createQr,getRequest,getTemplate,cb)=>{
                if(payloadData.inviteAt){
                if(payloadData.invites && invites.length && payloadData.inviteAt>startTime && payloadData.inviteAt<endTime){
                    // console.log("invite qr",invites);
                    let flag = 0;
                    for (let i = 0; i < invites.length; i++) {
                        (function (i) {
                            let id=eventId.toString();
                            let number=(invites[i].phoneNo.replace("+","")).toString().replace(/\s/g, '');
                            // let link = 'http://34.211.166.21/wethaq_dev/wethaq_admin/#/invitation/'+id+'/'+number; 
                            let link='http://admin.wethaqapp.net/wethaq_admin/#/invitation/'+id+'/'+number;
                            let subject = "Event Invite";
                            // let html = '<html><head></head><body><table style="border-collapse:collapse;table-layout:fixed;min-width:320px;width:100%;background-color:#fff;text-align:left;direction:ltr"><tbody><tr><td><div style=";text-align:left;direction:ltr"><div style="Margin:0 auto;max-width:560px;min-width:280px;width:280px;width:calc(28000% - 167440px);text-align:left;direction:ltr"> <br></div><div style="Margin:0 auto;max-width:600px;min-width:320px;width:320px;width:calc(28000% - 167400px);text-align:left;direction:ltr"><div style="font-size:26px;line-height:32px;Margin-top:6px;Margin-bottom:20px;color:#c3ced9;font-family:Roboto,Tahoma,sans-serif;Margin-left:20px;Margin-right:20px;text-align:left;direction:ltr" align="center"><div align="center" style=";text-align:left;direction:ltr"> <img style="display:block;height:auto;width:100%;border:0;max-width:230px" src="https://s3-us-west-2.amazonaws.com/wethaqbucket/profilePic_1523699389214imageBackground.png" alt="" width="230"></div></div></div></div><div style=";text-align:left;direction:ltr"><div style="Margin:0 auto;max-width:600px;min-width:320px;width:320px;width:calc(28000% - 167400px);word-wrap:break-word;word-break:break-word;text-align:left;direction:ltr"><div style="border-collapse:collapse;display:table;width:100%;background-color:#ffffff;text-align:left;direction:ltr"><div style="text-align:left;color:#8f8f8f;font-size:16px;line-height:24px;font-family:Open Sans,sans-serif;max-width:600px;min-width:320px;width:320px;width:calc(28000% - 167400px);text-align:left;direction:ltr"><div style="Margin-left:20px;Margin-right:20px;Margin-top:24px;text-align:left;direction:ltr"><div style=";text-align:left;direction:ltr"><h1 style="Margin-top:0;Margin-bottom:20px;font-style:normal;font-weight:normal;color:#404040;font-size:28px;line-height:36px;text-align:center;text-align:left;direction:ltr">'+payloadData.title+'</h1></div></div><div style="Margin-left:20px;Margin-right:20px;text-align:left;direction:ltr"><div style="line-height:10px;font-size:1px;text-align:left;direction:ltr"></div></div><div style="Margin-left:20px;Margin-right:20px;text-align:left;direction:ltr"><div style="line-height:10px;font-size:1px;text-align:left;direction:ltr"></div></div><div style="Margin-left:20px;Margin-right:20px;text-align:left;direction:ltr"><div style=";text-align:left;direction:ltr"><h1 style="Margin-top:0;Margin-bottom:0;font-style:normal;font-weight:normal;color:#404040;font-size:28px;line-height:36px;text-align:center;text-align:left;direction:ltr">'+invites[i].name+'أهلا بك</h1><h1 style="Margin-top:20px;Margin-bottom:0;font-style:normal;font-weight:normal;color:#404040;font-size:28px;line-height:36px;text-align:center;text-align:left;direction:ltr"> <strong>مرحباً بكم في تطبيق وثاق </strong></h1><h3 style="Margin-top:20px;Margin-bottom:12px;font-style:normal;font-weight:normal;color:#706f70;font-size:17px;line-height:26px;font-family:Cabin,Avenir,sans-serif;text-align:center;text-align:left;direction:ltr">التصميم المخصص للدعوة</h3></div></div><div style="font-size:12px;font-style:normal;font-weight:normal;line-height:19px;text-align:left;direction:ltr" align="center"> <img style="border:0;display:block;height:auto;width:100%;max-width:900px" alt="" width="600" src='+template.imageUrl+'></div><div style="Margin-left:20px;Margin-right:20px;text-align:left;direction:ltr"><div style=";text-align:left;direction:ltr"><h2>'+link+ ' : إليك الرابط </h2></div></div></div></div></div></div></td></tr></tbody></table></body></html>';
                            // UniversalFunctions.sendMail(invites[i].email, subject, html, function (err,result) {
                            //     console.log("res",err,result)
                            // });
                            TinyURL.shorten(link, function(res) {
                                link = res
                                invites[i].status = 'Pending';
                                let query = {
                                    phoneNo: invites[i].phoneNo,
                                    _id: {$ne: userData._id}
                                };
                                Service.userServices.getUsers(query, {}, {lean: true}, function (err, result) {
                                    if (err) {
                                        cb(err)
                                    } else {
                                        if (result.length) {
                                            existing.push({"_id": result[0]._id, "phoneNo": invites[i].phoneNo});
                                            ids.push(result[0]._id);
                                            emails.push(result[0].email);
                                            dTokens.push(result[0].deviceToken);
                                        }
                                        let criteria = {
                                            to: [invites[i].phoneNo],
                                        };

                                        if(payloadData.packageType == Config.APP_CONSTANTS.DATABASE.PACKAGES.FREE){
                                            payloadData.title = "";
                                         }

                                        criteria.body = "";
                                        if (payloadData.message && payloadData.message != '') {
                                            if (payloadData.lang == "en") {
                                                criteria.body += "Hello " + invites[i].name + ", " + payloadData.message + "\n";
                                            } else if (payloadData.lang == "ar") {
                                                criteria.body += " مرحبًا" + "\u202A" + invites[i].name + ", " + "\u202C" + payloadData.message + "\n";
                                            } else {
                                                criteria.body += " مرحبًا" + "\u202A" + invites[i].name + ", " + "\u202C" + payloadData.message + "\n";
                                            }
                                        } else {
                                            if(payloadData.packageType == Config.APP_CONSTANTS.DATABASE.PACKAGES.FREE){
                                            payloadData.title = "";
                                            }

                                            if (payloadData.lang == "en") {
                                                criteria.body+=/*"Hello "+invites[i].name+*/" It’s an honor to invite you to "+payloadData.title+"\n";
                                            } else if (payloadData.lang == "ar") {
                                                criteria.body += /*" أهلا" + "\u202A" + invites[i].name +*/ "\u202C" + " نتشرف بدعوتك لحضور مناسبة " + "\u202A" + payloadData.title + "\n";
                                            } else {
                                                criteria.body +=/* " أهلا" + "\u202A" + invites[i].name +*/ "\u202C" + " نتشرف بدعوتك لحضور مناسبة " + "\u202A" + payloadData.title + "\n";
                                            }
                                        }
                                        let templates = '<html><head></head><body><table style="border-collapse:collapse;table-layout:fixed;min-width:320px;width:100%;background-color:#fff;text-align:left;direction:ltr"><tbody><tr><td><div style=";text-align:left;direction:ltr"><div style="Margin:0 auto;max-width:560px;min-width:280px;width:280px;width:calc(28000% - 167440px);text-align:left;direction:ltr"> <br></div><div style="Margin:0 auto;max-width:600px;min-width:320px;width:320px;width:calc(28000% - 167400px);text-align:left;direction:ltr"><div style="font-size:26px;line-height:32px;Margin-top:6px;Margin-bottom:20px;color:#c3ced9;font-family:Roboto,Tahoma,sans-serif;Margin-left:20px;Margin-right:20px;text-align:left;direction:ltr" align="center"><div align="center" style=";text-align:left;direction:ltr"> <img style="display:block;height:auto;width:100%;border:0;max-width:230px" src="https://s3-us-west-2.amazonaws.com/wethaqbucket/profilePic_1523699389214imageBackground.png" alt="" width="230"></div></div></div></div><div style=";text-align:left;direction:ltr"><div style="Margin:0 auto;max-width:600px;min-width:320px;width:320px;width:calc(28000% - 167400px);word-wrap:break-word;word-break:break-word;text-align:left;direction:ltr"><div style="border-collapse:collapse;display:table;width:100%;background-color:#ffffff;text-align:left;direction:ltr"><div style="text-align:left;color:#8f8f8f;font-size:16px;line-height:24px;font-family:Open Sans,sans-serif;max-width:600px;min-width:320px;width:320px;width:calc(28000% - 167400px);text-align:left;direction:ltr"><div style="Margin-left:20px;Margin-right:20px;Margin-top:24px;text-align:left;direction:ltr"><div style=";text-align:left;direction:ltr"><h1 style="Margin-top:0;Margin-bottom:20px;font-style:normal;font-weight:normal;color:#404040;font-size:28px;line-height:36px;text-align:center;text-align:left;direction:ltr">' + payloadData.title + '</h1></div></div><div style="Margin-left:20px;Margin-right:20px;text-align:left;direction:ltr"><div style="line-height:10px;font-size:1px;text-align:left;direction:ltr"></div></div><div style="Margin-left:20px;Margin-right:20px;text-align:left;direction:ltr"><div style="line-height:10px;font-size:1px;text-align:left;direction:ltr"></div></div><div style="Margin-left:20px;Margin-right:20px;text-align:left;direction:ltr"><div style=";text-align:left;direction:ltr"><h1 style="Margin-top:0;Margin-bottom:0;font-style:normal;font-weight:normal;color:#404040;font-size:20px;line-height:36px;text-align:center;text-align:left;direction:ltr">' + invites[i].name + 'أهلا بك</h1><h1 style="Margin-top:20px;Margin-bottom:0;font-style:normal;font-weight:normal;color:#404040;font-size:28px;line-height:36px;text-align:center;text-align:left;direction:ltr"> <strong>مرحباً بكم في تطبيق وثاق </strong></h1><h3 style="Margin-top:20px;Margin-bottom:12px;font-style:normal;font-weight:normal;color:#706f70;font-size:17px;line-height:26px;font-family:Cabin,Avenir,sans-serif;text-align:center;text-align:left;direction:ltr">التصميم المخصص للدعوة</h3><h3 style="Margin-top:20px;Margin-bottom:12px;font-style:normal;font-weight:normal;color:#706f70;font-size:17px;line-height:26px;font-family:Cabin,Avenir,sans-serif;text-align:center;text-align:left;direction:ltr">' + criteria.body + '</h3></div></div><div style="font-size:12px;font-style:normal;font-weight:normal;line-height:19px;text-align:left;direction:ltr" align="center"> <img style="border:0;display:block;height:auto;width:100%;max-width:900px" alt="" width="600" src=' + template.imageUrl + '></div><div style="Margin-left:20px;Margin-right:20px;text-align:left;direction:ltr"><div style=";text-align:left;direction:ltr"><h2>' + link + ' : إليك الرابط</h2></div></div></div></div></div></div></td></tr></tbody></table></body></html>'

                                        UniversalFunctions.sendMail(invites[i].email, subject, templates, function (err, result) {
                                            // console.log("res",err,result) 
                                        });
                                        if (payloadData.lang == "en") {
                                            criteria.body += "For more info, please check the link: " + "\n";
                                        } else if (payloadData.lang == "ar") {
                                            criteria.body += "وللمزيد من المعلومات الرجاء فتح الرابط التالي: " + "\n";
                                        } else criteria.body += "وللمزيد من المعلومات الرجاء فتح الرابط التالي: " + "\n";

                                        criteria.body += link
                                        
                                        if(payloadData.packageType == Config.APP_CONSTANTS.DATABASE.PACKAGES.PREMIUM){
                                            console.log('== criteria ==',criteria)
                                            messageManager.sendSmsUnisoft(criteria, function ( result) {
                                                console.log("sms", err, result);
                                                // if(err==null){
                                                if(result.success=="true"){
                                                    invites[i].statusCode=200;
                                                    invites[i].createdAt=new Date().getTime();
                                                    // invites[i].message=[result.Data.MessageEn,result.Data.MessageAr];
                                                    invites[i].message = [ "SMS sent successfully", "تم الإرسال"];
                                                    invitees.push(invites[i]);
                                                }else{
                                                    // console.log("1234555",err);
                                                    invites[i].statusCode=400;
                                                    invites[i].createdAt=new Date().getTime();
                                                    // invites[i].message=[err.MessageEn,err.MessageAr];
                                                    invites[i].message = ["Mobile(s) number(s) is not specified or incorrect", "الأرقام المرسل لها غير صحيحه أو فارغه"];
                                                    invitees.push(invites[i]);
                                                } flag++;
                                                if (flag == invites.length) {
                                                    cb(null);
                                                }
                                            })
                                                messageManager.sendSMSToUser(criteria, (result) => {
                                                    console.log("== result ==",result)
                                                            if(result.customMessage == 'Success'){
                                                            invites[i].statusCode=200;
                                                            invites[i].createdAt=new Date().getTime();
                                                            // invites[i].message=[result.Data.MessageEn,result.Data.MessageAr];
                                                            invites[i].message = [ "SMS sent successfully", "تم الإرسال"];
                                                            invitees.push(invites[i]);
                                                        }else{
                                                            // console.log("1234555",err);
                                                            invites[i].statusCode=400;
                                                            invites[i].createdAt=new Date().getTime();
                                                            // invites[i].message=[err.MessageEn,err.MessageAr];
                                                            invites[i].message = ["Mobile(s) number(s) is not specified or incorrect", "الأرقام المرسل لها غير صحيحه أو فارغه"];
                                                            invitees.push(invites[i]);
                                                        }
                                                flag++;
                                                if (flag == invites.length) {
                                                    cb(null);
                                                }
                                            });
                                        }
                                else if(payloadData.packageType == Config.APP_CONSTANTS.DATABASE.PACKAGES.WHATSAPP){
                                    console.log('== criteria ==',criteria)
                                            messageManager.sendSMSToUser(criteria, (result) => {
                                        console.log("== result ==",result)
                                                if(result.customMessage == 'Success'){
                                                invites[i].statusCode=200;
                                                invites[i].createdAt=new Date().getTime();
                                                // invites[i].message=[result.Data.MessageEn,result.Data.MessageAr];
                                                invites[i].message = [ "SMS sent successfully", "تم الإرسال"];
                                                invitees.push(invites[i]);
                                            }else{
                                                // console.log("1234555",err);
                                                invites[i].statusCode=400;
                                                invites[i].createdAt=new Date().getTime();
                                                // invites[i].message=[err.MessageEn,err.MessageAr];
                                                invites[i].message = ["Mobile(s) number(s) is not specified or incorrect", "الأرقام المرسل لها غير صحيحه أو فارغه"];
                                                invitees.push(invites[i]);
                                            }
                                            flag++;
                                            if (flag == invites.length) {
                                                cb(null);
                                            }
                                          })
                                        }

                                        // messageManager.sendSmsUnisoft(criteria, function ( result) {
                                        //     console.log("sms", result);
                                        //     // if (err == null) {
                                        //     if (result.success=="true") {    
                                        //         invites[i].statusCode = 200;
                                        //         invites[i].createdAt = new Date().getTime();
                                        //         // invites[i].message = [result.Data.MessageEn, result.Data.MessageAr];
                                        //         invites[i].message = [ "SMS sent successfully", "تم الإرسال"];
                                        //         invitees.push(invites[i]);
                                        //     } else {
                                        //         console.log("1234555", err);
                                        //         invites[i].statusCode = 400;
                                        //         invites[i].createdAt = new Date().getTime();
                                        //         // invites[i].message = [err.MessageEn, err.MessageAr];
                                        //         invites[i].message = ["Mobile(s) number(s) is not specified or incorrect", "الأرقام المرسل لها غير صحيحه أو فارغه"];
                                        //         invitees.push(invites[i]);
                                        //     }
                                        //     flag++;
                                        //     if (flag == invites.length) {
                                        //         cb(null);
                                        //     }
                                        // });
                                        else{
                                            invitees.push(invites[i]);
                                            flag++;
                                            if (flag == invites.length) {
                                                cb(null);
                                            }
                                        }

                                    }
                                })
                            })

                        }(i))
                    }
                }else{
                    cb(null)
                }
            }
                else{
                    cb(null)
                }
            },
            sendNotificaiton:(insertEvent,cb)=>{
                if(ids && ids.length){
                    console.log("notification",ids);
                    let query={
                        createdFor:ids,
                        createdBy:[userData._id],
                        type:0,
                        text:[payloadData.title],
                        eventId:eventId,
                        timestamp:payloadData.startAt,
                        // categoryId:payloadData.categoryId,
                        templateId: payloadData.templateId,
                        notificationType:'Invite',
                        images:[{original:payloadData.imageUrl,thumbnail:payloadData.imageUrl}],
                    };
                    Service.notificationServices.createNotification(query,function (err,result) {
                        if(err){
                            cb(err)
                        }else{
                            cb(null);
                        }
                    });
                    // console.log("1111",unRegistered,phoneNos,ids);
                }else{
                    cb(null)
                }
            },
            sendPush:(insertEvent,cb)=>{
                if(dTokens && dTokens.length && payloadData.packageType != Config.APP_CONSTANTS.DATABASE.PACKAGES.FREE){
                    let data={
                        type:0,
                        title:'Wethaq',
                        multi:true,
                        body:'You are invited to attend “'+payloadData.title+'”',
                        eventId:eventId,
                        timestamp:payloadData.startAt,
                        images:{original:payloadData.imageUrl,thumbnail:payloadData.imageUrl},
                    };
                    NotificationManager.sendPush(data,dTokens, function (err, result) {
                        cb(null)
                    });
                    // console.log("1111",unRegistered,phoneNos,ids);
                }else{
                    cb(null)
                }
            },
            createTransc:(insertEvent,cb)=>{
                    let criteria = {
                        userId:userData._id,
                        eventId:eventId,
                        type:"Custom"
                    };
                    if(payloadData.finalPrice){
                        criteria.price=payloadData.finalPrice
                    }if(payloadData.trxId){
                        criteria.trxId=payloadData.trxId
                    }

                    Service.transactionServices.createTransaction(criteria, function (err, result) {
                        if (err) {
                            cb(err);
                        } else {
                            cb(null);
                        }
                    })
            },
            updateCoupon:(insertEvent,cb)=> {
                if(data && payloadData.couponCode && payloadData.couponCode!='') {
                    let obj = {
                        couponCode: payloadData.couponCode
                    };
                    let dataToSet = {};
                    dataToSet.$addToSet = {usedBy: userData._id};
                    Service.promoCodeServices.updatePromoCode(obj, dataToSet, {new: true}, function (err, result) {
                        if (err) {
                            cb(err);
                        } else {

                            cb(null);
                        }
                    })
                }else{
                    cb(null)
                }
            },
            updateWallet:(insertEvent,cb)=> {
                if(data) {
                    let obj = {
                        _id: userData._id
                    };
                    let dataToSet = {};
                    dataToSet.wallet=(userData.wallet+payloadData.totalSms)-invites.length;
                    Service.userServices.updateUser(obj, dataToSet, {lean:true,new: true}, function (err, result) {
                        if (err) {
                            cb(err);
                        } else {
                            data.wallet=result.wallet;
                            cb(null);
                        }
                    })
                }else{
                    cb(null)
                }

            },

        },
        (err,result)=>{
            if(err){
                callback(err);
            }
            else{
                callback(null,data);
            }
        })
};

const editEvent = (payloadData,userData,callback)=>{
    console.log("editEvent",payloadData);
    let tags=[];
    let params=[];
    async.autoInject({
            updateInvite:(cb)=>{
                let query = {
                    _id:payloadData.eventId,
                    isDeleted:false,
                };

                let datToSet={};

                if(payloadData.htmlUrl){
                    datToSet.htmlUrl=payloadData.htmlUrl
                }
                if(payloadData.address){
                    datToSet.address=payloadData.address
                }
                if(payloadData.imageUrl){
                    datToSet.imageUrl=payloadData.imageUrl
                }
                if(payloadData.lat && payloadData.long){
                    datToSet.location=[payloadData.long,payloadData.lat]
                }

                if(payloadData.startAt){
                    datToSet.startAt=payloadData.startAt
                }
                if(payloadData.endAt){
                    datToSet.endAt=payloadData.endAt
                }
                else{
                    datToSet.endAt=payloadData.startAt+25200000;
                }
                let options = {lean: true,new:true};
                console.log("criteria",query);
                Service.eventServices.updateEvent(query,datToSet,options,(err,result)=>{
                    console.log("event",err,result);
                    if(err){
                        cb(err);
                    }
                    else{
                        cb(null)

                    }
                })
            },

        },
        (err,result)=>{
            if(err){
                callback(err)
            }
            else{
                callback(null);
            }
        })

};

const dummyTemplate = (payloadData,userData,callback)=>{
    console.log("editEvent",payloadData);
    let data={};
    async.autoInject({
            createDummy:(cb)=>{
                let query={};

                if(payloadData.htmlUrl){
                    query.htmlUrl=payloadData.htmlUrl
                }
                if(payloadData.address){
                    query.address=payloadData.address
                }
                if(payloadData.lat && payloadData.long){
                    query.lat=payloadData.lat
                    query.long=payloadData.long
                }
                if(payloadData.startAt){
                    query.startAt=payloadData.startAt;
                    if(payloadData.endAt){
                        query.endAt=payloadData.endAt;
                    }else{
                        query.endAt=payloadData.startAt+25200000;
                    }
                }
                console.log("criteria",query);
                Service.dummyTempServices.createDummyTemplate(query,(err,result)=>{
                    console.log("dummy",err,result);
                    if(err){
                        cb(err);
                    }
                    else{
                        data=result;
                        cb(null)

                    }
                })
            },

        },
        (err,result)=>{
            if(err){
                callback(err)
            }
            else{
                callback(null,data);
            }
        })

};

//This is a common function to extract tags entered in the html templates
function extractTags(tags,params,html,key){
    if(html.indexOf("parameter",key)>-1){
        let startTag=html.indexOf("parameter=",key);
        let endTag=html.indexOf('>',startTag);
        let nextStartTag=html.indexOf('<',startTag);
        let text=endTag-startTag;
        console.log("sss",endTag,startTag,text);
        let startParamTag=html.indexOf('"',startTag);
        let endParamTag=html.indexOf('"',startParamTag+1);
        let tag=html.slice(startTag+text+1, nextStartTag);
        let param=html.slice(startParamTag+1, endParamTag);
        tags.push(tag);
        params.push(param);
        console.log("12222",tags,param);
        extractTags(tags,params,html,startTag+1)
    }
    return null
}

const getEventTemplate = (payloadData,userData,callback)=>{
    let tags=[];
    let params=[];
    let query = {
        _id:payloadData.eventId
    };
    let projection={
        htmlUrl:1,
        inviteAt:1,
        endAt:1,
        startAt:1,
        imageUrl:1
    };
    Service.eventServices.getEvent(query,projection,{lean:true},(err,result)=>{
        if(err){
            callback(err);
        }
        else{
            if(result && result.length) {
                extractTags(tags, params, result[0].htmlUrl, 0);
                result[0].tags = tags;
                result[0].params = params;
                callback(null, result);
            }else{
                callback(null)
            }

        }
    })
};

const getDummyTemp = (payloadData,userData,callback)=>{
    let query = {
        _id:payloadData.templateId
    };
    let projection={};
    Service.dummyTempServices.getDummyTemplate(query,projection,{lean:true},(err,result)=>{
        if(err){
            callback(err);
        }
        else{
            if(result && result.length) {
                callback(null, result[0]);
            }else{
                callback(null)
            }

        }
    })
};

const getEventDetails = (payloadData,userData,callback) => {
    let events=[];
    async.autoInject({
        getEvents:(cb)=>{
            let query = {
                isDeleted:false,
                isBlocked:false,
                _id: payloadData.eventId
            };
            let projection={
            };
            let populateArray=[
                {
                    path:'categoryId',
                    select:'_id name'
                },
                {
                    path:'createdBy',
                    select:'_id name phoneNo profilePicture email'
                },
                {
                    path:'guards.userId',
                    select:'_id name phoneNo profilePicture email'
                },
                {
                    path:'guardId',
                    select:'_id name phoneNo profilePicture email'
                },
                {
                    path:'templateId',
                    select:'isCustom'
                },
            ];
            let options={lean:true,};
            Service.eventServices.getEventPopulate(query,projection,options,populateArray,(err,result)=>{
                if(err){
                    cb(err);
                }
                else{
                    events=result;
                   let isCustom,templateId
                    events[0].invites=_.sortBy( events[0].invites,'name');
                    events[0].isCustom = events[0].templateId.isCustom
                    events[0].templateId = events[0].templateId._id 
                    
                    cb(null);

                }
            })
        },
    },(err,result)=>{
        if(err){
            callback(err)
        }
        else{
            callback(null,{events:events});
        }
    });
};

const getEventReminders = (payloadData,userData,callback) => {
    let reminders=[];
    let sender=0;
    async.autoInject({
        getEvents:(cb)=>{
            let aggregate=[
                {$match:{isDeleted:false,isBlocked:false,
                    _id: mongoose.Types.ObjectId(payloadData.eventId)}},
                {$unwind:"$reminder"},
                {$sort:{'reminder.createdAt':-1}},
                {$project:{
                   _id:'$reminder._id',
                    createdAt:'$reminder.createdAt',
                    inviteAt:'$reminder.inviteAt',
                    message:'$reminder.message',
                    sent:'$reminder.sent',
                }}
            ];
            Service.eventServices.eventAggregate(aggregate,(err,result)=>{
                if(err)cb(err);
                else{
                    if (result && result.length)reminders = result;
                        cb()
                }
            })
        },
        getReminders:(cb)=>{
            let aggregate=[
                {$match:{isDeleted:false,isBlocked:false,
                    _id: mongoose.Types.ObjectId(payloadData.eventId)}},
                {$project:{invited:{$size:"$invites"}}}
            ];
            Service.eventServices.eventAggregate(aggregate,(err,result)=>{
                if(err)cb(err);
                else{
                    if (result && result.length)sender = result[0].invited;
                    cb()
                }
            })
        },
    },(err,result)=>{
        if(err){
            callback(err)
        }
        else{
            callback(null,{ reminder:Math.floor(userData.wallet/sender),reminders:reminders});
        }
    });
};
const getAvailableCount = (payloadData,userData,callback) => {
    let sender=0;
    async.autoInject({
        getEvents:(cb)=>{
            let aggregate=[
                {$match:{isDeleted:false,isBlocked:false,
                    _id: mongoose.Types.ObjectId(payloadData.eventId)}},
                {$project:{invited:{$size:"$invites"}}}
            ];
            Service.eventServices.eventAggregate(aggregate,(err,result)=>{
                if(err)cb(err);
                else{
                    if (result && result.length)sender = result[0].invited;
                    cb()
                }
            })
        },
    },(err,result)=>{
        if(err){
            callback(err)
        }
        else{
            callback(null,{
                reminder:Math.floor(userData.wallet/sender),
                cancel:Math.floor(userData.wallet/sender),
                guest:userData.wallet,
            });
        }
    });
};

const getEventInvites = (payloadData,userData,callback) => {
    let invites=[];
    let count=0;
    async.autoInject({
        getEvents:(cb)=>{
            let aggregate=[
                {$match:{isDeleted:false,isBlocked:false,
                    _id: mongoose.Types.ObjectId(payloadData.eventId)}},
                {$unwind:"$invites"},
                {$match:{}},
                {$sort:{'invites.name':1}},
                {$skip:payloadData.skip},
                {$limit:payloadData.limit},
                {$project:{
                    email:'$invites.email',
                    name:'$invites.name',
                    phoneNo:'$invites.phoneNo',
                    statusCode:'$invites.statusCode',
                    qr:'$invites.qr',
                    message: '$invites.message',
                    isDeleted:'$invites.isDeleted',
                    createdAt: '$invites.createdAt',
                    updatedAt:'$invites.updatedAt',
                    checkedAt:'$invites.checkedAt',
                    status: '$invites.status'
                }}
            ]
            if(payloadData.status=='coming'){
                aggregate[2].$match={'invites.status': Config.APP_CONSTANTS.DATABASE.INIVTE_STATUS.ACCEPTED};
            }
            if(payloadData.status=='rejected'){
                aggregate[2].$match={'invites.status': Config.APP_CONSTANTS.DATABASE.INIVTE_STATUS.REJECTED};
            }
            if(payloadData.status=='pending'){
                aggregate[2].$match={
                    $or:[
                        {'invites.status': Config.APP_CONSTANTS.DATABASE.INIVTE_STATUS.PENDING},
                        {'invites.status': Config.APP_CONSTANTS.DATABASE.INIVTE_STATUS.AWAITED}
                    ]
                };
            }
            console.log(JSON.stringify(aggregate));
            Service.eventServices.eventAggregate(aggregate,(err,result)=>{
                if(err)cb(err);
                else{
                    if (result && result.length)invites = result;
                    cb()
                }
            })
        },
        getReminders:(cb)=>{
            let aggregate=[
                {$match:{isDeleted:false,isBlocked:false,
                    _id: mongoose.Types.ObjectId(payloadData.eventId)}},
                {$unwind:"$invites"},
                {$match:{}},
                {$group:{_id:null,count:{$sum:1}}}
            ];
            if(payloadData.status=='coming'){
                aggregate[2].$match={'invites.status': Config.APP_CONSTANTS.DATABASE.INIVTE_STATUS.ACCEPTED};
            }
            if(payloadData.status=='rejected'){
                aggregate[2].$match={'invites.status': Config.APP_CONSTANTS.DATABASE.INIVTE_STATUS.REJECTED};
            }
            if(payloadData.status=='pending'){
                aggregate[2].$match={
                    $or:[
                        {'invites.status': Config.APP_CONSTANTS.DATABASE.INIVTE_STATUS.PENDING},
                        {'invites.status': Config.APP_CONSTANTS.DATABASE.INIVTE_STATUS.AWAITED}
                    ]
                };
            }
            Service.eventServices.eventAggregate(aggregate,(err,result)=>{
                if(err)cb(err);
                else{
                    if (result && result.length)count = result[0].count;
                    cb()
                }
            })
        },
    },(err,result)=>{
        if(err){
            callback(err)
        }
        else{
            callback(null,{ count:count,invites:invites});
        }
    });
};

function calcAge(dateString) {
    let birthday = +new Date(dateString);
    return ~~((Date.now() - birthday) / (31557600000));
}

const getAdvertisements = (payloadData,userData,callback) => {
    let ads=[];
    let count=0;
    let age;
    let query = {
        isDeleted:false,
        isBlocked:false
    };
    if(userData && userData.dob){
        age=calcAge(userData.dob);
        query.$or=[
            {startAge:{$exists:false},endAge:{$exists:false},gender:{$exists:false}},
            {startAge:{$exists:false},endAge:{$exists:false},gender:userData.gender},
            {startAge:{$lt:age},endAge:{$gt:age},gender:{$exists:false}},
            {startAge:{$lt:age},endAge:{$gt:age},gender:userData.gender}
        ];
    }
  console.log("==== advertisement query ====", query)
    
    async.autoInject({
        getEvents:(cb)=>{
            let projection={
                adImageURL:1,
                url:1,
                createdOn:1
            };
            let options={lean:true,skip:payloadData.skip,limit:payloadData.limit,sort:{createdOn:-1}};
            Service.advertisementServices.getAdvertisement(query,projection,options,(err,result)=>{
                if(err){
                    cb(err);
                }
                else{
                    ads=result;
                    cb(null);

                }
            })
        },
        getCount:(cb)=>{
            Service.advertisementServices.countAdvertisement(query,(err,result)=>{
                if(err){
                    cb(err);
                }
                else{
                    count=result;
                    cb(null);
                }
            })
        }
    },(err,result)=>{
        if(err){
            callback(err)
        }
        else{
            callback(null,{ads:ads,count:count});
        }
    });
};

/***/
const getEventDetailsPdf = (payloadData, userData, callback) => {
    let query = {
        "_id": payloadData.eventId,
        "isDeleted": false,
        "isBlocked": false
    }
    
    let projection = {
        "title": 1,
        "categoryId": 1,
        "message": 1,
        "smsPrice": 1,
        "totalSms": 1,
        "totalPrice": 1,
        "categoryPrice": 1,
        "createdBy": 1,
        "invites": 1,
        "lang":1,
        "guards":1,
        "invites":1
    }

    let populateArray = [{
        path: 'categoryId',
        select: 'name isDeleted'
    }, {
        path: 'createdBy',
        select: 'isBlocked isDeleted phoneNo name language'
    }]
   
    
async.autoInject({
    getUser:(cb) => {
     Service.userServices.getUsers({"_id": userData._id,"isDeleted":false
    ,"isBlocked":false}, { language:1 },{lean:true},( err, result ) => {
       if(err){
           cb(err)
       }
       if(result){
           cb(null,result)
       }
     })
    },
     getEvent : (getUser,cb) => {
     Service.eventServices.getEventPopulate(query,projection,{lean:true},populateArray,(err,result) => {
      if(err){
          cb(err)
      }
      else if (result && result.length > 0) {
          if (result[0].createdBy.isDeleted) {
              if (result[0].createdBy.language == "en")
                  cb(Config.APP_CONSTANTS.STATUS_MSG.ERROR.USER_NOT_FOUND)
              else cb(Config.APP_CONSTANTS.STATUS_MSG.ERROR.USER_NOT_FOUND_AR)
          }
          else if (result[0].createdBy.isBlocked) {
              if (result[0].createdBy.language == "en")
                  cb(Config.APP_CONSTANTS.STATUS_MSG.ERROR.USER_BLOCKED)
              else cb(Config.APP_CONSTANTS.STATUS_MSG.ERROR.USER_BLOCKED_AR)
          }
          else{

          if(result[0].categoryId.isDeleted){
            if (result[0].createdBy.language == "en")
            cb(Config.APP_CONSTANTS.STATUS_MSG.ERROR.CATEGORY_DELETED)
           else cb(Config.APP_CONSTANTS.STATUS_MSG.ERROR.CATEGORY_DELETED_AR)
           } 
        else{
        
            // let eventDetails = {
            //     title:result[0].title,
            //     categoryName:result[0].categoryId.name
            // }
        
            let eventDetails = `<table><tr><td>Title</td><td>${result[0].title}</td></tr><tr><td>Category Name</td><td>${result[0].categoryId.name[0]}</td></tr></table>`
        
            let invitationDetails= `<table><tr><td>Message</td><td>${result[0].message}</td></tr>
            
            </table>`
            //  = {
            //     message: result[0].message,
            //     startAt: result[0].startAt
            // }

            let paymentDetails = `<table><tr><td>Category Price</td><td>${result[0].categoryPrice}</td></tr><tr><td>smsPrice</td><td>${result[0].smsPrice}</td></tr>
            <tr><td>Total Sms</td><td>${result[0].totalSms}</td></tr>
            <tr><td>Total Price</td><td>${result[0].totalPrice}</td></tr>
            </table>`
           
            
            let inviteUsers = {
               invites: result[0].invites
            }

            let temp = '<table>';
        for(let i = 0;i < result[0].guards.length;i++){
            temp += `<tr><td>Guards Phone Number</td><td>${result[0].guards[i].phoneNo}</td>`;
        }
            temp += '</table>'

 let htmlBody = `<!DOCTYPE html><html><head>
         <meta content="text/html; charset=utf-8" http-equiv="content-type">
       <title>A simple, clean, and responsive HTML invoice template</title>
             <style>
                 .invoice-box{
                     max-width:800px;
                     margin:auto;
                     padding:30px;
                     /*border:1px solid #eee;*/
                     /*box-shadow:0 0 10px rgba(0, 0, 0, .15); */
                     font-size:16px;
                     line-height:24px;
                     font-family:'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
                     color:#555;
                 }
             
             .invoice-box table{
                 width:100%;
                 line-height:inherit;
                 text-align:left;
             }
             
             .invoice-box table td{
                 padding:5px;
                 vertical-align:top;
             }
             
             .invoice-box table tr td:nth-child(2){
                 text-align:right;
             }
             
             .invoice-box table tr.top table td{
                 padding-bottom:20px;
             }
             
             .invoice-box table tr.top table td.title{
                 font-size:45px;
                 line-height:45px;
                 color:#333;
             }
             
             .invoice-box table tr.information table td{
                 padding-bottom:40px;
             }
             
             .invoice-box table tr.heading td{
                 background:#eee;
                 border-bottom:1px solid #ddd;
                 font-weight:bold;
             }
             
             .invoice-box table tr.details td{
                 padding-bottom:20px;
             }
             
             .invoice-box table tr.item td{
                 border-bottom:1px solid #eee;
             }
             
             .invoice-box table tr.item.last td{
                 border-bottom:none;
             }
             
             .invoice-box table tr.total td:nth-child(2){
                 border-top:2px solid #eee;
                 font-weight:bold;
             }
             
             @media only screen and (max-width: 600px) {
                 .invoice-box table tr.top table td{
                     width:100%;
                     display:block;
                     text-align:center;
                 }
                 
                 .invoice-box table tr.information table td{
                     width:100%;
                     display:block;
                     text-align:center;
                 }
             }
             </style>
             </head>
     <body>
         <div class="invoice-box">
             <table cellpadding="0" cellspacing="0">
                 <tbody>
                     <tr class="heading">
                         <td> Event Details ${eventDetails} </td>
                         <td> <br>
                         </td>
                     </tr>
                     #EVENT ITEMS#
                     <tr class="heading">
                         <td> Invitation Details ${invitationDetails}   </td>
                         <td> <br>
                         </td>
                     </tr>
                     #INVITATION ITEMS#
                     #INVITATION USERS#
                     <tr class="heading">
                         <td> Guard Details ${temp} </td>
                         <td> <br>
                         </td>
                     </tr>
                     #GUARD USERS#
                     <tr class="heading">
                         <td> Payment Details ${paymentDetails} </td>
                         <td> <br>
                         </td>
                     </tr>
                     #PAYMENT ITEMS#
                 </tbody>
             </table>
         </div>
         
     </body>
 </html>`;         

 
let options = { 
    format: 'Letter' 
};
let date =  (new Date().getTime()).toString();
let filePath = path.join(__dirname,'../uploads/'+date+'eventPdfFile.pdf');
console.log(filePath)
 let pdfFile = pdf.create(htmlBody,options).toFile(filePath,function(err, res) {
    if (err) {
       cb(err);
    }
    else{
     console.log(res);
     cb(null,{date,filePath});    
    }    
});

   } 
 }   
    
}
    else { 
        if(getUser[0].language == 'en')
        cb(Config.APP_CONSTANTS.STATUS_MSG.ERROR.EVENT_NOT_FOUND)
    else
    cb(Config.APP_CONSTANTS.STATUS_MSG.ERROR.EVENT_NOT_FOUND_AR)
    }
  
    
 })
},
    uploadFile:(getEvent,cb) => {
        // console.log('-----',getEvent)
    UploadManager.uploadFileToS3(getEvent.filePath,getEvent.date+'eventPdfFile.pdf',function(err,res){
        if(err){
            cb(err)
        }
        else{
           cb(null,{})
       }
        })
}
    }, (err, result) => {
        if (err) {
            callback(err)
        }
        else {
            callback(null, { 
        "fileLink": Config.awsS3Config.s3BucketCredentials.s3URL+result.getEvent.date+'eventPdfFile.pdf' 
            });
        }
    });

}


const updateUserLocation = (payloadData, userData, callback) => {
    let query = {
        "_id": userData.id,
        "isDeleted": false,
        "isBlocked": false
    }

    Service.userServices.updateUser(query, { "location": [payloadData.long,  payloadData.lat] }, 
        { lean: true, new: true }, (err, result) => {
            if(err){
                console.log("== err ==",err)
                callback(err)
            } 
            else if(result && result != null){
               console.log("== user ==",result)
                callback(null,result)
            }
        })
}



module.exports = {
    userSignUp:userSignUp,
    login:login,
    socialLoginUser:socialLoginUser,
    skipLogin:skipLogin,
    updateProfile:updateProfile,
    createEvent:createEvent,
    getUpcomingEvents:getUpcomingEvents,
    getOngoingEvents:getOngoingEvents,
    getMyEvents:getMyEvents,
    actionInvite:actionInvite,
    getNotifications:getNotifications,
    assignGuard:assignGuard,
    guardActionInvite:guardActionInvite,
    guardCheckIns:guardCheckIns,
    addInvites:addInvites,
    importCsvXl:importCsvXl,
    applyPromo:applyPromo,
    completeOffer:completeOffer,
    sendOtp:sendOtp,
    forgotPassword:forgotPassword,
    getUserWallet:getUserWallet,
    cancelGuard:cancelGuard,
    cancelEvent:cancelEvent,
    contactUs:contactUs,
    createReqEvent:createReqEvent,
    editEvent:editEvent,
    dummyTemplate:dummyTemplate,
    getEventTemplate:getEventTemplate,
    getDummyTemp:getDummyTemp,
    getEventDetails:getEventDetails,
    getEventReminders:getEventReminders,
    getAvailableCount:getAvailableCount,
    getEventInvites:getEventInvites,
    getAdvertisements:getAdvertisements,
    userLogout:userLogout,

    getEventDetailsPdf: getEventDetailsPdf,

    updateUserLocation: updateUserLocation
};