/**
 * Created by Shumi on 5/7/17.
 */
"use strict";


const Service = require('../services');
const UniversalFunctions = require('../Utils/UniversalFunction');
const async = require('async');
const Config = require('../Config');
const mongoose = require('mongoose');
const TokenManager = require('../Lib/TokenManager');
const emailFunction = require('../Lib/email');
const _ = require('lodash');
const pushNotification = require('../Lib/pushNotification');
const UploadManager = require('../Lib/UploadManager');
const NotificationManager = require('../Lib/pushNotification');
const messageManager = require('../Lib/messageManager');
const moment = require('moment');
const qr = require('qr-image');
const Path = require('path');
const knox = require('knox');
const fsExtra = require('fs-extra');
const TinyURL = require('tinyurl');
//This is admin 's login api
const adminLogin = (userData, callback) =>{

    var tokenToSend = null;
    var id = null;
    var responseToSend = {};
    var tokenData = null;
    var adminData = {};
    var pricingKey;

    //this is in series one function runs only after previous one is done running
    async.series([
        //In this function we check if admin's creds are true or not
        function (cb) {
            var getCriteria = {
                email: userData.email,
                password: UniversalFunctions.CryptData(userData.password)
            };
            Service.adminServices.getAdmin(getCriteria, {}, {}, function (err, data) {
                if (err) {
                    cb({errorMessage: 'DB Error: ' + err})
                } else {
                    if (data && data.length > 0 && data[0].email) {
                        id=data[0]._id
                        adminData=data[0];
                        tokenData = {
                            id: data[0]._id,
                            //username: data[0].email,
                            type :Config.APP_CONSTANTS.DATABASE.USER_TYPE.admin
                        };
                        cb()
                    } else {
                        cb(Config.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_USER_PASS)
                    }
                }
            });
        },
        //this function updates admin's login attempts in database
        function (cb) {
            var setCriteria = {
                email: userData.email
            };
            var setQuery = {
                $push: {
                    loginAttempts: {
                        validAttempt: (tokenData != null),
                        ipAddress: userData.ipAddress
                    }
                }
            };
            Service.adminServices.updateAdmin(setCriteria, setQuery, function (err, data) {
                cb(err,data);
            });
        },
        //this is last function that creates admin's access token
        function (cb) {
            if (tokenData && tokenData.id) {
                TokenManager.setToken(tokenData, function (err, output) {
                    if (err) {
                        cb(err);
                    } else {
                        tokenToSend = output && output.accessToken || null;
                        cb();
                    }
                });

            } else {
                cb();
            }
        }
    ], function (err, data) {
        responseToSend = {
            _id:id,
            category:adminData.category,template:adminData.template, package:adminData.package,
            user:adminData.user, event:adminData.event, guard:adminData.guard,
            promo:adminData.promo, chat:adminData.chat, superAdmin:adminData.superAdmin,
            accessToken: tokenToSend, ipAddress: userData.ipAddress,};
        if (err) {
            callback(err);
        } else {
            callback(null,responseToSend)
        }
    });
};

//In this api we create categories of templates to be saved by admin
const createCategories = (payloadData,userData,callback)=>{
    let data;
    let categoryImage={};
    let icon={};

    async.autoInject({

        //Firstly we check if same name category exists or not
        checkCategory:(cb)=>{
            let query = {
                name:payloadData.nameEng,
                isDeleted:false
            };
            Service.categoryServices.getCategory(query,{},{},(err,result)=>{
              if(err){
                  cb(err);
              }
                else{
                  if(result.length){
                      callback(Config.APP_CONSTANTS.STATUS_MSG.ERROR.DUPLICATE_CATEGORY)
                    }
                  else{
                      cb(null);
                  }
                  }
            })
        },
        //Then we upload the catgory's image
        uploadImage:(checkCategory,cb)=>{
                    let id = new Date().getTime() + "imageCategory";
                    UploadManager.uploadFileToS3WithThumbnail(payloadData.image, id, (err,result)=>{
                        if(err){
                            // console.log("here err",err);
                            cb(null);
                        }
                        else{
                            categoryImage.original=Config.awsS3Config.s3BucketCredentials.s3URL +result.original;
                            categoryImage.thumbnail= Config.awsS3Config.s3BucketCredentials.s3URL +result.thumbnail;
                            cb(null);
                        }
                    })
        },
            uploadicon:(checkCategory,cb)=>{
                    let id = new Date().getTime() + "iconImage";
                    UploadManager.uploadFileToS3WithThumbnail(payloadData.icon, id, (err,result)=>{
                        if(err){
                            // console.log("here err",err);
                            cb(null);
                        }
                        else{
                            icon.original=Config.awsS3Config.s3BucketCredentials.s3URL +result.original;
                            icon.thumbnail= Config.awsS3Config.s3BucketCredentials.s3URL +result.thumbnail;
                            cb(null);
                        }
                    })
        },
        //Then we create the category
        insertCategory:(uploadImage,uploadicon,cb)=>{

            let query = {
                name:[payloadData.nameEng,payloadData.nameArabic],
                categoryImage:categoryImage,
                icon:icon
            };

            Service.categoryServices.createCategory(query,(err,result)=>{
                if(err){
                    cb(err)
                }
                else{
                    cb(null);
                }
            })
        }
        },
        function(err,result){
            if(err){
                callback(err)
            }
            else{
                callback(null);
            }
        })
};

//This api is for editing the category details
const updateCategories = (payloadData,userData,callback)=>{

    let data;
    let categoryImage={};
    let icon={};

    async.autoInject({

        //Firstly we check if new entered category name is duplicate or not
        checkCategory:(cb)=>{
            let query = {
                name: { $in: [payloadData.nameEng ] },
                //name:payloadData.nameEng,
                isDeleted:false,
                _id:{$ne:payloadData._id}
            };
            Service.categoryServices.getCategory(query,{},{},(err,result)=>{
              if(err){
                  cb(err);
              }
                else{
                  if(result.length){
                      callback(Config.APP_CONSTANTS.STATUS_MSG.ERROR.DUPLICATE_CATEGORY)
                    }
                  else{
                      cb(null);
                  }
                  }
            })
        },
        // checkCategoryArabic:(cb)=>{
        //     let query = {
        //         name:payloadData.name,
        //         isDeleted:false
        //     };
        //     Service.categoryServices.getCategory(query,{},{},(err,result)=>{
        //       if(err){
        //           cb(err);
        //       }
        //         else{
        //           if(result.length){
        //               callback(Config.APP_CONSTANTS.STATUS_MSG.ERROR.DUPLICATE_CATEGORY)
        //             }
        //           else{
        //               cb(null);
        //           }
        //           }
        //     })
        // },

        // If category's new image is entered this function will run
            uploadImage: (checkCategory, cb) => {
                if (payloadData.image) {

                    let id = new Date().getTime() + "imageCategory";
                    UploadManager.uploadFileToS3WithThumbnail(payloadData.image, id, (err, result) => {
                        if (err) {
                            // console.log("here err", err);
                            cb(err);
                        }
                        else {

                            categoryImage.original = Config.awsS3Config.s3BucketCredentials.s3URL + result.original;
                            categoryImage.thumbnail = Config.awsS3Config.s3BucketCredentials.s3URL + result.thumbnail;
                            // console.log("1222222", categoryImage)
                            cb(null);
                        }
                    })
                }
                else {
                    cb(null);
                }
            },
            uploadIcon: (checkCategory, cb) => {
                if (payloadData.icon) {

                    let id = new Date().getTime() + "iconImage";
                    UploadManager.uploadFileToS3WithThumbnail(payloadData.icon, id, (err, result) => {
                        if (err) {
                            // console.log("here err", err);
                            cb(err);
                        }
                        else {
                            icon.original = Config.awsS3Config.s3BucketCredentials.s3URL + result.original;
                            icon.thumbnail = Config.awsS3Config.s3BucketCredentials.s3URL + result.thumbnail;
                            //console.log("1222222", categoryImage)
                            cb(null);
                        }
                    })
                }
                else {
                    cb(null);
                }
            },
        //This is last function to save the new details of category
        updateCategory:(uploadImage,uploadIcon,cb)=>{
            // console.log("232322323",categoryImage)

            let dataToSet = {
                name:[payloadData.nameEng,payloadData.nameArabic]
            };
            
            let query = {
            _id:payloadData._id
            };
            
            if(categoryImage.original){
                // console.log("===========cat image")
                dataToSet.categoryImage=categoryImage
            }
            if(icon.original){
                // console.log("===========icon")
                dataToSet.icon=icon
            }

            Service.categoryServices.updateCategory(query,dataToSet,{},(err,result)=>{
                if(err){
                    cb(err)
                }
                else{
                    cb(null);
                }
            })
        }
        },
        function(err,result){
            if(err){
                callback(err)
            }
            else{
                callback(null);
            }
        })
};

//This is api to get categories details for both users and details
const getCategories = (payloadData,userData,callback)=>{
            let data;
            let query = {
                isDeleted:false
            };
            Service.categoryServices.getCategory(query,{},{},(err,result)=>{
              if(err){

                  callback(err);

              }
                else{

                  callback(null,result)
                  
                  }
            })
};

//This is api to delete categories from our database
const deleteCategories = (payloadData,userData,callback)=>{
    
    
    let dataToSet = {
        isDeleted:true
    };

    let query = {
        _id:payloadData._id
    };
    
    Service.categoryServices.updateCategory(query,dataToSet,{},(err,result)=>{
        if(err){
            callback(err)
        }
        else{
            callback(null);
        }
    })
};

//This is api to create sms package where we check if same package exists if not we create new package
const createPackage = (payloadData,userData,callback)=>{


    let data;
    async.autoInject({
        checkPackage:(cb)=>{
            let query = {
                totalSms:payloadData.totalSms,
                totalPrice:payloadData.totalPrice,
                isDeleted:false
            };
            Service.pricingServices.getPricing(query,{},{},(err,result)=>{
              if(err){
                  cb(err);
              }
                else{
                  if(result.length){
                      callback(Config.APP_CONSTANTS.STATUS_MSG.ERROR.DUPLICATE_PACKAGE)
                    }
                  else{
                      cb(null);
                  }
                  }
            })
        },
        insertPackage:(checkPackage,cb)=>{

            let query = {
                totalSms:payloadData.totalSms,
                totalPrice:payloadData.totalPrice
            };

            Service.pricingServices.createPricing(query,(err,result)=>{
                if(err){
                    cb(err)
                }
                else{
                    cb(null);
                }
            })
        }
        },
        (err,result)=>{
            if(err){
                callback(err)
            }
            else{
                callback(null);
            }
        })
};

//This is api to update sms package where we check if entered package name is same and update the details
const updatePackage = (payloadData,userData,callback)=>{

    let data;

    async.autoInject({
            checkPackage:(cb)=>{
                let query = {
                    totalSms:payloadData.totalSms,
                    totalPrice:payloadData.totalPrice,
                    isDeleted:false
                };
                Service.pricingServices.getPricing(query,{},{},(err,result)=>{
                    if(err){
                        cb(err);
                    }
                    else{
                        if(result.length){
                            callback(Config.APP_CONSTANTS.STATUS_MSG.ERROR.DUPLICATE_PACKAGE)
                        }
                        else{
                            cb(null);
                        }
                    }
                })
            },
        updatePackage:(checkPackage,cb)=>{


            let dataToSet = {
                totalSms:payloadData.totalSms,
                totalPrice:payloadData.totalPrice
            };

            let query = {
            _id:payloadData._id
            };

            Service.pricingServices.updatePricing(query,dataToSet,{},(err,result)=>{
                if(err){
                    cb(err)
                }
                else{
                    cb(null);
                }
            })
        }
        },
        (err,result)=>{
            if(err){
                callback(err)
            }
            else{
                callback(null);
            }
        })
};

//This is api to get package details for both users and admins
const getPackage = (payloadData,userData,callback)=>{

    let data;
    
    let query = {
        isDeleted:false
    };
    let dataToGet = {
        "__v": 0,
        "isDeleted": 0
    };
            Service.pricingServices.getPricing(query,dataToGet,{sort:{totalSms:1}},(err,result)=>{
              if(err){
                  callback(err);
              }
                else{
                  callback(null,result)
                  }
            })
};

//This is api to delete package from our database
const deletePackage = (payloadData,userData,callback)=>{


    let dataToSet = {
        isDeleted:true
    };

    let query = {
        _id:payloadData._id
    };

    Service.pricingServices.updatePricing(query,dataToSet,{},(err,result)=>{
        if(err){
            callback(err)
        }
        else{
            callback(null);
        }
    })
};
function escapeRegExp(str) {
    return str.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&");
}
////This is api to get all user's details
const getAllUsers = (payloadData,userData,callback)=>{
    // console.log("2",payloadData)
    let data;
    let count;
    if(payloadData.search)payloadData.search=escapeRegExp(payloadData.search)
    async.autoInject({
            getAllUser:function(cb){
                let query = {};
                query.isDeleted=false;
          
                if(payloadData.name){
                    query.name = payloadData.name;
                }
                if(payloadData.search){
                    query.$or=[{phoneNo:{$regex:payloadData.search,$options:'i'}},{name:{$regex:payloadData.search,$options:'i'}},{email: {$regex: payloadData.search,$options:'i'}}]
                }
                if(payloadData.email){
                    query.email = payloadData.email;
                }
                if(payloadData.userId)query._id = payloadData.userId;

                if(payloadData.startDate){
                    query.startDate = {$gte:payloadData.startDate};
                }
                if(payloadData.endDate){
                    query.endDate = {$lt:payloadData.endDate};
                }
                if(payloadData.from && payloadData.to){
                    // if(payloadData.from==payloadData.to){
                        payloadData.to= (parseInt(payloadData.to)+86400000).toString();
                    // }
                    query.createdAt = {$gte:payloadData.from,$lte:payloadData.to};
                }
                query.isGuard=false
                let options = {
                  //  skip:payloadData.skip,
                //    limit: payloadData.limit,
                    sort:{createdAt:-1}
                };
                if(payloadData.skip) options.skip=payloadData.skip
                if(payloadData.limit) options.limit=payloadData.limit
                Service.userServices.getUsers(query,{__v:0,accessToken:0,password:0,cards:0},options,function(err,result){
                    if(err){
                        cb(err)
                    }
                    else{
                        data=result;

                        Service.userServices.getUsersCount(query,{_id:1, isDeleted:false},{},function(err,result1){
                            count=result1;
                            cb(null);
                        });
                    }
                })
            }
        },

        function(err,result){
            if(err){
                callback(err);
            }
            else{
                callback(null,{data:data,count:count});
            }
        })
};

//This is api to block user from accessing the app
const blockUsers = (payloadData,userData,callback)=>{

    let data;
    let myPost=[];
    async.autoInject({
            getAllUser:function(cb){
                let query = {
                    _id:payloadData.userId
                };
               let dataToSet = {
                   isBlocked:payloadData.block,
                   $unset:{deviceToken:1,accessToken:1}
               };
                Service.userServices.updateUser(query,dataToSet,{},function(err,result){
                    if(err){
                        cb(err)
                    }
                    else{
                        data=result;
                        cb(null);
                    }
                })
            }
        },
        function(err,result){
            if(err){
                callback(err);
            }
            else{
                callback(null);
            }
        })
};

//This is a common function to extract tags entered in the html templates
function extractTags(tags,params,html,key){
    if(html.indexOf("parameter",key)>-1){
        let startTag=html.indexOf("parameter=",key);
        let endTag=html.indexOf('>',startTag);
        let nextStartTag=html.indexOf('<',startTag);
        let text=endTag-startTag;
        // console.log("sss",endTag,startTag,text);
        let startParamTag=html.indexOf('"',startTag);
        let endParamTag=html.indexOf('"',startParamTag+1);
        let tag=html.slice(startTag+text+1, nextStartTag);
        let param=html.slice(startParamTag+1, endParamTag);
        tags.push(tag);
        params.push(param);
        // console.log("12222",tags,param);
        extractTags(tags,params,html,startTag+1)
    }
    return null
}

//This api for saving html template in the database
const createTemplate = (payloadData,userData,callback)=>{
    let tags=[]
    let params=[]
    async.autoInject({
        //firstly we extract the text in html with paramter="" in tags
            extractText:(cb)=>{
                if(payloadData.htmlUrl){
                    extractTags(tags,params,payloadData.htmlUrl,0)
                }
                cb()
            },
            checkAndInsertTemplate:(extractText,cb)=>{
                let flag=0;
                for(let i=0;i<payloadData.categoryId.length;i++){
                    let query = {
                        categoryId:payloadData.categoryId[i],
                        name:payloadData.name,
                        isDeleted:false
                    };
                    let projection={};
                    Service.templateServices.getTemplate(query,projection,{},(err,result)=>{
                            if(!result.length){
                                let query = {
                                    categoryId:payloadData.categoryId[i],
                                    name: payloadData.name,
                                    htmlUrl: payloadData.htmlUrl,
                                    imageUrl: payloadData.imageUrl,
                                    price: payloadData.price,
                                    tags:tags,
                                    params:params,
                                    usage:0,
                                    timeStamp:new Date().getTime()
                                };
                                if(payloadData.isPublic!=undefined){
                                    query.isPublic=payloadData.isPublic
                                }
                                Service.templateServices.createTemplate(query, (err, result) => {
                                       flag++;
                                    if(flag==payloadData.categoryId.length) cb(null);
                                })
                            }else{
                                flag++;
                                if(flag==payloadData.categoryId.length) cb(null);
                            }
                    })
                }

            }
        },
        (err,result)=>{
            if(err){
                callback(err);
            }
            else{
                callback(null);
            }
        })
};

//This is edit template details in which the keys passed are only edited
const updateTemplates = (payloadData,userData,callback)=>{
    let tags=[];
    let params=[];
    async.autoInject({
            checkPackage:(cb)=>{
                if(payloadData.name) {
                    let query = {};
                    query.name = payloadData.name;
                    query._id = {$ne:payloadData._id};
                    Service.templateServices.getTemplate(query, {}, {}, (err, result) => {
                        if (err) {
                            cb(err);
                        }
                        else {
                            if (result.length) {
                                callback(Config.APP_CONSTANTS.STATUS_MSG.ERROR.DUPLICATE_TEMPLATE)
                            }
                            else {
                                cb(null);
                            }
                        }
                    })
                }else{
                    cb(null)
                }
            },
            extractText:(checkPackage,cb)=>{
                if(payloadData.htmlUrl){
                    extractTags(tags,params,payloadData.htmlUrl,0)
                }
                cb()
            },
            insertTemplate:(extractText,cb)=>{
                let query={
                    _id:payloadData._id
                };
                let dataToSet = {};
                    if(payloadData.categoryId){
                        dataToSet.categoryId=payloadData.categoryId
                    }
                    if(payloadData.htmlUrl){
                        dataToSet.htmlUrl=payloadData.htmlUrl
                        dataToSet.tags=tags
                        dataToSet.params=params
                    }
                    if(payloadData.imageUrl){
                        dataToSet.imageUrl=payloadData.imageUrl
                    }
                    if(payloadData.name){
                        dataToSet.name=payloadData.name
                    }
                    if(payloadData.price>=0){
                        dataToSet.price=payloadData.price
                    }
                    // if(payloadData.isPublic){
                    // query.isPublic=payloadData.isPublic
                    // }
                if(payloadData.isPublic!=undefined){
                    dataToSet.isPublic=payloadData.isPublic
                }
                if(payloadData.isCustom != undefined){
                    dataToSet.isCustom=payloadData.isCustom
                }
                    Service.templateServices.updateTemplate(query,dataToSet,{}, (err, result) => {
                        if (err) {
                            cb(err);
                        }
                        else {
                            cb(null);
                        }
                    })
            }
        },
        (err,result)=>{
            if(err){
                callback(err)
            }
            else{
                callback(null);
            }
        })
};

//This is api to delete template from database
const deleteTemplates = (payloadData,userData,callback)=>{
    let dataToSet = {
        isDeleted:true
    };
    let query = {
        _id:payloadData._id
    };
    Service.templateServices.updateTemplate(query,dataToSet,{},(err,result)=>{
        if(err){
            callback(err)
        }
        else{
            callback(null);
        }
    })
};

function calcAge(dateString) {
    let birthday = +new Date(dateString);
    return ~~((Date.now() - birthday) / (31557600000));
}

//This is api to list templates (all,paid,free and category wise)
const getTemplates = (payloadData,userData,callback)=>{
    console.log('++++++++++++', payloadData,userData)
    let query = {
        isDeleted:false
    };
    if(payloadData.type=='Free'){
        query.price=0
    }
    if(payloadData.type=='Paid'){
        query.price={$ne:0}
    }

    if(payloadData.categoryId){
        query.categoryId=payloadData.categoryId
    }

    if(payloadData.templateId){
        query._id=payloadData.templateId
    }
    if(payloadData.isPublic){
        query.isPublic=payloadData.isPublic
    }
    if(userData && userData.type=="user"){
        query.isPublic=true
    }
    let populateArray = [ 
        {
            path:'categoryId',
            select:'_id name'
        }
    ];
    Service.templateServices.getTemplatePopulate(query,{},{ lean: true, sort: { createdAt:-1} },populateArray,(err,result)=>{
        if(err){
            callback(err);
        }
        else{

console.log("== result ==",result.length)

            if(payloadData.templateId){
                if(userData){
                    // console.log("== userData ==",userData)
                    if(userData.language == 'en'){
                    console.log("resulttttt",result)
                    result[0].lang = 'en';
                }
                    else result[0].lang = userData.language
                }
                result=JSON.parse(JSON.stringify(result))
            //  console.log("lang",result)
        }
            if(userData && userData.type == Config.APP_CONSTANTS.DATABASE.USER_TYPE.user
               && userData.location != undefined  && userData.location.length > 0 && userData.dob && payloadData.categoryId){
                
                let age=calcAge(userData.dob);
                let query = {
                    isDeleted:false,
                    isBlocked:false,
                    categoryId:{$in:[payloadData.categoryId,null]}  // for advertisements having category as all adding null
                };
                query.$or=[
                    {startAge:{$exists:false},endAge:{$exists:false},gender:{$exists:false}},
                    {startAge:{$exists:false},endAge:{$exists:false},gender:userData.gender},
                    {startAge:{$lt:age},endAge:{$gt:age},gender:{$exists:false}},
                    {startAge:{$lt:age},endAge:{$gt:age},gender:userData.gender}
                ];
                let projection={
                    adImageURL: 1,
                    url: 1,
                    createdOn: 1,
                    location: 1
                }

                let aggregate = [{
                        "$geoNear": {
                            near: { type: "Point", coordinates:[userData.location[0],userData.location[1]] },
                            distanceField: "dist.calculated",
                            query:query,
                            maxDistance: 100000,
                            spherical: true
                            }
                    },
                    { "$project": projection },
                    {$sort:{"createdOn":-1}}
            ];
            console.log("== aggregate ==",JSON.stringify(aggregate));
              
            Service.advertisementServices.advertisementAggregate(aggregate,(err,result1)=>{
                        if(result.length>10 && result1.length>1){
                        console.log("== Advertisements ==",result1);
                            for(let i=0;i<Math.ceil(result.length/10);i++){
                               let index= Math.floor(Math.random()*9);
                                result.splice(index, 0, result1[i]);
                                result.join();
                            }
                        }
                        
                        if(result.length<10 && result1.length){
                            result.push(result1[0])
                        }
                        callback(null,result)
                    })
            }
                
               else if(userData && !userData.dob && 
                userData.type == Config.APP_CONSTANTS.DATABASE.USER_TYPE.user
                && userData.location != undefined && userData.location.length > 0 && payloadData.categoryId){
                let query = {
                    isDeleted:false,
                    isBlocked:false,
                    categoryId:{$in:[payloadData.categoryId,null]}
                };
    
                let projection={
                    adImageURL:1,
                    url:1,
                    createdOn:1,
                    location:1
                }
             
                
                let aggregate = [{
                    "$geoNear": {
                        near: { type: "Point", coordinates:[userData.location[0],userData.location[1]] },
                        distanceField: "dist.calculated",
                        query:query,
                        maxDistance: 100000,
                        spherical: true
                        }
                },
                { "$project": projection },
                {$sort:{"createdOn":-1}}
        ];
               console.log("== advertisements without date check Query ==", JSON.stringify(aggregate));
                Service.advertisementServices.advertisementAggregate(aggregate,(err,result1)=>{
                    if(result.length>10 && result1.length > 1){
                    console.log("== Advertisements without date check ==",result1);
                        for(let i=0;i<Math.ceil(result.length/10);i++){
                           let index= Math.floor(Math.random()*9);
                            result.splice(index, 0, result1[i]);
                            result.join();
                        }
                    }
                    
                    if(result.length<10 && result1.length){
                        result.push(result1[0])
                    }
                    callback(null,result)
                })
               } 





    
               else if(userData && userData.type == Config.APP_CONSTANTS.DATABASE.USER_TYPE.user
                 && userData.dob && payloadData.categoryId){
               
               let age=calcAge(userData.dob);
               let query = {
                   isDeleted:false,
                   isBlocked:false,
                   categoryId:{$in:[payloadData.categoryId,null]}  // for advertisements having category as all adding null
               };
               query.$or=[
                   {startAge:{$exists:false},endAge:{$exists:false},gender:{$exists:false}},
                   {startAge:{$exists:false},endAge:{$exists:false},gender:userData.gender},
                   {startAge:{$lt:age},endAge:{$gt:age},gender:{$exists:false}},
                   {startAge:{$lt:age},endAge:{$gt:age},gender:userData.gender}
               ];
               let projection={
                   adImageURL: 1,
                   url: 1,
                   createdOn: 1,
                   location: 1
               }
let options = {lean:true,sort:{"createdOn":-1}}
           console.log("== query ==",JSON.stringify(query));
             
           Service.advertisementServices.getAdvertisement(query, projection, options,(err,result1)=>{
                       if(result.length>10 && result1.length>0){
                       console.log("== Advertisements ==",result1);
                           for(let i=0;i<Math.ceil(result.length/10);i++){
                              let index= Math.floor(Math.random()*9);
                               result.splice(index, 0, result1[i]);
                               result.join();
                           }
                       }
                       
                       if(result.length<10 && result1.length){
                           result.push(result1[0])
                       }
                       callback(null,result)
                   })
           }
               
              else if(userData && !userData.dob && 
               userData.type == Config.APP_CONSTANTS.DATABASE.USER_TYPE.user &&
               payloadData.categoryId){               
                let query = {
                   isDeleted:false,
                   isBlocked:false,
                   categoryId:{$in:[payloadData.categoryId,null]}
               };
   
               let projection={
                   adImageURL:1,
                   url:1,
                   createdOn:1,
                   location:1
               }
            
               
               let options = {lean:true,sort:{"createdOn":-1}}
              console.log("== without date check Query ==", JSON.stringify(query));
               Service.advertisementServices.getAdvertisement(query, projection, options,(err,result1)=>{
console.log("== result1 ==",result1.length)

                   if(result.length>10 && result1.length>0){
                   console.log("== Advertisements without date check ==",result1);
                       for(let i=0;i<Math.ceil(result.length/10);i++){
                          let index= Math.floor(Math.random()*9);
                           result.splice(index, 0, result1[i]);
                           result.join();
                       }
                   }
console.log("== result ==",result.length)
                   
                   if(result.length<10 && result1.length){
                       result.push(result1[0])
                   }
console.log("== result ==",result.length)

                   callback(null,result)
               })
              } 

                else if(!userData && payloadData.categoryId){
              
                    let criteria = { 
                    "isBlocked" : false,
                    "isDeleted" : false,
                    "url": { $ne: "url" }
                    }
                
                   let projection = { adImageURL: 1, url: 1, createdOn: 1,location:1 };
                   let options = { lean: true, sort: { createdOn: -1 } };
                  
               console.log("== advertisements Query ==", JSON.stringify(query));
                Service.advertisementServices.getAdvertisement(query, projection, options, (err,result1)=>{
               
                    if(err){ 
                    callback(err) 
                } 
                else {

                    if (result && result.length > 0 && result.length < 4
                    && result1 && result1.length > 0) {
                      result.push(result1[0])
                    console.log(" == Result == ", result)
                    }
    
                    if (result && result.length > 0 && result.length > 4
                    && result1 && result1.length > 0) {
                    for (let i = 0; i < Math.ceil(result.length / 3); i++) {
                    let index = Math.floor(Math.random() * 2);
                    console.log(result.length, Math.ceil(result.length / 3), index, result1[i])
                    result.splice(index, 0, result1[i])
                    result.join()
                }
            }
                callback(null, result);
        }
    
            })      
        }
              
                // console.log("======= result =======", result)
                // callback(null,result)
            
            else{
            callback(null, result)
        }
      }
    })
};

////This is api to get dashboard count
const getDashBoard = (payloadData,userData,callback) =>{
    let userCount;
    let guardCount;
    let categoryCount;
    let templateCount;
    let pricingCount;
    let eventCount;
    let promoCount;
    let ongoingEventCount;
    let pastEventCount;
    let upcomingEventCount;
    let requestCount;
    async.autoInject({
        totalUsers:(cb)=>{
            let query = {
                isDeleted:false,
                isGuard:false
            };
            Service.userServices.countUsers(query,function(err,result){
                if(err){
                    cb(err);
                }
                else
                {
                    userCount=result;
                    // console.log("====users",result)
                    cb(null)
                }
            })
        },
        totalGuards:(cb)=>{
            let query = {
                isDeleted:false,
                isGuard:true
            };
            Service.userServices.countUsers(query,function(err,result){
                if(err){
                    cb(err);
                }
                else
                {
                    guardCount=result;
                    // console.log("====guards",result)
                    cb(null)
                }
            })
        },
        totalCategory:(cb)=>{
            let query = {
                isDeleted:false
            };
            Service.categoryServices.countCategories(query,function(err,result){
                if(err){
                    cb(err);
                }
                else
                {
                    categoryCount=result;
                    // console.log("====category",result)
                    cb(null)
                }
            })
        },
        totalTemplates:(cb)=>{
            let query = {
                isDeleted:false
            };
            Service.templateServices.countTemplates(query,function(err,result){
                if(err){
                    cb(err);
                }
                else
                {
                    templateCount=result;
                    // console.log("====template",result)
                    cb(null)
                }
            })
        },
        totalPackages:(cb)=>{
            let query = {
                isDeleted:false
            };
            Service.pricingServices.countPricings(query,function(err,result){
                if(err){
                    cb(err);
                }
                else
                {
                    pricingCount=result;
                    // console.log("====package",result)
                    cb(null)
                }
            })
        },
        totalEvents: (cb)=>{
            let query = {
                isDeleted:false
            };
            Service.eventServices.countEvents(query,function(err,result){
                if(err){
                    cb(err);
                }
                else
                {
                    eventCount=result;
                    // console.log("====event",result)
                    cb(null)
                }
            })
        },
        OngoingEvents: (cb)=>{
            let query = {
                isDeleted:false
            };
            query.endAt={$gt:new Date().getTime()};
            query.startAt={$lt:new Date().getTime()};
            Service.eventServices.countEvents(query,function(err,result){
                if(err){
                    cb(err);
                }
                else
                {
                    ongoingEventCount=result;
                    // console.log("====ongoing event",result)
                    cb(null)
                }
            })
        },
        PastEvents: (cb)=>{
            let query = {
                isDeleted:false
            };
            query.endAt={$lt:new Date().getTime()}
            Service.eventServices.countEvents(query,function(err,result){
                if(err){
                    cb(err);
                }
                else
                {
                    pastEventCount=result;
                    // console.log("====past event",result)
                    cb(null)
                }
            })
        },
        UpcomingEvents: (cb)=>{
            let query = {
                isDeleted:false
            };
            query.startAt={$gt:new Date().getTime()}
            Service.eventServices.countEvents(query,function(err,result){
                if(err){
                    cb(err);
                }
                else
                {
                    upcomingEventCount=result;
                    // console.log("====upcoming event",result)
                    cb(null)
                }
            })
        },
        totalRequests: (cb)=>{
            let query = {
                isDeleted:false
            };
            Service.requestServices.countRequests(query,function(err,result){
                if(err){
                    cb(err);
                }
                else
                {
                    requestCount=result;
                    // console.log("====request",result)
                    cb(null)
                }
            })
        },
        totalPromos: (cb)=>{
            let query = {
                isDeleted:false
            };
            Service.promoCodeServices.countPromoCodes(query,function(err,result){
                if(err){
                    cb(err);
                }
                else
                {
                    promoCount=result;
                    // console.log("====promo",result)
                    cb(null)
                }
            })
        },

    },function (err,result) {
        callback(err,{
            userCount:userCount,
            guardCount:guardCount,
            categoryCount:categoryCount,
            templateCount:templateCount,
            pricingCount:pricingCount,
            eventCount:eventCount,
            ongoingEventCount:ongoingEventCount,
            pastEventCount:pastEventCount,
            upcomingEventCount:upcomingEventCount,
            promoCount:promoCount,
            requestCount:requestCount
            })
    })
};

const createBackground = function (payloadData, userData, callback) {
    let backgroundImage={};
    async.autoInject({
        uploadImage:(cb)=> {
            if (payloadData.image && payloadData.image.filename) {
                let id = new Date().getTime() + "imageBackground";
                UploadManager.uploadFileToS3WithThumbnail(payloadData.image,id, function (err, uploadedInfo) {
                    if (err) {
                        cb(err)
                    } else {
                        backgroundImage.original=Config.awsS3Config.s3BucketCredentials.s3URL +uploadedInfo.original;
                        backgroundImage.thumbnail= Config.awsS3Config.s3BucketCredentials.s3URL +uploadedInfo.thumbnail;
                        cb();
                    }
                })
            } else {
                cb();
            }
        },
        //Then we create the background image
        insertBackground:(uploadImage,cb)=>{

            let query = {
                backgroundImage:backgroundImage
            };

            Service.backgroundServices.createBackground(query,(err,result)=>{
                if(err){
                    cb(err)
                }
                else{
                    cb(null);
                }
            })
        }
    }, function (err, result) {
        if (err)
            callback(err)
        else
            callback(null, {
                original: backgroundImage.original,
                thumbnail: backgroundImage.thumbnail
            })
    })
};
//This is api to get background images
const getBackgrounds = (payloadData,userData,callback)=>{
    let query = {
        isDeleted:false
    };
    Service.backgroundServices.getBackground(query,{},{},(err,result)=>{
        if(err){
            callback(err);
        }
        else{
            callback(null,result)
        }
    })
};
//This is api to delete template from database
const deleteBackground = (payloadData,userData,callback)=>{
    let dataToSet = {
        isDeleted:true
    };
    let query = {
        _id:payloadData._id
    };
    Service.backgroundServices.updateBackground(query,dataToSet,{},(err,result)=>{
        if(err){
            callback(err)
        }
        else{
            callback(null);
        }
    })
};

//Image upload to aws server
const uploadImageAWS = function (payloadData, userData, callback) {
    let original;
    let thumbnail;
    async.autoInject({
        uploadImage:(cb)=> {
            if (payloadData.image && payloadData.image.filename) {
                let id = new Date().getTime() + "image";
                UploadManager.uploadFileToS3WithThumbnail(payloadData.image,id, function (err, uploadedInfo) {
                    if (err) {
                        cb(err)
                    } else {
                        original=Config.awsS3Config.s3BucketCredentials.s3URL +uploadedInfo.original;
                        thumbnail= Config.awsS3Config.s3BucketCredentials.s3URL +uploadedInfo.thumbnail;
                        cb();
                    }
                })
            } else {
                cb();
            }
        },
    }, function (err, result) {
        if (err)
            callback(err)
        else
            callback(null, {
                original: original,
                thumbnail: thumbnail
            })
    })
};

////This is api to get all guard's details
const getAllGuards = (payloadData,userData,callback)=>{

    let data;
    let count;

    async.autoInject({
            getAllGuard:function(cb){
                let query = {};
                query.isDeleted=false;
                query.isGuard=true
                let options = {
                    skip:payloadData.skip,
                    limit: payloadData.limit,
                    sort:{createdAt:-1}
                };
                Service.userServices.getUsers(query,{__v:0,accessToken:0,password:0,cards:0},options,function(err,result){
                    if(err){
                        cb(err)
                    }
                    else{
                        data=result;

                        Service.userServices.getUsersCount(query,{_id:1, isDeleted:false},{},function(err,result1){
                            count=result1;
                            cb(null);
                        });
                    }
                })
            }
        },

        function(err,result){
            if(err){
                callback(err);
            }
            else{
                callback(null,{data:data,count:count});
            }
        })
};

////This is api to get all event's details
const getAllEvents = (payloadData,userData,callback)=>{
    // console.log("1",payloadData)
    let data;
    let count;

    async.autoInject({
            getAllUser:function(cb){
                let query = {};
                query.isDeleted=false;
                let options = {
                 //   skip:payloadData.skip,
                   // limit: payloadData.limit,
                    sort:{startAt:1}
                };
                if(payloadData.skip) options.skip=payloadData.skip
                if(payloadData.limit) options.limit=payloadData.limit
                if(payloadData.filter=="past"){
                  query.endAt={$lt:new Date().getTime()}
                }
                if(payloadData.filter=="ongoing"){
                    query.endAt={$gt:new Date().getTime()};
                    query.startAt={$lt:new Date().getTime()}
                }
                if(payloadData.filter=="upcoming"){
                    query.startAt={$gt:new Date().getTime()}
                }
                if(payloadData.from && payloadData.to){
                    // if(payloadData.from==payloadData.to){
                        payloadData.to= parseInt(payloadData.to)+86400000;
                    // }
                    // query.startAt = {$gte:payloadData.from,$lte:payloadData.to};
                    // query.$or =[{startAt:{$gte:payloadData.from,$lte:payloadData.to}},{createdAt:{$gte:payloadData.from,$lte:payloadData.to}}];
                    query.createdAt={$gte:payloadData.from,$lte:payloadData.to};

                }
                if(payloadData.startFrom && payloadData.startTo){
                    // if(payloadData.startFrom==payloadData.startTo){
                        payloadData.startTo= parseInt(payloadData.startTo)+86400000;
                    // }
                    // query.startAt = {$gte:payloadData.from,$lte:payloadData.to};
                    query.startAt={$gte:payloadData.startFrom,$lte:payloadData.startTo};

                }
                if(payloadData.categoryId)query.categoryId =payloadData.categoryId
                if(payloadData.userId)query.createdBy =payloadData.userId

                let populateArray=[
                    {
                        path:'categoryId',
                        select:'_id name'
                    },
                    {
                        path:'templateId',
                        select:'_id name isPublic'
                    },
                    {
                        path:'createdBy',
                        select:'_id name email phoneNo profilePicture gender dob'
                    },
                    {
                        path:'guards.userId',
                        select:'_id name email phoneNo profilePicture gender dob'
                    },
                    {
                        path:'guardId',
                        select:'_id name email phoneNo profilePicture gender dob'
                    },
                ];
                let projection={
                    attendees:0,
                    checkIns:0,
                    unRegistered:0,
                    qrCodes:0,
                };
                // console.log("111",query)
                Service.eventServices.getEventPopulate(query,projection,options,populateArray,(err,result)=>{
                    // console.log("33",err,result)
                    if(err){
                        cb(err)
                    }
                    else{
                        data=result;

                        Service.eventServices.countEvents(query,function(err,result1){
                            count=result1;
                            cb(null);
                        });
                    }
                })
            }
        },

        function(err,result){
            if(err){
                callback(err);
            }
            else{
                callback(null,{data:data,count:count});
            }
        })
};
const sendPush=(payloadData,userData,callback)=>{
    let dTokens=[];
    async.autoInject({
        sendPushToUsers:(cb)=>{
            let data={
                title:payloadData.title,
                body:payloadData.body,
                createdAt:new Date().getTime(),
                sendAt:payloadData.sendAt
            };
            Service.scheduleNotifServices.createScheduleNotifs(data,(err,result)=>{
                if(err){
                    cb(err)
                }else{
                    cb(null)
                }
            });
        }
        },

        function(err,result){
            if(err){
                callback(err);
            }
            else{
                callback(null);
            }
        })


};

const getRequestUsers=(payloadData,userData,callback)=>{
    let ids=[];
    let count=0;
    async.autoInject({
        getUsers:function (cb) {
            let aggregate=[
                {$match:{isDeleted:false}},
                {$group:{_id:"$userId",timeStamp:{$max:"$timeStamp"},isRead:{$min:"$isRead"}}},
                {$sort:{timeStamp:-1}},
                {$lookup:{
                    "from" : "users",
                    "localField" : "_id",
                    "foreignField" : "_id",
                    "as" : "_id"
                }},
                {$unwind:"$_id"},
                {$project:{isRead:"$isRead",_id:"$_id._id",name:"$_id.name",email:"$_id.email",phoneNo:"$_id.phoneNo",profilePicture:"$_id.profilePicture",gender:"$_id.gender",dob:"$_id.dob"}},
                {$match:{}},
                {$skip:payloadData.skip},
                {$limit:payloadData.limit}

            ]
            if(payloadData.search)aggregate[6].$match.name={$regex:payloadData.search,$options:'i'}
            Service.requestServices.requestAggregate(aggregate,(err,res)=>{
                if(err)cb(err)
                else{
                    if(res.length)ids=res
                    cb(null)
                }
            })
        },
        getCount:function (cb) {
            let aggregate=[
                {$match:{isDeleted:false}},
                {$group:{_id:"$userId"}},
                {$lookup:{
                    "from" : "users",
                    "localField" : "_id",
                    "foreignField" : "_id",
                    "as" : "_id"
                }},
                {$unwind:"$_id"},
                {$project:{_id:"$_id._id",name:"$_id.name",email:"$_id.email",phoneNo:"$_id.phoneNo",profilePicture:"$_id.profilePicture",gender:"$_id.gender",dob:"$_id.dob"}},
                {$match:{}},
                {$group:{_id:null,count:{$sum:1}}}
            ]
            if(payloadData.search)aggregate[5].$match.name={$regex:payloadData.search,$options:'i'}
            Service.requestServices.requestAggregate(aggregate,(err,res)=>{
                if(err)cb(err)
                else{
                    if(res.length)count=res[0].count
                    cb(null)
                }
            })
        },
            /*getAllUser:function(cb){
                let query = {};
                query.isDeleted=false;
                let options = {
                    skip:payloadData.skip *  payloadData.limit,
                    limit: payloadData.limit,
                    sort:{timeStamp:-1}
                };
                let populateArray=[
                    {
                        path:'userId',
                        select:'_id name email phoneNo profilePicture gender dob'
                    }
                ];
                let projection={};
                Service.requestServices.getRequestPopulate(query,projection,options,populateArray,(err,result)=>{
                    if(err){
                        cb(err)
                    }
                    else{
                        if(result && result.length) {
                            for (let i = 0; i < result.length; i++) {
                                ids.push(result[i].userId);
                            }
                            ids=_.uniqBy(ids, '_id');
                            Service.requestServices.countRequests(query, function (err, result1) {
                                count = result1;
                                cb(null);
                            });
                        }else{
                            cb(null)
                        }
                    }
                })
            }*/
        },

        function(err,result){
            if(err){
                callback(err);
            }
            else{
                callback(null,{users:ids,count:count});
            }
        })
};
const getUserRequests=(payloadData,userData,callback)=>{
    if(userData.appVersion){
        let query = {};
        query.isDeleted=false;
        query.userId=payloadData.userId;
        let options = {
            sort:{timeStamp:-1},message: {$slice: -1}
        };
        let projection={};
        Service.requestServices.getRequest(query,projection,options,(err,result)=>{
            if(err){
                callback(err)
            }
            else{
                callback(null,result)
            }
        })
    }else{
        let requests;
        let limit = 0;
        let skip = 0;
        let count = 0;
        if (payloadData.limit && payloadData.skip && payloadData.skip!='' && payloadData.skip!='') {
            limit = parseInt(payloadData.limit);
            skip = parseInt(payloadData.skip);
        }
        async.autoInject({
            getCount:function (cb) {
                let query = {};
                query.isDeleted=false;
                query.userId=payloadData.userId;
                if(payloadData.search)query.title={$regex:payloadData.search,$options:'i'}
                Service.requestServices.countRequests(query,(err,result)=>{
                    if(err)cb(err)
                    else{
                        count=result
                        cb(null)
                    }
                })
            },
            getRequests: (cb)=> {
                let query = {};
                query.isDeleted=false;
                query.userId=payloadData.userId;
                if(payloadData.search)query.title={$regex:payloadData.search,$options:'i'}
                let options = {
                    sort:{timeStamp:-1}
                };
                let projection={message:0};
                if(payloadData.skip) options.skip=skip
                if(payloadData.limit) options.limit=limit
                Service.requestServices.getRequest(query,projection,options,(err,result)=>{
                    if(err){
                        cb(err)
                    }
                    else{
                        requests=result
                        cb(null)
                    }
                })
            },
        }, function (err, result) {

            if (err) {
                callback(err);
            } else {
                callback(null,{requests,count})
            }
        })
    }
};

const getRequestDesc = function (payloadData, userData, callback) {
    let chat;
    let limit = 0;
    let skip = 0;
    let count = 0;
    if (payloadData.limit && payloadData.skip && payloadData.skip!='' && payloadData.skip!='') {
        limit = parseInt(payloadData.limit);
        skip = parseInt(payloadData.skip);
    }
    async.autoInject({
        updateRequest:function (cb) {
            Service.requestServices.updateRequest({_id:payloadData._id},{isRead:true},{lean:true},(err,result)=>{
                    cb(null);
            })
        },
        getCount:function (cb) {
            let aggregate=[
                {$match:{_id:mongoose.Types.ObjectId(payloadData._id)}},
                {$unwind:"$message"},
                {$match:{}},
                {$group:{_id:null,count:{$sum:1}}}
            ]
            if(payloadData.search)aggregate[2].$match['message.text']={$regex:payloadData.search,$options:'i'}
            Service.requestServices.requestAggregate(aggregate,(err,res)=>{
                if(err)cb(err)
                else{
                    if(res.length)count=res[0].count
                    cb(null)
                }
            })
        },
        getRequests: (cb)=> {
            let aggregate=[
                {$match:{_id:mongoose.Types.ObjectId(payloadData._id)}},
                {$project:{ message: { $reverseArray: "$message" }}},
                {$unwind:"$message"},
                {$match:{}},
                {$skip:skip},
                {$limit:limit},
                {$sort:{'message.timeStamp':1}},
                {$group:{_id:null,message:{$push:"$message"}}}

            ]
            if(payloadData.search)aggregate[3].$match['message.text']={$regex:payloadData.search,$options:'i'}
            Service.requestServices.requestAggregate(aggregate,(err,res)=>{
                if(err)cb(err)
                else{
                    if(res.length)chat=res[0].message
                    cb(null)
                }
            })
        },
    }, function (err, result) {

        if (err) {
            callback(err);
        } else {
            callback(null,{chat,count})
        }
    })
};

const actionInvite = (payloadData,callback)=>{
    let data;
    let creater;
    let categoryId;
    let templateId;
    let eventName;
    let notificationCount=0;
    let notificationId;
    let dToken='';
    let name='';
    let id='';
    let profilePicture;
    let phoneNo=payloadData.phoneNo;
    let finalVals=[];
    finalVals.push(payloadData.phoneNo);
    if(phoneNo.indexOf('+')!=0){
        let withPlus="+"+phoneNo;
        finalVals.push(withPlus);
    }
    if(phoneNo.indexOf('+')==0) {
        let withoutPlus = phoneNo.split('');
        withoutPlus.splice(0, 1);
        withoutPlus= withoutPlus.join('');
        finalVals.push(withoutPlus);
    }
    // console.log("payload",payloadData);
    async.autoInject({
            getProfile:(cb)=>{
                // console.log("121221",finalVals);
                Service.userServices.getUsers({phoneNo:{$in:finalVals}},{},{},(err,result)=>{
                    if(err){
                        cb(err)
                    }else{
                        if(result && result.length){
                            name=result[0].name;
                            id=result[0]._id;
                            profilePicture=result[0].profilePicture;
                            cb(null)
                        }else{
                            cb(null)
                        }
                    }
                })
            },
            checkInvite:(cb)=>{
                let query = {
                    _id:payloadData.eventId,
                    isDeleted:false,
                   // invites: {$elemMatch: {phoneNo:payloadData.phoneNo,status:{$ne:"Pending"}}}
                };
                // console.log("121221",finalVals);
                query.invites={ $elemMatch: { phoneNo: {$in:finalVals},status:{$ne:"Pending"}  } }
                Service.eventServices.getEvent(query,{},{},(err,result)=>{
                    if(err){
                        cb(err);
                    }
                    else{
                        if(result.length){
                            callback(Config.APP_CONSTANTS.STATUS_MSG.ERROR.ACTION_ALREADY_DONE)
                        }
                        else{
                            cb(null);
                        }
                    }
                })
            },
            updateInvite:(getProfile,checkInvite,cb)=>{
                let query = {
                    _id:payloadData.eventId,
                    isDeleted:false,
              //      invites: {$elemMatch: {phoneNo:payloadData.phoneNo}}
                };
                let options = {lean: true,new:true};
                let dataToSet = {};
                if(payloadData.status=='Accepted'){
                    dataToSet.$inc={pending: -1,accepted:1};
                    dataToSet['invites.$.status']="Accepted";
                }
                if(id!=''){
                    dataToSet.$addToSet={attendees:id}
                }
                if(payloadData.status=='Rejected'){
                    dataToSet.$inc={pending: -1,rejected:1};
                    dataToSet['invites.$.status']="Rejected";
                }
                let phoneNo=payloadData.phoneNo;
                let finalVals=[];
                finalVals.push(payloadData.phoneNo);
                if(phoneNo.indexOf('+')!=0){
                    let withPlus="+"+phoneNo;
                    finalVals.push(withPlus);
                }
                if(phoneNo.indexOf('+')==0) {
                    let withoutPlus = phoneNo.split('');
                    withoutPlus.splice(0, 1);
                    withoutPlus= withoutPlus.join('');
                    finalVals.push(withoutPlus);
                }
                // console.log("121221",finalVals);
                query.invites={ $elemMatch: { phoneNo: {$in:finalVals}  } }
                dataToSet['invites.$.updatedAt']=new Date().getTime();
                Service.eventServices.updateEvent(query,dataToSet,options,(err,result)=>{
                    if(err){
                        cb(err);
                    }
                    else{
                        console.log("updated result",result);
                        if(result) {
                            data = result;
                            creater = result.createdBy;
                            eventName = result.title;
                            categoryId = result.categoryId;
                            templateId = result.templateId;
                            cb(null)
                        }else{
                            cb(null)
                        }
                    }
                })
            },
            getDtoken:(getProfile,updateInvite,cb)=>{
                if(id!='' && creater!=null) {
                    Service.userServices.getUsers({_id: creater}, {deviceToken: 1}, {lean: true}, (err, result) => {
                        if (err)
                            cb(err);
                        else {
                            if (result && result.length) {
                                dToken = result[0].deviceToken;
                                cb(null)
                            } else {
                                cb(null)
                            }
                        }
                    })
                }else{
                    cb(null)
                }
            },
            checkNotification:(getProfile,cb)=>{
                if(id!='') {
                    let query = {
                        eventId: payloadData.eventId,
                    };
                    if (payloadData.status == 'Accepted') {
                        query.type = 1;
                    }
                    if (payloadData.status == 'Rejected') {
                        query.type = 2;
                    }
                    Service.notificationServices.getNotificationTag(query, {}, {}, function (err, result) {
                        if (err) {
                            cb(err)
                        } else {
                            console.log("result", result);
                            if (result && result.length) {
                                notificationCount = result[0].createdBy.length;
                                notificationId = result[0]._id;
                                cb(null)
                            } else {
                                cb(null)
                            }
                        }
                    })
                }else{
                    cb()
                }
            },
            createNotification:(getProfile,checkNotification,updateInvite,cb)=>{
                if(id!=''){
                    // console.log("2222", notificationCount);
                    let query = {};
                    if (payloadData.status == 'Accepted') {
                        if (notificationCount == 0) {
                            query.text = [name + ' accepted your invite for “' + eventName + '”'];
                        } else {
                            query.text = [name + ' and ' + notificationCount + ' other accepted your invite for “' + eventName + '”'];
                        }

                        query.type = 1;
                    }
                    if (payloadData.status == 'Rejected') {
                        if (notificationCount == 0) {
                            query.text = [name + ' rejected your invite for “' + eventName + '”'];
                        } else {
                            query.text = [name + ' and ' + notificationCount + ' other rejected your invite for “' + eventName + '”'];
                        }
                        query.type = 2;
                    }
                    query.templateId = templateId;
                    query.categoryId = categoryId;
                    query.createdFor = [creater];
                    //query.$addToSet.images=userData.profilePicture
                    query.eventId = payloadData.eventId;
                    query.notificationType = "Notification";
                    query.timestamp = new Date().getTime();
                    if (notificationCount == 0) {
                        query.images = [profilePicture];
                        query.createdBy = [id];
                        Service.notificationServices.createNotification(query, (err, result) => {
                            if (err) {
                                cb(err);
                            }
                            else {
                                data = result;
                                cb(null);

                            }
                        })
                    } else {
                        query.$addToSet = {};
                        query.$addToSet.images = profilePicture;
                        query.$addToSet.createdBy = id;

                        Service.notificationServices.updateNotification({_id: notificationId}, query, {new: true}, (err, result) => {
                            if (err) {
                                cb(err);
                            }
                            else {
                                data = result;
                                cb(null);

                            }
                        })
                    }
                }else{
                    cb(null)
                }
            },
            removeNotification:(getProfile,updateInvite,cb)=>{
                if(id!='') {
                    let query = {
                        eventId:payloadData.eventId,
                        type:0
                    };
                    Service.notificationServices.updateNotification(query,{$addToSet: {deletedBy: id}},{}, (err, result) => {
                        if (err) {
                            cb(err);
                        }
                        else {
                            cb(null)

                        }
                    })
                }else{
                    cb(null)
                }
            },
            sendPush:(getProfile,getDtoken,cb)=>{
                if(id!='' && creater!=null && dToken && dToken!=''){
                    let data={
                        title:'Wethaq',
                        multi:false,
                        eventId:payloadData.eventId,
                        timestamp:new Date().getTime(),
                        images:profilePicture,
                    };
                    if(payloadData.status=='Accepted'){
                        data.body=name+' accepted your invite for “'+eventName+'”';
                        data.type=1;
                    }
                    if(payloadData.status=='Rejected'){
                        data.body=name+' rejected your invite for “'+eventName+'”';
                        data.type=2;
                    }
                    NotificationManager.sendPush(data,dToken, function (err, result) {
                        cb(null)
                    });
                    // console.log("1111",unRegistered,phoneNos,ids);
                }else{
                    cb(null)
                }
            }
        },
        (err,result)=>{
            if(err){
                callback(err)
            }
            else{
                callback(null);
            }
        })

};

const addPromoCode = function(payloadData,userData,callback){
    let phoneNos=[];
    // console.log("========",payloadData);
    async.autoInject({
        checkPromo:(cb)=>{
            let obj = {};
            obj.couponCode = payloadData.couponCode;
            Service.promoCodeServices.getPromoCode(obj,{},{},(err,result)=>{
                if(err){
                    cb(err)
                }else{
                    if(result && result.length){
                        callback(Config.APP_CONSTANTS.STATUS_MSG.ERROR.DUPLICATE_PROMO)
                    }else{
                        cb(null)
                    }
                }
            })
        },
        addPromo:(checkPromo,cb)=>{
            let obj = {};
            obj.amountType = payloadData.amountType;
            obj.amount = payloadData.amount;
            obj.couponCode = payloadData.couponCode;
            obj.endAt = payloadData.endAt;
            obj.startAt = payloadData.startAt;
            Service.promoCodeServices.createPromoCode(obj,(err,result)=>{
                if(err){
                    cb(err);
                }
                else{
                    cb(null);
                }
            })
        },
        getAllUser:(addPromo,cb)=>{
            if(payloadData.sms==true) {
                let query = {};
                query.isDeleted = false;
                let projection = {
                    phoneNo: 1
                };
                Service.userServices.getUsers(query, projection, {}, (err, result) => {
                    if (err) {
                        cb(err)
                    }
                    else {
                        if (result && result.length) {
                            let flag = 0;
                            for (let i = 0; i < result.length; i++) {
                                (function (i) {
                                    phoneNos.push(result[i].phoneNo);
                                    flag++;
                                    if (flag == result.length) {
                                        cb(null)
                                    }
                                }(i))
                            }
                        } else {
                            cb(null);
                        }
                    }
                })
            }else{
                cb(null)
            }
        },
        sendPromo:(getAllUser,cb)=>{
            if(payloadData.sms==true) {
                // console.log("phoneNos",phoneNos)
                let criteria = {
                    to: phoneNos,
                    body: "New promo code available: " + payloadData.couponCode + " valid from " + moment(payloadData.startAt).format("DD MMM YYYY") + " to " + moment(payloadData.endAt).format("DD MMM YYYY")
                };
                messageManager.sendSmsUnisoft(criteria, function ( result) {
                    // console.log("promo",result)
                    cb(null)
                })
            }else{
                cb(null)
            }
        }
    },(err,result)=>{
        if(err){
            callback(err)
        }
        else{
            callback(null);
        }
    })

};



////This is api to get all promoCode's details
const getAllPromoCodes = (payloadData,userData,callback)=>{
    let query = {
        isDeleted:false
    };
    Service.promoCodeServices.getPromoCode(query,{},{},(err,result)=>{
        if(err){
            callback(err);
        }
        else{
            callback(null,result)
        }
    })
};

//This is api to delete promo code from our database
const deletePromoCodes = (payloadData,userData,callback)=>{
    let dataToSet = {
        isDeleted:true
    };
    let query = {
        _id:payloadData._id
    };
    Service.promoCodeServices.updatePromoCode(query,dataToSet,{},(err,result)=>{
        if(err){
            callback(err)
        }
        else{
            callback(null);
        }
    })
};

let calculateEvents = function (date, date1,categoryId,cb) {
    let eventCount;
    let getCretria = {
        createdAt: {$gt: date, $lt: date1}
    };
    if(categoryId && categoryId!=undefined)getCretria.categoryId=mongoose.Types.ObjectId(categoryId)
    // console.log("count", getCretria);
    Service.eventServices.countEvents(getCretria, function (err, result) {
        if (err) {
            cb(err);
        }
        else {
            // console.log("count", result);
            eventCount ={date: moment(new Date(date)).format("DD MMM")+'-'+moment(new Date(date1)).format("DD MMM"), value: result}
            cb(null, eventCount)
        }
    })
};
let calculateUsers = function (date, date1, cb) {
    let userCount;
    let getCretria = {
        createdAt: {$gt: date, $lt: date1}
    };
    console.log("count", getCretria);
    Service.userServices.countUsers(getCretria, function (err, result) {
        if (err) {
            cb(err);
        }
        else {
            console.log("count", result);
            userCount = {date: moment(new Date(date)).format("DD MMM")+'-'+moment(new Date(date1)).format("DD MMM"), value: result}
            cb(null, userCount)
        }
    })
};
let getReports = function (payloadData, userData, callback) {
    let events = [];
    let users = [];
    let categoryId
    if(payloadData.categoryId){
        categoryId=payloadData.categoryId;
    }
    async.auto({
        calEvents: function (cb) {
                let flag = 0;
                for (let i = 0; i < 10; i++) {
                    (function (i) {
                        let day1=0;
                        let day2=0;
                        if(payloadData.days==10)  {day1=10;day2=1;}
                        if(payloadData.days==30) {day1=30;day2=3;}
                        if(payloadData.days==90)  {day1=90;day2=9;}
                        //  let date1 = new Date().getUTCDate();
                        let date1 = new Date().setDate((new Date().getDate()- day1)+ (i*day2));
                        date1 = new Date(date1).setHours(0);
                        date1 = new Date(date1).setMinutes(0);
                        date1 = new Date(date1).setSeconds(0);
                        let date2 = new Date().setDate((new Date().getDate() - day1)+(i*day2)+day2);
                        date2= new Date(date2).setHours(0);
                        date2 = new Date(date2).setMinutes(0);
                        date2 = new Date(date2).setSeconds(0);

                        calculateEvents(date1, date2,categoryId, function (err, result) {
                            events[i] = result;
                            flag++;
                            if (flag == 10) {
                                cb(null);
                            }
                        })

                    }(i))
                }
        },
        calUsers: function (cb) {
                let flag = 0;
            for (let i = 0; i < 10; i++) {
                (function (i) {
                    let day1=0;
                    let day2=0;
                    if(payloadData.days==10)  {day1=10;day2=1;}
                    if(payloadData.days==30) {day1=30;day2=3;}
                    if(payloadData.days==90)  {day1=90;day2=9;}
                    //  let date1 = new Date().getUTCDate();
                    let date1 = new Date().setDate((new Date().getDate()- day1)+ (i*day2));
                    date1 = new Date(date1).setHours(0);
                    date1 = new Date(date1).setMinutes(0);
                    date1 = new Date(date1).setSeconds(0);
                    let date2 = new Date().setDate((new Date().getDate() - day1)+(i*day2)+day2);
                    date2= new Date(date2).setHours(0);
                    date2 = new Date(date2).setMinutes(0);
                    date2 = new Date(date2).setSeconds(0);

                        calculateUsers(date1, date2, function (err, result) {
                            users[i] = result;
                            flag++;
                            if (flag == 10) {
                                cb(null);
                            }
                        })

                    }(i))
                }
        },
    }, function (err, result) {
        if (err)
            callback(err);
        else {
            console.log("4444444444444");
            callback(null, {events: events,users:users})
        }
    })
};
//This is api to list user's notifications and invites
const getAdminNotifications = (payloadData,userData,callback)=>{
    let query = {};
    let projection={};
    let options = {lean: true,sort:{createdAt:-1}};
    Service.adminNotifServices.getAdminNotifs(query,projection,options,(err,result)=>{
        if(err){
            callback(err);
        }
        else{
            callback(null,result);

        }
    })
};
const getEventDesc = (payloadData,callback)=>{
    let tags=[];
    let params=[];
    let query = {
        _id:payloadData.eventId
    };
    let populateArray=[
        {
            path:'categoryId',
            select:'_id name'
        },
        {
            path:'createdBy',
            select:'_id name email phoneNo profilePicture gender dob'
        },
        {
            path:'guardId',
            select:'_id name email phoneNo profilePicture gender dob'
        },
    ];
    let projection={
        address:1,
        createdBy:1,
        title:1,
        startAt:1,
        endAt:1,
        location:1,
        qrCodes:1,
        htmlUrl:1,
        lang:1,
        imageUrl:1,
        'invites.visitedCount':1,
        'invites.inviteCount':1,
       
        'invites.phoneNo':1,
        'invites.status':1,
    };
    let phoneNo=payloadData.phoneNo
    let finalVals=[];
    finalVals.push(payloadData.phoneNo)
    if(phoneNo.indexOf('+')!=0){
        var withPlus="+"+phoneNo;
        finalVals.push(withPlus);
    }
    if(phoneNo.indexOf('+')==0) {
        var withoutPlus = phoneNo.split('');
        withoutPlus.splice(0, 1);
        withoutPlus= withoutPlus.join('');
        finalVals.push(withoutPlus);
    }
    console.log("121221",finalVals);
    projection.qrCodes={ $elemMatch: { userId: {$in:finalVals}  } };
    projection.invites={ $elemMatch: { phoneNo: {$in:finalVals}  } };
    Service.eventServices.getEventPopulate(query,projection,{lean:true},populateArray,(err,result)=>{
        if(err){
            callback(err);
        }
        else{
            console.log("== result ==", result)
            if(result && result.length) {
                extractTags(tags, params, result[0].htmlUrl, 0);
                result[0].tags = tags;
                result[0].params = params;
                callback(null, result);
            }else{
                callback(null)
            }

        }
    })
};

const addSubAdmin = function(payloadData,userData,callback){
    let phoneNos=[];
    console.log("========",payloadData);
    async.autoInject({
        checkAdmin:(cb)=>{
            let obj = {};
            obj.name = payloadData.name;
            obj.email = payloadData.email;
            Service.adminServices.getAdmin(obj,{},{},(err,result)=>{
                if(err){
                    cb(err)
                }else{
                    if(result && result.length){
                        callback(Config.APP_CONSTANTS.STATUS_MSG.ERROR.DUPLICATE_ADMIN)
                    }else{
                        cb(null)
                    }
                }
            })
        },
        addPromo:(checkAdmin,cb)=>{
            let obj = {};
            obj.name = payloadData.name;
            obj.email = payloadData.email;
            obj.password = UniversalFunctions.CryptData(payloadData.password);
            obj.category = payloadData.category;
            obj.template = payloadData.template;
            obj.package = payloadData.package;
            obj.user = payloadData.user;
            obj.event = payloadData.event;
            obj.guard = payloadData.guard;
            obj.promo = payloadData.promo;
            obj.chat = payloadData.chat;
            obj.superAdmin = false;
            Service.adminServices.createAdmin(obj,(err,result)=>{
                if(err){
                    cb(err);
                }
                else{
                    cb(null);
                }
            })
        }
    },(err,result)=>{
        if(err){
            callback(err)
        }
        else{
            callback(null);
        }
    })

};

////This is api to get all guard's details
const getAllSubAdmins = (payloadData,userData,callback)=>{

    let data;
    async.autoInject({
            getAllGuard:function(cb){
                let query = {};
                query.superAdmin=false;
                let options = {
                    sort:{registrationDate:-1}
                };
                Service.adminServices.getAdmin(query,{__v:0,accessToken:0,password:0,socketId:0,passwordResetTokenTime:0},options,function(err,result){
                    if(err){
                        cb(err)
                    }
                    else{
                        data=result;
                        cb(null);
                    }
                })
            }
        },

        function(err,result){
            if(err){
                callback(err);
            }
            else{
                callback(null,data);
            }
        })
};

const editSubAdmin = function(payloadData,userData,callback){
    console.log("editSubAdmin",payloadData);
    async.autoInject({
        changeroles:(cb)=> {
            let query = {
                _id: payloadData._id
            };
            let obj = {};
            if (payloadData.category) {
                obj.category = payloadData.category;
            }
            if (payloadData.template) {
                obj.template = payloadData.template;
            }
            if (payloadData.package) {
                obj.package = payloadData.package;
            }
            if (payloadData.user) {
            obj.user = payloadData.user;
            }
            if(payloadData.event) {
                obj.event = payloadData.event;
            }
            if(payloadData.guard) {
                obj.guard = payloadData.guard;
            }
            if(payloadData.promo) {
                obj.promo = payloadData.promo;
            }
            if(payloadData.chat) {
                obj.chat = payloadData.chat;
            }
            Service.adminServices.updateAdmin(query,obj,{new:true},(err,result)=>{
                if(err){
                    cb(err);
                }
                else{
                    cb(null);
                }
            })
        }
    },(err,result)=>{
        if(err){
            callback(err)
        }
        else{
            callback(null);
        }
    })
};

let getScheduledNotifs = function (payloadData, userData, callback) {
    let data=[];
    async.autoInject({
            getAllGuard:function(cb){
                let query = {};
                query.isDeleted=false;
                query.isSent=false;
                query.sendAt={$gt:new Date().getTime()};
                let options = {
                    lean:true,
                    sort:{sendAt:1}
                };
                console.log("query noti",query);
                Service.scheduleNotifServices.getScheduleNotifs(query,{},options,(err,result)=>{
                    if(err){
                        cb(err)
                    }
                    else{
                        console.log("result noti",result);
                        data=result;
                        cb(null);
                    }
                })
            }
        },

        function(err,result){
            if(err){
                callback(err);
            }
            else{
                callback(null,data);
            }
        })

};

//This is api to delete template from database
const deleteScheduledNotifs = (payloadData,userData,callback)=>{
    let dataToSet = {
        isDeleted:true
    };
    let query = {
        _id:payloadData._id
    };
    Service.scheduleNotifServices.updateScheduleNotifs(query,dataToSet,{},(err,result)=>{
        if(err){
            callback(err)
        }
        else{
            callback(null);
        }
    })
};

//This is api to get all ContactUs
const getContactUs = (payloadData,userData,callback)=>{

    let data;
    let count;

    async.autoInject({
            getAllUser:function(cb){
                let query = {};
                query.isDeleted=false;
                let options = {
                    skip:payloadData.skip *  payloadData.limit,
                    limit: payloadData.limit,
                    sort:{timeStamp:-1}
                };
                let populateArray=[
                    {
                        path:'createdBy',
                        select:'_id name profilePicture gender'
                    }
                ];
                let projection={};
                Service.contactServices.getContactPopulate(query,projection,options,populateArray,(err,result)=>{
                    if(err){
                        cb(err)
                    }
                    else{
                        data=result;

                        Service.contactServices.countContact(query,function(err,result1){
                            count=result1;
                            cb(null);
                        });
                    }
                })
            }
        },

        function(err,result){
            if(err){
                callback(err);
            }
            else{
                callback(null,{data:data,count:count});
            }
        })
};
const blockUnblockEvent = (payloadData,userData,callback)=>{
    let inviteeLength=0;
    let invitees=[];
    // let sent=false;
    let updated=false;
    let eventTitle='';
    let status='';
    if(payloadData.status==true) status="blocked";
    if(payloadData.status==false) status="unlocked";
    let deviceToken='';
    // let phoneNos=[];
    async.autoInject({
            getEvent:(cb)=>{
                let query = {
                    _id:payloadData.eventId,
                };
                let options = {lean: true,new:true};
                let dataToSet = {};
                let populateArray=[
                    {
                        path:'createdBy',
                        select:'_id deviceToken'
                    }
                ];
                Service.eventServices.getEventPopulate(query,dataToSet,options,populateArray,(err,result)=>{
                    if(err){
                        cb(err);
                    }
                    else{
                        if(result[0].isBlocked==payloadData.status)  callback(Config.APP_CONSTANTS.STATUS_MSG.ERROR.ACTION_DONE)
                        else {
                            console.log("blockUnblockEvent result", result);
                            inviteeLength = result[0].invites.length;
                            deviceToken = result[0].createdBy.deviceToken;
                            eventTitle = result[0].title;
                            invitees = result[0].invites;
                            cb(null)

                           /* if (result[0].inviteAt >= new Date().getTime()) {
                                cb(null)
                            }
                            if (result[0].inviteAt <= new Date().getTime()) {
                                if (invitees && invitees.length) {
                                    sent = true;
                                    let flag = 0;
                                    for (let i = 0; i < invitees.length; i++) {
                                        (function (i) {
                                            phoneNos.push(invitees[i].phoneNo);
                                            flag++;
                                            if (flag == invitees.length) {
                                                console.log("phone", phoneNos);
                                                cb(null);
                                            }
                                        }(i))
                                    }
                                } else {
                                    cb(null)
                                }
                            }*/
                        }
                    }
                })
            },
            updateEvent:(getEvent,cb)=>{

                    Service.eventServices.updateEvent({_id: payloadData.eventId}, {isBlocked: payloadData.status,cancelReason:payloadData.reason}, {}, (err, result) => {
                        if (err) {
                            cb(err)
                        } else {
                            updated=true;
                            cb(null)
                        }
                    })
            },
         /*   sendMessage:(updateEvent,cb)=>{
                if(sent==true && updated==true && phoneNos.length){
                    console.log("phoneNos", phoneNos);
                    let criteria = {
                        to: phoneNos
                    };
                    criteria.body ="Event "+eventTitle.toString()+" you were invited to, was "+status+" by the admin for the reason \n";
                    criteria.body+= payloadData.reason+"\n";
                   // criteria.body="Event "+eventTitle+" you were invited to,was "+status+" by the admin for the reason: "+payloadData.reason;

                    console.log("criteria reg", criteria.body);
                    messageManager.sendSmsUnisoft(criteria, function (err, result1) {
                        console.log("registered sms", err, result1);
                        cb(null);

                    });
                }else{
                    cb(null)
                }
            },*/
            sendPush:(updateEvent,cb)=>{
                if(/*sent==true &&*/ updated==true /*&& phoneNos.length*/){
                    // console.log("phoneNos", phoneNos);
                    let pushData={
                        title:"Event Blocked",
                        body:"Your event "+eventTitle+" was "+status+" by the admin",
                        multi:false,
                        type:8
                    };
                    NotificationManager.sendPush(pushData,deviceToken, function (err, result) {
                        cb(null)
                    })
                }else{
                    cb(null)
                }
            }
        },
        (err,result)=>{
            if(err){
                callback(err)
            }
            else{
                callback(null);
            }
        })

};
//This is api to add more guests in event
const addInvites = (payloadData,userData,callback)=>{
    let data;
    let invites;
    let title;
    let message='';
    let startAt;
    let categoryId;
    let templateId;
    let imageUrl;
    let eventId;
    let inviteAt;
    if(payloadData.invites) {
        invites = payloadData.invites
    }
    let ids=[];
    let qrs=[];
    let qrCode=false;
    let unRegistered=[];
    let dTokens=[];
    let existing=[];
    let invitees=[];
    let oldNos=[];
    let location=[]
    let map="";
    let lang="en";
    let inviteeLength=invites.length;
    let messageSent=false;
    let userId;
    let startTime=new Date().setUTCMinutes(new Date().getUTCMinutes()-2);
    let endTime=new Date().setUTCMinutes(new Date().getUTCMinutes()+2);
    console.log("payload",payloadData);
    async.autoInject({
            getEvent:(cb)=>{
                let populateArray=[
                    {
                        path:'createdBy',
                        select:'_id wallet'
                    },
                ];
                Service.eventServices.getEventPopulate({_id:payloadData.eventId},{},{},populateArray,(err,result)=>{
                    if(err) {
                        cb(err)
                    }else{
                        if(result && result.length) {
                            qrCode=result[0].isQrCode;
                            userId=result[0].createdBy;
                            eventId=result[0]._id;
                            title=result[0].title;
                            message=result[0].message;
                            lang=result[0].lang;
                            categoryId=result[0].categoryId;
                            templateId=result[0].templateId;
                            imageUrl=result[0].imageUrl;
                            startAt=result[0].startAt;
                            inviteAt=result[0].inviteAt;
                            location=result[0].location;
                            data=result[0];
                            if (result[0].invites.length) {
                                let flag=0;
                                for (let i = 0; i < result[0].invites.length; i++) {
                                    (function (i) {
                                        oldNos.push({
                                            phoneNo:result[0].invites[i].phoneNo,
                                            name:result[0].invites[i].name,
                                            email:result[0].invites[i].email
                                        });
                                        flag++;
                                        if (flag == result[0].invites.length) {
                                            console.log("first",invites);
                                            console.log("second",oldNos);
                                            invites=(_.differenceBy(invites,oldNos, 'phoneNo'));
                                            inviteeLength=invites.length;
                                            console.log("third",invites);
                                            cb(null);
                                        }
                                    }(i))
                                }
                            }else{
                                cb(null)
                            }
                        }else{
                            cb(null)
                        }
                    }
                })
            },
            updateWallet:(getEvent,cb)=>{
                if((userId.wallet)<inviteeLength){
                            callback(Config.APP_CONSTANTS.STATUS_MSG.ERROR.INSUFFICIENT_BALANCE)
                }
                if((userId.wallet)>=inviteeLength){
                    messageSent=true;
                            cb(null)

                }
            },
            checkInvites:(updateWallet,cb)=>{
                console.log("check invites",invites.length,inviteAt,startTime,endTime)
                if(payloadData.invites && invites.length /*&& inviteAt>startTime*/ && inviteAt<endTime){
                    console.log("invites",invites);
                    let flag = 0;
                    for (let i = 0; i < invites.length; i++) {
                        (function (i) {
                            let id=eventId.toString();
                            let number=(invites[i].phoneNo.replace("+","")).toString();
                           // let link='http://34.211.166.21/wethaq_admin/#/invitation/'+id+'/'+number;
                            let link='http://admin.wethaqapp.net/wethaq_admin/#/invitation/'+id+'/'+number;
                            let subject = "Event Invite";
                            TinyURL.shorten(link, function(res) {
                                link = res
                                // let template ='<style>@import url(http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,700italic,300,400,700);html,body{margin:0;padding:0;background-color:#ededed;font-family:Open Sans,sans-serif;font-weight:300}a h1,h2,h3,h4,h5,h6,span,p{margin:0;padding:0;border:0;outline:0;font-size:100%;background:transparent;font-weight:normal}a,a:hover,a:focus,a:active{text-decoration:none;outline:none}a{-webkit-transition:all 0.4s ease-in-out;-moz-transition:all 0.4s ease-in-out;-ms-transition:all 0.4s ease-in-out;-o-transition:all 0.4s ease-in-out}input,textare,select{font-weight:normal}*{outline:none}*{box-sizing:border-box}::i-block-chrome,body{-webkit-text-stroke:rgba(255, 255, 255, 0.01) 0.1px}select::-ms-expand{display:none}.clearfix{clear:both}ul{margin:0;padding:0}li{list-style-type:none}</style><table style="width:679px;background-color:#ededed;padding:10px;margin:0 auto 60px"><tr><td style="font-weight: 600; font-size: 38px; text-align: center; padding-top: 19px;"> <img style="max-width:250px; margin-bottom: 22px;" src="https://s3-us-west-2.amazonaws.com/wethaqbucket/profilePic_1523699389214imageBackground.png"></td></tr><tr style=" background-color: #fff; padding: 32px 26px; display: block; margin-top: 0px; border-radius: 5px;"><td style="width: 679px;"><p style=" padding:32px 16px 32px 16px;background-color:#f5f5f5;font-size:18px;color:#333;margin:0px;">Hi '+invites[i].name+'</p><p style="padding:0px 16px 0px 16px;background-color:#f5f5f5;font-size:18px;color:#333;margin:0px;">'+message+'</p><p style="padding:32px 16px 32px 16px;background-color:#f5f5f5;font-size:18px;color:#333;margin:0px;">- Team Wethaq</p></td><td style="display: block;"><a href='+link+' style="background-color: #C78CF5; padding: 14px 0px; color: #fff; font-size: 14px; font-weight: 500; border-radius: 4px; display: block; width: 192px; text-align: center; margin: 0 auto; text-decoration: none;margin-top: 28px;">JOIN NOW</a></td></tr></table>';
                                // UniversalFunctions.sendMail(invites[i].email, subject, template, function (err,result) {
                                //     console.log("res",err,result)
                                // });
                                invites[i].status = 'Pending';
                                let query = {
                                    phoneNo: invites[i].phoneNo,
                                    _id: {$ne: userData._id}
                                };
                                Service.userServices.getUsers(query, {}, {lean: true}, function (err, result) {
                                    if (err) {
                                        cb(err)
                                    } else {
                                        if (result.length) {
                                            existing.push({"_id": result[0]._id, "phoneNo": invites[i].phoneNo});
                                            ids.push(result[0]._id);
                                            dTokens.push(result[0].deviceToken);
                                        }
                                        let criteria = {
                                            to: [invites[i].phoneNo],
                                        };
                                        // console.log("temp",template);
                                        // criteria.body+="From: "+userData.name+"\n";
                                        criteria.body = "";
                                        if (message && message != '') {
                                            if (payloadData.lang == "en") {
                                                criteria.body += "Hello " + invites[i].name + ", " + message + "\n";
                                            } else if (payloadData.lang == "ar") {
                                                criteria.body += "مرحبًا" + invites[i].name + ", " + message + "\n";
                                            } else {
                                                criteria.body += "مرحبًا" + invites[i].name + ", " + message + "\n";
                                            }
                                            //   criteria.body+="Hello "+invites[i].name+", "+ message + "\n";
                                        } else {
                                            if (payloadData.lang == "en") {
                                                criteria.body += "Hello " + invites[i].name + " It’s an honor to invite you to " + title + "\n";
                                            } else if (payloadData.lang == "ar") {
                                                criteria.body += "مرحبًا" + invites[i].name + "إنه لشرف أن أدعوك " + title + "\n";
                                            } else {
                                                criteria.body += "مرحبًا" + invites[i].name + "إنه لشرف أن أدعوك " + title + "\n";
                                            }

                                        }
                                        let template = '<html><head></head><body><table style="border-collapse:collapse;table-layout:fixed;min-width:320px;width:100%;background-color:#fff;text-align:left;direction:ltr"><tbody><tr><td><div style=";text-align:left;direction:ltr"><div style="Margin:0 auto;max-width:560px;min-width:280px;width:280px;width:calc(28000% - 167440px);text-align:left;direction:ltr"> <br></div><div style="Margin:0 auto;max-width:600px;min-width:320px;width:320px;width:calc(28000% - 167400px);text-align:left;direction:ltr"><div style="font-size:26px;line-height:32px;Margin-top:6px;Margin-bottom:20px;color:#c3ced9;font-family:Roboto,Tahoma,sans-serif;Margin-left:20px;Margin-right:20px;text-align:left;direction:ltr" align="center"><div align="center" style=";text-align:left;direction:ltr"> <img style="display:block;height:auto;width:100%;border:0;max-width:230px" src="https://s3-us-west-2.amazonaws.com/wethaqbucket/profilePic_1523699389214imageBackground.png" alt="" width="230"></div></div></div></div><div style=";text-align:left;direction:ltr"><div style="Margin:0 auto;max-width:600px;min-width:320px;width:320px;width:calc(28000% - 167400px);word-wrap:break-word;word-break:break-word;text-align:left;direction:ltr"><div style="border-collapse:collapse;display:table;width:100%;background-color:#ffffff;text-align:left;direction:ltr"><div style="text-align:left;color:#8f8f8f;font-size:16px;line-height:24px;font-family:Open Sans,sans-serif;max-width:600px;min-width:320px;width:320px;width:calc(28000% - 167400px);text-align:left;direction:ltr"><div style="Margin-left:20px;Margin-right:20px;Margin-top:24px;text-align:left;direction:ltr"><div style=";text-align:left;direction:ltr"><h1 style="Margin-top:0;Margin-bottom:20px;font-style:normal;font-weight:normal;color:#404040;font-size:28px;line-height:36px;text-align:center;text-align:left;direction:ltr">' + title + '</h1></div></div><div style="Margin-left:20px;Margin-right:20px;text-align:left;direction:ltr"><div style="line-height:10px;font-size:1px;text-align:left;direction:ltr"></div></div><div style="Margin-left:20px;Margin-right:20px;text-align:left;direction:ltr"><div style="line-height:10px;font-size:1px;text-align:left;direction:ltr"></div></div><div style="Margin-left:20px;Margin-right:20px;text-align:left;direction:ltr"><div style=";text-align:left;direction:ltr"><h1 style="Margin-top:0;Margin-bottom:0;font-style:normal;font-weight:normal;color:#404040;font-size:28px;line-height:36px;text-align:center;text-align:left;direction:ltr">' + invites[i].name + 'أهلا بك</h1><h1 style="Margin-top:20px;Margin-bottom:0;font-style:normal;font-weight:normal;color:#404040;font-size:28px;line-height:36px;text-align:center;text-align:left;direction:ltr"> <strong>مرحباً بكم في تطبيق وثاق </strong></h1><h3 style="Margin-top:20px;Margin-bottom:12px;font-style:normal;font-weight:normal;color:#706f70;font-size:17px;line-height:26px;font-family:Cabin,Avenir,sans-serif;text-align:center;text-align:left;direction:ltr">التصميم المخصص للدعوة</h3><h3 style="Margin-top:20px;Margin-bottom:12px;font-style:normal;font-weight:normal;color:#706f70;font-size:17px;line-height:26px;font-family:Cabin,Avenir,sans-serif;text-align:center;text-align:left;direction:ltr">' + criteria.body + '</h3></div></div><div style="font-size:12px;font-style:normal;font-weight:normal;line-height:19px;text-align:left;direction:ltr" align="center"> <img style="border:0;display:block;height:auto;width:100%;max-width:900px" alt="" width="600" src=' + imageUrl + '></div><div style="Margin-left:20px;Margin-right:20px;text-align:left;direction:ltr"><div style=";text-align:left;direction:ltr"><h2>' + link + ' : إليك الرابط</h2></div></div></div></div></div></div></td></tr></tbody></table></body></html>'

                                        UniversalFunctions.sendMail(invites[i].email, subject, template, function (err, result) {
                                            console.log("res", err, result)
                                        });

                                        if (lang == "en") {
                                            criteria.body += "For more info please check the link: " + link + "\n";
                                        }

                                        if (lang == "ar") {
                                            criteria.body += "لمزيد من المعلومات يرجى التحقق من الرابط: " + link + "\n";
                                        }
                                        messageManager.sendSmsUnisoft(criteria, function ( result) {
                                            console.log("sms", criteria, result);
                                            // if (err == null) {
                                            if (result.success=="true") {
                                                invites[i].statusCode = 200;
                                                // invites[i].message = [result.Data.MessageEn, result.Data.MessageAr];
                                                invites[i].message = [ "SMS sent successfully", "تم الإرسال"];
                                                invitees.push(invites[i]);
                                            } else {
                                                console.log("1234555", err);
                                                invites[i].statusCode = 400;
                                                // invites[i].message = [err.MessageEn, err.MessageAr];
                                                invites[i].message = ["Mobile(s) number(s) is not specified or incorrect", "الأرقام المرسل لها غير صحيحه أو فارغه"];
                                                invitees.push(invites[i]);
                                            }
                                            flag++;
                                            if (flag == invites.length) {
                                                cb(null);
                                            }
                                        });

                                    }
                                })
                            })

                        }(i))
                    }
                    console.log("1111",unRegistered,ids)
                }else{
                    cb(null)
                }
            },
            createQr:(updateWallet,cb)=>{
                if(qrCode==true && invites && invites.length  /*&& inviteAt>startTime*/ && inviteAt<endTime){
                    console.log("payloadData qrCode",qrCode);
                    let flag=0;
                    for(let i=0;i<invites.length;i++){
                        (function (i) {
                            let  pic = "qr-userId="+ invites[i].phoneNo+"eventId=" + eventId
                            let qr_svg = qr.image(pic, {type: 'png'});
                            let date=new Date().getTime();
                            let stream = qr_svg.pipe(require('fs').createWriteStream('uploads/' +date+'eventId=' + eventId+'.png'));
                            stream.on('finish', function (err, result) {
                                let  qrImage = Path.resolve(".") + "/uploads/" +date+'eventId=' + eventId+'.png';
                                let client = knox.createClient({
                                    key: Config.awsS3Config.s3BucketCredentials.accessKeyId
                                    , secret: Config.awsS3Config.s3BucketCredentials.secretAccessKey
                                    , bucket: Config.awsS3Config.s3BucketCredentials.bucket
                                });
                                let s3ClientOptions = {'x-amz-acl': 'public-read'};
                                client.putFile(qrImage,date+ eventId+'.png', s3ClientOptions, function (err, res) {
                                    console.log(err);
                                    deleteFile(qrImage);
                                    _.map(invites, function(obj) {
                                        if (obj.phoneNo ==invites[i].phoneNo)
                                            obj.qr=Config.awsS3Config.s3BucketCredentials.s3URL +date + eventId+  '.png';
                                    });
                                    qrs.push({"qr":Config.awsS3Config.s3BucketCredentials.s3URL +date + eventId+ '.png',"userId":invites[i].phoneNo});
                                    flag++;
                                    if (flag == invites.length) {
                                        console.log("qr",qrs);
                                        cb(null);
                                    }

                                });
                            });
                        }(i))
                    }
                }else{
                    cb(null)
                }
            },
            insertEvent:(checkInvites,createQr,cb)=>{
                console.log("12345",invitees);
                let query = {
                    _id:payloadData.eventId
                };
                let dataToSet ={};
                if(invitees && invitees.length){
                    if(qrs && qrs.length) {
                        dataToSet.$addToSet={qrCodes:{$each:qrs},invites: {$each: invitees},unRegistered: {$each: unRegistered}};
                    }else{
                        dataToSet.$addToSet={invites: {$each: invitees},unRegistered: {$each: unRegistered}};
                    }
                    dataToSet.$inc={pending: invitees.length,total:invitees.length};
                }else{
                    dataToSet.$addToSet={invites: {$each: invites}};
                    dataToSet.$inc={pending: (invites).length,total:(invites).length};
                }
                console.log("update event",dataToSet);
                Service.eventServices.updateEvent(query,dataToSet,{lean:true,new:true},(err,result)=>{
                    if(err){
                        cb(err);
                    }
                    else{
                        cb(null);
                    }
                })
            },
            sendNotificaiton:(insertEvent,checkInvites,cb)=>{
                if(ids && ids.length){
                    console.log("notification",ids);
                    let query={
                        createdFor:ids,
                        createdBy:[userData._id],
                        type:0,
                        text:[title],
                        eventId:payloadData.eventId,
                        timestamp:startAt,
                        categoryId:categoryId,
                        templateId:templateId,
                        notificationType:'Invite',
                        images:[{original:imageUrl,thumbnail:imageUrl}],
                    };
                    Service.notificationServices.createNotification(query,function (err,result) {
                        if(err){
                            cb(err)
                        }else{
                            cb(null);
                        }
                    });
                }else{
                    cb(null)
                }
            },
        },
        (err,result)=>{
            if(err){
                callback(err);
            }
            else{
                callback(null,data);
            }
        })
};
function deleteFile(path) {
    fsExtra.remove(path, function (err) {
        console.log('error deleting file>>', err)
    });
}
const addEditAdvertisements = (payloadData,userData,callback)=>{
    // let advt;
    let dataToSave = {
        // _id:Joi.string().optional(),
        // categoryId:payloadData.categoryId,
        // // isDeleted:payloadData.isDeleted,
        // // isBlocked:payloadData.isBlocked,
        // url:payloadData.url,
        // adImageURL:payloadData.adImageURL,
        // gender:payloadData.gender,
        // startAge:payloadData.startAge,
        // endAge:payloadData.endAge,
    };
    if(payloadData.categoryId ){ dataToSave.categoryId = payloadData.categoryId}
    if(payloadData.categoryId=="")dataToSave.categoryId=null
    if(payloadData.url){ dataToSave.url = payloadData.url}
    if(payloadData.adImageURL){ dataToSave.adImageURL = payloadData.adImageURL}
    if(payloadData.gender || payloadData.gender==""){ dataToSave.gender = payloadData.gender}
    if(payloadData.startAge){ dataToSave.startAge = payloadData.startAge}
    if(payloadData.endAge){ dataToSave.endAge = payloadData.endAge}
    
    if(payloadData.long && payloadData.lat){dataToSave.location = [payloadData.long,payloadData.lat]}
    if(payloadData.address){dataToSave.address = payloadData.address}  


    if(payloadData.isDeleted){
        dataToSave.isDeleted = payloadData.isDeleted
    }else dataToSave.isDeleted=false
    if(payloadData.isBlocked){
        dataToSave.isBlocked = payloadData.isDeleted
    }else dataToSave.isBlocked=false
    if(payloadData._id){
        Service.advertisementServices.updateAdvertisement({_id:payloadData._id},
            {$set:dataToSave},{new:true},(err,data)=>{
            if(err){
                callback(err);
            }else{
                // advt = data;
             callback(null,data);
            }
        });
    }else{
        Service.advertisementServices.createAdvertisement(dataToSave,(err,data)=>{
            if(err){
                callback(err);
            }else{
                // advt = data;                
                callback(null,data);
            }
        });
    }
};
const listAdvertisements = (payloadData,userData,callback)=>{
    let response = {};
    let criteria = {
        isDeleted:false,
        isBlocked:false
    };
    if(payloadData._id){
        criteria._id = payloadData._id
    }
    if(payloadData.startAge){
        criteria.startAge = {$gt:payloadData.startAge}
    }
    if(payloadData.endAge){
        criteria.endAge = {$lt:payloadData.endAge}
    }
    if(payloadData.gender){
        criteria.gender = payloadData.gender
    }
    

    // if(payloadData.lat && payloadData.long){
    //     criteria.lat = payloadData.lat
    // }
    // if(payloadData.long){
    //     criteria.long = payloadData.long
    // }

    if(payloadData.address){
        criteria.address = payloadData.address
    }
   
    console.log("= = criteria = =", criteria);

    async.autoInject({
        getAdvt:(cb)=>{
            Service.advertisementServices.getAdvertisementPopulate(criteria,{},{skip:payloadData.skip,limit:payloadData.limit},
                [{path:"categoryId",select:{name:1}}],(err,data)=>{
                if(err){
                    callback(err);
                }else{
                    response.advertisement = data;
                    cb(null,data);
                }
            });
        },
        getCount:(cb)=>{
            Service.advertisementServices.countAdvertisement(criteria,(err,data)=>{
                if(err){
                    callback(err);
                }else{
                    response.count = data;
                    cb(null,data);
                }
            });
        }
    },(err,result)=>{
        if(err){
            callback(err);
        }else{
            callback(null,response);
        }
    });

};
module.exports = {
    adminLogin:adminLogin,
    getAllUsers:getAllUsers,
    blockUsers:blockUsers,
    createCategories:createCategories,
    getCategories:getCategories,
    updateCategories:updateCategories,
    deleteCategories:deleteCategories,
    getPackage:getPackage,
    createPackage:createPackage,
    updatePackage:updatePackage,
    deletePackage:deletePackage,
    createTemplate:createTemplate,
    updateTemplates:updateTemplates,
    deleteTemplates:deleteTemplates,
    getTemplates:getTemplates,
    getDashBoard:getDashBoard,
    createBackground:createBackground,
    getBackgrounds:getBackgrounds,
    deleteBackground:deleteBackground,
    uploadImageAWS:uploadImageAWS,
    getAllGuards:getAllGuards,
    getAllEvents:getAllEvents,
    getRequestUsers:getRequestUsers,
    getUserRequests:getUserRequests,
    getRequestDesc:getRequestDesc,
    sendPush:sendPush,
    actionInvite:actionInvite,
    addPromoCode:addPromoCode,
    getAllPromoCodes:getAllPromoCodes,
    deletePromoCodes:deletePromoCodes,
    getAdminNotifications:getAdminNotifications,
    getReports:getReports,
    getContactUs:getContactUs,
    getEventDesc:getEventDesc,
    addSubAdmin:addSubAdmin,
    getAllSubAdmins:getAllSubAdmins,
    editSubAdmin:editSubAdmin,
    getScheduledNotifs:getScheduledNotifs,
    blockUnblockEvent:blockUnblockEvent,
    deleteScheduledNotifs:deleteScheduledNotifs,
    addInvites:addInvites,
    addEditAdvertisements:addEditAdvertisements,
    listAdvertisements:listAdvertisements
};