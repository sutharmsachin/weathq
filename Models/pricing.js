/**
 * Created by Shumi on 7/7/17.
 */


var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var pricing = new Schema({
    totalSms:{type:Number,default:0},
    totalPrice:{type: Number,default:0},
    isDeleted:{type: Boolean, default: false}
});

module.exports = mongoose.model('pricing',pricing);