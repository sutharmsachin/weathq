/**
 * Created by cbluser51 on 15/3/18.
 */

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Config = require('../Config');


var adminNotifs = new Schema({
    title:{type: String, default: ''},
    body:{type: String, default: ''},
    userId:{type: Schema.ObjectId, ref: 'user'},
    eventId:{type: Schema.ObjectId, ref: 'event'},
    timestamp:{type: Date, default: Date.now},
    createdAt: {type: Number, default:new Date().getTime()}
});

module.exports = mongoose.model('adminNotifs', adminNotifs);