/**
 * Created by Shumi Gupta on 29/8/17.
 */


var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Config = require('../Config');


var category = new Schema({
    name:[{type: String, default: null}], // 0 for english 1 for arabic
    categoryImage: {
        original: {type: String, default: ''},
        thumbnail: {type: String, default: ''}
    },
    icon: {
        original: {type: String, default: ''},
        thumbnail: {type: String, default: ''}
    },
    createdAt: {type: Date, default: Date.now, required: true},
    isDeleted:{type: Boolean, default:false}
});

module.exports = mongoose.model('category', category);