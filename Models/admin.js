/**
 * Created by Shumi Gupta on 29/3/17.
 */

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Config = require('../Config');

//these are login attempts of admin,we save ip addresses of every attempt for admin login
var LoginAttempts = new Schema({
    timestamp: {type: Date, default: Date.now},
    validAttempt: {type: Boolean, required: true},
    ipAddress: {type: String, required: true}
});

var admin = new Schema({
    name: {type: String, trim: true, default: null},
    socketId:{type: String,default:''},
    deviceToken: {type: String,default:null, trim: true},
    email: {type: String, trim: true, unique: true, index: true},
    accessToken: {type: String, trim: true, index: true, unique: true, sparse: true},
    password: {type: String, required:true},
    passwordResetToken: {type: String, trim: true, unique: true, sparse:true},
    passwordResetTokenTime: {type: Date, default: Date.now},
    registrationDate: {type: Date, default: Date.now, required: true},
    loginAttempts: [LoginAttempts],
    category:{
        add:{type: Boolean, default: false},
        edit:{type: Boolean, default: false},
        view:{type: Boolean, default: false},
        delete:{type: Boolean, default: false}
    },
    template:{
        add:{type: Boolean, default: false},
        edit:{type: Boolean, default: false},
        view:{type: Boolean, default: false},
        delete:{type: Boolean, default: false}
    },
    package:{
        add:{type: Boolean, default: false},
        edit:{type: Boolean, default: false},
        view:{type: Boolean, default: false},
        delete:{type: Boolean, default: false}
    },
    user:{
        add:{type: Boolean, default: false},
        edit:{type: Boolean, default: false},
        view:{type: Boolean, default: false},
        block:{type: Boolean, default: false}
    },
    event:{
        view:{type: Boolean, default: false}
    },
    guard:{
        add:{type: Boolean, default: false},
        view:{type: Boolean, default: false},
        edit:{type: Boolean, default: false},
        block:{type: Boolean, default: false},
    },
    promo:{
        add:{type: Boolean, default: false},
        view:{type: Boolean, default: false},
        delete:{type: Boolean, default: false}
    },
    chat:{
        view:{type: Boolean, default: false},
        allow:{type: Boolean, default: false}
    },
    profilePicture: {
        original: {type: String, default: ''},
        thumbnail: {type: String, default: ''}
    },
    superAdmin : {type: Boolean, default: false},
});

module.exports = mongoose.model('admin', admin);