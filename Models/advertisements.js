/**
 * Created by cbluser51 on 7/3/19.
 */


let mongoose = require('mongoose');
let Schema = mongoose.Schema;
let Config = require('../Config');

let advertisements = new Schema({
    categoryId:{type: Schema.ObjectId, ref: 'category'},
    url : {type : String, default : ""},
    startAge: {type: Number},
    endAge: {type: Number},
    location : { type: [Number], default: [0,0] },
    address: { type: String, trim: true },
    gender:  {
        type: String, enum: [
            Config.APP_CONSTANTS.DATABASE.GENDER_TYPE.MALE,
            Config.APP_CONSTANTS.DATABASE.GENDER_TYPE.FEMALE,
            ""
        ]
    },
    adImageURL : {
        original:{type: String, default: ""},
        thumbnail:{type: String, default: ""}
    },
    clicks : {type : Number, default : 0},
    isDeleted : {type : Boolean, default : false,required:true},
    isBlocked : {type : Boolean, default : false,reqquired:true},
    createdOn:{type: Date, default:Date.now, required:true}
});

advertisements.index({ "location": "2dsphere" })
module.exports = mongoose.model('advertisements', advertisements);