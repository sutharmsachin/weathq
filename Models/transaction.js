/**
 * Created by cbluser51 on 19/3/18.
 */

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Config = require('../Config');
var transaction = new Schema({
    userId:{type:Schema.ObjectId,ref:'user',default:null},
    requestId:{type:Schema.ObjectId,ref:'request',default:null},
    eventId:{type:Schema.ObjectId,ref:'event',default:null},
    trxId:{type: String, default: '', trim: true},
    price:{type: Number, default: 0},
    timeStamp: {type: Number, default:new Date().getTime()},
    type:{ type:String,trim:true,
        enum:[
            Config.APP_CONSTANTS.DATABASE.PAYMENT_OPTIONS.EVENT,
            Config.APP_CONSTANTS.DATABASE.PAYMENT_OPTIONS.CUSTOM,
            Config.APP_CONSTANTS.DATABASE.PAYMENT_OPTIONS.REQUEST
        ]
    },
    registrationDate: {type: Date, default: Date.now, required: true},
    isDeleted:{type: Boolean, default: false, required: true},
});

module.exports = mongoose.model('transaction', transaction);
