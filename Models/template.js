/**
 * Created by Shumi Gupta on 29/8/17.
 */


var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Config = require('../Config');


var template = new Schema({
        categoryId:{type: Schema.ObjectId, ref: 'category'},   //ref is given for foreign key(category here)
        name:{type: String, default: null},
        htmlUrl:{type: String},
        imageUrl:{type: String},
        tags:[{type: String}],
        params:[{type: String}],
        price:{type: Number, required:true, default:0},
        createdAt: {type: Date, default: Date.now, required: true},    //utc date format is entered by default while creating template
        isDeleted:{type: Boolean, default:false},
        isPublic:{type: Boolean, default:true},
        timeStamp: {type: Number, default:0},
        usage: {type: Number, default:0},

        lang: { type: String },
        isCustom: { type: Boolean, default: false }
});

module.exports = mongoose.model('template', template);