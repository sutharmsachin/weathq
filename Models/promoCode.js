/**
 * Created by sushant on 14/3/18.
 */


var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Config = require('../Config');

var promoCode = new Schema({
    amountType:{type: String,enum: [
        Config.APP_CONSTANTS.DATABASE.PROMO_CODE.AMOUNT,
        Config.APP_CONSTANTS.DATABASE.PROMO_CODE.PERCENTAGE]},
    amount: {type: Number},
    couponCode:{type:String,unique: true,index: true},
    registrationDate: {type: Date, default: Date.now, required: true},
    createdAt: {type: Number, default:new Date().getTime()},
    startAt: {type:Number, default: 0},
    endAt: {type:Number, default: 0},
    isDeleted:{type:Boolean,default:false},
    usedBy:[{type: Schema.ObjectId, ref: 'user'}]
});

module.exports = mongoose.model('promoCode',promoCode);