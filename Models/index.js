/**
 * Created by Shumi on 29/3/17.
 */

module.exports = {
    user : require('./user'),
    admin: require('./admin'),
    pricing : require('./pricing'),
    category : require('./category'),
    template : require('./template'),
    event : require('./event'),
    advertisement: require('./advertisements'),
    notification : require('./notification'),
    background : require('./background'),
    request : require('./request'),
    promoCode : require('./promoCode'),
    adminNotifs : require('./adminNotifs'),
    scheduleNotifs : require('./scheduleNotifs'),
    transaction : require('./transaction'),
    contact : require('./contact'),
    dummyTemplate : require('./dummyTemplate'),
    appVersions : require('./appVersions')
};
