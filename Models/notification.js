/**
 * Created by Shumi Gupta on 21/8/17.
 */


var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Config = require('../Config');


var notification = new Schema({
    type:{type: Number, default: 0},
    //0 for event invite,1 for accept invite,2 for reject invite,3 for guard invite,4 for guard accept,5 for guard reject,6 for payment,7 for guard cancel
    //text:{type: String, default: ''},
    text:[{type: String, default: ''}],
    //0 for english 1 for arab
    createdFor:[{type: Schema.ObjectId, ref: 'user', required: true}],
    createdBy:[{type: Schema.ObjectId, ref: 'user'}],
    deletedBy:[{type: Schema.ObjectId, ref: 'user'}],
    eventId:{type: Schema.ObjectId, ref: 'event'},
    templateId:{type: Schema.ObjectId, ref: 'template'},
    categoryId:{type: Schema.ObjectId, ref: 'category'},
    images: [{
        original: {type: String, default: ''},
        thumbnail: {type: String, default: ''}
    }],
    // isDeleted:{type: Boolean, default: false, required: true},
    timestamp:{type: Number, default: 0},
    notificationType:{
        type : String, enum : [
            Config.APP_CONSTANTS.DATABASE.NOTIFICATION_TYPE.INVITE,
            Config.APP_CONSTANTS.DATABASE.NOTIFICATION_TYPE.NOTIFICATION,
        ]
    },
    createdAt: {type: Number, default:new Date().getTime()}
});

module.exports = mongoose.model('notification', notification);