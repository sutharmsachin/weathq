/**
 * Created by Shumi Gupta on 29/3/17.
 */


var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Config = require('../Config');

//This is user's collection in database
var user = new Schema({
    isGuard:{type: Boolean, default:false},
    name: {type: String,index: true, trim: true},     //indexing is done for fast searching and trim to remove extra spaces from both sides of string
    email: {type: String, index: true, trim: true},
    phoneNo: {type: String, index: true, trim: true},
    password:{type: String, trim: true},
    socialId:{type: String,index: true, trim: true},
    loginType:  {
        type: String, enum: [
            Config.APP_CONSTANTS.DATABASE.LOGIN_TYPE.GOOGLE,
            Config.APP_CONSTANTS.DATABASE.LOGIN_TYPE.FACEBOOK,
            Config.APP_CONSTANTS.DATABASE.LOGIN_TYPE.PHONE_NO
        ]
    }, //enum is used where we have a choice of string
    profilePicture: {
        original: {type: String, default: ''},
        thumbnail: {type: String, default: ''}
    },
    gender:  {
        type: String, enum: [
            Config.APP_CONSTANTS.DATABASE.GENDER_TYPE.MALE,
            Config.APP_CONSTANTS.DATABASE.GENDER_TYPE.FEMALE
        ]
    },
    location:{type: [Number] },
    dob: { type: Date },
    accessToken: {type: String, trim: true, index: true},
    isDeleted:{type: Boolean, default:false},
    isBlocked:{type: Boolean, default:false},
    pushNotification:{type: Boolean, default:true},
    deviceToken: {type: String,default:null, trim: true},
    deviceType: {
        type: String, enum: [
            Config.APP_CONSTANTS.DATABASE.DEVICE_TYPES.IOS,
            Config.APP_CONSTANTS.DATABASE.DEVICE_TYPES.ANDROID
        ]
    },
    language: {
        type: String,
        default: Config.APP_CONSTANTS.DATABASE.LANGUAGE.ENGLISH,
        enum: [
            Config.APP_CONSTANTS.DATABASE.LANGUAGE.ENGLISH,
            Config.APP_CONSTANTS.DATABASE.LANGUAGE.ARAB
        ]
    },
    lang: {
        type: String,
        default: Config.APP_CONSTANTS.DATABASE.LANGUAGE.ENGLISH,
        enum: [
            Config.APP_CONSTANTS.DATABASE.LANGUAGE.ENGLISH,
            Config.APP_CONSTANTS.DATABASE.LANGUAGE.ARAB
        ]
    },
    socketId:{type: String,default:''},
    createdAt:{type: Date, default: Date.now, required: true},
    timestamp:{type: Number, default: new Date().getTime()},
    appVersion: {type: String,default:null, trim: true},
    passwordResetToken:{type: String, trim: true},
    wallet:{type: Number, default: 0},
    eventsCreated:{type: Number, default: 0},
    passwordResetTokenTime:{type: Date, default: Date.now}
});

user.index({ "location": "2dsphere" })
module.exports = mongoose.model('user',user);