/**
 * Created by cbluser51 on 16/3/18.
 */


var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Config = require('../Config');


var scheduleNotifs = new Schema({
    title:{type: String, default: ''},
    body:{type: String, default: ''},
    isSent: {type: Boolean, default: false, required: true},
    isDeleted: {type: Boolean, default: false, required: true},
    sendAt:{type: Number, default: 0},
    createdAt: {type: Number, default:new Date().getTime()}
});

module.exports = mongoose.model('scheduleNotifs', scheduleNotifs);