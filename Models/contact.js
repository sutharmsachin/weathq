/**
 * Created by cbl34 on 21/6/17.
 */
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Config = require('../Config');

var contact = new Schema({
    createdBy:{type: Schema.ObjectId, ref: 'user'},
    name:{type: String, default: '', trim: true},
    message:{type: String, default: '', trim: true},
    email:{type: String, default: '', trim: true},
    phoneNo:{type: String, default: '', trim: true},

    registrationDate: {type: Date, default: Date.now, required: true},
    timeStamp: {type: Number, default: new Date().getTime(), required: true},
    isDeleted:{type: Boolean, default: false, required: true},
});

module.exports = mongoose.model('contact', contact);