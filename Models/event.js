/**
 * Created by Shumi Gupta on 16/02/2018.
 */



let mongoose = require('mongoose');
let Schema = mongoose.Schema;

let Config = require('../Config');

//invites(guest list) for event
let invites=new Schema({
    email:{type: String, trim: true, default: ''},
    name:{type: String, trim: true, default: ''},
    phoneNo: {type: String, trim: true, default: ''},
    statusCode: {type: String, trim: true, default: ''},
    qr: {type: String, trim: true, default: ''},
    message: [{type: String, trim: true, default: ''}],
    isDeleted:{type: Boolean, default: false, required: true},
    inviteCount:{type:Number},
    visitedCount:{type:Number,default:0},
    createdAt: {type: Number
        // , default:new Date().getTime()
    },
    updatedAt: {type: Number, default:0},
    checkedAt: {type: Number, default:0},
    status: {
        type: String, trim: true,
        default: Config.APP_CONSTANTS.DATABASE.INIVTE_STATUS.AWAITED,
        enum: [
            Config.APP_CONSTANTS.DATABASE.INIVTE_STATUS.AWAITED,
            Config.APP_CONSTANTS.DATABASE.INIVTE_STATUS.ACCEPTED,
            Config.APP_CONSTANTS.DATABASE.INIVTE_STATUS.REJECTED,
            Config.APP_CONSTANTS.DATABASE.INIVTE_STATUS.PENDING,
            Config.APP_CONSTANTS.DATABASE.INIVTE_STATUS.CHECKED
        ]
    },
});

//invites(guest list) for event
let reminder=new Schema({
    message: {type: String, trim: true, default: ''},
    sent:{type: Boolean, default:false,required:true},
    createdAt: {type: Number, default:new Date().getTime()},
    inviteAt: {type: Number, default:0},
});

let guards=new Schema({
    phoneNo: {type: String, trim: true, default: ''},
    userId:{type: Schema.ObjectId, ref: 'user'},
    isDeleted:{type: Boolean, default: false, required: true},
    createdAt: {type: Number, default:new Date().getTime()},
    updatedAt: {type: Number, default:0},
    status: {
        type: String, trim: true,
        default: Config.APP_CONSTANTS.DATABASE.INIVTE_STATUS.PENDING,
        enum: [
            Config.APP_CONSTANTS.DATABASE.INIVTE_STATUS.AWAITED,
            Config.APP_CONSTANTS.DATABASE.INIVTE_STATUS.ACCEPTED,
            Config.APP_CONSTANTS.DATABASE.INIVTE_STATUS.REJECTED,
            Config.APP_CONSTANTS.DATABASE.INIVTE_STATUS.CANCELLED,
            Config.APP_CONSTANTS.DATABASE.INIVTE_STATUS.PENDING,
        ]
    },
});

//event details
let event = new Schema({
    categoryId:{type: Schema.ObjectId, ref: 'category'},
    templateId:{type: Schema.ObjectId, ref: 'template'},
    createdBy:{type: Schema.ObjectId, ref: 'user'},
    guardId:{type: Schema.ObjectId, ref: 'user',default:null},
    attendees:[{type: Schema.ObjectId, ref: 'user'}],
    checkIns:[{type: String, default: ''}],
    unRegistered:[{type: String, default: ''},],
    title:{type: String, default: ''},
    message:{type: String, default: ''},
    packageType: {
        type:String,enum: [
        Config.APP_CONSTANTS.DATABASE.PACKAGES.FREE,
        Config.APP_CONSTANTS.DATABASE.PACKAGES.WHATSAPP,
        Config.APP_CONSTANTS.DATABASE.PACKAGES.PREMIUM
    ]},
    imageUrl:{type: String, default: ''},
    cancelReason:{type: String, default: ''},
    htmlUrl:{type: String, default: ''},
    lang:{type: String, default: ''},
  //  qrCodes:[{type: String, default: ''}],
    qrCodes:[{
        qr: {type: String, default:""},
        userId: {type: String, default:""}
    }],
    categoryPrice:{type: Number, required:true, default:0},
    totalSms:{type: Number, required:true, default:0},
    smsPrice:{type: Number, required:true, default:0},
    totalPrice:{type: Number, required:true, default:0},
    inviteAt: {type: Number, default:0},
    startAt: {type: Number, default:0},
    endAt: {type: Number, default:0},
    pending: {type: Number, default:0},
    accepted: {type: Number, default:0},
    rejected: {type: Number, default:0},
    checked: {type: Number, default:0},
    total: {type: Number, default:0},
    reminders: {type: Number, default:0},
    address: {type: String, trim: true, default: ''},
    location: {type: [Number], index: '2dsphere'}, //2d sphere is used for location based sorting
    createdAt: {type: Number},  //here we set default creation time(in mills)
    invites:[invites],
    reminder:[reminder],
    guards:[guards],
    isQrCode:{type: Boolean, default:true,required:true},
    isSent:{type: Boolean, default:false,required:true},
    isDeleted:{type: Boolean, default:false,required:true},
    isBlocked:{type: Boolean, default: false, required: true},
    type: { type: String, enum: [
        Config.APP_CONSTANTS.DATABASE.EVENT_TYPE.EVENT,
        Config.APP_CONSTANTS.DATABASE.EVENT_TYPE.REQUEST_EVENT
    ]},
    
    
    isCustom: { type: Boolean },
    eventPdfUrl: { type: String, trim: true }
});

module.exports = mongoose.model('event', event);