
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Config = require('../Config');

var appVersions = new Schema({
    latestIOSVersion : {type: Number, required:true},
    latestAndroidVersion : {type: Number, required:true},
    criticalAndroidVersion : {type: Number, required:true},
    criticalIOSVersion : {type: Number, required:true},
    timeStamp: {type: Date, default: Date.now}
});


module.exports = mongoose.model('appVersions', appVersions);