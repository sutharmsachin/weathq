/**
 * Created by cbluser51 on 20/2/18.
 */

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Config = require('../Config');


var background = new Schema({
    backgroundImage: {
        original: {type: String, default: ''},
        thumbnail: {type: String, default: ''}
    },
    createdAt: {type: Number, default: new Date().getTime(), required: true},
    isDeleted:{type: Boolean, default:false}
});

module.exports = mongoose.model('background', background);