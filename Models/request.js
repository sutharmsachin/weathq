/**
 * Created by sushant on 8/3/18.
 */
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var Config = require('../Config');


var message = new Schema({
    senderId: {type: String, default: null},
    text: [{type: String, default: ''}],
    duration: {type:Number, default: 0},
    templateId:{type: Schema.ObjectId, ref: 'template', default: null},
    cost:  {type:Number, default: 0},
    htmlUrl:  {type:String, default: ''},
    url: {original:{type: String, default: ''},thumbnail: {type: String, default: ''}},
    status: {
        type: String, trim: true,
        default: Config.APP_CONSTANTS.DATABASE.REQUEST_STATUS.PENDING,
        enum: [
            Config.APP_CONSTANTS.DATABASE.REQUEST_STATUS.REJECTED,
            Config.APP_CONSTANTS.DATABASE.REQUEST_STATUS.ACCEPTED,
            Config.APP_CONSTANTS.DATABASE.REQUEST_STATUS.PENDING
        ]
    },
    msgType:{
        type: String, enum: [
            Config.APP_CONSTANTS.DATABASE.MSG_TYPES.IMAGE,
            Config.APP_CONSTANTS.DATABASE.MSG_TYPES.INFO,
            Config.APP_CONSTANTS.DATABASE.MSG_TYPES.TEMPLATE,
            Config.APP_CONSTANTS.DATABASE.MSG_TYPES.OFFER,
            Config.APP_CONSTANTS.DATABASE.MSG_TYPES.TEXT,
            Config.APP_CONSTANTS.DATABASE.MSG_TYPES.ACCEPT,
            Config.APP_CONSTANTS.DATABASE.MSG_TYPES.REJECT,
        ]
    },
    timeStamp: {type:Number, default: 0},
});


var request = new Schema({
    userId: {type: Schema.ObjectId, ref: "user",required:true},
    adminId: {type: Schema.ObjectId, ref: "admin",required:true},
    categoryId: {type: String, default: null},
    categoryName:[{type: String, default: ''}],
    title:{type: String, default: ''},
    address:{type: String, default: ''},
    lat:{type: Number, default: 0},
    long:{type: Number, default: 0},
    startAt:{type: Number, default: 0},
    endAt:{type: Number, default: 0},
    rejectTimes:{type: Number, default: 0},
    message:[message],
    status: {
        type: String, trim: true,
        default: Config.APP_CONSTANTS.DATABASE.REQUEST_STATUS.ACTIVE,
        enum: [
            Config.APP_CONSTANTS.DATABASE.REQUEST_STATUS.COMPLETED,
            Config.APP_CONSTANTS.DATABASE.REQUEST_STATUS.PENDING,
            Config.APP_CONSTANTS.DATABASE.REQUEST_STATUS.REJECTED,
            Config.APP_CONSTANTS.DATABASE.REQUEST_STATUS.ACCEPTED,
            Config.APP_CONSTANTS.DATABASE.REQUEST_STATUS.ACTIVE,
        ]
    },
    requestCreatedAt: {type: Date, default: null},
    timeStamp: {type:Number, default: 0},
    isDeleted: {type: Boolean, default: false, required: true},
    isRead: {type: Boolean, default: false, required: true},
    designSent: {type: Boolean, default: false, required: true},
    designAccept: {type: Boolean, default: false, required: true},
});

module.exports = mongoose.model('request', request);