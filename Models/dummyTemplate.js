/**
 * Created by cbluser51 on 13/4/18.
 */


var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Config = require('../Config');

var dummyTemplate = new Schema({
    createdBy:{type: Schema.ObjectId, ref: 'user'},
    startAt: {type: Number, default:0},
    endAt: {type: Number, default:0},
    address: {type: String, trim: true, default: ''},
    htmlUrl:{type: String, default: ''},
    lat:{type: Number, default: 0},
    long:{type: Number, default: 0},
    createdAt: {type: Number, default:new Date().getTime()},  //here we set default creation time(in mills)
    isDeleted:{type: Boolean, default:false,required:true}
});

module.exports = mongoose.model('dummyTemplate', dummyTemplate);