/**
 * Created by Shumi Gupta on 29/3/17.
 */
const Joi = require('joi');
const Controller = require('../Controllers');
const UniversalFunctions = require('../Utils/UniversalFunction');
const Config = require('../Config');

module.exports = [
    {
        method: 'POST',
        path: '/user/signUp',
        config: {
            handler: function (request, reply) {
                //console.log("1111111",request.payload)
                Controller.userController.userSignUp(request.payload, function (err, data) {
                    if (err) {
                        reply(UniversalFunctions.sendError(err));
                    } else {
                        reply(UniversalFunctions.sendSuccess(null,data))
                    }
                });
            },
            description: 'user signup api',
            notes: 'sign up api',
           // auth: 'UserAuth',
            tags: ['api', 'user'], // ADD THIS TAG
           /* payload:{
                maxBytes:30485760,
                parse: true,
                output: 'file',
                allow: 'multipart/form-data'
            },*/
            validate: {
                payload: {
                   /* photo: Joi.any()
                        .meta({swaggerType: 'file'})
                        .description('image file'),*/
                    socialImage:Joi.object().keys({
                        original: Joi.any(),
                        thumbnail: Joi.any()
                    }),
                    name : Joi.string().required().trim(),
                    phoneNo : Joi.string().required(),
                    appVersion : Joi.string().optional(),
                    deviceToken : Joi.string().optional(),
                    byAdmin : Joi.string().optional(),
                    guard : Joi.boolean().optional(),
                    email : Joi.string().lowercase().email().trim().allow(''),
                    password : Joi.string().trim(),
                    socialId : Joi.string().trim(),
                    language:Joi.string().valid([Config.APP_CONSTANTS.DATABASE.LANGUAGE.ENGLISH,Config.APP_CONSTANTS.DATABASE.LANGUAGE.ARAB]),
                    loginType : Joi.string().required().valid([Config.APP_CONSTANTS.DATABASE.LOGIN_TYPE.GOOGLE,Config.APP_CONSTANTS.DATABASE.LOGIN_TYPE.FACEBOOK, Config.APP_CONSTANTS.DATABASE.LOGIN_TYPE.PHONE_NO]),
                    dob : Joi.any(),
                    gender:Joi.string().valid([Config.APP_CONSTANTS.DATABASE.GENDER_TYPE.MALE,Config.APP_CONSTANTS.DATABASE.GENDER_TYPE.FEMALE])
                },
              //  headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType : 'form',
                    responses:Config.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },
    {
        method: 'POST',
        path: '/user/login',
        config: {
            handler: function (request, reply) {
                Controller.userController.login(request.payload, function (err, data) {
                    if (err) {
                        //console.log("-----------")
                        reply(UniversalFunctions.sendError(err));
                    } else {
                        reply(UniversalFunctions.sendSuccess(null,data))
                    }
                });
            },
            description: 'user login api',
            notes: 'user login api',
           // auth: 'UserAuth',
            tags: ['api', 'user'], // ADD THIS TAG
            validate: {
                payload: {
                    loginId:Joi.string().trim().required(),
                    appVersion : Joi.string().optional(),
                    deviceToken : Joi.string().optional(),
                    language:Joi.string().valid([Config.APP_CONSTANTS.DATABASE.LANGUAGE.ENGLISH,Config.APP_CONSTANTS.DATABASE.LANGUAGE.ARAB]),
                    type:Joi.number().description('1 for phoneNo 0 for email'),
                    password:Joi.string().required()
                },
              //  headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType : 'form',
                    responses:Config.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },
    {
        method: 'POST',
        path: '/user/socialLogin',
        config: {
            handler: function (request, reply) {
                Controller.userController.socialLoginUser(request.payload, function (err, data) {
                    if (err) {
                        //console.log("-----------")
                        reply(UniversalFunctions.sendError(err));
                    } else {
                        reply(UniversalFunctions.sendSuccess(null,data))
                    }
                });
            },
            description: 'user login api',
            notes: 'user login api',
            // auth: 'UserAuth',
            tags: ['api', 'user'], // ADD THIS TAG
            validate: {
                payload: {
                    socialId:Joi.string().required(),
                    appVersion : Joi.string().optional(),
                    deviceToken : Joi.string().optional(),
                    language:Joi.string().valid([Config.APP_CONSTANTS.DATABASE.LANGUAGE.ENGLISH,Config.APP_CONSTANTS.DATABASE.LANGUAGE.ARAB]),
                    loginType : Joi.string().required().valid([Config.APP_CONSTANTS.DATABASE.LOGIN_TYPE.GOOGLE,Config.APP_CONSTANTS.DATABASE.LOGIN_TYPE.FACEBOOK])
                },
                //  headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType : 'form',
                    responses:Config.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },
    {
        method: 'GET',
        path: '/user/skipLogin',
        config: {
            handler: function (request, reply) {
                Controller.userController.skipLogin(request.query, function (err, data) {
                    if (err) {
                        //console.log("-----------")
                        reply(UniversalFunctions.sendError(err));
                    } else {
                        reply(UniversalFunctions.sendSuccess(null,data))
                    }
                });
            },
            description: 'user skipLogin api',
            notes: 'user skipLogin api',
            tags: ['api', 'user'], // ADD THIS TAG
            validate: {
                query: {
                },
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType : 'form',
                    responses:Config.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },
     {
        method: 'PUT',
        path: '/user/updateProfile',
        config: {
            handler: function (request, reply) {
                console.log("updateProfile",request.payload)
                var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
                if(userData.id){
                    Controller.userController.updateProfile(request.payload,userData, function (err, data) {
                        if (err) {
                            reply(UniversalFunctions.sendError(err));
                        } else {
                            reply(UniversalFunctions.sendSuccess(null,data))
                        }
                    });
                }
                else{
                    reply(UniversalFunctions.sendError(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR));
                }
            },
            description: 'user updateProfile api',
            notes: 'updateProfile up api',
            auth: 'commonAuth',
            tags: ['api', 'user'], // ADD THIS TAG
            payload:{
                maxBytes:30485760,
                parse: true,
                output: 'file',
                allow: 'multipart/form-data'
            },
            validate: {
                payload: {
                    userId : Joi.string().optional(),
                    photo: Joi.any()
                        .optional()
                        .meta({swaggerType: 'file'})
                        .description('image file'),
                    name : Joi.string(),
                    phoneNo : Joi.string(),
                    dob : Joi.string(),
                    gender:Joi.string().valid([Config.APP_CONSTANTS.DATABASE.GENDER_TYPE.MALE,Config.APP_CONSTANTS.DATABASE.GENDER_TYPE.FEMALE]),
                    language:Joi.string().valid([Config.APP_CONSTANTS.DATABASE.LANGUAGE.ENGLISH,Config.APP_CONSTANTS.DATABASE.LANGUAGE.ARAB]),
                    email : Joi.string().lowercase().email().trim().allow(''),
                    newPassword:Joi.string(),
                    oldPassword:Joi.string()
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType : 'form',
                    responses:Config.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },
    {
        method: 'post',
        path: '/user/createEvent',
        config: {
            handler: function (request, reply) {
                var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
                if(userData.id){
                    if(request.payload.inviteAt != undefined && request.payload.inviteAt > new Date().setUTCMinutes(new Date().getUTCMinutes()+1) ){
                        Controller.userController.createEvent(request.payload,userData, function (err, data) {
                            if (err) {
                                //    console.log("-----------",err)
                                reply(UniversalFunctions.sendError(err));
                            } else {
                                reply(UniversalFunctions.sendSuccess(null,data))
                            }
                        });
                    }
                    else if(request.payload.inviteAt == undefined){
                        Controller.userController.createEvent(request.payload,userData, function (err, data) {
                            if (err) {
                                //    console.log("-----------",err)
                                reply(UniversalFunctions.sendError(err));
                            } else {
                                reply(UniversalFunctions.sendSuccess(null,data))
                            }
                        })
                    }
                    
                    else  {
                        if(userData.language=='en')reply(UniversalFunctions.sendError(Config.APP_CONSTANTS.STATUS_MSG.ERROR.INVITE_TIME_PASSED))
                        else reply(UniversalFunctions.sendError(Config.APP_CONSTANTS.STATUS_MSG.ERROR.INVITE_TIME_PASSED_AR))
                    }

                }
                else{
                    reply(UniversalFunctions.sendError(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR));
                }
            },
            description: 'createEvent',
            notes: ' createEvent',
            auth: 'UserAuth',
            tags: ['api', 'user'], // ADD THIS TAG
            validate: {
                payload: {
                    language:Joi.string().valid([Config.APP_CONSTANTS.DATABASE.LANGUAGE.ENGLISH,Config.APP_CONSTANTS.DATABASE.LANGUAGE.ARAB]),
                    lang:Joi.string().valid([Config.APP_CONSTANTS.DATABASE.LANGUAGE.ENGLISH,Config.APP_CONSTANTS.DATABASE.LANGUAGE.ARAB]),
                    categoryId:Joi.string().required(),
                    templateId:Joi.string().required(),
                    title:Joi.string().optional().allow(""),
                    message:Joi.string().optional(),
                    htmlUrl:Joi.string().optional(),
                    dummyTemplateId:Joi.string().optional().allow(""),
                    packageType:Joi.string().valid(
                        Config.APP_CONSTANTS.DATABASE.PACKAGES.FREE,
                        Config.APP_CONSTANTS.DATABASE.PACKAGES.WHATSAPP,
                        Config.APP_CONSTANTS.DATABASE.PACKAGES.PREMIUM
                    ).optional(), //package == "PREMIUM"
                    imageUrl:Joi.string().optional(),
                    address:Joi.string().optional(),
                    lat:Joi.number().optional(),
                    long:Joi.number().optional(),
                    inviteAt:Joi.number().optional(),
                    startAt:Joi.number().optional(),
                    endAt:Joi.number().optional(),
                    categoryPrice:Joi.number().required(),
                    smsPrice:Joi.number().required(),
                    totalSms:Joi.number().required(),
                    finalPrice:Joi.number().optional(),
                    wallet:Joi.number().optional(),
                    trxId:Joi.number().optional(),
                    totalPrice:Joi.number().required(),
                    qrCode:Joi.number().optional(),
                    couponCode: Joi.string().optional(),
                    invites:Joi.array().optional().description("[{email: '',name:'',phoneNo:'',inviteCount:''}]")
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType : 'form',
                    responses:Config.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },
    {
        method: 'GET',
        path: '/user/getUpcomingEvents',
        config: {
            handler: function (request, reply) {
                var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
                if(userData.id){
                    Controller.userController.getUpcomingEvents(request.query,userData, function (err, data) {
                        if (err) {
                            reply(UniversalFunctions.sendError(err));
                        } else {
                            reply(UniversalFunctions.sendSuccess(null,data))
                        }
                    });
                }
                else{
                    reply(UniversalFunctions.sendError(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR));
                }
            },
            description: 'getUpcomingEvents',
            notes: 'getUpcomingEvents',
            auth: 'UserAuth',
            tags: ['api', 'user'], // ADD THIS TAG
            validate: {
                query: {
                    skip:Joi.string().optional(),
                    limit:Joi.string().optional(),
                    search:Joi.string().optional()
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType : 'form',
                    responses:Config.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },
    {
        method: 'GET',
        path: '/user/getNotifications',
        config: {
            handler: function (request, reply) {
                var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
                if(userData.id){
                    Controller.userController.getNotifications(request.query,userData, function (err, data) {
                        if (err) {
                            reply(UniversalFunctions.sendError(err));
                        } else {
                            reply(UniversalFunctions.sendSuccess(null,data))
                        }
                    });
                }
                else{
                    reply(UniversalFunctions.sendError(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR));
                }
            },
            description: 'getNotifications',
            notes: 'getNotifications',
            auth: 'UserAuth',
            tags: ['api', 'user'], // ADD THIS TAG
            validate: {
                query: {
                    status:Joi.string().optional().valid(["Notification","Invite"])
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType : 'form',
                    responses:Config.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },
    {
        method: 'GET',
        path: '/user/getMyEvents',
        config: {
            handler: function (request, reply) {
                var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
                if(userData.id){
                    Controller.userController.getMyEvents(request.query,userData, function (err, data) {
                        if (err) {
                            reply(UniversalFunctions.sendError(err));
                        } else {
                            reply(UniversalFunctions.sendSuccess(null,data))
                        }
                    });
                }
                else{
                    reply(UniversalFunctions.sendError(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR));
                }
            },
            description: 'getMyEvents',
            notes: 'getMyEvents',
            auth: 'UserAuth',
            tags: ['api', 'user'], // ADD THIS TAG
            validate: {
                query: {
                    skip:Joi.string().optional(),
                    limit:Joi.string().optional(),
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType : 'form',
                    responses:Config.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },
    {
        method: 'post',
        path: '/user/actionInvite',
        config: {
            handler: function (request, reply) {
                var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
                if(userData.id){
                    Controller.userController.actionInvite(request.payload,userData, function (err, data) {
                        if (err) {
                            //console.log("-----------")
                            reply(UniversalFunctions.sendError(err));
                        } else {
                            reply(UniversalFunctions.sendSuccess(null,data))
                        }
                    });
                }
                else{
                    reply(UniversalFunctions.sendError(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR));
                }
            },
            description: 'actionInvite',
            notes: ' actionInvite',
            auth: 'UserAuth',
            tags: ['api', 'user'], // ADD THIS TAG
            validate: {
                payload: {
                    eventId:Joi.string().required(),
                    notificationId:Joi.string().optional(),
                    language:Joi.string().valid([Config.APP_CONSTANTS.DATABASE.LANGUAGE.ENGLISH,Config.APP_CONSTANTS.DATABASE.LANGUAGE.ARAB]),
                    status:Joi.string().optional().valid(["Accepted","Rejected"])
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType : 'form',
                    responses:Config.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },{
        method: 'post',
        path: '/user/assignGuard',
        config: {
            handler: function (request, reply) {
                var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
                if(userData.id){
                    Controller.userController.assignGuard(request.payload,userData, function (err, data) {
                        if (err) {
                            //console.log("-----------")
                            reply(UniversalFunctions.sendError(err));
                        } else {
                            reply(UniversalFunctions.sendSuccess(null,data))
                        }
                    });
                }
                else{
                    reply(UniversalFunctions.sendError(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR));
                }
            },
            description: 'assignGuard',
            notes: ' assignGuard',
            auth: 'commonAuth',
            tags: ['api', 'user'], // ADD THIS TAG
            validate: {
                payload: {
                    eventId:Joi.string().required(),
                    language:Joi.string().valid([Config.APP_CONSTANTS.DATABASE.LANGUAGE.ENGLISH,Config.APP_CONSTANTS.DATABASE.LANGUAGE.ARAB]),
                    guardNo:Joi.string().optional()
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType : 'form',
                    responses:Config.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },
    {
        method: 'post',
        path: '/user/guardActionInvite',
        config: {
            handler: function (request, reply) {
                var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
                if(userData.id){
                    Controller.userController.guardActionInvite(request.payload,userData, function (err, data) {
                        if (err) {
                            //console.log("-----------")
                            reply(UniversalFunctions.sendError(err));
                        } else {
                            reply(UniversalFunctions.sendSuccess(null,data))
                        }
                    });
                }
                else{
                    reply(UniversalFunctions.sendError(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR));
                }
            },
            description: 'guardActionInvite',
            notes: ' guardActionInvite',
            auth: 'UserAuth',
            tags: ['api', 'user'], // ADD THIS TAG
            validate: {
                payload: {
                    eventId:Joi.string().required(),
                    language:Joi.string().valid([Config.APP_CONSTANTS.DATABASE.LANGUAGE.ENGLISH,Config.APP_CONSTANTS.DATABASE.LANGUAGE.ARAB]),
                    notificationId:Joi.string().optional(),
                    status:Joi.string().optional().valid(["Accepted","Rejected"])
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType : 'form',
                    responses:Config.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },
    {
        method: 'post',
        path: '/user/guardCheckIns',
        config: {
            handler: function (request, reply) {
                var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
                if(userData.id){
                    Controller.userController.guardCheckIns(request.payload,userData, function (err, data) {
                        if (err) {
                            //console.log("-----------")
                            reply(UniversalFunctions.sendError(err));
                        } else {
                            reply(UniversalFunctions.sendSuccess(null,data))
                        }
                    });
                }
                else{
                    reply(UniversalFunctions.sendError(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR));
                }
            },
            description: 'guardCheckIns',
            notes: ' guardCheckIns',
            auth: 'UserAuth',
            tags: ['api', 'user'], // ADD THIS TAG
            validate: {
                payload: {
                    eventId:Joi.string().required(),
                    phoneNo:Joi.string().optional(),
                    language:Joi.string().valid([
                        Config.APP_CONSTANTS.DATABASE.LANGUAGE.ENGLISH,
                        Config.APP_CONSTANTS.DATABASE.LANGUAGE.ARAB
                    ]),
                    userId:Joi.string().optional(),
                    count: Joi.number().optional()
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType : 'form',
                    responses:Config.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },
    {
        method: 'post',
        path: '/user/addInvites',
        config: {
            handler: function (request, reply) {
                var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
                if(userData.id){
                    Controller.userController.addInvites(request.payload,userData, function (err, data) {
                        if (err) {
                            // console.log("-----------",err)
                            reply(UniversalFunctions.sendError(err));
                        } else {
                            reply(UniversalFunctions.sendSuccess(null,data))
                        }
                    });
                }
                else{
                    reply(UniversalFunctions.sendError(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR));
                }
            },
            description: 'addInvites',
            notes: ' addInvites',
            auth: 'UserAuth',
            tags: ['api', 'user'], // ADD THIS TAG
            validate: {
                payload: {
                    language:Joi.string().valid([Config.APP_CONSTANTS.DATABASE.LANGUAGE.ENGLISH,Config.APP_CONSTANTS.DATABASE.LANGUAGE.ARAB]),
                    eventId:Joi.string().required(),
                    addedWallet:Joi.number().optional(),
                    invites:Joi.array().required().description("[{email: '',name:'',phoneNo:'',inviteCount:''}]")
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType : 'form',
                    responses:Config.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },
    {
        method: 'GET',
        path: '/user/getOngoingEvents',
        config: {
            handler: function (request, reply) {
                var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
                if(userData.id){
                    Controller.userController.getOngoingEvents(request.query,userData, function (err, data) {
                        if (err) {
                            reply(UniversalFunctions.sendError(err));
                        } else {
                            reply(UniversalFunctions.sendSuccess(null,data))
                        }
                    });
                }
                else{
                    reply(UniversalFunctions.sendError(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR));
                }
            },
            description: 'getOngoingEvents',
            notes: 'getOngoingEvents',
            auth: 'UserAuth',
            tags: ['api', 'user'], // ADD THIS TAG
            validate: {
                query: {
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType : 'form',
                    responses:Config.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },
    {
        method: 'GET',
        path: '/user/getUserWallet',
        config: {
            handler: function (request, reply) {
                var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
                if(userData.id){
                    Controller.userController.getUserWallet(request.query,userData, function (err, data) {
                        if (err) {
                            reply(UniversalFunctions.sendError(err));
                        } else {
                            reply(UniversalFunctions.sendSuccess(null,data))
                        }
                    });
                }
                else{
                    reply(UniversalFunctions.sendError(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR));
                }
            },
            description: 'getUserWallet',
            notes: 'getUserWallet',
            auth: 'UserAuth',
            tags: ['api', 'user'], // ADD THIS TAG
            validate: {
                query: {
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType : 'form',
                    responses:Config.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },
    {
        method: 'POST',
        path: '/user/importCsvXl',
        config: {
            handler: function (request, reply) {
                var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
                if(userData.id){
                    Controller.userController.importCsvXl(request.payload,userData, function (err, data) {
                        if (err) {
                            reply(UniversalFunctions.sendError(err));
                        } else {
                            reply(UniversalFunctions.sendSuccess(null,data))
                        }
                    });
                }
                else{
                    reply(UniversalFunctions.sendError(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR));
                }
            },
        notes: 'importCsvXl',
        auth: 'UserAuth',
        description: 'importCsvXl',
        tags: ['api', 'user'],
        payload: {
            maxBytes: 2000000,
            parse: true,
            output: 'file'
        },
        validate: {
            payload: {
                language:Joi.string().valid([Config.APP_CONSTANTS.DATABASE.LANGUAGE.ENGLISH,Config.APP_CONSTANTS.DATABASE.LANGUAGE.ARAB]),
                file: Joi.any()
                    .meta({swaggerType: 'file'})
                    .optional()
                    .description('file'),
            },
            headers: UniversalFunctions.authorizationHeaderObj,
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responses:Config.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
    },{
        method: 'POST',
        path: '/user/applyPromo',
        config: {
            handler: function (request, reply) {
                var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
                if(userData.id){
                    Controller.userController.applyPromo(request.payload,userData, function (err, data) {
                        if (err) {
                            reply(UniversalFunctions.sendError(err));
                        } else {
                            reply(UniversalFunctions.sendSuccess(null,data))
                        }
                    });
                }
                else{
                    reply(UniversalFunctions.sendError(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR));
                }
            },
        notes: 'applyPromo',
        auth: 'UserAuth',
        description: 'applyPromo',
        tags: ['api', 'user'],
        validate: {
            payload: {
                language:Joi.string().valid([Config.APP_CONSTANTS.DATABASE.LANGUAGE.ENGLISH,Config.APP_CONSTANTS.DATABASE.LANGUAGE.ARAB]),
                couponCode: Joi.string().required()
            },
            headers: UniversalFunctions.authorizationHeaderObj,
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responses:Config.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
    },{
        method: 'POST',
        path: '/user/completeOffer',
        config: {
            handler: function (request, reply) {
                var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
                if(userData.id){
                    Controller.userController.completeOffer(request.payload,userData, function (err, data) {
                        if (err) {
                            reply(UniversalFunctions.sendError(err));
                        } else {
                            reply(UniversalFunctions.sendSuccess(null,data))
                        }
                    });
                }
                else{
                    reply(UniversalFunctions.sendError(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR));
                }
            },
        notes: 'completeOffer',
        auth: 'UserAuth',
        description: 'completeOffer',
        tags: ['api', 'user'],
        validate: {
            payload: {
                requestId: Joi.string().required(),
                msgId: Joi.string().optional(),
                trxId: Joi.string().optional(),
                price: Joi.string().optional(),
                language:Joi.string().valid([Config.APP_CONSTANTS.DATABASE.LANGUAGE.ENGLISH,Config.APP_CONSTANTS.DATABASE.LANGUAGE.ARAB]),
                status: Joi.string().required().valid([Config.APP_CONSTANTS.DATABASE.REQUEST_STATUS.REJECTED,Config.APP_CONSTANTS.DATABASE.REQUEST_STATUS.ACCEPTED]),
            },
            headers: UniversalFunctions.authorizationHeaderObj,
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responses:Config.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
    },
    {
        method: 'POST',
        path: '/user/forgotPassword',
        config: {
        handler: function (request, reply) {
            let payloadData = request.payload;
            Controller.userController.forgotPassword(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null,data))
                }
            })
        },
        description: 'forgotPassword',
         notes: 'forgotPassword',
        tags: ['api', 'user'],
        validate: {
            payload: {
                language:Joi.string().valid([Config.APP_CONSTANTS.DATABASE.LANGUAGE.ENGLISH,Config.APP_CONSTANTS.DATABASE.LANGUAGE.ARAB]),
                phoneNo:Joi.string().required()
            },
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: Config.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
    },
    {
        method: 'POST',
        path: '/user/sendOtp',
        config: {
        handler: function (request, reply) {
            let payloadData = request.payload;
            Controller.userController.sendOtp(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null,data))
                }
            })
        },
        description: 'sendOtp',
         notes: 'sendOtp',
        tags: ['api', 'user'],
        validate: {
            payload: {
                language:Joi.string().valid([Config.APP_CONSTANTS.DATABASE.LANGUAGE.ENGLISH,Config.APP_CONSTANTS.DATABASE.LANGUAGE.ARAB]),
                phoneNo:Joi.string().required(),
                email:Joi.string().optional(),
                userId:Joi.string().optional(),
                socialId:Joi.string().optional(),
                edit:Joi.number().optional(),
            },
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: Config.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
    },
    {
        method: 'PUT',
        path: '/user/logout',
        handler: function (request, reply) {

            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData || null;
            Controller.userController.userLogout(userData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });
        },
        config: {
            description: 'Logout User',
            tags: ['api', 'user'],
            auth: 'UserAuth',
            validate: {
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form',
                    responseMessages: Config.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },
    {
        method: 'post',
        path: '/user/cancelGuard',
        config: {
            handler: function (request, reply) {
                var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
                if(userData.id){
                    Controller.userController.cancelGuard(request.payload,userData, function (err, data) {
                        if (err) {
                            //console.log("-----------")
                            reply(UniversalFunctions.sendError(err));
                        } else {
                            reply(UniversalFunctions.sendSuccess(null,data))
                        }
                    });
                }
                else{
                    reply(UniversalFunctions.sendError(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR));
                }
            },
            description: 'cancelGuard',
            notes: ' cancelGuard',
            auth: 'UserAuth',
            tags: ['api', 'user'], // ADD THIS TAG
            validate: {
                payload: {
                    eventId:Joi.string().required(),
                    phoneNo:Joi.string().optional(),
                    language:Joi.string().valid([Config.APP_CONSTANTS.DATABASE.LANGUAGE.ENGLISH,Config.APP_CONSTANTS.DATABASE.LANGUAGE.ARAB]),
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType : 'form',
                    responses:Config.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    }, {
        method: 'post',
        path: '/user/cancelEvent',
        config: {
            handler: function (request, reply) {
                var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
                if(userData.id){
                    Controller.userController.cancelEvent(request.payload,userData, function (err, data) {
                        if (err) {
                            //console.log("-----------")
                            reply(UniversalFunctions.sendError(err));
                        } else {
                            reply(UniversalFunctions.sendSuccess(null,data))
                        }
                    });
                }
                else{
                    reply(UniversalFunctions.sendError(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR));
                }
            },
            description: 'cancelEvent',
            notes: ' cancelEvent',
            auth: 'UserAuth',
            tags: ['api', 'user'], // ADD THIS TAG
            validate: {
                payload: {
                    eventId:Joi.string().required(),
                    reason:Joi.string().optional(),
                    isReminder:Joi.boolean().optional(),
                    inviteAt:Joi.number().optional(),
                    addedWallet:Joi.number().optional(),
                    language:Joi.string().valid([Config.APP_CONSTANTS.DATABASE.LANGUAGE.ENGLISH,Config.APP_CONSTANTS.DATABASE.LANGUAGE.ARAB]),
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType : 'form',
                    responses:Config.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },
    {
        method: 'POST',
        path: '/user/contactUs',
        config: {
            handler: function (request, reply) {
                var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
                if(userData.id){
                    Controller.userController.contactUs(request.payload,userData, function (err, data) {
                        if (err) {
                            reply(UniversalFunctions.sendError(err));
                        } else {
                            reply(UniversalFunctions.sendSuccess(null,data))
                        }
                    });
                }
                else{
                    reply(UniversalFunctions.sendError(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR));
                }
            },
            notes: 'contactUs',
            auth: 'UserAuth',
            description: 'contactUs',
            tags: ['api', 'user'],
            validate: {
                payload: {
                    name: Joi.string().optional(),
                    email: Joi.string().optional(),
                    phoneNo: Joi.string().optional(),
                    message: Joi.string().optional(),
                    language:Joi.string().valid([Config.APP_CONSTANTS.DATABASE.LANGUAGE.ENGLISH,Config.APP_CONSTANTS.DATABASE.LANGUAGE.ARAB]),
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form',
                    responses:Config.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },
    {
        method: 'post',
        path: '/user/createReqEvent',
        config: {
            handler: function (request, reply) {
                var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
                if(userData.id){
                    Controller.userController.createReqEvent(request.payload,userData, function (err, data) {
                        if (err) {
                            // console.log("-----------",err)
                            reply(UniversalFunctions.sendError(err));
                        } else {
                            reply(UniversalFunctions.sendSuccess(null,data))
                        }
                    });
                }
                else{
                    reply(UniversalFunctions.sendError(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR));
                }
            },
            description: 'createReqEvent',
            notes: ' createReqEvent',
            auth: 'UserAuth',
            tags: ['api', 'user'], // ADD THIS TAG
            validate: {
                payload: {
                    language:Joi.string().valid([Config.APP_CONSTANTS.DATABASE.LANGUAGE.ENGLISH,Config.APP_CONSTANTS.DATABASE.LANGUAGE.ARAB]),
                    lang:Joi.string().valid([Config.APP_CONSTANTS.DATABASE.LANGUAGE.ENGLISH,Config.APP_CONSTANTS.DATABASE.LANGUAGE.ARAB]),
                    templateId:Joi.string().required(),
                    requestId:Joi.string().optional(),
                    title:Joi.string().optional(),
                    packageType:Joi.string().valid(
                        Config.APP_CONSTANTS.DATABASE.PACKAGES.FREE,
                        Config.APP_CONSTANTS.DATABASE.PACKAGES.WHATSAPP,
                        Config.APP_CONSTANTS.DATABASE.PACKAGES.PREMIUM
                    ).optional(), //packages
                    message:Joi.string().optional(),
                    address:Joi.string().optional(),
                    lat:Joi.number().optional(),
                    long:Joi.number().optional(),
                    inviteAt:Joi.number().optional(),
                    startAt:Joi.number().optional(),
                    endAt:Joi.number().optional(),
                    categoryPrice:Joi.number().optional(),
                    smsPrice:Joi.number().required(),
                    totalSms:Joi.number().required(),
                    finalPrice:Joi.number().optional(),
                    wallet:Joi.number().optional(),
                    trxId:Joi.number().optional(),
                    totalPrice:Joi.number().optional(),
                    qrCode:Joi.number().optional(),
                    couponCode: Joi.string().optional(),
                    invites:Joi.array().optional().description("[{email: '',name:'',phoneNo:''}]")
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType : 'form',
                    responses:Config.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },
    {
        method: 'post',
        path: '/user/editEvent',
        config: {
            handler: function (request, reply) {
                var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
                if(userData.id){
                    Controller.userController.editEvent(request.payload,userData, function (err, data) {
                        if (err) {
                            // console.log("-----------",err)
                            reply(UniversalFunctions.sendError(err));
                        } else {
                            reply(UniversalFunctions.sendSuccess(null,data))
                        }
                    });
                }
                else{
                    reply(UniversalFunctions.sendError(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR));
                }
            },
            description: 'editEvent',
            notes: ' editEvent',
            auth: 'UserAuth',
            tags: ['api', 'user'], // ADD THIS TAG
            validate: {
                payload: {
                    eventId:Joi.string().required(),
                    htmlUrl:Joi.string().optional(),
                    imageUrl:Joi.string().optional(),
                    address:Joi.string().optional(),
                    /*lat:Joi.number().optional(),
                    long:Joi.number().optional(),
                    startAt:Joi.number().optional(),*/
                    lat:Joi.number().optional(),
                    long:Joi.number().optional(),
                    startAt:Joi.number().optional(),
                    endAt:Joi.number().optional(),
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType : 'form',
                    responses:Config.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },
    {
        method: 'post',
        path: '/user/dummyTemplate',
        config: {
            handler: function (request, reply) {
                var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
                if(userData.id){
                    Controller.userController.dummyTemplate(request.payload,userData, function (err, data) {
                        if (err) {
                            // console.log("-----------",err)
                            reply(UniversalFunctions.sendError(err));
                        } else {
                            reply(UniversalFunctions.sendSuccess(null,data))
                        }
                    });
                }
                else{
                    reply(UniversalFunctions.sendError(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR));
                }
            },
            description: 'dummyTemplate',
            notes: ' dummyTemplate',
            auth: 'UserAuth',
            tags: ['api', 'user'], // ADD THIS TAG
            validate: {
                payload: {
                    htmlUrl:Joi.string().optional(),
                    address:Joi.string().optional(),
                    lat:Joi.number().optional(),
                    long:Joi.number().optional(),
                    startAt:Joi.number().optional(),
                    endAt:Joi.number().optional(),
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType : 'form',
                    responses:Config.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },
    {
        method: 'GET',
        path: '/user/getEventTemplale',
        config: {
            handler: function (request, reply) {
                var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
                if(userData.id){
                    Controller.userController.getEventTemplate(request.query,userData, function (err, data) {
                        if (err) {
                            // console.log("-----------",err);
                            reply(UniversalFunctions.sendError(err));
                        } else {
                            reply(UniversalFunctions.sendSuccess(null,data))
                        }
                    });
                }
                else{
                    reply(UniversalFunctions.sendError(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR));
                }
            },
            description: 'getEventTemplate',
            notes: ' getEventTemplate',
            auth: 'UserAuth',
            tags: ['api', 'user'],
            validate: {
                query: {
                    eventId:Joi.string().required(),
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType : 'form',
                    responses:Config.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },
    {
        method: 'GET',
        path: '/user/getDummyTemp',
        config: {
            handler: function (request, reply) {
                var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
                if(userData.id){
                    Controller.userController.getDummyTemp(request.query,userData, function (err, data) {
                        if (err) {
                            reply(UniversalFunctions.sendError(err));
                        } else {
                            reply(UniversalFunctions.sendSuccess(null,data))
                        }
                    });
                }
                else{
                    reply(UniversalFunctions.sendError(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR));
                }
            },
            description: 'getDummyTemp',
            notes: 'getDummyTemp',
            auth: 'UserAuth',
            tags: ['api', 'user'], // ADD THIS TAG
            validate: {
                query: {
                    templateId:Joi.string().optional()
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType : 'form',
                    responses:Config.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },
    {
        method: 'GET',
        path: '/user/getEventDetails',
        config: {
            handler: function (request, reply) {
                var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
                if(userData.id){
                    Controller.userController.getEventDetails(request.query,userData, function (err, data) {
                        if (err) {
                            reply(UniversalFunctions.sendError(err));
                        } else {
                            reply(UniversalFunctions.sendSuccess(null,data))
                        }
                    });
                }
                else{
                    reply(UniversalFunctions.sendError(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR));
                }
            },
            description: 'getEventDetails',
            notes: 'getEventDetails',
            auth: 'UserAuth',
            tags: ['api', 'user'], // ADD THIS TAG
            validate: {
                query: {
                    eventId:Joi.string().optional(),
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType : 'form',
                    responses:Config.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },
    {
        method: 'GET',
        path: '/user/getEventReminders',
        config: {
            handler: function (request, reply) {
                var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
                if(userData.id){
                    Controller.userController.getEventReminders(request.query,userData, function (err, data) {
                        if (err) {
                            reply(UniversalFunctions.sendError(err));
                        } else {
                            reply(UniversalFunctions.sendSuccess(null,data))
                        }
                    });
                }
                else{
                    reply(UniversalFunctions.sendError(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR));
                }
            },
            description: 'getEventReminders',
            notes: 'getEventReminders',
            auth: 'UserAuth',
            tags: ['api', 'user'], // ADD THIS TAG
            validate: {
                query: {
                    eventId:Joi.string().required(),
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType : 'form',
                    responses:Config.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },
    {
        method: 'GET',
        path: '/user/getAvailableCount',
        config: {
            handler: function (request, reply) {
                var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
                if(userData.id){
                    Controller.userController.getAvailableCount(request.query,userData, function (err, data) {
                        if (err) {
                            reply(UniversalFunctions.sendError(err));
                        } else {
                            reply(UniversalFunctions.sendSuccess(null,data))
                        }
                    });
                }
                else{
                    reply(UniversalFunctions.sendError(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR));
                }
            },
            description: 'balance status',
            notes: 'balance status',
            auth: 'UserAuth',
            tags: ['api', 'user'], // ADD THIS TAG
            validate: {
                query: {
                    eventId:Joi.string().required(),
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType : 'form',
                    responses:Config.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },
    {
        method: 'GET',
        path: '/user/getEventInvites',
        config: {
            handler: function (request, reply) {
                var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
                if(userData.id){
                    Controller.userController.getEventInvites(request.query,userData, function (err, data) {
                        if (err) {
                            reply(UniversalFunctions.sendError(err));
                        } else {
                            reply(UniversalFunctions.sendSuccess(null,data))
                        }
                    });
                }
                else{
                    reply(UniversalFunctions.sendError(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR));
                }
            },
            description: 'getEventInvites',
            notes: 'getEventInvites',
            auth: 'UserAuth',
            tags: ['api', 'user'], // ADD THIS TAG
            validate: {
                query: {
                    eventId:Joi.string().required(),
                    skip:Joi.number().required(),
                    limit:Joi.number().required(),
                    status:Joi.string().valid(["coming","rejected","pending"]).required(),
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType : 'form',
                    responses:Config.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },{
        method: 'GET',
        path: '/user/getAdvertisements',
        config: {
            handler: function (request, reply) {
                var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
                // if(userData.id){
                    Controller.userController.getAdvertisements(request.query,userData, function (err, data) {
                        if (err) {
                            reply(UniversalFunctions.sendError(err));
                        } else {
                            reply(UniversalFunctions.sendSuccess(null,data))
                        }
                    });
                // }
               
                // else{
                //     reply(UniversalFunctions.sendError(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR));
                // }
            },
            description: 'getAdvertisements',
            notes: 'getAdvertisements',
            auth: {
            strategy:'UserAuth',
            mode:'optional'
            },
            tags: ['api', 'user'], // ADD THIS TAG
            validate: {
                query: {
                    skip:Joi.number().required(),
                    limit:Joi.number().required(),
                },
                headers: UniversalFunctions.authorizationHeaderObjOptional,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType : 'form',
                    responses:Config.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },
    {
        method: 'get',
        path: '/user/getEventDetailsPdf',
        config: {
            handler: function (request, reply) {
                var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
                if(userData.id){
                    Controller.userController.getEventDetailsPdf(request.query, userData, function (err, data) {
                        if (err) {
                            //console.log("-----------")
                            reply(UniversalFunctions.sendError(err));
                        } else {
                            reply(UniversalFunctions.sendSuccess(null,data))
                        }
                    });
                }
                else{
                    reply(UniversalFunctions.sendError(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR));
                }
            },
            description: 'getEventDetailsPdf',
            notes: ' getEventDetailsPdf',
            auth: 'UserAuth',
            tags: ['api', 'user'], // ADD THIS TAG
            validate: {
                query: {
                    eventId:Joi.string().required()
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType : 'form',
                    responses:Config.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },
    //Added on 15th may 2019 
    {
        method: 'POST',
        path: '/user/updateUserLocation',
        config: {
            handler: function (request, reply) {
                var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
                if(userData.id){
                    Controller.userController.updateUserLocation(request.payload,userData, function (err, data) {
                        if (err) {
                            reply(UniversalFunctions.sendError(err));
                        } else {
                            reply(UniversalFunctions.sendSuccess(null,data))
                        }
                    });
                }
                else{
                    reply(UniversalFunctions.sendError(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR));
                }
            },
            description: 'updateUserLocation',
            notes: 'updateUserLocation',
            auth: 'UserAuth',
            tags: ['api', 'user'], // ADD THIS TAG
            validate: {
                payload: {
                   lat: Joi.number().required(),
                   long: Joi.number().required()
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType : 'form',
                    responses:Config.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    }
];
