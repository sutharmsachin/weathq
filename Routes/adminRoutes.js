/**
 * Created by Shumi on 5/7/17.
 */
"use strict";

const Controller = require('../Controllers');
const UniversalFunctions = require('../Utils/UniversalFunction');
const Joi = require('joi');
const Config = require('../Config');


const adminLogin = [
    {
        method: 'POST',
        path: '/admin/login',
        config: {
            handler: function (request, reply) {
                Controller.adminController.adminLogin(request.payload, function (err, data) {
                    if (err) {
                        //console.log("-----------")
                        reply(UniversalFunctions.sendError(err));
                    } else {
                        reply(UniversalFunctions.sendSuccess(null,data))
                    }
                });
            },
            description: 'admin login api',
            notes: 'admin login api',
            tags: ['api', 'admin'], // ADD THIS TAG
            validate: {
                payload: {
                    email:Joi.string().required(),
                    password:Joi.string().required()
                },
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType : 'form',
                    responses:Config.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },
    {
        method: 'POST',
        path: '/admin/getAllUsers',
        config: {
            handler: function (request, reply) {
                var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
                if(userData.id){
                    Controller.adminController.getAllUsers(request.payload,userData, function (err, data) {
                        if (err) {
                            //console.log("-----------")
                            reply(UniversalFunctions.sendError(err));
                        } else {
                            reply(UniversalFunctions.sendSuccess(null,data))
                        }
                    });
                }
                else{
                    reply(UniversalFunctions.sendError(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR));

                }
            },
            description: 'admin getAllUsers api',
            notes: 'admin getAllUsers api',
            auth: 'AdminAuth',
            tags: ['api', 'admin'], // ADD THIS TAG
            validate: {
                payload: {
                    skip:Joi.number().optional(),
                    limit:Joi.number().optional(),
                    name:Joi.string(),
                    search:Joi.string(),
                    userId:Joi.string().optional(),
                     from : Joi.date().optional(),
                     to : Joi.date().optional(),
                    lastName:Joi.string(),
                    startDate:Joi.date(),
                    endDate:Joi.date(),
                    email:Joi.string().email()

                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType : 'form',
                    responses:Config.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },
    {
        method: 'POST',
        path: '/admin/blockUser',
        config: {
            handler: function (request, reply) {
                var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
                if(userData.id){
                    Controller.adminController.blockUsers(request.payload,userData, function (err, data) {
                        if (err) {
                            //console.log("-----------")
                            reply(UniversalFunctions.sendError(err));
                        } else {
                            reply(UniversalFunctions.sendSuccess(null,data))
                        }
                    });
                }
                else{
                    reply(UniversalFunctions.sendError(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR));
                }
            },
            description: 'admin blockUser api',
            notes: 'admin blockUser api',
            auth: 'AdminAuth',
            tags: ['api', 'admin'], // ADD THIS TAG
            validate: {
                payload: {
                    userId:Joi.string().required(),
                    block:Joi.boolean().required()
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType : 'form',
                    responses:Config.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },
    {
        method: 'GET',
        path: '/admin/getCategories',
        config: {
            handler: function (request, reply) {
                var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
                if(userData.id){
                    Controller.adminController.getCategories(request.payload,userData, function (err, data) {
                        if (err) {
                            reply(UniversalFunctions.sendError(err));
                        } else {
                            reply(UniversalFunctions.sendSuccess(null,data))
                        }
                    });
                }
                else{
                    reply(UniversalFunctions.sendError(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR));
                }
            },
            description: 'getCategories',
            notes: 'getCategories',
            auth: 'commonAuth',
            tags: ['api', 'admin'], // ADD THIS TAG
            validate: {
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType : 'form',
                    responses:Config.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },
    {
        method: 'post',
        path: '/admin/createCategories',
        config: {
            handler: function (request, reply) {
                var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
                if(userData.id){
                    Controller.adminController.createCategories(request.payload,userData, function (err, data) {
                        if (err) {
                            //console.log("-----------")
                            reply(UniversalFunctions.sendError(err));
                        } else {
                            reply(UniversalFunctions.sendSuccess(null,data))
                        }
                    });
                }
                else{
                    reply(UniversalFunctions.sendError(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR));
                }
            },
            description: 'createCategories',
            notes: 'createCategories',
            auth: 'AdminAuth',
            tags: ['api', 'admin'], // ADD THIS TAG
            payload:{
                maxBytes:30485760,
                parse: true,
                output: 'file',
                allow: 'multipart/form-data'
            },
            validate: {
                payload: {
                    nameEng: Joi.string().trim().required(),
                    nameArabic:Joi.string().trim().required(),
                    image: Joi.any()
                        .meta({swaggerType: 'file'})
                        .description('image file')
                        .required(),
                    icon: Joi.any()
                        .meta({swaggerType: 'file'})
                        .description('image file')
                        .required()
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType : 'form',
                    responses:Config.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },
    {
        method: 'post',
        path: '/admin/updateCategories',
        config: {
            handler: function (request, reply) {
                var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
                if(userData.id){
                    Controller.adminController.updateCategories(request.payload,userData, function (err, data) {
                        if (err) {
                            //console.log("-----------")
                            reply(UniversalFunctions.sendError(err));
                        } else {
                            reply(UniversalFunctions.sendSuccess(null,data))
                        }
                    });
                }
                else{
                    reply(UniversalFunctions.sendError(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR));
                }
            },
            description: 'updateCategories',
            notes: 'updateCategories',
            auth: 'AdminAuth',
            tags: ['api', 'admin'], // ADD THIS TAG
            payload:{
                maxBytes:30485760,
                parse: true,
                output: 'file',
                allow: 'multipart/form-data'
            },
            validate: {
                payload: {
                    _id:Joi.string().trim().required(),
                    nameEng:Joi.string().trim().required(),
                    nameArabic:Joi.string().trim().required(),
                    image: Joi.any()
                        .meta({swaggerType: 'file'})
                        .description('image file'),
                    icon: Joi.any()
                        .meta({swaggerType: 'file'})
                        .description('image file')
                        .required()
                      
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType : 'form',
                    responses:Config.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },
    {
        method: 'post',
        path: '/admin/deleteCategories',
        config: {
            handler: function (request, reply) {
                var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
                if(userData.id){
                    Controller.adminController.deleteCategories(request.payload,userData, function (err, data) {
                        if (err) {
                            //console.log("-----------")
                            reply(UniversalFunctions.sendError(err));
                        } else {
                            reply(UniversalFunctions.sendSuccess(null,data))
                        }
                    });
                }
                else{
                    reply(UniversalFunctions.sendError(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR));
                }
            },
            description: 'updateCategories',
            notes: 'updateCategories',
            auth: 'AdminAuth',
            tags: ['api', 'admin'], // ADD THIS TAG
            validate: {
                payload: {
                    _id:Joi.string().trim().required()
                    
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType : 'form',
                    responses:Config.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },
    {
        method: 'GET',
        path: '/admin/getPackage',
        config: {
            handler: function (request, reply) {
                var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
                if(userData.id){
                    Controller.adminController.getPackage(request.payload,userData, function (err, data) {
                        if (err) {
                            reply(UniversalFunctions.sendError(err));
                        } else {
                            reply(UniversalFunctions.sendSuccess(null,data))
                        }
                    });
                }
                else{
                    reply(UniversalFunctions.sendError(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR));
                }
            },
            description: 'getPackage',
            notes: 'getPackage',
            auth: 'commonAuth',
            tags: ['api', 'admin'], // ADD THIS TAG
            validate: {
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType : 'form',
                    responses:Config.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },
    {
        method: 'post',
        path: '/admin/createPackage',
        config: {
            handler: function (request, reply) {
                var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
                if(userData.id){
                    Controller.adminController.createPackage(request.payload,userData, function (err, data) {
                        if (err) {
                            //console.log("-----------")
                            reply(UniversalFunctions.sendError(err));
                        } else {
                            reply(UniversalFunctions.sendSuccess(null,data))
                        }
                    });
                }
                else{
                    reply(UniversalFunctions.sendError(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR));
                }
            },
            description: 'createPackage',
            notes: 'createPackage',
            auth: 'AdminAuth',
            tags: ['api', 'admin'], // ADD THIS TAG
            validate: {
                payload: {
                    totalSms:Joi.number().required(),
                    totalPrice:Joi.number().required()
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType : 'form',
                    responses:Config.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },{
        method: 'post',
        path: '/admin/actionInvite',
        config: {
            handler: function (request, reply) {
                    Controller.adminController.actionInvite(request.payload, function (err, data) {
                        if (err) {
                            //console.log("-----------")
                            reply(UniversalFunctions.sendError(err));
                        } else {
                            reply(UniversalFunctions.sendSuccess(null,data))
                        }
                    });
            },
            description: 'actionInvite',
            notes: 'actionInvite',
            tags: ['api', 'admin'], // ADD THIS TAG
            validate: {
                payload: {
                    eventId:Joi.string().required(),
                    phoneNo:Joi.string().required(),
                    status:Joi.string().required().valid(["Accepted","Rejected"])
                },
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType : 'form',
                    responses:Config.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },
    {
        method: 'post',
        path: '/admin/updatePackage',
        config: {
            handler: function (request, reply) {
                var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
                if(userData.id){
                    Controller.adminController.updatePackage(request.payload,userData, function (err, data) {
                        if (err) {
                            //console.log("-----------")
                            reply(UniversalFunctions.sendError(err));
                        } else {
                            reply(UniversalFunctions.sendSuccess(null,data))
                        }
                    });
                }
                else{
                    reply(UniversalFunctions.sendError(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR));
                }
            },
            description: 'updatePackage',
            notes: 'updatePackage',
            auth: 'AdminAuth',
            tags: ['api', 'admin'], // ADD THIS TAG
            validate: {
                payload: {
                    _id:Joi.string().trim().required(),
                    totalSms:Joi.number().required(),
                    totalPrice:Joi.number().required()
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType : 'form',
                    responses:Config.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },
    {
        method: 'post',
        path: '/admin/deletePackage',
        config: {
            handler: function (request, reply) {
                var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
                if(userData.id){
                    Controller.adminController.deletePackage(request.payload,userData, function (err, data) {
                        if (err) {
                            //console.log("-----------")
                            reply(UniversalFunctions.sendError(err));
                        } else {
                            reply(UniversalFunctions.sendSuccess(null,data))
                        }
                    });
                }
                else{
                    reply(UniversalFunctions.sendError(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR));
                }
            },
            description: 'deletePackage',
            notes: 'deletePackage',
            auth: 'AdminAuth',
            tags: ['api', 'admin'], // ADD THIS TAG
            validate: {
                payload: {
                    _id:Joi.string().trim().required()
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType : 'form',
                    responses:Config.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    {
        method: 'post',
        path: '/admin/createTemplate',
        config: {
            handler: function (request, reply) {
                var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
                if(userData.id){
                    Controller.adminController.createTemplate(request.payload,userData, function (err, data) {
                        if (err) {
                            //console.log("-----------")
                            reply(UniversalFunctions.sendError(err));
                        } else {
                            reply(UniversalFunctions.sendSuccess(null,data))
                        }
                    });
                }
                else{
                    reply(UniversalFunctions.sendError(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR));
                }
            },
            description: 'createTemplate',
            notes: ' createTemplate',
            auth: 'AdminAuth',
            tags: ['api', 'admin'], // ADD THIS TAG
            validate: {
                payload: {
                    categoryId:Joi.array().required(),
                    name:Joi.string().required(),
                    htmlUrl:Joi.string().required(),
                    isPublic:Joi.boolean().optional(),
                    imageUrl:Joi.string().required(),
                    price:Joi.number().required(),
                    isCustom: Joi.boolean().valid(true,false).optional()
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType : 'form',
                    responses:Config.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },
    {
        method: 'post',
        path: '/admin/updateTemplates',
        config: {
            handler: function (request, reply) {
                var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
                if(userData.id){
                    Controller.adminController.updateTemplates(request.payload,userData, function (err, data) {
                        if (err) {
                            //console.log("-----------")
                            reply(UniversalFunctions.sendError(err));
                        } else {
                            reply(UniversalFunctions.sendSuccess(null,data))
                        }
                    });
                }
                else{
                    reply(UniversalFunctions.sendError(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR));
                }
            },
            description: 'updateTemplates',
            notes: 'updateTemplates',
            auth: 'AdminAuth',
            tags: ['api', 'admin'], // ADD THIS TAG
            validate: {
                payload: {
                    _id:Joi.string().trim().required(),
                    categoryId:Joi.string().optional(),
                    name:Joi.string().optional(),
                    isPublic:Joi.boolean().optional(),
                    isCustom:Joi.boolean().optional(),
                    htmlUrl:Joi.string().optional(),
                    imageUrl:Joi.string().optional(),
                    price:Joi.number().optional()
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType : 'form',
                    responses:Config.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },
    {
        method: 'post',
        path: '/admin/deleteTemplates',
        config: {
            handler: function (request, reply) {
                var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
                if(userData.id){
                    Controller.adminController.deleteTemplates(request.payload,userData, function (err, data) {
                        if (err) {
                            //console.log("-----------")
                            reply(UniversalFunctions.sendError(err));
                        } else {
                            reply(UniversalFunctions.sendSuccess(null,data))
                        }
                    });
                }
                else{
                    reply(UniversalFunctions.sendError(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR));
                }
            },
            description: 'deleteTemplates',
            notes: 'deleteTemplates',
            auth: 'AdminAuth',
            tags: ['api', 'admin'], // ADD THIS TAG
            validate: {
                payload: {
                    _id:Joi.string().trim().required()
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType : 'form',
                    responses:Config.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },
    {
        method: 'GET',
        path: '/admin/getTemplates',
        config: {
            handler: function (request, reply) {
                var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
                console.log('++++++++++AAAA', request.query)
                // if(userData.id){
                    Controller.adminController.getTemplates(request.query,userData, function (err, data) {
                        if (err) {
                            reply(UniversalFunctions.sendError(err));
                        } else {
                            reply(UniversalFunctions.sendSuccess(null,data))
                        }
                    });
                // }
                // else{
                //     reply(UniversalFunctions.sendError(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR));
                // }
            },
            description: 'getTemplates',
            notes: 'getTemplates',
            auth: {
            strategy:'commonAuth',
            mode:'optional'
           },
            tags: ['api', 'admin'], // ADD THIS TAG
            validate: {
                query: {
                    type:Joi.string().optional().valid(["Free","Paid"]),
                    isPublic:Joi.boolean().optional(),
                    templateId:Joi.string().optional(),
                    categoryId:Joi.string().optional()
                },
                headers: UniversalFunctions.authorizationHeaderObjOptional,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType : 'form',
                    responses:Config.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },
    {
        method: 'GET',
        path: '/admin/getDashBoard',
        config: {
            handler: function (request, reply) {
                var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
                if(userData.id){
                    Controller.adminController.getDashBoard(request.query,userData, function (err, data) {
                        if (err) {
                            //console.log("-----------")
                            reply(UniversalFunctions.sendError(err));
                        } else {
                            reply(UniversalFunctions.sendSuccess(null,data))
                        }
                    });
                }
                else{
                    reply(UniversalFunctions.sendError(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR));
                }
            },
            description: 'get DashBoard',
            notes: 'get DashBoard',
            auth: 'AdminAuth',
            tags: ['api', 'admin'],
            validate: {
                query: {
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType : 'form',
                    responses:Config.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },
    {
        method: 'post',
        path: '/admin/createBackground',
        config: {
            handler: function (request, reply) {
                var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
                if(userData.id){
                    Controller.adminController.createBackground(request.payload,userData, function (err, data) {
                        if (err) {
                            //console.log("-----------")
                            reply(UniversalFunctions.sendError(err));
                        } else {
                            reply(UniversalFunctions.sendSuccess(null,data))
                        }
                    });
                }
                else{
                    reply(UniversalFunctions.sendError(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR));
                }
            },
            description: 'createBackground',
            notes: 'createBackground',
            auth: 'AdminAuth',
            tags: ['api', 'admin'], // ADD THIS TAG
            payload: {
                maxBytes: 20000000,
                parse: true,
                output: 'file'
            },
            validate: {
                payload: {
                    image: Joi.any()
                        .meta({swaggerType: 'file'})
                        .optional()
                        .description('image/pdf file'),
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType : 'form',
                    responses:Config.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    {
        method: 'GET',
        path: '/admin/getBackgrounds',
        config: {
            handler: function (request, reply) {
                var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
                if(userData.id){
                    Controller.adminController.getBackgrounds(request.payload,userData, function (err, data) {
                        if (err) {
                            reply(UniversalFunctions.sendError(err));
                        } else {
                            reply(UniversalFunctions.sendSuccess(null,data))
                        }
                    });
                }
                else{
                    reply(UniversalFunctions.sendError(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR));
                }
            },
            description: 'getBackgrounds',
            notes: 'getBackgrounds',
            auth: 'AdminAuth',
            tags: ['api', 'admin'], // ADD THIS TAG
            validate: {
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType : 'form',
                    responses:Config.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },
    {
        method: 'post',
        path: '/admin/deleteBackground',
        config: {
            handler: function (request, reply) {
                var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
                if(userData.id){
                    Controller.adminController.deleteBackground(request.payload,userData, function (err, data) {
                        if (err) {
                            //console.log("-----------")
                            reply(UniversalFunctions.sendError(err));
                        } else {
                            reply(UniversalFunctions.sendSuccess(null,data))
                        }
                    });
                }
                else{
                    reply(UniversalFunctions.sendError(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR));
                }
            },
            description: 'deleteBackground',
            notes: 'deleteBackground',
            auth: 'AdminAuth',
            tags: ['api', 'admin'], // ADD THIS TAG
            validate: {
                payload: {
                    _id:Joi.string().trim().required()
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType : 'form',
                    responses:Config.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },
    {
        method: 'post',
        path: '/admin/uploadImageAWS',
        config: {
            handler: function (request, reply) {
                var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
                if(userData.id){
                    Controller.adminController.uploadImageAWS(request.payload,userData, function (err, data) {
                        if (err) {
                            //console.log("-----------")
                            reply(UniversalFunctions.sendError(err));
                        } else {
                            reply(UniversalFunctions.sendSuccess(null,data))
                        }
                    });
                }
                else{
                    reply(UniversalFunctions.sendError(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR));
                }
            },
            description: 'uploadImageAWS',
            notes: 'uploadImageAWS',
            auth: 'commonAuth',
            tags: ['api', 'admin'], // ADD THIS TAG
            payload: {
                maxBytes: 20000000,
                parse: true,
                timeout: false,
                output: 'file'
            },
            validate: {
                payload: {
                    image: Joi.any()
                        .meta({swaggerType: 'file'})
                        .optional()
                        .description('image/pdf file'),
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType : 'form',
                    responses:Config.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },
    {
        method: 'post',
        path: '/admin/sendPush',
        config: {
            handler: function (request, reply) {
                var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
                if(userData.id){
                    Controller.adminController.sendPush(request.payload,userData, function (err, data) {
                        if (err) {
                            //console.log("-----------")
                            reply(UniversalFunctions.sendError(err));
                        } else {
                            reply(UniversalFunctions.sendSuccess(null,data))
                        }
                    });
                }
                else{
                    reply(UniversalFunctions.sendError(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR));
                }
            },
            description: 'sendPush',
            notes: 'sendPush',
            auth: 'commonAuth',
            tags: ['api', 'admin'], // ADD THIS TAG
            validate: {
                payload: {
                    body: Joi.string().optional(),
                    sendAt: Joi.number().optional(),
                    title: Joi.string().optional(),
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType : 'form',
                    responses:Config.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },   {
        method: 'post',
        path: '/admin/blockUnblockEvent',
        config: {
            handler: function (request, reply) {
                var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
                if(userData.id){
                    Controller.adminController.blockUnblockEvent(request.payload,userData, function (err, data) {
                        if (err) {
                            //console.log("-----------")
                            reply(UniversalFunctions.sendError(err));
                        } else {
                            reply(UniversalFunctions.sendSuccess(null,data))
                        }
                    });
                }
                else{
                    reply(UniversalFunctions.sendError(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR));
                }
            },
            description: 'sendPush',
            notes: 'sendPush',
            auth: 'commonAuth',
            tags: ['api', 'admin'], // ADD THIS TAG
            validate: {
                payload: {
                    eventId: Joi.string().required(),
                    reason: Joi.string().required(),
                    status:  Joi.boolean().required()
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType : 'form',
                    responses:Config.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    }, {
        method: 'post',
        path: '/admin/addSubAdmin',
        config: {
            handler: function (request, reply) {
                var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
                if(userData.id){
                    Controller.adminController.addSubAdmin(request.payload,userData, function (err, data) {
                        if (err) {
                            //console.log("-----------")
                            reply(UniversalFunctions.sendError(err));
                        } else {
                            reply(UniversalFunctions.sendSuccess(null,data))
                        }
                    });
                }
                else{
                    reply(UniversalFunctions.sendError(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR));
                }
            },
            description: 'sendPush',
            notes: 'sendPush',
            auth: 'AdminAuth',
            tags: ['api', 'admin'], // ADD THIS TAG
            validate: {
                payload: {
                    name: Joi.string().optional(),
                    email: Joi.string().optional(),
                    password: Joi.string().optional(),
                    category:Joi.object().keys({
                        add: Joi.boolean(),
                        edit: Joi.boolean(),
                        view: Joi.boolean(),
                        delete: Joi.boolean()
                   }),
                    template:Joi.object().keys({
                        add: Joi.boolean(),
                        edit: Joi.boolean(),
                        view: Joi.boolean(),
                        delete: Joi.boolean()
                   }),
                    package:Joi.object().keys({
                        add: Joi.boolean(),
                        edit: Joi.boolean(),
                        view: Joi.boolean(),
                        delete: Joi.boolean()
                   }),
                    user:Joi.object().keys({
                        add: Joi.boolean(),
                        edit: Joi.boolean(),
                        view: Joi.boolean(),
                        block: Joi.boolean()
                   }),
                    event:Joi.object().keys({
                        view: Joi.boolean()
                   }),
                    guard:Joi.object().keys({
                        add: Joi.boolean(),
                        edit: Joi.boolean(),
                        view: Joi.boolean(),
                        block: Joi.boolean()
                    }),
                    promo:Joi.object().keys({
                        add: Joi.boolean(),
                        view: Joi.boolean(),
                        delete: Joi.boolean()
                    }),
                    chat:Joi.object().keys({
                        view: Joi.boolean(),
                        allow: Joi.boolean()
                    }),
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType : 'form',
                    responses:Config.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },
    {
        method: 'post',
        path: '/admin/addPromoCode',
        config: {
            handler: function (request, reply) {
                var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
                if(userData.id){
                    Controller.adminController.addPromoCode(request.payload,userData, function (err, data) {
                        if (err) {
                            //console.log("-----------")
                            reply(UniversalFunctions.sendError(err));
                        } else {
                            reply(UniversalFunctions.sendSuccess(null,data))
                        }
                    });
                }
                else{
                    reply(UniversalFunctions.sendError(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR));
                }
            },
            description: 'addPromoCode',
            notes: 'addPromoCode',
            auth: 'commonAuth',
            tags: ['api', 'admin'], // ADD THIS TAG
            validate: {
                payload: {
                    couponCode:Joi.string().trim().required(),
                    amount:Joi.string().trim().required(),
                    sms:Joi.boolean().optional(),
                    // promoTimes:Joi.number().required(),
                    // userLimit:Joi.number().required(),
                    startAt:Joi.number().required(),
                    endAt:Joi.number().required(),
                    amountType:Joi.string().required().valid([Config.APP_CONSTANTS.DATABASE.PROMO_CODE.AMOUNT,Config.APP_CONSTANTS.DATABASE.PROMO_CODE.PERCENTAGE])
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType : 'form',
                    responses:Config.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },
    {
        method: 'GET',
        path: '/admin/getAllGuards',
        config: {
            handler: function (request, reply) {
                var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
                if(userData.id){
                    Controller.adminController.getAllGuards(request.query,userData, function (err, data) {
                        if (err) {
                            //console.log("-----------")
                            reply(UniversalFunctions.sendError(err));
                        } else {
                            reply(UniversalFunctions.sendSuccess(null,data))
                        }
                    });
                }
                else{
                    reply(UniversalFunctions.sendError(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR));

                }
            },
            description: 'admin getAllGuards api',
            notes: 'admin getAllGuards api',
            auth: 'AdminAuth',
            tags: ['api', 'admin'], // ADD THIS TAG
            validate: {
                query: {
                    skip:Joi.number().required(),
                    limit:Joi.number().required(),

                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType : 'form',
                    responses:Config.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },{
        method: 'GET',
        path: '/admin/getAllSubAdmins',
        config: {
            handler: function (request, reply) {
                var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
                if(userData.id){
                    Controller.adminController.getAllSubAdmins(request.query,userData, function (err, data) {
                        if (err) {
                            //console.log("-----------")
                            reply(UniversalFunctions.sendError(err));
                        } else {
                            reply(UniversalFunctions.sendSuccess(null,data))
                        }
                    });
                }
                else{
                    reply(UniversalFunctions.sendError(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR));

                }
            },
            description: 'admin getAllSubAdmins api',
            notes: 'admin getAllSubAdmins api',
            auth: 'AdminAuth',
            tags: ['api', 'admin'], // ADD THIS TAG
            validate: {
                query: {
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType : 'form',
                    responses:Config.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },
    {
        method: 'GET',
        path: '/admin/getAllPromoCodes',
        config: {
            handler: function (request, reply) {
                var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
                if(userData.id){
                    Controller.adminController.getAllPromoCodes(request.query,userData, function (err, data) {
                        if (err) {
                            //console.log("-----------")
                            reply(UniversalFunctions.sendError(err));
                        } else {
                            reply(UniversalFunctions.sendSuccess(null,data))
                        }
                    });
                }
                else{
                    reply(UniversalFunctions.sendError(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR));

                }
            },
            description: 'admin getAllPromoCodes api',
            notes: 'admin getAllPromoCodes api',
            auth: 'AdminAuth',
            tags: ['api', 'admin'], // ADD THIS TAG
            validate: {
                query: {
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType : 'form',
                    responses:Config.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },
    {
        method: 'GET',
        path: '/admin/getAllEvents',
        config: {
            handler: function (request, reply) {
                var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
                if(userData.id){
                    Controller.adminController.getAllEvents(request.query,userData, function (err, data) {
                        if (err) {
                            //console.log("-----------")
                            reply(UniversalFunctions.sendError(err));
                        } else {
                            reply(UniversalFunctions.sendSuccess(null,data))
                        }
                    });
                }
                else{
                    reply(UniversalFunctions.sendError(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR));

                }
            },
            description: 'admin getAllEvents api',
            notes: 'admin getAllEvents api',
            auth: 'AdminAuth',
            tags: ['api', 'admin'], // ADD THIS TAG
            validate: {
                query: {
                    userId:Joi.string().optional(),
                    skip:Joi.number().optional(),
                    limit:Joi.number().optional(),
                    from : Joi.any().optional(),
                    startFrom : Joi.any().optional(),
                    categoryId : Joi.string().optional(),
                    to : Joi.any().optional(),
                    startTo : Joi.any().optional(),
                    filter:Joi.string().valid(["past","upcoming","ongoing"]).optional(),
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType : 'form',
                    responses:Config.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },
    {
        method: 'GET',
        path: '/admin/getEventDesc',
        config: {
            handler: function (request, reply) {
                    Controller.adminController.getEventDesc(request.query, function (err, data) {
                        if (err) {
                            //console.log("-----------")
                            reply(UniversalFunctions.sendError(err));
                        } else {
                            reply(UniversalFunctions.sendSuccess(null,data))
                        }
                    });
            },
            description: 'admin getEventDesc api',
            notes: 'admin getEventDesc api',
            tags: ['api', 'admin'], // ADD THIS TAG
            validate: {
                query: {
                    eventId:Joi.string().required(),
                    phoneNo:Joi.string().required()
                },
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType : 'form',
                    responses:Config.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },
    {
        method: 'GET',
        path: '/admin/getReports',
        config: {
            handler: function (request, reply) {
                var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
                if(userData.id){
                    Controller.adminController.getReports(request.query,userData, function (err, data) {
                        if (err) {
                            //console.log("-----------")
                            reply(UniversalFunctions.sendError(err));
                        } else {
                            reply(UniversalFunctions.sendSuccess(null,data))
                        }
                    });
                }
                else{
                    reply(UniversalFunctions.sendError(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR));

                }
            },
            description: 'admin getReports api',
            notes: 'admin getReports api',
            auth: 'AdminAuth',
            tags: ['api', 'admin'], // ADD THIS TAG
            validate: {
                query: {
                    days : Joi.number().valid([10,30,90]).default(30).optional(),
                    categoryId : Joi.string().optional(),
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType : 'form',
                    responses:Config.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    }, {
        method: 'GET',
        path: '/admin/getContactUs',
        config: {
            handler: function (request, reply) {
                var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
                if(userData.id){
                    Controller.adminController.getContactUs(request.query,userData, function (err, data) {
                        if (err) {
                            //console.log("-----------")
                            reply(UniversalFunctions.sendError(err));
                        } else {
                            reply(UniversalFunctions.sendSuccess(null,data))
                        }
                    });
                }
                else{
                    reply(UniversalFunctions.sendError(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR));

                }
            },
            description: 'admin getContactUs api',
            notes: 'admin getContactUs api',
            auth: 'AdminAuth',
            tags: ['api', 'admin'], // ADD THIS TAG
            validate: {
                query: {
                    skip:Joi.number().required(),
                    limit:Joi.number().required(),
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType : 'form',
                    responses:Config.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },
    {
        method: 'GET',
        path: '/admin/getScheduledNotifs',
        config: {
            handler: function (request, reply) {
                var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
                if(userData.id){
                    Controller.adminController.getScheduledNotifs(request.query,userData, function (err, data) {
                        if (err) {
                            //console.log("-----------")
                            reply(UniversalFunctions.sendError(err));
                        } else {
                            reply(UniversalFunctions.sendSuccess(null,data))
                        }
                    });
                }
                else{
                    reply(UniversalFunctions.sendError(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR));

                }
            },
            description: 'admin getScheduledNotifs api',
            notes: 'admin getScheduledNotifs api',
            auth: 'AdminAuth',
            tags: ['api', 'admin'], // ADD THIS TAG
            validate: {
                query: {
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType : 'form',
                    responses:Config.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },
    {
        method: 'GET',
        path: '/admin/getAdminNotifications',
        config: {
            handler: function (request, reply) {
                var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
                if(userData.id){
                    Controller.adminController.getAdminNotifications(request.query,userData, function (err, data) {
                        if (err) {
                            //console.log("-----------")
                            reply(UniversalFunctions.sendError(err));
                        } else {
                            reply(UniversalFunctions.sendSuccess(null,data))
                        }
                    });
                }
                else{
                    reply(UniversalFunctions.sendError(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR));

                }
            },
            description: 'admin getAdminNotifications api',
            notes: 'admin getAdminNotifications api',
            auth: 'AdminAuth',
            tags: ['api', 'admin'], // ADD THIS TAG
            validate: {
                query: {
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType : 'form',
                    responses:Config.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },
    {
        method: 'GET',
        path: '/admin/getRequestUsers',
        config: {
            handler: function (request, reply) {
                var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
                if(userData.id){
                    Controller.adminController.getRequestUsers(request.query,userData, function (err, data) {
                        if (err) {
                            //console.log("-----------")
                            reply(UniversalFunctions.sendError(err));
                        } else {
                            reply(UniversalFunctions.sendSuccess(null,data))
                        }
                    });
                }
                else{
                    reply(UniversalFunctions.sendError(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR));

                }
            },
            description: 'admin getRequestUsers api',
            notes: 'admin getRequestUsers api',
            auth: 'AdminAuth',
            tags: ['api', 'admin'], // ADD THIS TAG
            validate: {
                query: {
                    search:Joi.string().optional(),
                    skip:Joi.number().required(),
                    limit:Joi.number().required(),
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType : 'form',
                    responses:Config.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },
    {
        method: 'GET',
        path: '/admin/getUserRequests',
        config: {
            handler: function (request, reply) {
                var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
                if(userData.id){
                    Controller.adminController.getUserRequests(request.query,userData, function (err, data) {
                        if (err) {
                            //console.log("-----------")
                            reply(UniversalFunctions.sendError(err));
                        } else {
                            reply(UniversalFunctions.sendSuccess(null,data))
                        }
                    });
                }
                else{
                    reply(UniversalFunctions.sendError(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR));

                }
            },
            description: 'admin getUserRequests api',
            notes: 'admin getUserRequests api',
            auth: 'commonAuth',
            tags: ['api', 'admin'], // ADD THIS TAG
            validate: {
                query: {
                    search:Joi.string().optional(),
                    skip:Joi.string().optional(),
                    limit:Joi.string().optional(),
                    userId:Joi.string().required()
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType : 'form',
                    responses:Config.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },
    {
        method: 'GET',
        path: '/admin/getRequestDesc',
        config: {
            handler: function (request, reply) {
                var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
                if(userData.id){
                    Controller.adminController.getRequestDesc(request.query,userData, function (err, data) {
                        if (err) {
                            //console.log("-----------")
                            reply(UniversalFunctions.sendError(err));
                        } else {
                            reply(UniversalFunctions.sendSuccess(null,data))
                        }
                    });
                }
                else{
                    reply(UniversalFunctions.sendError(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR));

                }
            },
            description: 'admin getRequestDesc api',
            notes: 'admin getRequestDesc api',
            auth: 'commonAuth',
            tags: ['api', 'admin'], // ADD THIS TAG
            validate: {
                query: {
                    _id:Joi.string().required(),
                    search:Joi.string().optional(),
                    //  pageNo:Joi.string().required(),
                    skip:Joi.string().required(),
                    limit:Joi.string().required()
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType : 'form',
                    responses:Config.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },
    {
        method: 'post',
        path: '/admin/deletePromoCodes',
        config: {
            handler: function (request, reply) {
                var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
                if(userData.id){
                    Controller.adminController.deletePromoCodes(request.payload,userData, function (err, data) {
                        if (err) {
                            //console.log("-----------")
                            reply(UniversalFunctions.sendError(err));
                        } else {
                            reply(UniversalFunctions.sendSuccess(null,data))
                        }
                    });
                }
                else{
                    reply(UniversalFunctions.sendError(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR));
                }
            },
            description: 'deletePromoCodes',
            notes: 'deletePromoCodes',
            auth: 'AdminAuth',
            tags: ['api', 'admin'], // ADD THIS TAG
            validate: {
                payload: {
                    _id:Joi.string().trim().required()

                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType : 'form',
                    responses:Config.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },
    {
        method: 'post',
        path: '/admin/editSubAdmin',
        config: {
            handler: function (request, reply) {
                var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
                if(userData.id){
                    Controller.adminController.editSubAdmin(request.payload,userData, function (err, data) {
                        if (err) {
                            //console.log("-----------")
                            reply(UniversalFunctions.sendError(err));
                        } else {
                            reply(UniversalFunctions.sendSuccess(null,data))
                        }
                    });
                }
                else{
                    reply(UniversalFunctions.sendError(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR));
                }
            },
            description: 'editSubAdmin',
            notes: 'editSubAdmin',
            auth: 'AdminAuth',
            tags: ['api', 'admin'], // ADD THIS TAG
            validate: {
                payload: {
                    _id: Joi.string().required(),
                    category:Joi.object().optional().keys({
                        add: Joi.boolean(),
                        edit: Joi.boolean(),
                        view: Joi.boolean(),
                        delete: Joi.boolean()
                    }).optional(),
                    template:Joi.object().keys({
                        add: Joi.boolean(),
                        edit: Joi.boolean(),
                        view: Joi.boolean(),
                        delete: Joi.boolean()
                    }).optional(),
                    package:Joi.object().keys({
                        add: Joi.boolean(),
                        edit: Joi.boolean(),
                        view: Joi.boolean(),
                        delete: Joi.boolean()
                    }).optional(),
                    user:Joi.object().keys({
                        add: Joi.boolean(),
                        edit: Joi.boolean(),
                        view: Joi.boolean(),
                        block: Joi.boolean()
                    }).optional(),
                    event:Joi.object().keys({
                        view: Joi.boolean()
                    }).optional(),
                    guard:Joi.object().keys({
                        add: Joi.boolean(),
                        edit: Joi.boolean(),
                        view: Joi.boolean(),
                        block: Joi.boolean()
                    }).optional(),
                    promo:Joi.object().keys({
                        add: Joi.boolean(),
                        view: Joi.boolean(),
                        delete: Joi.boolean()
                    }).optional(),
                    chat:Joi.object().keys({
                        view: Joi.boolean(),
                        allow: Joi.boolean()
                    }).optional(),
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType : 'form',
                    responses:Config.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },
    {
        method: 'post',
        path: '/admin/deleteScheduledNotifs',
        config: {
            handler: function (request, reply) {
                var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
                if(userData.id){
                    Controller.adminController.deleteScheduledNotifs(request.payload,userData, function (err, data) {
                        if (err) {
                            //console.log("-----------")
                            reply(UniversalFunctions.sendError(err));
                        } else {
                            reply(UniversalFunctions.sendSuccess(null,data))
                        }
                    });
                }
                else{
                    reply(UniversalFunctions.sendError(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR));
                }
            },
            description: 'deleteScheduledNotifs',
            notes: 'deleteScheduledNotifs',
            auth: 'AdminAuth',
            tags: ['api', 'admin'], // ADD THIS TAG
            validate: {
                payload: {
                    _id:Joi.string().trim().required()
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType : 'form',
                    responses:Config.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },
    {
        method: 'post',
        path: '/admin/addInvites',
        config: {
            handler: function (request, reply) {
                var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
                if(userData.id){
                    Controller.adminController.addInvites(request.payload,userData, function (err, data) {
                        if (err) {
                            console.log("-----------",err)
                            reply(UniversalFunctions.sendError(err));
                        } else {
                            reply(UniversalFunctions.sendSuccess(null,data))
                        }
                    });
                }
                else{
                    reply(UniversalFunctions.sendError(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR));
                }
            },
            description: 'addInvites',
            notes: ' addInvites',
            auth: 'AdminAuth',
            tags: ['api', 'user'], // ADD THIS TAG
            validate: {
                payload: {
                    eventId:Joi.string().required(),
                    invites:Joi.array().required().description("[{email: '',name:'',phoneNo:''}]")
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType : 'form',
                    responses:Config.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },
    {
        method: 'post',
        path: '/admin/addEditAdvertisements',
        config: {
            handler: function (request, reply) {
                var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
                if(userData.id){
                    Controller.adminController.addEditAdvertisements(request.payload,userData, function (err, data) {
                        if (err) {
                            //console.log("-----------")
                            reply(UniversalFunctions.sendError(err));
                        } else {
                            reply(UniversalFunctions.sendSuccess(null,data))
                        }
                    });
                }
                else{
                    reply(UniversalFunctions.sendError(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR));
                }
            },
            description: 'addEditAdvertisements',
            notes: 'addEditAdvertisements',
            auth: 'AdminAuth',
            tags: ['api', 'admin'], // ADD THIS TAG
            validate: {
                payload: {
                    _id:Joi.string().optional(),
                    categoryId:Joi.string().optional().allow(""),
                    isDeleted:Joi.boolean().optional(),
                    isBlocked:Joi.boolean().optional(),
                    url:Joi.string().optional(),
                    adImageURL:Joi.object().keys({
                        original: Joi.string(),
                        thumbnail: Joi.string()
                    }).optional(),
                    gender:Joi.string().optional().valid([Config.APP_CONSTANTS.DATABASE.GENDER_TYPE.MALE,Config.APP_CONSTANTS.DATABASE.GENDER_TYPE.FEMALE,""]).allow(""),
                    startAge:Joi.number().optional(),
                    endAge:Joi.number().optional().min(Joi.ref('startAge')),
                    lat: Joi.number().optional(),
                    long: Joi.number().optional(),
                    address: Joi.string().trim().optional().allow("")
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType : 'form',
                    responses:Config.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },
    {
        method: 'GET',
        path: '/admin/listAdvertisements',
        config: {
            handler: function (request, reply) {
                var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
                if(userData.id){
                    Controller.adminController.listAdvertisements(request.query,userData, function (err, data) {
                        if (err) {
                            //console.log("-----------")
                            reply(UniversalFunctions.sendError(err));
                        } else {
                            reply(UniversalFunctions.sendSuccess(null,data))
                        }
                    });
                }
                else{
                    reply(UniversalFunctions.sendError(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR));

                }
            },
            description: 'listAdvertisements',
            notes: 'listAdvertisements',
            auth: 'commonAuth',
            tags: ['api', 'admin'], // ADD THIS TAG
            validate: {
                query: {
                    _id:Joi.string().optional(),
                    gender:Joi.string().optional().valid([Config.APP_CONSTANTS.DATABASE.GENDER_TYPE.MALE,Config.APP_CONSTANTS.DATABASE.GENDER_TYPE.FEMALE]),
                    //  pageNo:Joi.string().required(),
                    skip:Joi.number().required(),
                    limit:Joi.number().required(),
                    startAge:Joi.number().optional(),
                    endAge:Joi.number().optional().min(Joi.ref('startAge')),
                    lat: Joi.number().optional(),
                    long: Joi.number().optional(),
                    address: Joi.string().trim().optional().allow("")
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType : 'form',
                    responses:Config.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },
    ];
var authRoutes = [].concat( adminLogin);
module.exports = authRoutes;