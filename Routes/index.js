/**
 * Created by Shumi Gupta on 29/3/17.
 */

'use strict';

const userRoute = require('./userRoutes');
const adminRoute = require('./adminRoutes');


const all = [].concat(userRoute,adminRoute);

module.exports = all;