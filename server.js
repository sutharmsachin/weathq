const Hapi = require('hapi');
const Config = require('./Config');
const Plugins = require('./Plugins');
const Routes = require('./Routes');
const bootStrap = require('./Utils/bootStrap');
const privacyPolicy = require('./Utils/privacyPolicy');
const termAndConditions = require('./Utils/tearmsandconditions');
const Schedular = require('./Lib/Schedular');
Schedular.schedular()
/*let string="Please click this link to continue";
let link = string.link('http://34.211.166.21/wethaq_dev/wethaq-invite.html?eventId='+"abc"+'&phoneNo='+"+9138678276");
console.log("link",link)*/
// let numCPUs = require('os').cpus().length;
/*let map=""
var GoogleUrl = require( 'google-url' );
googleUrl = new GoogleUrl({ key: 'AIzaSyC-7hgMKChd9_9DmbNUP3LMDMacfTx6OXg' });
googleUrl.shorten("https://www.google.com/maps?q=28.7041,77.1025", function( err, shortUrl ) {
    map=shortUrl.toString()
    const mobilySms = require('mobily-sms')('wethaq','haq104');
// {Custome message}
// {For more info please check the link}
// {Event location in google map is}
    let link='http://34.211.166.21/wethaq_admin/#/invitation/'+123+'/'+13121;
    let body="";
    body+="Please come to my birthday party\n";
    body+="For more info please check the link: "+link+"\n";
    body+="Event location in google map is: "+map+"\n";
    mobilySms.sendSms(body,["+917508452045"],'Wethaq-app',{},function(error,result){
        console.log("1233",error,result);
    });
    console.log("1111",shortUrl,err)
});*/
console.log("==========num OF CPUs=============",require('os').cpus().length);
//Create Server
const server = new Hapi.Server();
console.log("date",new Date("February 18, 2018 08:00:00 AM").getTime())
//create connection
let port=Config.APP_CONSTANTS.SERVER.PORTS.HAPI_CLIENT;
if(process.env.NODE_ENV == 'dev')port=Config.APP_CONSTANTS.SERVER.PORTS.HAPI;
server.connection({
    // port: Config.APP_CONSTANTS.SERVER.PORTS.HAPI,
    // port: Config.APP_CONSTANTS.SERVER.PORTS.HAPI_TEST,
    //  port: Config.APP_CONSTANTS.SERVER.PORTS.HAPI_CLIENT,
     port: port,
     routes: { cors: true }
});
//Register All Plugins
server.register(Plugins, function (err) {

    if (err){
        console.log("===========err=========",err);
        server.error('Error while loading plugins : ' + err)
    }else {

        server.route(Routes);
        server.log('info','Plugins Loaded');
    }
});

//Adding Views
// server.views({
//     engines: {
//         html: require('handlebars')
//     },
//     relativeTo: __dirname,
//     path: './Views'
// });


server.route(
    [
        {
            method: 'GET',
            path: '/',
            handler: function (req, res) {
                //TODO Change for production server
                res('Hello Thanks for visiting here')
               // res.view('index.html')
            }
        },
        {
            method: 'GET',
            path: '/privacyPolicy',
            handler: function (req, res) {
                privacyPolicy.privacyPolicy(function(err,result){
                    res(result)
                });
                //TODO Change for production server

            }
        },
        {
            method: 'GET',
            path: '/termsandconditions',
            handler: function (req, res) {
                //TODO Change for production server
                termAndConditions.termAndConditions(function(err,result){
                    res(result)
                });
            }
        }
    ]);
//Bootstrap admin data
bootStrap.bootstrapAdmin(function (err, message) {
    if (err) {
        console.log('Error while bootstrapping admin : ' + err)
    } else {
        console.log(message);
    }
});


bootStrap.connectSocket(server);

bootStrap.bootstrapAppVersion(function (err, message) {
    if (err) {
        console.log('Error while bootstrapping admin : ' + err)
    } else {
        console.log(message);
    }
});


bootStrap.bootstrapInsertUser(function (err, message) {
    if (err) {
        console.log('Error while bootstrapping admin : ' + err)
    } else {
        console.log(message);
    }
});

//bootStrap.connectSocket(server);

//Start Server



server.start(function () {
    fileCheck()
    server.log('info', 'Server running at: ' + server.info.uri);
    console.log("server successfully started")
});


const fs = require("fs")

function fileCheck(){

    fs.readFile('./dummyTemp.html','utf8', (err, data)=> {
        if (err) {
            throw err;
        }
        else{
           // console.log(data[520],data[523],data[522],data[521]);
            checkKeyWord(data,0)

            
        }
    });

}

let arr=[];
function checkKeyWord(data,key){
    if(data.indexOf("parameter",key)>-1){
    /*    let k1 =data.indexOf("parameter",key);
        console.log((data.indexOf("parameter",k1)));
        checkKeyWord(data,k1+1)*/

      let startTag=data.indexOf("parameter=",key);
      let endtag=data.indexOf('>',startTag);
      let startParamTag=data.indexOf('"',startTag);
      let endParamTag=data.indexOf('"',startParamTag+1);
      let nextStartTag=data.indexOf('<',startTag);
        let text=endtag-startTag;
      //  console.log("sss",endtag,startTag,text,endParamTag,startParamTag);

        let str=data.slice(startTag+text+1, nextStartTag);
        let str2=data.slice(startParamTag+1, endParamTag);
        arr.push(str);
    //    console.log("12222",arr,str2);
        checkKeyWord(data,startTag+1)

 /*      let text=data
         text= text.replace(/parameter="googlelocation">[^>]+</ig, 'parameter="googlelocation">Chandigarh<');
         text= text.replace(/parameter="invities">[^>]+</ig, 'parameter="invities">This is a test flight<');
         text= text.replace(/parameter="comment">[^>]+</ig, 'parameter="comment">udgaviubauodbanoidnsdaoinonoidanodsanosdan<');
       // console.log("1111",text)*/
    }
    else{

    }
}