/**
 * Created by Shumi Gupta on 29/3/17.
 */


module.exports = [
    { register: require('./swagger')},
     { register: require('./good-console')}
    ,{ register: require('./auth-token')}
];