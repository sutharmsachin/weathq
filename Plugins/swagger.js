/**
 * Created by Shumi gupta on 29/3/17.
 */

const Inert = require('inert');
const Vision = require('vision');
const HapiSwagger = require('hapi-swagger');
const Pack = require('../package');

const options = {
    info: {
        'title': 'Documentation',
        'version': Pack.version,
    }
};

exports.register = function(server,options,next){
    server.register([
        Inert,
        Vision,
        {
            'register': HapiSwagger,
            'options': options 
        }],function(err){
        if (err) {
            server.log(['error'], 'hapi-swagger load error: ' + err)
        }else{
            server.log(['start'], 'hapi-swagger interface loaded')
        }
    });
    next()
};

exports.register.attributes = {
    name: 'swagger-plugin'
};