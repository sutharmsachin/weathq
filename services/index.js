/**
 * Created by shumi on 29/3/17.
 */

module.exports = {
    userServices : require('./userServices'),
    adminServices :require('./adminServices'),
    pricingServices:require('./pricingServices'),
    advertisementServices:require('./advertisementServices'),
    categoryServices: require('./categoryServices'),
    notificationServices: require('./notificationServices'),
    eventServices: require('./eventServices'),
    templateServices: require('./templateServices'),
    backgroundServices: require('./backgroundServices'),
    requestServices: require('./requestServices'),
    promoCodeServices: require('./promoCodeServices'),
    adminNotifServices: require('./adminNotifServices'),
    scheduleNotifServices: require('./scheduleNotifServices'),
    transactionServices: require('./transactionServices'),
    contactServices: require('./contactServices'),
    dummyTempServices: require('./dummyTempServices'),
    appVersionServices: require('./appVersionServices')
};