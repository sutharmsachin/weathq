/**
 * Created by Shumi on 19/2/18.
 */


'use strict';

var Models = require('../Models');


//Insert Event in DB
var createEvent = function (objToSave, callback) {
    new Models.event(objToSave).save(callback)
};

//Update Event in DB
var updateEvent = function (criteria, dataToSet, options, callback) {
    Models.event.findOneAndUpdate(criteria, dataToSet, options, callback);
};

//Delete Event in DB
var deleteEvent = function (criteria, callback) {
    Models.event.findOneAndRemove(criteria, callback);
};

var getEvent = function (criteria, projection,options, callback) {
    Models.event.find(criteria, projection,options,callback)
};

var getEventPopulate = function (criteria, project, options,populateArray, callback) {
    Models.event.find(criteria, project, options).populate(populateArray).exec(function (err, docs) {
        if (err) {
            return callback(err, docs);
        }else{
            callback(null, docs);
        }
    });
};

var countEvents = function (criteria, callback) {
    Models.event.count(criteria,callback);
};

var eventAggregate = function (criteria, callback) {
    Models.event.aggregate(criteria)
        .exec(function(error, result) {
            callback(error,result);
        });
};
module.exports = {
    createEvent: createEvent,
    updateEvent:updateEvent,
    deleteEvent:deleteEvent,
    getEvent:getEvent,
    getEventPopulate:getEventPopulate,
    eventAggregate:eventAggregate,
    countEvents:countEvents
};