/**
 * Created by shumi on 29/3/17.
 */


'use strict';

var Models = require('../Models');


//Insert User in DB
var createUser = function (objToSave, callback) {
    new Models.user(objToSave).save(callback)
};

//Update User in DB
var updateUser = function (criteria, dataToSet, options, callback) {
    Models.user.findOneAndUpdate(criteria, dataToSet, options, callback);
};

var updateUserMulti = function (criteria, dataToSet, options, callback) {
    Models.user.update(criteria, dataToSet, options, callback);
};

//Delete User in DB
var deleteUser = function (criteria, callback) {
    Models.user.findOneAndRemove(criteria, callback);
};

var getUsers = function (criteria, projection,options, callback) {
    Models.user.find(criteria, projection,options,callback)
};
var getUsersCount = function (criteria, projection,options, callback) {
    Models.user.find(criteria, projection,options).count(callback)
};

var getPostPopulated= function (criteria, projection,options,populate, callback) {

    Models.user.find(criteria, projection, options)
        .populate(populate)
        .exec(function (err, result) {
                Models.post.populate(result,{path:'savedPost.userId',model:'user',select:'firstName lastName profilePicture'},function(err, populatedTransactions){
                   // console.log("===========populatedTransactions==================",populatedTransactions)
                    callback(err,populatedTransactions);
                })
    });

};

var getPostPopulatedCount= function (criteria, projection,options,populate, callback) {
    Models.user.find(criteria, projection, options)
        .populate(populate)
        .exec(function (err, docs) {
            
          //  console.log("============docs================",docs)
            if (err) {
                return callback(err, docs);
            }else{
                callback(null, docs);
            }
        });
};

var getUsersPopulate = function (criteria, project, options,populateArray, callback) {
    Models.user.find(criteria, project, options).populate(populateArray).exec(function (err, docs) {
        if (err) {
            return callback(err, docs);
        }else{
            callback(null, docs);
        }
    });
};

var userAggregate = function (criteria, callback) {
    Models.user.aggregate(criteria)
        .exec(function(error, result) {
            callback(error,result);
        });
};

var countUsers = function (criteria, callback) {
    Models.user.count(criteria,callback);
};

module.exports = {
    createUser: createUser,
    updateUser:updateUser,
    deleteUser:deleteUser,
    getUsers:getUsers,
    getPostPopulated:getPostPopulated,
    getUsersPopulate:getUsersPopulate,
    getUsersCount:getUsersCount,
    userAggregate:userAggregate,
    getPostPopulatedCount:getPostPopulatedCount,
    updateUserMulti:updateUserMulti,
    countUsers:countUsers
};