/**
 * Created by cbluser51 on 16/3/18.
 */


'use strict';

var Models = require('../Models');


//Insert Request in DB
var createScheduleNotifs = function (objToSave, callback) {
    new Models.scheduleNotifs(objToSave).save(callback)
};

//Update ScheduleNotifs in DB
var updateScheduleNotifs= function (criteria, dataToSet, options, callback) {
    Models.scheduleNotifs.findOneAndUpdate(criteria, dataToSet, options, callback);
};

//Delete ScheduleNotifs in DB
var deleteScheduleNotifs= function (criteria, callback) {
    Models.scheduleNotifs.findOneAndRemove(criteria, callback);
};

var getScheduleNotifs = function (criteria, projection,options, callback) {
    Models.scheduleNotifs.find(criteria, projection,options,callback)
};

var getScheduleNotifsPopulate = function (criteria, project, options,populateArray, callback) {
    Models.scheduleNotifs.find(criteria, project, options).populate(populateArray).exec(function (err, docs) {
        if (err) {
            return callback(err, docs);
        }else{
            callback(null, docs);
        }
    });
};

var countScheduleNotifs = function (criteria, callback) {
    Models.scheduleNotifs.count(criteria,callback);
};

module.exports = {
    createScheduleNotifs: createScheduleNotifs,
    updateScheduleNotifs:updateScheduleNotifs,
    deleteScheduleNotifs:deleteScheduleNotifs,
    getScheduleNotifs:getScheduleNotifs,
    getScheduleNotifsPopulate:getScheduleNotifsPopulate,
    countScheduleNotifs:countScheduleNotifs
};