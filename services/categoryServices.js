/**
 * Created by Shumi on 29/8/17.
 */


'use strict';

var Models = require('../Models');


//Insert Category in DB
var createCategory = function (objToSave, callback) {
    new Models.category(objToSave).save(callback)
};

//Update Category in DB
var updateCategory = function (criteria, dataToSet, options, callback) {
    Models.category.findOneAndUpdate(criteria, dataToSet, options, callback);
};

//Delete Category in DB
var deleteCategory = function (criteria, callback) {
    Models.category.findOneAndRemove(criteria, callback);
};

var getCategory = function (criteria, projection,options, callback) {
    Models.category.find(criteria, projection,options,callback)
};

var getCategoryPopulate = function (criteria, project, options,populateArray, callback) {
    Models.category.find(criteria, project, options).populate(populateArray).exec(function (err, docs) {
        if (err) {
            return callback(err, docs);
        }else{
            callback(null, docs);
        }
    });
};

var countCategories = function (criteria, callback) {
    Models.category.count(criteria,callback);
};

module.exports = {
    createCategory: createCategory,
    updateCategory:updateCategory,
    deleteCategory:deleteCategory,
    getCategory:getCategory,
    getCategoryPopulate:getCategoryPopulate,
    countCategories:countCategories
};