/**
 * Created by Shumi on 19/2/18.
 */


'use strict';

var Models = require('../Models');


//Insert Event in DB
var createAdvertisement = function (objToSave, callback) {
    new Models.advertisement(objToSave).save(callback)
};

//Update Event in DB
var updateAdvertisement = function (criteria, dataToSet, options, callback) {
    Models.advertisement.findOneAndUpdate(criteria, dataToSet, options, callback);
};

//Delete Event in DB
var deleteAdvertisement = function (criteria, callback) {
    Models.advertisement.findOneAndRemove(criteria, callback);
};

var getAdvertisement = function (criteria, projection,options, callback) {
    Models.advertisement.find(criteria, projection,options,callback)
};

var getAdvertisementPopulate = function (criteria, project, options,populateArray, callback) {
    Models.advertisement.find(criteria, project, options).populate(populateArray).exec(function (err, docs) {
        if (err) {
            return callback(err, docs);
        }else{
            callback(null, docs);
        }
    });
};

var countAdvertisement = function (criteria, callback) {
    Models.advertisement.count(criteria,callback);
};

var advertisementAggregate = function (criteria, callback) {
    Models.advertisement.aggregate(criteria)
        .exec(function(error, result) {
            callback(error,result);
        });
};
module.exports = {
    createAdvertisement: createAdvertisement,
    updateAdvertisement:updateAdvertisement,
    deleteAdvertisement:deleteAdvertisement,
    getAdvertisement:getAdvertisement,
    getAdvertisementPopulate:getAdvertisementPopulate,
    advertisementAggregate:advertisementAggregate,
    countAdvertisement:countAdvertisement
};