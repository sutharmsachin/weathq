/**
 * Created by Shumi on 29/8/17.
 */


'use strict';

var Models = require('../Models');


//Insert Template in DB
var createTemplate = function (objToSave, callback) {
    new Models.template(objToSave).save(callback)
};

//Update Template in DB
var updateTemplate = function (criteria, dataToSet, options, callback) {
    Models.template.findOneAndUpdate(criteria, dataToSet, options, callback);
};

//Delete Template in DB
var deleteTemplate = function (criteria, callback) {
    Models.template.findOneAndRemove(criteria, callback);
};

var getTemplate = function (criteria, projection,options, callback) {
    Models.template.find(criteria, projection,options,callback)
};

var getTemplatePopulate = function (criteria, project, options,populateArray, callback) {
    Models.template.find(criteria, project, options).populate(populateArray).exec(function (err, docs) {
        if (err) {
            return callback(err, docs);
        }else{
            callback(null, docs);
        }
    });
};

var countTemplates = function (criteria, callback) {
    Models.template.count(criteria,callback);
};

module.exports = {
    createTemplate: createTemplate,
    updateTemplate:updateTemplate,
    deleteTemplate:deleteTemplate,
    getTemplate:getTemplate,
    getTemplatePopulate:getTemplatePopulate,
    countTemplates:countTemplates
};