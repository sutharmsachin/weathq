/**
 * Created by shumi on 29/3/17.
 */


'use strict';

var Models = require('../Models');


//Insert Pricing in DB
var createPricing = function (objToSave, callback) {
    new Models.pricing(objToSave).save(callback)
};

//Update Pricing in DB
var updatePricing = function (criteria, dataToSet, options, callback) {
    Models.pricing.findOneAndUpdate(criteria, dataToSet, options, callback);
};


var updatePricing1 = function (criteria, dataToSet, options, callback) {
    Models.pricing.update(criteria, dataToSet, options, callback);
};

//Delete Pricing in DB
var deletePricing = function (criteria, callback) {
    Models.pricing.findOneAndRemove(criteria, callback);
};

var getPricing = function (criteria, projection,options, callback) {
    Models.pricing.find(criteria, projection,options,callback)
};

var countPricings = function (criteria, callback) {
    Models.pricing.count(criteria,callback);
};

module.exports = {
    createPricing: createPricing,
    updatePricing:updatePricing,
    deletePricing:deletePricing,
    getPricing:getPricing,
    updatePricing1:updatePricing1,
    countPricings:countPricings
};