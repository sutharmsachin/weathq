/**
 * Created by Shumi on 20/2/18.
 */


'use strict';

var Models = require('../Models');


//Insert Background in DB
var createBackground = function (objToSave, callback) {
    new Models.background(objToSave).save(callback)
};

//Update Background in DB
var updateBackground = function (criteria, dataToSet, options, callback) {
    Models.background.findOneAndUpdate(criteria, dataToSet, options, callback);
};

//Delete Background in DB
var deleteBackground = function (criteria, callback) {
    Models.background.findOneAndRemove(criteria, callback);
};

var getBackground = function (criteria, projection,options, callback) {
    Models.background.find(criteria, projection,options,callback)
};

var getBackgroundPopulate = function (criteria, project, options,populateArray, callback) {
    Models.background.find(criteria, project, options).populate(populateArray).exec(function (err, docs) {
        if (err) {
            return callback(err, docs);
        }else{
            callback(null, docs);
        }
    });
};

var countBackgrounds = function (criteria, callback) {
    Models.background.count(criteria,callback);
};

module.exports = {
    createBackground: createBackground,
    updateBackground:updateBackground,
    deleteBackground:deleteBackground,
    getBackground:getBackground,
    getBackgroundPopulate:getBackgroundPopulate,
    countBackgrounds:countBackgrounds
};