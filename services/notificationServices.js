/**
 * Created by Shumi Gupta on 21/8/17.
 */

'use strict';

var Models = require('../Models');



var createNotification = function (objToSave, callback) {
    new Models.notification(objToSave).save(callback)
};


var createNotificationMany = function (objToSave,options, callback) {
    new Models.notification.insertMany(objToSave,options,callback)
};

var createNotificationUpsert = function (criteria, dataToSet, options, callback) {
    Models.notification.update(criteria, dataToSet, options, callback);
};

//Update Notification in DB
var updateNotification = function (criteria, dataToSet, options, callback) {
    Models.notification.update(criteria, dataToSet, options, callback);
};
var updateAllNotification = function (criteria, dataToSet, options, callback) {
    Models.notification.update(criteria, dataToSet, options, callback);
};

//Delete Notification in DB
var deleteNotification = function (criteria, callback) {
    Models.notification.findOneAndRemove(criteria, callback);
};

var getNotification = function (criteria, projection,options, callback) {
    Models.notification.find(criteria, projection,options).count().exec(function (err, result) {
        if (err) {
            callback(err);
        } else {
            callback(null, result)
        }
    });
};
var getNotificationTag = function (criteria, projection,options, callback) {
    Models.notification.find(criteria, projection,options).exec(function (err, result) {
        if (err) {
            callback(err);
        } else {
            callback(null, result)
        }
    });
};

var getNotificationPopulated = function (criteria, projection,options,populate, callback) {

    Models.notification.find(criteria, projection, options).populate(populate).exec(function (err, result) {
        if (err) {
            callback(err);
        } else {
            callback(null, result)
        }
    });

}

module.exports = {
    createNotification: createNotification,
    updateNotification:updateNotification,
    deleteNotification:deleteNotification,
    getNotification:getNotification,
    getNotificationPopulated:getNotificationPopulated,
    createNotificationUpsert:createNotificationUpsert,
    createNotificationMany:createNotificationMany,
    updateAllNotification:updateAllNotification,
    getNotificationTag:getNotificationTag
};