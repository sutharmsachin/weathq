/**
 * Created by sushant on 29/3/18.
 */



'use strict';

var Models = require('../Models');


//Insert Contact in DB
var createContact = function (objToSave, callback) {
    new Models.contact(objToSave).save(callback)
};

//Delete Contact in DB
var deleteContact = function (criteria, callback) {
    Models.contact.findOneAndRemove(criteria, callback);
};

var getContact = function (criteria, projection,options, callback) {
    Models.contact.find(criteria, projection,options,callback)
};

var getContactPopulate = function (criteria, project, options,populateArray, callback) {
    Models.contact.find(criteria, project, options).populate(populateArray).exec(function (err, docs) {
        if (err) {
            return callback(err, docs);
        }else{
            callback(null, docs);
        }
    });
};

var countContact = function (criteria, callback) {
    Models.contact.count(criteria,callback);
};

module.exports = {
    createContact: createContact,
    deleteContact:deleteContact,
    getContact:getContact,
    getContactPopulate:getContactPopulate,
    countContact:countContact
};