/**
 * Created by cbluser51 on 9/3/18.
 */
/**
 * Created by Shumi on 29/8/17.
 */


'use strict';

var Models = require('../Models');


//Insert Request in DB
var createRequest = function (objToSave, callback) {
    new Models.request(objToSave).save(callback)
};

//Update Request in DB
var updateRequest = function (criteria, dataToSet, options, callback) {
    Models.request.findOneAndUpdate(criteria, dataToSet, options, callback);
};

//Delete Request in DB
var deleteRequest = function (criteria, callback) {
    Models.request.findOneAndRemove(criteria, callback);
};

var getRequest = function (criteria, projection,options, callback) {
    Models.request.find(criteria, projection,options,callback)
};

var getRequestPopulate = function (criteria, project, options,populateArray, callback) {
    Models.request.find(criteria, project, options).populate(populateArray).exec(function (err, docs) {
        if (err) {
            return callback(err, docs);
        }else{
            callback(null, docs);
        }
    });
};

var countRequests = function (criteria, callback) {
    Models.request.count(criteria,callback);
};

var requestAggregate = function (criteria, callback) {
    Models.request.aggregate(criteria)
        .exec(function(error, result) {
            callback(error,result);
        });
};

module.exports = {
    createRequest: createRequest,
    updateRequest:updateRequest,
    deleteRequest:deleteRequest,
    getRequest:getRequest,
    getRequestPopulate:getRequestPopulate,
    countRequests:countRequests,
    requestAggregate:requestAggregate
};