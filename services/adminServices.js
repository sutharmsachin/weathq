/**
 * Created by Shumi on 5/7/17.
 */


'use strict';

var Models = require('../Models');


//Insert User in DB
var createAdmin= function (objToSave, callback) {
    new Models.admin(objToSave).save(callback)
};

//Update User in DB
var updateAdmin = function (criteria, dataToSet, options, callback) {
    Models.admin.findOneAndUpdate(criteria, dataToSet, options, callback);
};

//Delete User in DB
var deleteAdmin = function (criteria, callback) {
    Models.admin.findOneAndRemove(criteria, callback);
};

var getAdmin = function (criteria, projection, options, callback) {
    Models.admin.find(criteria, projection, options, callback);
};



module.exports = {
    createAdmin: createAdmin,
    updateAdmin:updateAdmin,
    deleteAdmin:deleteAdmin,
    getAdmin:getAdmin
};

