/**
 * Created by cbluser51 on 14/3/18.
 */


var Models = require('../Models');

//Insert User in DB
var createPromoCode = function (objToSave, callback) {
    new Models.promoCode(objToSave).save(callback)
};

//Update User in DB
var findUpdatePromoCode = function (criteria, dataToSet, options, callback) {
    Models.promoCode.findOneAndUpdate(criteria, dataToSet, options, callback);
};

var updatePromoCode = function (criteria, dataToSet, options, callback) {
    Models.promoCode.update(criteria, dataToSet, options, callback);
};

var getPromoCode = function (criteria, projection, options, callback) {
    Models.promoCode.find(criteria, projection, options,callback)
};

var countPromoCodes = function (criteria, callback) {
    Models.promoCode.count(criteria,callback);
};
module.exports = {
    createPromoCode: createPromoCode,
    findUpdatePromoCode:findUpdatePromoCode,
    updatePromoCode:updatePromoCode,
    getPromoCode:getPromoCode,
    countPromoCodes:countPromoCodes
};