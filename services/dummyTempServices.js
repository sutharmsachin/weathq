/**
 * Created by cbluser51 on 13/4/18.
 */
/**
 * Created by shumi on 29/3/17.
 */


'use strict';

var Models = require('../Models');


var createDummyTemplate = function (objToSave, callback) {
    new Models.dummyTemplate(objToSave).save(callback)
};

var deleteDummyTemplate = function (criteria, callback) {
    Models.dummyTemplate.findOneAndRemove(criteria, callback);
};

var getDummyTemplate = function (criteria, projection,options, callback) {
    Models.dummyTemplate.find(criteria, projection,options,callback)
};

module.exports = {
    createDummyTemplate: createDummyTemplate,
    deleteDummyTemplate:deleteDummyTemplate,
    getDummyTemplate:getDummyTemplate,
};