/**
 * Created by cbluser51 on 21/3/18.
 */


'use strict';

var Models = require('../Models');


//Insert Template in DB
var createTransaction = function (objToSave, callback) {
    new Models.transaction(objToSave).save(callback)
};

//Update Template in DB
var updateTransaction = function (criteria, dataToSet, options, callback) {
    Models.transaction.findOneAndUpdate(criteria, dataToSet, options, callback);
};

//Delete Template in DB
var deleteTransaction = function (criteria, callback) {
    Models.transaction.findOneAndRemove(criteria, callback);
};

var getTransaction = function (criteria, projection,options, callback) {
    Models.transaction.find(criteria, projection,options,callback)
};

var getTransactionPopulate = function (criteria, project, options,populateArray, callback) {
    Models.transaction.find(criteria, project, options).populate(populateArray).exec(function (err, docs) {
        if (err) {
            return callback(err, docs);
        }else{
            callback(null, docs);
        }
    });
};

var countTransactions = function (criteria, callback) {
    Models.transaction.count(criteria,callback);
};

module.exports = {
    createTransaction: createTransaction,
    updateTransaction:updateTransaction,
    deleteTransaction:deleteTransaction,
    getTransaction:getTransaction,
    getTransactionPopulate:getTransactionPopulate,
    countTransactions:countTransactions
};