/**
 * Created by cbluser51 on 15/3/18.
 */



'use strict';

var Models = require('../Models');


//Insert Template in DB
var createAdminNotifs= function (objToSave, callback) {
    new Models.adminNotifs(objToSave).save(callback)
};


var getAdminNotifs = function (criteria, projection,options, callback) {
    Models.adminNotifs.find(criteria, projection,options,callback)
};

var getAdminNotifsPopulate = function (criteria, project, options,populateArray, callback) {
    Models.adminNotifs.find(criteria, project, options).populate(populateArray).exec(function (err, docs) {
        if (err) {
            return callback(err, docs);
        }else{
            callback(null, docs);
        }
    });
};

var countAdminNotifs = function (criteria, callback) {
    Models.adminNotifs.count(criteria,callback);
};

var updateAdminNotifs= function (criteria, dataToSet, options, callback) {
    Models.adminNotifs.findOneAndUpdate(criteria, dataToSet, options, callback);
};

module.exports = {
    createAdminNotifs: createAdminNotifs,
    getAdminNotifs:getAdminNotifs,
    getAdminNotifsPopulate:getAdminNotifsPopulate,
    countAdminNotifs:countAdminNotifs,
    updateAdminNotifs:updateAdminNotifs
};